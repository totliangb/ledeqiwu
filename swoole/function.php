<?php
function closeData(&$ws, $fd, $str = '')
{
	$ws->push($fd, $str);
	$ws->close($fd);
}

/* 公共配置 */
function getConfigPub() {
	$key='getConfigPub';
	$config=Db::redis()->get($key);
	if(!$config){
		$config= Db::name("option")->where(["option_name"=>'site_info'])->limit(1)->select('option_value');
        if($config){
            Db::redis()->set($key,$config[0]->option_value);
        }

        $config=json_decode($config[0]->option_value,true);
	} else {
		$config = json_decode($config,true);
	}
    if(isset($config['live_time_coin'])){
        if(is_array($config['live_time_coin'])){
            
        }else if($config['live_time_coin']){
            $config['live_time_coin']=preg_split('/,|，/',$config['live_time_coin']);
        }else{
            $config['live_time_coin']=array();
        }
    }else{
        $config['live_time_coin']=array();
    }
    
    if(isset($config['login_type'])){
        if(is_array($config['login_type'])){
            
        }else if($config['login_type']){
            $config['login_type']=preg_split('/,|，/',$config['login_type']);
        }else{
            $config['login_type']=array();
        }
    }else{
        $config['login_type']=array();
    }
    
    if(isset($config['share_type'])){
        if(is_array($config['share_type'])){
            
        }else if($config['share_type']){
            $config['share_type']=preg_split('/,|，/',$config['share_type']);
        }else{
            $config['share_type']=array();
        }
    }else{
        $config['share_type']=array();
    }
    
    if(isset($config['live_type'])){
        if(is_array($config['live_type'])){
            
        }else if($config['live_type']){
            $live_type=preg_split('/,|，/',$config['live_type']);
            foreach($live_type as $k=>$v){
                $live_type[$k]=preg_split('/;|；/',$v);
            }
            $config['live_type']=$live_type;
        }else{
            $config['live_type']=array();
        }
    }else{
        $config['live_type']=array();
    }
    
	return 	$config;
}

//获取网站信息
function getConfigPri()
{
	if(Db::redis()->EXISTS('getConfigPri'))
	{
		$getConfigPri = Db::redis()->get('getConfigPri');
	}
	else
	{
		$getConfigPri = Db::name("option")->where(["option_name"=>'configpri'])->limit(1)->select('option_value');
		Db::redis()->set('getConfigPri', $getConfigPri[0]->option_value);
		$getConfigPri = $getConfigPri[0]->option_value;
	}

	return json_decode($getConfigPri, true);
}

//推送全局信息
function sendData(&$ws, $redisKey, $data)
{
	$userid =  Db::redis()->SMEMBERS($redisKey);

	foreach ($userid as $key => $value) 
	{
		$ws->push($value, json_encode($data));
	}
}

/**
 * 转化数据库保存的文件路径，为可以访问的url
 */
function get_upload_path($file){
    if($file==''){
        return $file;
    }
	if(strpos($file,"http")===0){
		return html_entity_decode($file);
	}else if(strpos($file,"/")===0){
		$filepath= (getConfigPub())['site'].'/'.$file;
		return html_entity_decode($filepath);
	}else{

		$space_host = getQiniuConfig();
		$filepath=$space_host['protocol'].'://'.$space_host['domain']."/".$file;
		return html_entity_decode($filepath);
	}
}

//获取七牛配置
function getQiniuConfig()
{
	if(Db::redis()->EXISTS('qiniu'))
	{
		$info = Db::redis()->get('qiniu');
	}
	else
	{
		$info = Db::name('plugin')->where(['name' => 'Qiniu'])->limit(1)->select('config');
		$info = $info[0]->config;
		Db::redis()->set('qiniu', $info);
	}

	$info = json_decode($info, true);
	return $info;
}

//获取会员等级列表
function getLevelList()
{
	if(Db::redis()->EXISTS('live'))
	{
		$list = json_decode(Db::redis()->get('live'), true);
	}
	else
	{
		$list = Db::name('level')->order('level_up asc')->select();
        
        if($list)
        {
        	Db::redis()->set('live', json_encode($list));
        }
	}

	foreach($list as $k=>&$v){
        $v['thumb']=get_upload_path($v['thumb']);
        $v['thumb_mark']=get_upload_path($v['thumb_mark']);
        $v['bg']=get_upload_path($v['bg']);
        if($v['colour']){
            $v['colour']='#'.$v['colour'];
        }else{
            $v['colour']='#ffdd00';
        }
    }
    
    return $list;
}

//获取用户等级
function getLevel($experience){
	$levelid=1;
    $level_a=1;
	$level=getLevelList();

	foreach($level as $k=>$v){
		if( $v['level_up']>=$experience){
			$levelid=$v['levelid'];
			break;
		}else{
			$level_a = $v['levelid'];
		}
	}
	$levelid = $levelid < $level_a ? $level_a:$levelid;
	//return (string)$levelid;
	return $level[$levelid-1];
}

/* 主播等级 */
function getLevelAnchorList(){
	$key='levelanchor';
	if(Db::redis()->EXISTS($key))
	{
		$list = json_decode(Db::redis()->get('live'), true);
	}
	else
	{
		$level = Db::name('level_anchor')->order('level_up asc')->select();
        if($level){
            Db::redis()->set($key,json_encode($level));
        }
	}
    
    foreach($level as $k=>$v){
        $v['thumb']=get_upload_path($v['thumb']);
        $v['thumb_mark']=get_upload_path($v['thumb_mark']);
        $v['bg']=get_upload_path($v['bg']);
        $level[$k]=$v;
    }
    
    return $level;
}

//获取主播等级
function getLevelAnchor($experience){
	$levelid=1;
	$level_a=1;
    $level=getLevelAnchorList();

	foreach($level as $k=>$v){
		if( $v['level_up']>=$experience){
			$levelid=$v['levelid'];
			break;
		}else{
			$level_a = $v['levelid'];
		}
	}
	$levelid = $levelid < $level_a ? $level_a:$levelid;
	//return (string)$levelid;
	return $level[$levelid-1];
}

//获取用户信息
function getUserinfo(&$key, $uid)
{
	$key->setUserinfo($uid);
	if (Db::redis()->EXISTS($key->getUserinfo())) {
		return json_decode(Db::redis()->get($key->getUserinfo()));
	} else {
		$user = Db::name('user')->where(['id'=>$uid])->limit(1)->select();
		if (empty($user)) {
			return null;
		}

		return $user[0];
	}
}

//判断登录状态
function checkToken(&$ws, $fd, &$key, $uid, $method)
{
	$key->setToken($uid);

	if(!Db::redis()->EXISTS($key->getToken()))
	{
		closeData($ws, $fd, json_encode(['code' => 1003, 'data' => [], '_method_' => $method, 'msg' => '登录信息错误']));
		return false;
	}
	return true;
}

//判断主播
function checkAnchor(&$ws, $fd, &$key, $uid, $method)
{
	$key->setRoom($uid);
	if(empty($uid) || !Db::redis()->EXISTS($key->getRoom()))
	{
		closeData($ws, $fd, json_encode(['code' => 1006, 'data' => [], '_method_' => $method, 'msg' => '该主播未开播']));
		return false;
	}

	return true;
}

//判断主播是否认证
function checkAnchorCertification(&$ws, $fd, $id, $method)
{
	$getConfigPri = getConfigPri();
	if($getConfigPri['auth_islimit'] && !Db::name('user_auth')->where(['uid' => intval($id), 'state' => 1])->limit(1)->select('id'))
	{
		closeData($ws, $fd, json_encode(['code' => 1005, 'data' => [], '_method_' => $method, 'msg' => '主播未认证']));
		return false;
	}

	return true;
}

//验证用户是否是管理员
function checkAdmin(&$ws, $fd, &$key, $id, $method)
{
	$key->setUser_room($id);
	$anchor_id = json_decode(Db::redis()->get($key->getUser_room()));

	$chatroom = Db::name('chatroom')->where(['uid' => $userinfo->id, 'status' => 1])->limit(1)->select('id');
	if (!Db::name('chatroom_admin')->where(['chatroomid' => $chatroom[0]->id, 'uid' => $id, 'status' => 1])->limit(1)->select()) {
		closeData($ws, $fd, json_encode(['code' => 1016, 'data' => [], '_method_' => $method, 'msg' => '用户不是管理员']));
		return false;
	}

	return true;
}

//过滤敏感词
function SensitiveWords(&$info)
{
	$getConfigPub = getConfigPub();
    if (!empty($getConfigPub['sensitive_words'])) {
    	$getConfigPub['sensitive_words'] = explode('|', $getConfigPub['sensitive_words']);
    	$info = str_replace($getConfigPub['sensitive_words'], "***", $info);
    }
}

/* curl get请求 */
function curl_get($url){
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_NOBODY, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);  // 从证书中检查SSL加密算法是否存在
	$return_str = curl_exec($curl);
	curl_close($curl);
	return $return_str;
}