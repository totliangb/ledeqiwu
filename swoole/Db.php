<?php
class Db{
	public static $conn = null;
	private static $host = null;
	private static $user = null;
	private static $password = null;
	private static $database = null;
	private static $tablename = null;
	private static $dbConfig = null;
	private static $redis = null;
    private static $domain_name = null;
	private static $sql = [
		'where'  => null,
		'orderBy' => null,
		'limit'  => null,
	];

	private static $databaseUrl = '/data/config/database.php';
	public static function name($tablename = '')
	{
		if(self::$conn != null)
		{
			self::$tablename = self::$dbConfig['prefix'].$tablename;
			return new self();
		}

		$config = require  dirname(__DIR__).self::$databaseUrl;
		self::$dbConfig = $config;

		self::$tablename = $config['prefix'].$tablename;
        self::$user = $config['username'];
        self::$host = $config['hostname'];
        self::$password = $config['password'];
        self::$database = $config['database'];
        self::$domain_name = $config['domain_name'];
        $dsn = 'mysql:dbname='.self::$database.';host='.self::$host.';port='.$config['hostport'];
        try {
            self::$conn = new PDO($dsn, self::$user, self::$password); // also allows an extra parameter of configuration
        } catch(Exception $e) {
            echo 'Could not connect to the database:<br/>' . $e->getMessage();
            exit;
        }

		return new self();
	}

    //获取配置的域名
    public static function getDomain_name()
    {
        if (self::$domain_name) {
            return self::$domain_name;
        }
        
        $config = require  dirname(__DIR__).self::$databaseUrl;
        return $config['domain_name'];
    }

	public static function getConn()
	{
		if(self::$conn == null) self::name();
		
		return  self::$conn;
	}

    public function select($fields = '*') {
        $querySql = sprintf("SELECT %s FROM %s", $fields, self::$tablename);
        if(!empty(self::$sql['where'])) {
            $querySql .= ' WHERE ' . self::$sql['where'];
        }
        if(!empty(self::$sql['orderBy'])) {
            $querySql .= ' ORDER BY ' . self::$sql['orderBy'];
        }
        if(!empty(self::$sql['limit'])) {
            $querySql .= ' LIMIT ' . self::$sql['limit'];
        }
        return $this->query($querySql);
    }

    public function find($fields = '*') {
        $result = $this->select($fields);
        return isset($result[0]) ? $result[0] : null;
    }

    public function insert($data) {
        foreach ($data as $key => &$value) {
            $value = addslashes($value);
        }
        $keys = "`".implode('`,`', array_keys($data))."`";
        $values = "'".implode("','", array_values($data))."'";
        $querySql = sprintf("INSERT INTO %s ( %s ) VALUES ( %s )", self::$tablename, $keys, $values);
        return $this->query($querySql);
    }

    public function delete() {
        $querySql = sprintf("DELETE FROM %s WHERE ( %s )", self::$tablename, self::$sql['where']);
        return $this->query($querySql);
    }

    public function update($data) {
        $updateFields = [];
        foreach ($data as $key => $value) {
            $up_value = addslashes($value);
            $updateFields[] = "`$key`='$up_value'";
        }
        $updateFields = implode(',', $updateFields);
        $querySql = sprintf("UPDATE %s SET %s", self::$tablename, $updateFields);

        if(!empty(self::$sql['where'])) {
            $querySql .= ' WHERE ' . self::$sql['where'];
        }

        return $this->query($querySql);
    }

    public function query($querySql) {
        $querystr = strtolower(trim(substr($querySql,0,6)));
        $stmt = self::$conn->prepare($querySql);
        $ret = $stmt->execute();

        if(!$ret) print_r($stmt->errorInfo());

        if($querystr == 'select') {
            $retData = $stmt->fetchAll(PDO::FETCH_OBJ);//PDO::FETCH_ASSOC
            return $retData;
        }elseif($ret && $querystr == 'insert') {
			$id = self::$conn->lastInsertId();
            return $id;
        }else{
            return $ret;
        }
    }


    public function limit($limit, $limitCount = null) {
        if(!$limitCount) {
            self::$sql['limit'] = $limit;
        }else{
            self::$sql['limit'] = $limit .','. $limitCount;
        }
        return $this;
    }

    public function orderBy($orderBy) {
        self::$sql['orderBy'] = $orderBy;
        return $this;
    }

    public static function close() {
        self::$redis = null;
        return self::$conn = null;
    }

    public function where($where) {
        if(!is_array($where)) {
            return null;
        }
        $crondsArr = [];
        foreach ($where as $key => $value) {
            $fieldValue = $value;
            if(is_array($fieldValue)) {
                $crondsArr[] = "$key ".$fieldValue[0]. ' ' . addslashes($fieldValue[1]);
            }else{
                $fieldValue = addslashes($fieldValue);
                $crondsArr[] = "$key='$fieldValue'";
            }
        }
        self::$sql['where'] = implode(' AND ', $crondsArr);

        return $this;
    }

	public static function redis()
	{
		if(self::$redis != null) return self::$redis;
		$config = require  dirname(__DIR__).self::$databaseUrl;
		self::$redis  = new redis();
		self::$redis -> connect($config['REDIS_HOST'],$config['REDIS_PORT']);
        if ($config['REDIS_AUTH']) {
            self::$redis -> auth($config['REDIS_AUTH']);
        }
        
        self::$domain_name = $config['domain_name'];
		return self::$redis;
	}


}