<?php
/**
* 退出聊天室/断开连接/关播
*/
class LeaveChatRoom
{
    public function approach(&$ws, &$frame, $data = [])
    {
        //断开连接
        if (is_int($frame)) {
            $this->Disconnect($ws, $frame);
        } else {
            $this->Disconnect($ws, $frame->fd);
        }
    }

    //断开连接处理方式
    private function Disconnect(&$ws, &$fd) 
    {
        $redisKey = new redisKey();
        $redisKey->setAnchorKey($fd);

        if (!Db::redis()->EXISTS($redisKey->getAnchorKey())) return false;

        $uid = Db::redis()->get($redisKey->getAnchorKey());

        $redisKey->setUser_room($uid);

        if (!Db::redis()->EXISTS($redisKey->getUser_room())) return false;

        $User_room = json_decode(Db::redis()->get($redisKey->getUser_room()), true);

        //取出历史上麦人列表
        $redisKey->setHistoryWheatList($User_room[0]);
        $HistoryWheatList = [];
        if (Db::redis()->EXISTS($redisKey->getHistoryWheatList())) {
            $HistoryWheatList =  json_decode(Db::redis()->get($redisKey->getHistoryWheatList()), true);
        }

        //取出上麦人列表
        $redisKey->setLianmaiList($User_room[0]);
        $LianmaiList = [];
        if (Db::redis()->EXISTS($redisKey->getLianmaiList())) {
            $LianmaiList = json_decode(Db::redis()->get($redisKey->getLianmaiList()), true);
        }

        //如果断开连接的是主播
        $redisKey->setRoom($User_room[0]);
        $user = getUserinfo($uid);

        if ($User_room[0] == $uid) {
            if (!Db::redis()->EXISTS($redisKey->getRoom())) return false;

            //删除申请上麦列表
            $redisKey->setApplyWheatList($uid);
            Db::redis()->del($redisKey->getApplyWheatList());

            //取出聊天室所有人
            $Room = Db::redis()->SMEMBERS($redisKey->getRoom());

            //删除各种数据
            if($HistoryWheatList) Db::redis()->del($redisKey->getHistoryWheatList());
            Db::redis()->del($redisKey->getRoom());
            
            //把上麦人数据添加到历史上麦人数据
            $AnchorFd = [];//用于存放上麦人的进程标识
            if ($LianmaiList) {
                Db::redis()->del($redisKey->getLianmaiList());
                foreach ($LianmaiList as $key => $value) {
                    if (isset($HistoryWheatList[$key])) {
                        $HistoryWheatList[$key]['num'] += $value['num'];
                    } else {
                        $HistoryWheatList[$key] = $value;
                    }

                    $redisKey->setUser_room($value['id']);
                    $redisKey->setAnchorKey($value['fd']);

                    Db::redis()->del($redisKey->getUser_room());
                    Db::redis()->del($redisKey->getAnchorKey());
                    $AnchorFd[] = $value['fd'];
                    Db::redis()->SREM($redisKey->getChatroom(), $value['fd']);

                    closeData($ws, $value['fd'], json_encode(['code' => 1001, 'data' => [
                        'uid' => $user->id, 
                        'user_nicename' => $user->user_nicename, 
                        'type' => 1,
                        'avatar' => get_upload_path($user->avatar),
                        'num' => $HistoryWheatList[$key]['num']
                    ], '_method_' => 'LeaveChatRoom', 'msg' => '聊天室已关闭']));
                }

                //断开直播间普通用户的连接
                foreach ($Room as $key => $value) {
                    if (!in_array($value, $AnchorFd)) {
                        $redisKey->setAnchorKey($value);

                        if (!Db::redis()->EXISTS($redisKey->getAnchorKey())) continue;
                        $redisKey->setUser_room($redisKey->getAnchorKey());
                        Db::redis()->SREM($redisKey->getChatroom(), $value);
                        Db::redis()->del($redisKey->getUser_room());
                        Db::redis()->del($redisKey->getAnchorKey());

                        closeData($ws, $value, json_encode(['code' => 1001, 'data' => [
                            'type' => 2,
                        ], '_method_' => 'LeaveChatRoom', 'msg' => '聊天室已关闭']));
                    }
                }
                $redisKey->setNumberOfGifts($uid);
                $numberOfGifts = Db::redis()->get($redisKey->getNumberOfGifts());
                //清空房主的相关数据
                Db::redis()->SREM($redisKey->getAnchor(), $fd);
                Db::redis()->del($redisKey->getNumberOfGifts());


                $getConfigPri = getConfigPri();

                if ($getConfigPri['wheat_switch'] && !empty($getConfigPri['wheat_time']) && $getConfigPri['wheat_time'] > 0) {
                    $temporaryStorage = [
                        'historyWheatList' => $HistoryWheatList,//历史上麦数据
                        'numberOfGifts'    => $numberOfGifts
                    ];

                    $redisKey->setTemporaryStorage($uid);
                    Db::redis()->SETEX($redisKey->getTemporaryStorage(), $getConfigPri['wheat_time']*60, json_encode($temporaryStorage));
                } 
            }
        } else {
            //是否在上麦列表
            $redisKey->setApplyWheatList($User_room[0]);
            $ApplyWheatList = json_decode(Db::redis()->get($redisKey->getApplyWheatList()), true);
            if (isset($ApplyWheatList[$uid])) {
                unset($ApplyWheatList[$uid]);
                Db::redis()->set($redisKey->getApplyWheatList(), json_decode($ApplyWheatList));
                $ws->push($User_room[0], json_encode(['code' => 0, 'data' => [[
                    'num' => count($HistoryWheatList), 
                    'type' => 2,
                ]], '_method_' => 'chatroomWheat', 'msg' => '申请上麦']));
            }

            //判断是否在麦上
            if ($LianmaiList && isset($LianmaiList[$uid])) {
                unset($LianmaiList[$uid]);
                Db::redis()->set($redisKey->getLianmaiList(), json_decode($LianmaiList));
                $ret = ['code' => 0, 'data' => [[
					'uid' => $user->id, 
					'user_nicename' => $user->user_nicename, 
				]], '_method_' => 'DownWheat', 'msg' => '已下麦'];

				sendData($ws, $redisKey->getRoom(), $ret);
            }

            //清空普通用户的数据
            $redisKey->setAnchorKey($fd);
            Db::redis()->SREM($redisKey->getChatroom(), $fd);
            Db::redis()->del($redisKey->getUser_room());
            Db::redis()->del($redisKey->getAnchorKey());
        }

        unset($redisKey);
        Db::close();
    }
}