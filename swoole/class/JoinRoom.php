<?php

/**
* 加入直播间
*/
class JoinRoom
{
	
	public function approach(&$ws, &$frame, &$data)
	{
		$redisKey = new redisKey();

		if (!checkToken($ws, $frame->fd, $redisKey, $data['id'], 'JoinRoom')) return false;

		if (!checkAnchor($ws, $frame->fd, $redisKey, $data['anchor_id'], 'JoinRoom')) return false;
		
		$member = getUserinfo($redisKey, $data['id']);

		$redisKey->setUser_room($member->id);
		$redisKey->setAnchorKey($frame->fd);
		Db::redis()->SADD($redisKey->getRoom(), $frame->fd);
		Db::redis()->SADD($redisKey->getChatroom(), $frame->fd);
		Db::redis()->set($redisKey->getUser_room(), json_encode([$data['anchor_id'], $frame->fd]));
		Db::redis()->set($redisKey->getAnchorKey(), $data['id']);

		sendData($ws, $redisKey->getRoom(),['code' => 0, 'data' => [
			'id' => $member->id,
			'user_nicename' => $member->user_nicename,
			'level' => $member->level,
		], '_method_' => 'JoinRoom', 'msg' => '连接成功']);
		unset($redisKey);
		Db::close();
		return false;
	}
}

