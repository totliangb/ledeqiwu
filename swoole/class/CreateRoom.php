<?php
/**
* 创建聊天室
*/
class CreateRoom
{
	public function approach(&$ws, &$frame, &$data)
	{

        $redisKey = new redisKey();

		if (!checkToken($ws, $frame->fd, $redisKey, $data['id'], 'CreateRoom')) return false;

		$userinfo = getUserinfo($redisKey, $data['id']);

		if (!checkAnchorCertification($ws, $frame->fd, $userinfo->id, 'CreateRoom')) return false;

		$chatroom = Db::name('chatroom')->where(['uid' => $userinfo->id, 'status' => 1])->limit(1)->select('id,stream');
		if (empty($chatroom[0])) {
			closeData($ws, $frame->fd, json_encode(['code' => 1006, 'data' => [], '_method_' => 'CreateRoom', 'msg' => '未开播']));
			return false;
		}

		$redisKey->setRoom($userinfo->id);
		$redisKey->setUser_room($userinfo->id);
		$redisKey->setAnchorKey($frame->fd);

		Db::redis()->SADD($redisKey->getRoom(), $frame->fd);
		Db::redis()->SET($redisKey->getUser_room(), json_encode([$userinfo->id, $frame->fd, $chatroom[0]->stream]));
		Db::redis()->SADD($redisKey->getChatroom(), $frame->fd);
		Db::redis()->SADD($redisKey->getAnchor(), $userinfo->id);
		Db::redis()->set($redisKey->getAnchorKey(), $data['id']);

		$ws->push($frame->fd, json_encode(['code' => 0, 'data' => [], '_method_' => 'CreateRoom', 'msg' => '录入信息成功']));
		Db::close();
		unset($redisKey);
		return false;
	}
}