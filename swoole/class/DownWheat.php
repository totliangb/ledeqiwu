<?php
/**
* 下麦
*/
class DownWheat
{
    public function approach(&$ws, &$frame, &$data)
    {
        if (empty($data['id'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'DownWheat', 'msg' => '参数错误']));
            return false;
        }
        $redisKey = new redisKey();
        $redisKey->setUser_room($data['id']);
        $redisKey->setRoom($data['id']);

        $User_room = Db::redis()->get($redisKey->getUser_room());
        if (!$User_room) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'DownWheat', 'msg' => '参数错误']));
            return false;
        }
        
        //验证是否是当前用户
        $User_room = json_decode($User_room, true);
        if ($User_room[1] != $frame->fd) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'DownWheat', 'msg' => '参数错误']));
            return false;
        }
        
        //验证是否在直播
        $redisKey->setRoom($User_room[0]);
        if (!Db::redis()->EXISTS($redisKey->getRoom())) {
            closeData($ws, $frame->fd, json_encode(['code' => 1006, 'data' => [], '_method_' => 'DownWheat', 'msg' => '该主播未开播']));
            return false;
        }

        //验证是否在麦上
        $redisKey->setLianmaiList($User_room[0]);
        $LianmaiList = Db::redis()->get($redisKey->getLianmaiList());
        if (!$LianmaiList) {
            closeData($ws, $frame->fd, json_encode(['code' => 1009, 'data' => [], '_method_' => 'DownWheat', 'msg' => '用户不在麦上']));
            return false;
        }
        $LianmaiList = json_decode($LianmaiList, true);
        if (!isset($LianmaiList[$data['id']])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1009, 'data' => [], '_method_' => 'DownWheat', 'msg' => '用户不在麦上']));
            return false;
        }

        //把用户数据添加到历史上麦人列表中
        $redisKey->setHistoryWheatList($User_room[0]);
        $historyWheatList = Db::redis()->get($redisKey->getHistoryWheatList());

        //存在房间历史数据
        if ($historyWheatList) {
            $historyWheatList = json_decode($historyWheatList, true);
            //历史数据中有该用户的数据
            if (isset($historyWheatList[$data['id']])) {
                $historyWheatList[$data['id']]['num'] += $LianmaiList[$data['id']]['num'];
            } else {
                $historyWheatList[$data['id']] = [
                   'uid' => $data['id'],
                   'fd'  => $frame->fd,
                   'num' => $LianmaiList[$data['id']]['num']
                ];
            }
        } else {
        	$historyWheatList[$data['id']] = [
               'uid' => $data['id'],
               'fd'  => $frame->fd,
               'num' => $LianmaiList[$data['id']]['num']
            ];
        }

        Db::redis()->get($redisKey->getHistoryWheatList(), json_encode($historyWheatList));

        $ws->push($frame->fd, json_encode(['code' => 1029, 'data' => [], '_method_' => 'DownWheat', 'msg' => '下麦成功']));

        $user = getUserinfo($redisKey, $data['id']);
        $ret = ['code' => 0, 'data' => [[
			'uid' => $user->id, 
			'user_nicename' => $user->user_nicename, 
		]], '_method_' => 'DownWheat', 'msg' => '已下麦'];

		sendData($ws, $redisKey->getRoom(), $ret);
		Db::close();
        unset($redisKey);
    }
}