<?php
/**
* 设置/取消管理员
*/
class operatingAdmin
{
	
    public function approach(&$ws, &$frame, &$data)
    {
    	if (!isset($data['id']) || !isset($data['type'])  || !isset($data['adminid'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'operatingAdmin', 'msg' => '参数错误']));
            return false;
    	}
        $redisKey = new redisKey();

        if (!checkToken($ws, $frame->fd, $redisKey, $data['id'], 'operatingAdmin')) return false;

        $userinfo = getUserinfo($redisKey, $data['id']);

        if (!checkAnchorCertification($ws, $frame->fd, $userinfo->id, 'operatingAdmin')) return false;

        $chatroom = Db::name('chatroom')->where(['uid' => $userinfo->id])->limit(1)->select('id');
        if (!isset($chatroom[0])) {
        	$ws->push($frame->fd, json_encode(['code' => 1013, 'data' => [], '_method_' => 'operatingAdmin', 'msg' => '直播间不存在']));
		    Db::close();
		    unset($redisKey);
		    return false;
        }

        $chatroom_admin = Db::name('chatroom_admin')->where(['chatroomid' => $chatroom[0]->id, 'uid' => $data['adminid']])->limit(1)->select('id');
        
        $member = getUserinfo($redisKey, $data['adminid']);
        $redisKey->setRoom($userinfo->id);

        if (empty($member)) {
            $ws->push($frame->fd, json_encode(['code' => 1018, 'data' => [], '_method_' => 'operatingAdmin
                ', 'msg' => '用户不在房间']));
            Db::close();
            unset($redisKey,$chatroom,$chatroom_mute);
            return false;
        }

        //判断是设置/取消管理员  1设置  0取消
        if ($data['type'] == 1) {

            if (isset($chatroom_admin[0])) {
                $ws->push($frame->fd, json_encode(['code' => 1014, 'data' => [], '_method_' => 'operatingAdmin', 'msg' => '用户已是管理员']));
                Db::close();
                unset($redisKey);
                return false;
            }

            if (!Db::name('chatroom_admin')->insert(['chatroomid' => $chatroom[0]->id, 'uid' => $data['adminid'], 'addtime' => time()])) {
                $ws->push($frame->fd, json_encode(['code' => 1015, 'data' => [], '_method_' => 'operatingAdmin', 'msg' => '设置管理员失败']));
                Db::close();
                unset($redisKey);
                return false;
            }

            //如果该用户在房间内就单独通知他一次
            $redisKey->setUser_room($data['adminid']);
            if (Db::redis()->EXISTS($redisKey->getUser_room())) {
                $User_room = json_decode(Db::redis()->get($redisKey->getUser_room()), true);
                if (Db::redis()->SISMEMBER($redisKey->getRoom(), $User_room[1])) {
                    $ws->push($User_room[1], json_encode([
                        'code' => 0, 
                        'data' => [['id' => $member->id, 'user_nicename' => $member->user_nicename, 'type' => 3]],
                        '_method_' => 'operatingAdmin', 
                        'msg' => '成为管理员'
                    ]));
                }
            }

            sendData($ws, $redisKey->getRoom(),['code' => 0, 'data' => [[
			    'id' => $member->id,
			    'user_nicename' => $member->user_nicename,
			    'type' => 1
		    ]], '_method_' => 'operatingAdmin', 'msg' => '用户成为管理员']);
		    unset($redisKey);
		    Db::close();
		    return false;

        } else {
            if (!isset($chatroom_admin[0])) {
            	$ws->push($frame->fd, json_encode(['code' => 1016, 'data' => [], '_method_' => 'operatingAdmin', 'msg' => '用户不是管理员']));
                Db::close();
                unset($redisKey);
                return false;
            }

            if (!Db::name('chatroom_admin')->where(['chatroomid' => $chatroom[0]->id, 'uid' => $data['adminid']])->delete()) {
                $ws->push($frame->fd, json_encode(['code' => 1017, 'data' => [], '_method_' => 'operatingAdmin', 'msg' => '取消管理员失败']));
                Db::close();
                unset($redisKey);
                return false;
            }

            //如果该管理员在房间内就单独通知他一次
            $redisKey->setUser_room($data['adminid']);
            if (Db::redis()->EXISTS($redisKey->getUser_room()))) {
                $User_room = json_decode(Db::redis()->get($redisKey->getUser_room()), true);
                if (Db::redis()->SISMEMBER($redisKey->getRoom(), $User_room[1])) {
                    $ws->push(Db::redis()->get($redisKey->getUser_room()), json_encode([
                        'code' => 0, 
                        'data' => [['id' => $member->id, 'user_nicename' => $member->user_nicename, 'type' => 2]], 
                        '_method_' => 'operatingAdmin', 
                        'msg' => '被移除管理员'
                    ]));
                }
                
            }
        }

		sendData($ws, $redisKey->getRoom(), [
            'code' => 0, 
            'data' => [['id' => $member->id, 'user_nicename' => $member->user_nicename, 'type' => 0]], 
            '_method_' => 'operatingAdmin', 
            'msg' => '被移除管理员'
        ]);

		Db::close();
		unset($redisKey, $member);
		return false;
	}
}