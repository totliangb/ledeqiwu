<?php
/**
* 禁麦/开麦
*/
class SolitaryBan
{
    public function approach(&$ws, &$frame, &$data)
    {
        if (!isset($data['id']) || !isset($data['userid']) || !isset($data['type'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'SolitaryBan', 'msg' => '参数错误']));
            return false;
        }

        $redisKey = new redisKey();

        if (!checkToken($ws, $frame->fd, $redisKey, $data['id'], 'SolitaryBan')) return false;
        if (!checkAnchorCertification($ws, $frame->fd, $data['id'], 'SolitaryBan')) return false;

        $user = getUserinfo($redisKey, $data['userid']);
        $redisKey->setBanWheatList($data['userid']);

        $redisKey->setUser_room($data['userid']);
        $User_room = json_decode(Db::redis()->get($redisKey->getUser_room()), true);
        //1开麦  0禁麦
        if ($data['type'] == 0) {
            //验证是否在禁麦列表
            if (Db::redis()->EXISTS($redisKey->getBanWheatList()) && Db::redis()->SISMEMBER($redisKey->getBanWheatList(), $data['userid'])) {
                $ws->push($frame->fd, json_encode(['code' => 1034, 'data' => [], '_method_' => 'SolitaryBan', 'msg' => '该用户已被禁麦']));
	            Db::close();
	            unset($redisKey);
	            return false;
            }

            //禁麦
            if (!Db::redis()->sadd($redisKey->getBanWheatList(), $data['userid'])) {
            	$ws->push($frame->fd, json_encode(['code' => 1035, 'data' => [], '_method_' => 'SolitaryBan', 'msg' => '禁麦失败']));
	            Db::close();
	            unset($redisKey);
	            return false;
            }

            //通知上麦人
            $ws->push($frame->fd, json_encode(['code' => 0, 'data' => [['type' => 2]], '_method_' => 'SolitaryBan', 'msg' => '被禁麦']));

        } else {
        	//验证是否在禁麦列表
        	if (Db::redis()->EXISTS($redisKey->getBanWheatList()) && !Db::redis()->SISMEMBER($redisKey->getBanWheatList(), $data['userid'])) {
                $ws->push($frame->fd, json_encode(['code' => 1034, 'data' => [], '_method_' => 'SolitaryBan', 'msg' => '用户未被禁麦']));
	            Db::close();
	            unset($redisKey);
	            return false;
            }

            //移除禁麦用户
            if (!Db::redis()->SREM($redisKey->getBanWheatList(), $data['userid'])) {
            	$ws->push($frame->fd, json_encode(['code' => 1036, 'data' => [], '_method_' => 'SolitaryBan', 'msg' => '移除禁麦失败']));
	            Db::close();
	            unset($redisKey);
	            return false;
            }

            //通知上麦人
            $ws->push($frame->fd, json_encode(['code' => 0, 'data' => [['type' => 3]], '_method_' => 'SolitaryBan', 'msg' => '禁麦解除']));
        }

        $ret = ['code' => 0, 'data' => [[
			'uid' => $user->id, 
			'user_nicename' => $user->user_nicename, 
			'info' => $data['info'],
			'avatar' => get_upload_path($user['avatar']),
			'type' => $data['type'] ? 1 : 0
		]], '_method_' => 'SolitaryBan', 'msg' => $data['type'] ? '开麦成功' : '禁麦成功'];

		$redisKey->setRoom($data['id']);
		sendData($ws, $redisKey->getRoom(), $ret);

		Db::close();
		unset($redisKey, $ret, $user);
    }
}