<?php
/**
* 红包
*/
class SendRed
{
    public function approach(&$ws, &$frame, &$data)
    {
    	if (!isset($data['id'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'SendRed', 'msg' => '参数错误']));
            return false;
        }

        $redisKey = new redisKey();
        $redisKey->setRoom($data['id']);

        if (Db::redis()->EXISTS($redisKey->getRoom())) {
        	sendData($ws, $redisKey->getRoom(), $data);
        }
    }
}