<?php
/**
* 房间公告
*/
class RoomAnnouncement
{
    public function approach(&$ws, &$frame, &$data)
    {
        if (!isset($data['id']) || empty($data['content'])) {
        	closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'RoomAnnouncement', 'msg' => '参数错误']));
            return false;
        }

        $redisKey = new redisKey();
        if (!checkAnchorCertification($ws, $frame->fd, $data['id'], 'RoomAnnouncement')) return false;
		if (!checkAnchor($ws, $frame->fd, $redisKey, $data['id'], 'RoomAnnouncement')) return false;

		$data['content'] = htmlspecialchars($data['content']);

		if (!Db::name('chatroom')->where(['uid'=>$data['id']])->update(['content' => $data['content']])) {
			$ws->push($frame->fd, json_encode(['code' => 1025, 'data' => [], '_method_' => 'RoomAnnouncement', 'msg' => '修改直播间公告失败']));
            Db::close();
            unset($redisKey);
            return false;
		}

		//过滤敏感词
        SensitiveWords($data['content']);
        
		sendData($ws, $redisKey->getRoom(), ['code' => 0, 'data' => ['info' => $data['content']], '_method_' => 'RoomAnnouncement', 'msg' => '修改直播间公告成功']);
		Db::close();
		unset($redisKey);
		return false;
    }
}