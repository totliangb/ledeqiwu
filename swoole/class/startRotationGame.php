<?php
/**
* 转盘
*/
class startRotationGame
{
    public function approach(&$ws, &$frame, &$data)
    {
        if (!isset($data['id'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'startRotationGame', 'msg' => '参数错误']));
            return false;
        }

        $redisKey = new redisKey();
        $redisKey->setRoom($data['id']);

        if (Db::redis()->EXISTS($redisKey->getRoom())) {
            sendData($ws, $redisKey->getRoom(), $data);

            sleep($data['time']);

            $redisKey->setToken($data['id']);
            $Userinfo = json_decode(Db::redis()->get($redisKey->getToken()));
            $url = Db::getDomain_name().'/appapi/?service=Game.endGame&liveuid='.$data['id'].'&token='.$Userinfo->token.'&gameid='.$data['gameid'].'&type=1';

            $curlData = json_decode(curl_get($url), true);
            if ($curlData && $curlData['data']['code'] == 0) {
            	$getData = json_decode($frame->data, true);
            	$getData['msg'][0]['ct'] = $curlData['data']['info'];
            	$getData['msg'][0]['ct'] = 'startRotationGame';
            	$getData['msg'][0]['action'] = '6';
            	sendData($ws, $redisKey->getRoom(), $getData);
            } else {
            	$ws->push($frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'startRotationGame', 'msg' => '参数错误']));
            }
            Db::close();
            unset($redisKey);
            return false;
        }
        
        $ws->push($frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'startRotationGame', 'msg' => '参数错误']));
        Db::close();
        unset($redisKey);
        return false;
    }
}