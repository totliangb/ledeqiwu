<?php
/**
* 平台公告
*/
class PlatformAdvertising
{
	public function approach(&$ws, &$frame, &$data)
    {
    	if (!isset($data['id']) || empty($data['content'])) {
        	closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'PlatformAdvertising', 'msg' => '参数错误']));
            return false;
        }

        if (!Db::name('user')->where(['id' => $data['id'], 'user_type' => 1, 'user_status' => 1])->limit(1)->select()) {
        	closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'PlatformAdvertising', 'msg' => '参数错误']));
            return false;
        }

        $data['content'] = htmlspecialchars($data['content']);

		//过滤敏感词
        SensitiveWords($data['content']);
        
        $redisKey = new redisKey();
        sendData($ws, $redisKey->getChatroom(),['code' => 0, 'data' => [
			'info' => $data['content'],
		], '_method_' => 'PlatformAdvertising', 'msg' => '平台广告']);
    }
}