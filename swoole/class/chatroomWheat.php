<?php

/**
* 上麦申请/退出上麦申请列表
*/
class chatroomWheat
{
    public function approach(&$ws, &$frame, &$data)
    {
        if (empty($data['id']) || empty($data['anchor_id']) || !isset($data['type']) || !isset($data['num'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'chatroomWheat', 'msg' => '参数错误']));
            return false;
        }
        
        $redisKey = new redisKey();

        if (!checkToken($ws, $frame->fd, $redisKey, $data['id'], 'chatroomWheat')) return false;
        if (!checkAnchor($ws, $frame->fd, $redisKey, $data['anchor_id'], 'chatroomWheat')) return false;

        $user = getUserinfo($redisKey, $data['id']);

        $redisKey->setApplyWheatList($data['anchor_id']);
        $redisKey->setLianmaiList($data['anchor_id']);

        //上麦申请
        if ($data['type'] == 1) {
            //判断是否有上麦申请列表当前用户的数据
            if (Db::redis()->EXISTS($redisKey->getApplyWheatList())) {
                $ApplyWheatList = json_decode(Db::redis()->get($redisKey->getApplyWheatList()), true);
                if (isset($ApplyWheatList[$data['id']])) {
                    $ws->push($frame->fd, json_encode(['code' => 1011, 'data' => [], '_method_' => 'chatroomWheat', 'msg' => '您已经在申请上麦列表']));
                    Db::close();
                    unset($redisKey);
                    return false;
                }
            }

            //验证用户是否在麦上
            if (Db::redis()->EXISTS($redisKey->getLianmaiList())) {
                $LianmaiList = json_decode(Db::redis()->get($redisKey->getLianmaiList()), true);
                if (isset($LianmaiList[$data['id']])) {
                    $ws->push($frame->fd, json_encode(['code' => 1008, 'data' => [], '_method_' => 'chatroomWheat', 'msg' => '您已经在麦上']));
                    Db::close();
                    unset($redisKey);
                    return false;
                }
            }

            $ApplyWheatList[$data['id']] = [
                'id' => $data['id'],
                'fd' => $frame->fd,
                'num' => 0
            ];

            if (!Db::redis()->set($redisKey->getApplyWheatList(), json_encode($ApplyWheatList))) {
                $ws->push($frame->fd, json_encode(['code' => 1031, 'data' => [], '_method_' => 'chatroomWheat', 'msg' => '申请上麦失败']));
                Db::close();
                unset($redisKey);
                return false;
            }

            //给主播发送信息
            $redisKey->setUser_room($data['anchor_id']);

            $ws->push((json_decode(Db::redis()->get($redisKey->getUser_room()), true))[1], json_encode(['code' => 0, 'data' => [[
                'num' => count($ApplyWheatList), 
                'type' => 2,
            ]], '_method_' => 'chatroomWheat', 'msg' => '更新上麦人的数量']));

            //给申请的用户发送信息
            $ret = ['code' => 0, 'data' => [[
                'uid' => $user->id, 
                'user_nicename' => $user->user_nicename, 
                'type' => 1,
                'avatar' => get_upload_path($user->avatar)
            ]], '_method_' => 'chatroomWheat', 'msg' => '申请上麦成功'];

            $ws->push($frame->fd, json_encode($ret));

        } else {
            //判断是否在上麦列表
            if (!Db::redis()->EXISTS($redisKey->getApplyWheatList())) {
                $ws->push($frame->fd, json_encode(['code' => 1012, 'data' => [], '_method_' => 'chatroomWheat', 'msg' => '您不在上麦列表']));
                Db::close();
                unset($redisKey);
                return false;
            }

            $ApplyWheatList = json_decode(Db::redis()->get($redisKey->getApplyWheatList()), true);

            if (!isset($ApplyWheatList[$data['id']])) {
                $ws->push($frame->fd, json_encode(['code' => 1012, 'data' => [], '_method_' => 'chatroomWheat', 'msg' => '您不在上麦列表']));
                Db::close();
                unset($redisKey);
            }

            unset($ApplyWheatList[$data['id']]);

            if (Db::redis()->set($redisKey->getApplyWheatList(), json_encode($ApplyWheatList))) {
                //给主播发送信息
                $redisKey->setUser_room($data['anchor_id']);

                $ws->push((json_decode(Db::redis()->get($redisKey->getUser_room()), true))[1], json_encode(['code' => 0, 'data' => [[
                    'num' => count($ApplyWheatList), 
                    'type' => 2,
                ]], '_method_' => 'chatroomWheat', 'msg' => '更新上麦人的数量']));

                $ret = ['code' => 0, 'data' => [[
                    'uid' => $user->id, 
                    'user_nicename' => $user->user_nicename, 
                    'type' => 0,
                    'avatar' => get_upload_path($user->avatar)
                ]], '_method_' => 'chatroomWheat', 'msg' => '退出上麦申请列表成功'];
                sendData($ws, $redisKey->getRoom(), $ret);

            } else {
                $ws->push($frame->fd, json_encode(['code' => 1010, 'data' => [], '_method_' => 'chatroomWheat', 'msg' => '退出上麦申请列表失败']));
            }
        }

        Db::close();
        unset($redisKey);
    }
}