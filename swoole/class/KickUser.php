<?php

/**
* 聊天室踢人
*/
class KickUser
{
	public function approach(&$ws, &$frame, &$data)
    {
        if (!isset($data['id']) || !isset($data['userid']) || !isset($data['userType'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'KickUser', 'msg' => '参数错误']));
            return false;
    	}

        //anchor是主播 admin是管理员
        $userType = isset($data['userType']) ? $data['userType'] : 'anchor';

        $redisKey = new redisKey();
        if (!checkToken($ws, $frame->fd, $redisKey, $data['id'], 'KickUser')) return false;

        $userinfo = getUserinfo($redisKey, $data['id']);

        //验证是否是主播或管理员
        if ($userType == 'anchor') {
            if (!checkAnchorCertification($ws, $frame->fd, $userinfo->id, 'KickUser')) return false;
        } else {
            if (!checkAdmin($ws, $frame->fd, $redisKey, $userinfo->id, 'KickUser')) return false;
        }
        
        
        $memberInfo = getUserinfo($redisKey, $data['userid']);
        
        if (empty($memberInfo)) {
            $ws->push($frame->fd, json_encode(['code' => 1018, 'data' => [], '_method_' => 'operatingAdmin
                ', 'msg' => '用户不在房间']));
            Db::close();
            unset($redisKey,$chatroom,$chatroom_mute);
            return false;
        }

        $redisKey->setUser_room($memberInfo->id);
        $redisKey->setRoom($userinfo->id);

        //判断用户是否在房间
        if (Db::redis()->EXISTS($redisKey->getUser_room())) {
        	$User_room = json_decode(Db::redis()->get($redisKey->getUser_room()), true);
        	if (Db::redis()->SISMEMBER($redisKey->getRoom(), $User_room[1])) {
        		closeData($ws, $User_room[1], json_encode(['code' => 1019, 'data' => [], '_method_' => 'KickUser', 'msg' => '已被踢出房间']));
	            sendData($ws, $redisKey->getRoom(), [
		            'code' => 0, 
		            'data' => [['id' => $memberInfo->id, 'user_nicename' => $memberInfo->user_nicename]], 
		            '_method_' => 'KickUser', 
		            'msg' => '用户被踢出了房间'
		        ]);

	            Db::close();
	            unset($redisKey);
	            return false;
        	}

            $ws->push($frame->fd, json_encode(['code' => 1018, 'data' => [], '_method_' => 'KickUser', 'msg' => '用户不在房间']));
            Db::close();
            unset($redisKey);
            return false;

        } else {
            $ws->push($frame->fd, json_encode(['code' => 1018, 'data' => [], '_method_' => 'KickUser', 'msg' => '用户不在房间']));
            Db::close();
            unset($redisKey);
            return false;
        }
    }
}