<?php

/**
* 发送聊天信息
*/
class SendMsg
{
	public function approach(&$ws, &$frame, &$data)
	{
		$redisKey = new redisKey();

        if (!checkToken($ws, $frame->fd, $redisKey, $data['id'], 'SendMsg')) return false;

		if (!checkAnchor($ws, $frame->fd, $redisKey, $data['anchor_id'], 'SendMsg')) return false;

		//判断是否全体禁言
		if (Db::redis()->EXISTS($redisKey->getMuteChatroomList()) && Db::redis()->SISMEMBER($redisKey->getMuteChatroomList(), $data['anchor_id'])) {
		    $ws->push($frame->fd, json_encode(['code' => 1020, 'data' => [], '_method_' => 'SendMsg', 'msg' => '房间开启了全体禁言']));
            Db::close();
            unset($redisKey);
            return false;
		}

		$user = getUserinfo($redisKey, $data['id']);

        $chatroom = Db::name('chatroom')->where(['uid' => $data['anchor_id']])->limit(1)->select('id');
        if (!isset($chatroom[0])) {
        	$ws->push($frame->fd, json_encode(['code' => 1013, 'data' => [], '_method_' => 'SendMsg', 'msg' => '直播间不存在']));
            Db::close();
            unset($redisKey);
            return false;
        }

        $redisKey->setChatroomMute($data['anchor_id'], $user->uid);

	    //判断用户是否被房间禁言
	    $mute = Db::name('chatroom_mute')->where(['uid' => $user->uid, 'chatroomid' => $chatroom[0]->id])->limit(1)->select('id');
        if (!empty($mute) || Db::redis()->EXISTS($redisKey->getChatroomMute())) {
            $ws->push($frame->fd, json_encode(['code' => 1021, 'data' => [], '_method_' => 'SendMsg', 'msg' => '已被禁言']));
            Db::close();
            unset($redisKey);
            return false;
        }

	    //过滤敏感词
        $getConfigPub = getConfigPub();
        if (!empty($getConfigPub['sensitive_words'])) {
        	$getConfigPub['sensitive_words'] = explode('|', $getConfigPub['sensitive_words']);
        	$data['info'] = str_replace($getConfigPub['sensitive_words'], "***", $data['info']);
        }

		$ret = ['code' => 0, 'data' => [[
			'uid' => $user->id, 
			'user_nicename' => $user->user_nicename, 
			'info' => $data['info'],
			'avatar' => get_upload_path($user['avatar'])
		]], '_method_' => 'SendMsg', 'msg' => '聊天信息'];

		sendData($ws, $redisKey->getRoom(), $ret);

		Db::close();
		unset($redisKey);
	}
}