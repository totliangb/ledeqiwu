<?php
/**
* 全体禁麦/全体开麦
*/
class BanWheat
{
    public function approach(&$ws, &$frame, &$data)
    {
        if (empty($data['id']) || !isset($data['type'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'BanWheat', 'msg' => '参数错误']));
            return false;
        }

        $redisKey = new redisKey();

        if (!checkAnchorCertification($ws, $frame->fd, $data['id'], 'BanWheat')) return false;
		if (!checkAnchor($ws, $frame->fd, $redisKey, $data['id'], 'BanWheat')) return false;

		$redisKey->setBanWheat($data['id']);
		$BanWheat = Db::redis()->EXISTS($redisKey->getBanWheat());
		$BanWheat = Db::redis()->EXISTS($redisKey->getBanWheat());

		//0全体禁麦  1全体开麦
        if ($data['type'] == 0) {
            if ($BanWheat) {
            	$ws->push($frame->fd, json_encode(['code' => 1032, 'data' => [], '_method_' => 'BanWheat', 'msg' => '已开启全体禁麦']));
	            Db::close();
	            unset($redisKey,$BanWheat);
	            return false;
            }

            Db::redis()->set($redisKey->getBanWheat(), 1);
            $ret = ['code' => 0, 'data' => [['type' => 0]], '_method_' => 'BanWheat', 'msg' => '房主开启了全体禁麦'];
        } else {
        	if (!$BanWheat) {
        		$ws->push($frame->fd, json_encode(['code' => 1033, 'data' => [], '_method_' => 'BanWheat', 'msg' => '未开启全体禁麦']));
	            Db::close();
	            unset($redisKey,$BanWheat);
	            return false;
        	}

        	Db::redis()->del($redisKey->getBanWheat());
        	$ret = ['code' => 0, 'data' => [['type' => 1]], '_method_' => 'BanWheat', 'msg' => '房主关闭了全体禁麦'];
        }

        sendData($ws, $redisKey->getRoom(), $ret);
        unset($redisKey);
	    Db::close();
	    return false;
    }
}