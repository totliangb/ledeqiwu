<?php
/**
* 禁言/取消禁言
*/
class ShutUpUser
{
    public function approach(&$ws, &$frame, &$data)
    {
        if (!isset($data['id']) || !isset($data['type']) || !isset($data['uid']) || !isset($data['time'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'ShutUpUser', 'msg' => '参数错误']));
            return false;
        }
        $redisKey = new redisKey();

        if (!checkToken($ws, $frame->fd, $redisKey, $data['id'], 'ShutUpUser')) return false;

        $userinfo = getUserinfo($redisKey, $data['id']);

        //anchor是主播 admin是管理员
        $userType = isset($data['userType']) ? $data['userType'] : 'anchor';

        //验证是否是主播或管理员
        if ($userType == 'anchor') {
            if (!checkAnchorCertification($ws, $frame->fd, $userinfo->id, 'ShutUpUser')) return false;
        } else {
            if (!checkAdmin($ws, $frame->fd, $redisKey, $userinfo->id, 'ShutUpUser')) return false;
        }

        $chatroom = Db::name('chatroom')->where(['uid' => $userinfo->id, 'status' => 1])->limit(1)->select('id');
        if (!isset($chatroom[0])) {
            $ws->push($frame->fd, json_encode(['code' => 1013, 'data' => [], '_method_' => 'ShutUpUser', 'msg' => '直播间不存在']));
            Db::close();
            unset($redisKey);
            return false;
        }

        $member = getUserinfo($redisKey, $data['uid']);

        $chatroom_mute = Db::name('chatroom_mute')->where(['uid' => $member->id, 'chatroomid' => $chatroom[0]->id])->limit(1)->select('id');
        

        if (empty($member)) {
            $ws->push($frame->fd, json_encode(['code' => 1018, 'data' => [], '_method_' => 'ShutUpUser', 'msg' => '用户不在房间']));
            Db::close();
            unset($redisKey,$chatroom,$chatroom_mute);
            return false;
        }

        $redisKey->setRoom($userinfo->id);
        $redisKey->setChatroomMute($userinfo->id, $member->id);

        //分类1禁言 0取消禁言
        if ($data['type'] == 1) {
            if (isset($chatroom_mute[0]) || Db::redis()->EXISTS($redisKey->getChatroomMute())) {
                $ws->push($frame->fd, json_encode(['code' => 1021, 'data' => [], '_method_' => 'ShutUpUser', 'msg' => '已被禁言']));
                Db::close();
                unset($redisKey,$chatroom,$chatroom_mute);
                return false;
            }

            //判断是否是永久禁言
            if ($data['time'] == 0) {
                if (!Db::name('chatroom_mute')->insert(['uid' => $member->id, 'chatroomid' => $chatroom[0]->id, 'addtime' => time()])) {
                    $ws->push($frame->fd, json_encode(['code' => 1022, 'data' => [], '_method_' => 'ShutUpUser', 'msg' => '禁言失败']));
                    Db::close();
                    unset($redisKey,$chatroom,$chatroom_mute);
                    return false;
                }
            } else {
                if (!Db::redis()->SETEX($redisKey->getChatroomMute(), (int)$data['time']*60, time())) {
                    $ws->push($frame->fd, json_encode(['code' => 1022, 'data' => [], '_method_' => 'ShutUpUser', 'msg' => '禁言失败']));
                    Db::close();
                    unset($redisKey,$chatroom,$chatroom_mute);
                    return false;
                }
            }

            $ret = [
                'code' => 0, 
                'data' => [['id' => $member->id, 'user_nicename' => $member->user_nicename, 'type' => 1]],
                '_method_' => 'ShutUpUser', 
                'msg' => '用户被禁言'
            ];
        } else {
            if (empty($chatroom_mute) && !Db::redis()->EXISTS($redisKey->getChatroomMute())) {
                $ws->push($frame->fd, json_encode(['code' => 1023, 'data' => [], '_method_' => 'ShutUpUser', 'msg' => '用户不在禁言列表']));
                Db::close();
                unset($redisKey,$chatroom,$chatroom_mute);
                return false;
            }

            //如果是永久禁言就删除记录
            if (!empty($chatroom_mute) && !Db::name('chatroom_mute')->where(['uid' => $member->id, 'chatroomid' => $chatroom[0]->id])->delete()) {
                $ws->push($frame->fd, json_encode(['code' => 1024, 'data' => [], '_method_' => 'ShutUpUser', 'msg' => '取消禁言失败']));
                Db::close();
                unset($redisKey,$chatroom,$chatroom_mute);
                return false;
            }
            
            //如果存在redis那就删除对应记录
            if (Db::redis()->EXISTS($redisKey->getChatroomMute()) && !Db::redis()->del($redisKey->getChatroomMute())) {
                $ws->push($frame->fd, json_encode(['code' => 1024, 'data' => [], '_method_' => 'ShutUpUser', 'msg' => '取消禁言失败']));
                Db::close();
                unset($redisKey,$chatroom,$chatroom_mute);
                return false;
            }

            $ret = [
                'code' => 0, 
                'data' => [['id' => $member->id, 'user_nicename' => $member->user_nicename, 'type' => 0]],
                '_method_' => 'ShutUpUser', 
                'msg' => '用户被取消禁言'
            ];
        }

        sendData($ws, $redisKey->getRoom(), $ret);

		Db::close();
		unset($redisKey, $member);
		return false;
    }
}