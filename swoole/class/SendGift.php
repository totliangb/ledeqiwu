<?php
/**
* 送礼物
*/
class SendGift extends AnotherClass
{
	public function approach(&$ws, &$frame, &$data)
	{
		$res = Db::redis()->get($data['ct']);
		if (!$res) return false;

		$frame->data = json_decode($frame->data, true);
		$frame->data['msg'][0]['ct'] = $res = json_decode($res, true);

		$redisKey = new redisKey();
		$redisKey->setAnchorKey($frame->fd);
		$liveuid = Db::redis()->get($redisKey->getAnchorKey());
		$redisKey->setUser_room($liveuid);
		$User_room = json_decode(Db::redis()->getUser_room(), true);

		$redisKey->setUser_room($User_room[0]);
		$User_room = json_decode(Db::redis()->getUser_room(), true);

		$ifpk=$pkuid1=$pkuid2=$pktotal1=$pktotal2='0';

		if ($res['ispk'] == 1) {
			$ifpk = '1';
			$pkuid1 = ''.$res['pkuid1'];
			$pkuid2 = ''.$res['pkuid2'];
			$pktotal1 = ''.$res['pktotal1'];
			$pktotal2 = ''.$res['pktotal2'];
		}

		$frame->data['msg'][0]['ifpk'] = $ifpk;
		$frame->data['msg'][0]['pkuid1'] = $pkuid1;
		$frame->data['msg'][0]['pkuid2'] = $pkuid2;
		$frame->data['msg'][0]['pktotal1'] = $pktotal1;
		$frame->data['msg'][0]['pktotal2'] = $pktotal2;

		if ($res['isplatgift'] == '1') {//全站礼物  应该没用
			$platdata_obj = [
				'msg' => [[
					"_method_" => "Sendplatgift",
                    "action" => "0",
                    "giftcount" => $res['giftcount'],
                    "giftname" => $res['giftname'],
                    "uname" => $frame->data['msg'][0]['uname'],
                    "livename" => $frame->data['msg'][0]['livename'],
                    "liveuid" => $liveuid,
                    "stream" => $User_room[2],
                    "msgtype" => '0',
				]],
				"retcode" => "000000",
                "retmsg" => "OK"
			];
			
			$ws->push($frame->fd, json_encode($platdata_obj));
			unset($platdata_obj);
		}

		$ws->push($User_room[1], json_encode($frame->data));

		if (intval($pkuid2) > 0) {
			$redisKey->setUser_room(intval($pkuid2));
			$pkuid2Data = json_decode(Db::redis()->getUser_room(), true);
			$ws->push($pkuid2Data[1], json_encode($frame->data));
		}

		/* 幸运中奖 */
		if ($res['isluck'] == 1 && $res['isluckall'] == 1) {
            $data_luck = ["msg" => [[
	            	"_method_"=>"luckWin",
	                "action"=>"1",
	                "uid"=>''.$data['uid'],
	                "uname"=>''.$data['uname'],
	                "uhead"=>''.$data['uhead'],
	                "giftid"=>''.$res['giftid'],
	                "giftname"=>''.$res['giftname'],
	                "lucktimes"=>''.$res['lucktimes'],
	                "msgtype"=>"4"
	            ]],
	            "retcode"=>"000000",
	            "retmsg"=>"OK"
            ];
            $ws->push($frame->fd, json_encode($data_luck));
            unset($data_luck);
		}

		/* 奖池升级 */
		if ($res['isup'] == 1) {
			$data_up = ["msg" => [[
	            	"_method_"=>"jackpotUp",
                    "action"=>"1",
                    "uplevel"=>''.$res['uplevel'],
                    "msgtype"=>"4"
	            ]],
	            "retcode"=>"000000",
	            "retmsg"=>"OK"
            ];
            $ws->push($frame->fd, json_encode($data_up));
            unset($data_up);
		}

		/* 奖池中奖 */
		if ($res['iswin'] == 1) {
			$data_win = ["msg" => [[
	            	"_method_" => "jackpotWin",
                    "action" => "1",
                    "uid" => ''.$data['uid'],
                    "uname" => ''.$data['uname'],
                    "uhead" => ''.$data['uhead'],
                    "wincoin" => ''.$res['wincoin'],
                    "msgtype" => "4"
	            ]],
	            "retcode"=>"000000",
	            "retmsg"=>"OK"
            ];
            $ws->push($frame->fd, json_encode($data_win));

            $data_up = ["msg" => [[
	            	"_method_" => "jackpotUp",
                    "action" => "1",
                    "uplevel" => '0',
                    "msgtype" => "4"
	            ]],
	            "retcode"=>"000000",
	            "retmsg"=>"OK"
            ];
            $ws->push($frame->fd, json_encode($data_up));
            unset($data_up, $data_win);
		}

		Db::redis()->del($data['ct']);
		unset($redisKey);
		Db::close();
		return false;
	}
}