<?php
/**
* 确认上麦/移除上麦申请用户
*/
class Confirmation
{
    public function approach(&$ws, &$frame, &$data)
    {
        if (empty($data['id']) || empty($data['uid']) || !isset($data['type']) || !isset($data['site'])) {
            closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => [], '_method_' => 'Confirmation', 'msg' => '参数错误']));
            return false;
        }

        if (!checkAnchorCertification($ws, $frame->fd, $data['id'], 'Confirmation')) return false;

        $redisKey = new redisKey();
        $redisKey->setApplyWheatList($data['id']);
        $redisKey->setUser_room($data['uid']);

        //判断用户是否在房间
        if (!Db::redis()->EXISTS($redisKey->getUser_room())) {
            $ws->push($frame->fd, json_encode(['code' => 1018, 'data' => [], '_method_' => 'Confirmation', 'msg' => '用户不在房间']));
            Db::close();
            unset($redisKey);
            return false;
        }

        $User_room = json_decode(Db::redis()->get($redisKey->getUser_room()));
        if (!Db::redis()->EXISTS($redisKey->getApplyWheatList())) {
            $ws->push($frame->fd, json_encode(['code' => 1012, 'data' => [], '_method_' => 'Confirmation', 'msg' => '用户不在申请上麦列表']));
            Db::close();
            unset($redisKey);
            return false;
        }

        $ApplyWheatList = json_encode(Db::redis()->get($redisKey->getApplyWheatList()), true);
        if (!isset($ApplyWheatList[$data['id']])) {
            $ws->push($frame->fd, json_encode(['code' => 1012, 'data' => [], '_method_' => 'Confirmation', 'msg' => '用户不在申请上麦列表']));
            Db::close();
            unset($redisKey);
            return false;
        }


        //验证贵宾位
        if ($data['type'] == 1 && in_array($data['site'], $redisKey->getVip())) {
        	$guard_user = Db::name('guard_user')->where(['liveuid' => $data['id'], 'uid' => $data['uid']])->limit(1)->select('endtime');
        	if (!$guard_user || $guard_user[0]->endtime < time()) {
        		$ws->push($frame->fd, json_encode(['code' => 1027, 'data' => [], '_method_' => 'Confirmation', 'msg' => '未购买守护']));
	            Db::close();
	            unset($redisKey);
	            return false;
        	}
        }

        //判断是否超出最大上麦位置
        if ($redisKey->getServingWheatNum() < $data['type']) {
        	$ws->push($User_room[1], json_encode(['code' => 1028, 'data' => ['type' => 3], '_method_' => 'Confirmation', 'msg' => '超出最大上麦位置']));
            Db::close();
            unset($redisKey);
            return false;
        }

        $redisKey->setLianmaiList($data['id']);
        $redisKey->setHistoryWheatList($data['id']);

        $LianmaiList = Db::redis()->get($redisKey->getLianmaiList());
        $HistoryWheatList = Db::redis()->get($redisKey->getHistoryWheatList());

        if ($LianmaiList) $LianmaiList = json_decode($LianmaiList, true);

        if ($HistoryWheatList) $HistoryWheatList = json_decode($HistoryWheatList, true);

        //判断该用户是否在历史上麦人列表并且是移除上麦用户
        if ($data['type'] == 0) {
            if (isset($HistoryWheatList[$data['uid']])) {
                $HistoryWheatList[$data['uid']]['num'] += $ApplyWheatList[$data['id']]['num'];
            } else {
                $HistoryWheatList[$data['uid']] = $ApplyWheatList[$data['id']];
            }
        }

        //把用户移除排麦列表
        unset($ApplyWheatList[$data['id']]);
        if (!Db::redis()->set($redisKey->getApplyWheatList(), json_encode($ApplyWheatList))) {
            $ws->push($frame->fd, json_encode(['code' => 1012, 'data' => [], '_method_' => 'Confirmation', 'msg' => '用户不在申请上麦列表']));
            Db::close();
            unset($redisKey);
            return false;
        }

        //1确认上麦 0移除上麦申请用户
        if ($data['type'] == 0) {
            Db::redis()->set($redisKey->getHistoryWheatList(), json_encode($HistoryWheatList));

            $ws->push($frame->fd, json_encode(['code' => 1026, 'data' => ['num' => count($ApplyWheatList), 'type' => 2], '_method_' => 'Confirmation', 'msg' => '已被移除上麦列表']));
            $ws->push($User_room[1], json_encode(['code' => 1026, 'data' => ['type' => 0], '_method_' => 'Confirmation', 'msg' => '已被移除上麦列表']));
            Db::close();
            unset($redisKey);
            return false;
        }

        //如果有历史上麦人数据
        $num = 0;
        if (isset($HistoryWheatList[$data['uid']])) $num = $HistoryWheatList[$data['uid']]['num'];

        $LianmaiList[$data['type']] = ['id' => $data['uid'], 'fd' => $User_room[1], 'num' => $num];

        Db::redis()->set($redisKey->getLianmaiList(), json_encode($LianmaiList));


        $user = getUserinfo($redisKey, $data['uid']);
        $redisKey->setRoom($data['id']);

        $ws->push($User_room[1], json_encode(['code' => 1030, 'data' => [], '_method_' => 'Confirmation', 'msg' => '上麦成功']));

        $ret = ['code' => 0, 'data' => [[
			'uid' => $user->id, 
			'user_nicename' => $user->user_nicename, 
			'type' => 1,
			'avatar' => get_upload_path($user->avatar),
			'num' => $num
		]], '_method_' => 'Confirmation', 'msg' => '用户上麦'];

		sendData($ws, $redisKey->getRoom(), $ret);
    }
}