<?php

/**
* 弹幕
*/
class SendBarrage
{
	public function approach(&$ws, &$frame, &$data)
	{
		$redisKey = new redisKey();

        if (!checkToken($ws, $frame->fd, $redisKey, $data['id'], 'SendBarrage')) return false;

		$userinfo = getUserinfo($redisKey, $data['id']);
		$redisKey->setUser_room($userinfo->id);
		$anchor_id = (json_decode(Db::redis()->get($redisKey->getUser_room()), true))[0];
		$redisKey->setRoom($anchor_id);

		$getConfigPri = getConfigPri();
		if($getConfigPri['barrage_fee'] > 0)
		{
			$user = Db::name('user')->where(['id'=>$userinfo->id])->limit(1)->select('votes');
			if($user[0]->votes < $getConfigPri['barrage_fee'])
			{
				$ws->push($frame->fd, json_encode(['code' => 1007, 'data' => [], '_method_' => 'SendBarrage', 'msg' => '余额不足']));
				Db::close();
				return false;
			}

			try {
				Db::getConn()->beginTransaction();
				if(!Db::name('user')->where(['id' => $userinfo->id])->update(['votes' => 'votes-'.$getConfigPri['barrage_fee']]))
				{
					throw new \Exception("扣款失败");
				}

				if(!Db::name('user_coinrecord')->insert([
					'type' => 1,
					'action' => 2,
					'uid' => $userinfo->id,
					'touid' => $userinfo->id,
					'giftcount' => 1,
					'totalcoin' => $getConfigPri['barrage_fee'],
					'showid' => $anchor_id,
					'classification' => 1,
					'addtime' => time()
				])) throw new \Exception("添加记录失败");

				Db::getConn()->commit();
			} catch (\Exception $e) {
				file_put_contents('./log/'.date('Ymd').'.txt', date('Y-m-d H:i:s').':'.$e->getMessage()."\n\rdata:".json_encode($data));
				Db::getConn()->rollBack();
				Db::close();
				unset($redisKey);
				$ws->push($frame->fd, json_encode(['code' => 1007, 'data' => [], '_method_' => 'SendBarrage', 'msg' => '余额不足']));
				return false;
			}
		}

		//过滤敏感词
        SensitiveWords($data['info']);

		sendData($ws, $redisKey->getRoom(), ['code' => 0, 'data' => ['user' => $userinfo, 'info' => $data['info']], '_method_' => 'SendBarrage', 'msg' => '弹幕信息']);
		Db::close();
		unset($redisKey, $getConfigPub);
		return false;

	}
}