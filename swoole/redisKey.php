<?php
/**
* 存放redis所有的key
*/
class redisKey
{
	const user_rooms = 'chatroom_user';
	const anchor_key = 'anchorKey';
	private $vip  = [1,2];                         //贵宾位
	private $servingWheatNum  = 8;                 //上麦人数
	private $room = 'chatroom_';                   //聊天室房间
	private $user_room = '';                       //用户房间
	private $anchor    = 'chatroom_anchor';        //正在直播的房主id
	private $chatroom  = 'chatroom_all';           //聊天室所有人列表
	private $historyWheatList = 'historyWheatList';//历史上麦人列表
	private $applyWheatList   = 'applyWheatList';  //申请上麦人列表
	private $lianmaiList      = 'lianmaiList';     //房间在麦人列表
	private $muteChatroomList = 'muteChatroomList';//禁言全体的聊天室列表
	private $userinfo         = 'userinfo_';       //用户信息
	private $token            = 'token_';          //登录状态
	private $chatroomMute     = 'chatroomMute_';   //禁言用户
	private $anchorKey        = '';                //存用户标识的id
	private $banWheat         = 'BanWheat';        //全体禁麦
	private $banWheatList     = 'BanWheatList';    //房间禁麦人列表
	private $temporaryStorage = 'temporaryStorage';//暂存关播数据
	private $numberOfGifts    = 'numberOfGifts';   //收到的礼物数

	public function __call($name,$args)
	{
		if (is_int(strpos($name, 'get'))) {
			$key = lcfirst(substr($name, 3));
			return (property_exists($this, $key)) ? $this->{$key} : false;

		} else if (is_int(strpos($name, 'set'))) {
			$key = lcfirst(substr($name, 3));
			if (property_exists($this, $key)) {
				$this->{$key} .= $args[0];
			}
		}
	}

	//设置禁言用户
	public function setChatroomMute($anchorid, $uid)
	{
		$this->chatroomMute .= $anchorid.'_'.$uid;
	}

	//设置用户房间
    public function setUser_room($uid) 
    {
    	$this->user_room = self::user_rooms.$uid;
    }

    //获取用户房间
    public function getUser_room($uid) 
    {
    	return (empty($this->user_room)) ? self::user_rooms : $this->user_room;
    }

	//设置用户房间
    public function setAnchorKey($uid) 
    {
    	$this->anchorKey = self::anchor_key.$uid;
    }

    //获取用户房间
    public function getAnchorKey($uid) 
    {
    	return (empty($this->anchorKey)) ? self::anchor_key : $this->anchorKey;
    }
}