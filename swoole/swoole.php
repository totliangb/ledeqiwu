<?php

$ws = new \Swoole\WebSocket\Server('0.0.0.0', 6659);

require './Db.php';

//直播改价格
//监听WebSocket连接打开事件
$ws->on('open', function ($ws, $request) {
    
});

$ws->set(array(
    'daemonize' => 1, //持久化
));

//监听WebSocket消息事件
$ws->on('message', function ($ws, $frame) {
    $data = json_decode($frame->data, true);
	if(!isset($data['type']))
	{
		closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => '参数错误']));
		return false;
	}

	//心跳
	if(500 == $data['type'])
	{
		$ws->push($frame->fd, json_encode(['code' => 700, 'data' => '成功']));
		return false;
	}

	//主播信息
	if(200 == $data['type'] || 400 == $data['type'])
	{

		$user = Db::name('user_token')->where(['user_id'=>intval($data['uid'])])->limit(1)->select('token');
		if(!$user || $user[0]->token != $data['token'])
		{
		    setlog($frame);
			closeData($ws, $frame->fd, json_encode(['code' => 1003, 'data' => '登录信息错误1']));
			return false;
		}
		if(isset($data['shop_id']))
		{
			$shop = Db::name('shop')->where(['state'=>1,'uid'=>intval($data['uid'])])->limit(1)->select('id');

			if(!$shop || $shop[0]->id != $data['shop_id'])
			{
				closeData($ws, $frame->fd, json_encode(['code' => 1000, 'data' => '店铺信息不正确']));
				return false;
			}

			if(200 == $data['type']) //主播登录标记
			{
				Db::redis()->ZADD('room_'.$data['uid'], $frame->fd, $frame->fd);
				Db::redis()->SET('user_room'.$frame->fd, $data['uid']);
				Db::redis()->ZADD('anchor', $data['uid'], $frame->fd);

				$ws->push($frame->fd, json_encode(['code' => 0, 'data' => '录入信息成功']));
        		Db::close();
        		return false;
			}
		}
	}
	
	//用户连接登记
	if(100 == $data['type'])
	{
		if(empty($data['anchor_id']) || !Db::redis()->ZCARD('room_'.$data['anchor_id']))
		{
			closeData($ws, $frame->fd, json_encode(['code' => 1000, 'data' => '该主播没有店铺']));
			return false;
		}

		Db::redis()->ZADD('room_'.$data['anchor_id'], $frame->fd, $frame->fd);
		Db::redis()->set('user_room'.$frame->fd, $data['anchor_id']);

		$ws->push($frame->fd, json_encode(['code' => 0, 'data' => '连接成功']));
		return false;
	}

	//主播修改价格
	if(400 == $data['type'])
	{
		if(empty($data['commodity_id']) || empty($data['price']))
		{
			closeData($ws, $frame->fd, json_encode(['code' => 1004, 'data' => '参数错误']));
			return false;
		}
	    

		if(!Db::name('shop_commodity')->where(['state'=>1,'id'=>intval($data['commodity_id']),'uid'=>intval($data['uid'])])->find())
		{
			$ws->push($frame->fd,  json_encode(['code' => 1002, 'data' => '商品不存在']));
			Db::close();
			return false;
		}
		
		if(!Db::name('shop_commodity')->where(['id'=>$data['commodity_id']])->update(['price' => $data['price']]))
		{
			$ws->push($frame->fd, json_encode(['code' => 600, 'data' => '修改失败']));
			Db::close();
			return false;
		}

		$userid =  Db::redis()->ZRANGE('room_'.$data['uid'], 0, -1);//$GLOBALS['room'][$data['uid']];

		if($userid)
		{
			$ws->push($frame->fd, json_encode(['code' => 200, 'data' => '修改成功']));
			foreach ($userid as $k => $v) 
			{
				$ws->push($v, json_encode(['code' => 100, 'data' => ['id' => $data['commodity_id'], 'price' => $data['price']]]));
			}
		}
		Db::close();
		return false;
	}
    
	//主播修改推荐产品
	if(600 == $data['type'])
	{
		
		$info = Db::name('shop_commodity')->where(['is_recommend'=>1,'state'=>1,'shop_id'=>intval($data['shop_id'])])->limit(1)->select('id');

		try {
			Db::getConn()->beginTransaction();
			if($info)
			{
				if($info[0]->id == $data['commodity_id']) throw new Exception("推荐商品失败");

				if(!Db::name('shop_commodity')->where(['shop_id'=>$data['shop_id']])->update(['is_recommend'=>0]))
				{
					throw new \Exception("推荐商品失败");
				}
			}

			if(!Db::name('shop_commodity')->where(['id'=> $data['commodity_id']])->update(['is_recommend' => 1]))
			{
				throw new \Exception("推荐商品失败");
			}
			Db::getConn()->commit();
		} catch (\Exception $e) {
			Db::getConn()->rollBack();
			Db::close();
			$ws->push($frame->fd, json_encode(['code' => 800, 'data' => '推荐商品失败']));
			return false;
		}
		
		$userid = Db::redis()->ZRANGE('room_'.$data['uid'], 0, -1);
		if($userid)
		{
			$ret_data = Db::name('shop_commodity')
						->where(['is_recommend'=>1,'state'=>1,'shop_id'=>intval($data['shop_id'])])
						->limit(1)->select('id,title,img,amount,price,original_price');

			$config = Db::name('plugin')->where(['name'=>'Qiniu'])->limit(1)->select('config');
        	$config = json_decode($config[0]->config, true);

        	$ret_data[0]->img = $config['protocol'].'://'.$config['domain'].'/'.$ret_data[0]->img;

			$ws->push($frame->fd, json_encode(['code' => 201, 'data' => '推荐商品成功']));
			foreach ($userid as $k => $v) 
			{
				$ws->push($v, json_encode(['code' => 101, 'data' => $ret_data[0]]));
			}
		}
		Db::close();
		return false;
	}
});

//监听WebSocket连接关闭事件
$ws->on('close', function ($ws, $fd) {
	
    $anchor_id = Db::redis()->GET('user_room'.$fd);
    if(!$anchor_id) return false;
    if(Db::redis()->ZRANK('anchor', $fd)) //如果退出的是主播
    {
    	$userid = Db::redis()->ZRANGE('room_'.$anchor_id, 0, -1);
    	foreach ($userid as $k => $v) 
		{
			$ws->push($v, json_encode(['code' => 1001, 'data' => '主播已关闭']));
		}

		$shop = Db::name('shop')->where(['uid'=> $anchor_id])->limit(1)->select('id');
		if($shop)
		{
			Db::name('shop_commodity')->where(['shop_id'=> $shop[0]->id])->update(['is_recommend'=>0]);
		}

	    Db::redis()->ZREM('anchor', $anchor_id);
    	Db::redis()->del('room_'.$anchor_id);
    	Db::redis()->del('user_room'.$fd);
    	
    }
    else
    {

    	Db::redis()->ZREM('room_'.$anchor_id, Db::redis()->ZRANK('room_'.$anchor_id, $fd));
    	Db::redis()->del('user_room'.$fd);
    }
    Db::redisClose();
});

function closeData($ws, $fd, $str = '')
{
	$ws->push($fd, $str);
	$ws->close($fd);
}

function setlog($frame){
    file_put_contents("/data/swoolelog.txt",json_encode($fd),FILE_APPEND);
}

$ws->start();
