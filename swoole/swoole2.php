<?php

$ws = new \Swoole\WebSocket\Server('0.0.0.0', 6658);

require './Db.php';
require './redisKey.php';
require './function.php';

//直播改价格
//监听WebSocket连接打开事件
$ws->on('open', function ($ws, $request) {

});

/*$ws->set(array(
    'daemonize' => 1, //持久化
));*/

//监听WebSocket消息事件
$ws->on('message', function ($ws, $frame) {
    $data = (json_decode($frame->data, true))['msg'][0];
	$method = $data['_method_'];
    $url = './class/'.$method.'.php';
    if(file_exists($url))
    {
    	require_once $url;
    	$current = new $method();
    	$current->approach($ws, $frame, $data);
    	unset($current);
    }
});

//监听WebSocket连接关闭事件
$ws->on('close', function ($ws, $fd) {
	$url = './class/LeaveChatRoom.php';
    if(file_exists($url))
    {
        require_once $url;
        $current = new LeaveChatRoom();
        $current->approach($ws, $fd);
        unset($current);
    }
});


$ws->start();
