<?php

class Model_FuliMarket extends PhalApi_Model_NotORM {

    public function getLevel() {
        $rs=DI()->notorm->fuli_level
            ->select("level_id,level_name,goods_shared_num")
            ->order("goods_shared_num desc")
            ->fetchAll();
        return $rs;
    }

    public function getFuliGoodsDataByLevel($level) {

        if ($level) {
            $where = 'goods_storage > 0 AND level_id = '.$level;
        }else{
            $where = 'goods_storage > 0';
        }

        $rs=DI()->notorm->fuli_goods
            ->select("id,goods_name,goods_price,goods_shared_num,goods_image,goods_storage")
            ->where($where)
            ->order("goods_shared_num desc")
            ->fetchAll();

        return $rs;
    }

    public function getFuliGoodsIndexData() {

        $rs=DI()->notorm->fuli_goods
            ->select("id,goods_name,goods_price,goods_shared_num,goods_image,goods_storage")
            ->where('goods_storage > 0')
            ->order("goods_shared_num asc")
            ->limit(2)
            ->fetchAll();

        return $rs;
    }

    public function getFuliGoodsCount() {

        $rs=DI()->notorm->fuli_goods
            ->select("count(id) as c")
            ->where('goods_storage > 0')
            ->fetchOne();

        return $rs['c'];
    }

    public function getFuliGoodsSharedData($uid,$goods_id) {

        $rs=DI()->notorm->fuli_share_log
            ->select("id,shared_uid")
            ->where('uid = ? AND goodsid =? AND is_used = 0',$uid,$goods_id)
            ->order("addtime asc")
            ->fetchAll();

        return $rs;
    }

    public function saveSharedLogData($data) {
        $run_break = false;
        if($data['uid']){
            $userinfo = getUserInfo($data['uid'],0);
            if($userinfo){
                $data['nickname'] = $userinfo['user_nicename'];
            }else{
                $run_break = true;
            }
        }else{
            $run_break = true;
        }

        if($data['shared_uid']){
            $shareduserinfo = getUserInfo($data['shared_uid'],0);
            if($shareduserinfo){
                $data['shared_nickname'] = $shareduserinfo['user_nicename'];
            }else{
                $run_break = true;
            }
        }else{
            $run_break = true;
        }

        if($data['goodsid']){
            $goodsinfo = DI()->notorm->fuli_goods->select('goods_shared_num,goods_name')->where('id=?',$data['goodsid'])->fetchOne();
            if($goodsinfo){
                // 校验当前商品邀请数量是否超出
                $sharedData = $this->getFuliGoodsSharedData($data['uid'],$data['goodsid']);
                $sharedNum = $sharedData ? count($sharedData) : 0;
                if($sharedNum >= $goodsinfo['goods_shared_num']){
                    $run_break = true;
                }
                $data['goodsname'] = $goodsinfo['goods_name'];
            }else{
                $run_break = true;
            }
        }else{
            $run_break = true;
        }

        if(!$run_break){
            $data['addtime'] = time();
            $rs=DI()->notorm->fuli_share_log->insert($data);
        }else{
            $rs = false;
        }

        return $rs;
    }

    // 更新福利商品的库存及销量
    public function updateFuliGoodsStorageAndSale($goods_id,$num)
    {
        $updateData = array(
            'goods_storage' => new NotORM_Literal("goods_storage - {$num}"),
            'goods_salenum' => new NotORM_Literal("goods_salenum + {$num}"),
        );
        $return = DI()->notorm->fuli_goods
                    ->where('id = ?',$goods_id)
                    ->update($updateData);
        return $return;
    }

    // 更新用户福利商品邀请记录使用状态
    public function updateFuliGoodsSharedLogState($uid,$goods_id)
    {
        $updateData = array(
            'is_used' => 1,
        );
        $return = DI()->notorm->fuli_share_log
                    ->where('uid = ? AND goodsid = ? AND is_used = 0',$uid,$goods_id)
                    ->update($updateData);
        return $return;
    }

    //生成唯一订单号
    private function trade_no()
    {
        $time = explode(' ', microtime());
        return $time[1].intval($time[0] * 1000000).mt_rand(10000, 99999);
    }

    public function orderCreate($commodity_info,$uid,$address_info) {
        $result = array();

        //添加订单表数据
        try {
            DI()->notorm->beginTransaction('db_appapi');

            $shop = [
                'order_sn' => $this->trade_no(),
                'uid' => $uid,
                'add_time' => time(),
                'address' => $address_info['id'],
                'address_info' => $address_info['title'],
            ];
            if(!DI()->notorm->fuli_order->insert($shop)) throw new Exception('生成订单失败1');

            $ordergoods = [
                'goodsid' => $commodity_info['id'],
                'goodsname' => $commodity_info['goods_name'],
                'goodsnum' => 1,
                'goodsimage' => get_upload_path($commodity_info['goods_image'], 0),
                'orderid' => DI()->notorm->fuli_order->insert_id()
            ];

            if(!DI()->notorm->fuli_ordergoods->insert($ordergoods)) throw new Exception('生成订单失败2');

            // 更新福利商品库存 及 销量
            if(!$this->updateFuliGoodsStorageAndSale($commodity_info['id'],1)) throw new Exception('生成订单失败3');

            // 更新 用户 福利商品的邀请记录 使用状态
            if(!$this->updateFuliGoodsSharedLogState($uid,$commodity_info['id'])) throw new Exception('生成订单失败4');

            $result = [
                'code' => 0, 
                'msg' => '', 
                'info' => [
                    'order_id' => $ordergoods['orderid'], 
                    'numbering' => $shop['order_sn']
                ]
            ];

            DI()->notorm->commit('db_appapi');
        } catch (Exception $e) {
            DI()->notorm->rollback('db_appapi');
            return ['code' => 1005, 'msg' => $e->getMessage()];
            // $result = ['code' => 1005, 'msg' => '生成订单失败'];
        }

        return $result;
    }

}