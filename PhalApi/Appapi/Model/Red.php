<?php

class Model_Red extends PhalApi_Model_NotORM {
	/* 发布红包 */
	public function sendRed($data) {
        
        $rs = array('code' => 0, 'msg' => '发送成功', 'info' => array());
        
        $uid=$data['uid'];
        $total=$data['amount'];
        $ifok=DI()->notorm->user
				->where('id = ? and available_predeposit >= ?', $uid,$total)
				->fetchOne();
        if(!$ifok){
            $rs['code']=1009;
            $rs['msg']='余额不足';		
            return $rs;
        }
        
        $result= DI()->notorm->red->insert($data);
        
        if(!$result){
            $rs['code']=1009;
            $rs['msg']='发送失败，请重试';		
            return $rs;
        }

		$rs['info']=$result;
		return $rs;
	}		
    
    /* 红包列表 */
    public function getRedList($liveuid,$showid){
        $list=DI()->notorm->red
                ->select("*")
                ->where('liveuid = ? and showid= ?',$liveuid,$showid)
                ->order('addtime desc')
                ->fetchAll();
        return $list;
    }

	/* 抢红包 */
	public function robRed($data) {

        $uid=$data['uid'];
        $giftid=$data['redid'];
        $total=$data['amount'];
        $showid=$data['showid'];
        $addtime=$data['addtime'];
        $record_sn=$data['record_sn'];
        unset($data['showid']);

    
        // 红包领取记录
        $result= DI()->notorm->red_record->insert($data);
        if(!$result){
            throw new Exception("红包领取记录添加失败");
        }

        // 红包领取余额记录
        $now_user_info = DI()->notorm->user
                        ->select("user_nicename")
                        ->where('id=?',$uid)
                        ->fetchOne();
        $data_log = array();
        $data_log['uid'] = $uid;
        $data_log['nickname'] = $now_user_info['user_nicename'];
        $data_log['amount'] = $total;
        $data_log['order_sn'] = $record_sn;

        $model = new Model_Pd();
        $rs = $model->changePd('red_record',$data_log);

        // 更新红包信息
        $updateData = array(
            'coin_rob' => new NotORM_Literal("coin_rob + {$total}") ,
            'nums_rob' => new NotORM_Literal("nums_rob + 1"),
        );
        // 判断是否抢完
        $item_red_info = DI()->notorm->red
                ->where('id = ?', $giftid)
                ->fetchOne();
        if($item_red_info['amount']==$item_red_info['coin_rob']+$total && $item_red_info['nums']==$item_red_info['nums_rob']+1){
            // 已抢完
            $updateData['status'] = 1;
            // 结束时间
            $updateData['endtime'] = time();
        }

        $rs2 = DI()->notorm->red
                ->where('id = ?', $giftid)
                ->update($updateData);
        if(!$rs2){
            throw new Exception("红包信息更新失败");
        }
        // 更新redis
        if($updateData['status'] == 1 && $rs2){
            $stream = $item_red_info['liveuid'].'_'.$item_red_info['showid'];
            $redid = $item_red_info['id'];
            // 添加倒计时
            $configpri=getConfigPri();
            $red_end_mins = isset($configpri['red_end_mins']) ? $configpri['red_end_mins'] : 1;
            $key3='red_list_'.$stream.'_'.$redid.'_overtime';
            // 毫秒单位
            $overtime = ($updateData['endtime'] + ($red_end_mins * 60) )*1000;
            DI()->redis->set($key3,$overtime);
        }

		return $result;
	}			

    /* 抢红包列表 */
    public function getRedRobList($redid){
        $list=DI()->notorm->red_record
                ->select("*")
                ->where('redid = ?',$redid)
                ->order('addtime desc')
                ->fetchAll();
        return $list;
    }
    
    /* 红包信息 */
    public function getRedInfo($redid){
        $redinfo=DI()->notorm->red
                ->select("*")
                ->where('id = ? ',$redid )
                ->fetchOne();
        if($redinfo){
            unset($redinfo['showid']);
            unset($redinfo['liveuid']);
            unset($redinfo['effecttime']);
            unset($redinfo['addtime']);
            unset($redinfo['status']);
        }
        return $redinfo;
        
    }

    /* 直播间当前红包信息 */
    public function getRedNow($liveuid,$showid){
        // 未抢完 或者 抢完后界面未关闭
        $redinfo=DI()->notorm->red
                ->select("*")
                ->where("liveuid = ? and showid= ? and (status = 0 OR (status = 1 AND endtime > ?))",$liveuid,$showid,time()-(5*60))
                ->order('addtime desc')
                ->fetchOne();
        if($redinfo){
            unset($redinfo['showid']);
            unset($redinfo['liveuid']);
        }
        return $redinfo;
        
    }
}
