<?php

/**
 * 余额
 */
class Model_Pd extends PhalApi_Model_NotORM {

    /**
     * 获取操作名称
     * @return string
     */
    public function getActionName($action){

        $actions = array(
            'appraise_pay' => '收到鉴定申请',
            'appraise' => '鉴定完成',
            'appraise_refund' => '鉴定超时',
            'cash_apply' => '申请提现',
            'cash_pay' => '提现',
            'cash_del' => '取消提现',
            'recharge' => '充值',
            'red_pay' => '红包支付',
            'red_record' => '红包领取',
            'red_refund' => '红包退回',
        );
        
        return isset($actions[$action]) ? $actions[$action] : '';
    }
    /**
     * 生成编号
     * @return string
     */
    public function makeSn($uid) {
       return mt_rand(10,99)
              . sprintf('%010d',time() - 946656000)
              . sprintf('%03d', (float) microtime() * 1000)
              . sprintf('%03d', (int) $uid % 1000);
    }

    /**
     * 余额
     */
    public function changePd($change_type,$data = array()) {
        $data_log = array();
        $data_pd = array();
        $whereData = array();
        $data_log['uid'] = $data['uid'];
        $data_log['nickname'] = $data['nickname'];
        $data_log['add_time'] = time();
        $data_log['type'] = $change_type;

        $where = 'id=?';
        $whereData[] = $data['uid'];
        switch ($change_type){
            case 'appraise_pay':
                // 鉴定支付完成 鉴定专家增加冻结金额
                $data_log['freeze_amount'] = $data['amount'];
                $data_log['desc'] = '鉴定订单支付成功: '.$data['order_sn'];
                $now_data = DI()->notorm->user
                                ->select('freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();

                $where .= ' and freeze_predeposit = ?';
                $whereData[] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] + $data['amount'];
                break;
            case 'appraise':
                // 鉴定专家完成鉴定 鉴定专家冻结金额转入余额
                $data_log['av_amount'] = $data['amount'];
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '鉴定订单完成，冻结转可用: '.$data['order_sn'];
                $now_data = DI()->notorm->user
                                ->select('available_predeposit,freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();
                $where .= ' and available_predeposit = ?';
                $where .= ' and freeze_predeposit = ?';
                $whereData[] = $now_data['available_predeposit'];
                $whereData[] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'appraise_refund':
                // 鉴定订单超时退款 扣除鉴定专家冻结金额
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '鉴定订单超时退款，扣除冻结金额: '.$data['order_sn'];
                $now_data = DI()->notorm->user
                                ->select('freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();
                $where .= ' and freeze_predeposit = ?';
                $whereData[] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'cash_apply':
                $data_log['av_amount'] = -$data['amount'];
                $data_log['freeze_amount'] = $data['amount'];
                $data_log['desc'] = '申请提现，冻结余额，提现单号: '.$data['order_sn'];

                $now_data = DI()->notorm->user
                                ->select('available_predeposit,freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();
                $where .= ' and available_predeposit = ?';
                $where .= ' and freeze_predeposit = ?';
                $whereData[] = $now_data['available_predeposit'];
                $whereData[] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] + $data['amount'];
                $data_pd['available_predeposit'] = $now_data['available_predeposit'] - $data['amount'];

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['desc'];
                $data_msg['change_amount'] = $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['available_predeposit']<0){
                    throw new Exception('余额不足');
                }
                break;
            case 'cash_pay':
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '提现成功，提现单号: '.$data['order_sn'];
                $data_log['admin_name'] = $data['admin_name'];

                $now_data = DI()->notorm->user
                                ->select('available_predeposit,freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();
                $where .= ' and freeze_predeposit = ?';
                $whereData[] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'cash_del':
                $data_log['av_amount'] = $data['amount'];
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '取消提现申请，解冻余额，提现单号: '.$data['order_sn'];
                $data_log['admin_name'] = $data['admin_name'];

                $now_data = DI()->notorm->user
                                ->select('available_predeposit,freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();
                $where .= ' and available_predeposit = ?';
                $where .= ' and freeze_predeposit = ?';
                $whereData[] = $now_data['available_predeposit'];
                $whereData[] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'recharge':
                $data_log['av_amount'] = $data['amount'];
                $data_log['desc'] = '充值，充值单号: '.$data['order_sn'];


                $now_data = DI()->notorm->user
                                ->select('available_predeposit,freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();
                $where .= ' and available_predeposit = ?';
                $whereData[] = $now_data['available_predeposit'];

                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                break;
            case 'red_pay':
                $data_log['av_amount'] = -$data['amount'];
                $data_log['desc'] = '发送红包，红包单号: '.$data['order_sn'];


                $now_data = DI()->notorm->user
                                ->select('available_predeposit,freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();
                $where .= ' and available_predeposit = ?';
                $whereData[] = $now_data['available_predeposit'];

                $data_pd['available_predeposit'] = $now_data['available_predeposit'] - $data['amount'];
                break;

            case 'red_record':
                $data_log['av_amount'] = $data['amount'];
                $data_log['desc'] = '领取红包，红包领取单号: '.$data['order_sn'];


                $now_data = DI()->notorm->user
                                ->select('available_predeposit,freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();
                $where .= ' and available_predeposit = ?';
                $whereData[] = $now_data['available_predeposit'];

                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                break;

            case 'red_refund':
                $data_log['av_amount'] = $data['amount'];
                $data_log['desc'] = '红包退款，红包单号: '.$data['order_sn'];


                $now_data = DI()->notorm->user
                                ->select('available_predeposit,freeze_predeposit')
                                ->where('id=?',$data['uid'])
                                ->fetchOne();
                $where .= ' and available_predeposit = ?';
                $whereData[] = $now_data['available_predeposit'];

                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                break;
            
            default:
                throw new Exception('参数错误');
                break;
        }

        $update = DI()->notorm->user
                        ->where($where,$whereData)
                        ->update($data_pd);
        if (!$update) {
            throw new Exception('操作失败');
        }
        $insert = DI()->notorm->pd_log->insert($data_log);
        if (!$insert) {
            throw new Exception('操作失败');
        }

        return $insert;
    }

    /**
     * 变更日志明细列表
     */
    public function getPdList($uid,$p) {

        if($p<1){
            $p=1;
        }
        $result=array();
        $pnum=50;
        $start=($p-1)*$pnum;

        if($p!=1){
            $id=$_SESSION['lg_id'];
            if($id){
                $start=0;
                $where.=" and id < {$id}";
            }
            
        }   
    
        $where =" uid = ? ";
        $result=DI()->notorm->pd_log
                ->select('')
                ->where($where,$uid)
                ->order("id desc")
                ->limit($start,$pnum)
                ->fetchAll();
    
        foreach($result as $k=>$v){
            $v['add_time_str'] = date('Y-m-d H:i', $v['add_time']);
            $v['type_name'] = $this->getActionName($v['type']);
            
            $result[$k]=$v;
        }   

        if($result){
            $last=end($result);
            $_SESSION['lg_id']=$last['id'];
        }

        return $result; 
    }

    /**
     * 增加余额提现申请
     */
    public function addPdCash($data) {
        $result = false;
        $nowtime=time();
        
        $uid=$data['uid'];
        $accountid=$data['accountid'];
        $amount=$data['amount'];
        
        /* 钱包信息 */
        $accountinfo=DI()->notorm->cash_account
                ->select("*")
                ->where('id=? and uid=?',$accountid,$uid)
                ->fetchOne();
        if(!$accountinfo){
            throw new Exception("提现账号信息不正确");
        }


        $now_user_info=DI()->notorm->user
            ->select('user_nicename,available_predeposit,freeze_predeposit')
            ->where('id = ?', $uid)
            ->fetchOne();
        // 获取用户余额及冻结余额
        if(!empty($now_user_info)){
            // 检查余额是否不足
            if($now_user_info['available_predeposit'] < $amount){
                throw new Exception("余额不足");
            }
        }
        try {
            DI()->notorm->beginTransaction('db_appapi');
            // 添加提现申请
            $data = array(
                    'uid' => $uid,
                    'nickname' => $now_user_info['user_nicename'],
                    'cash_sn' => $this->makeSn($uid),
                    'amount' => $amount,
                    'type' => $accountinfo['type'],
                    'account_bank' => $accountinfo['account_bank'],
                    'account' => $accountinfo['account'],
                    'name' => $accountinfo['name'],
                    'addtime' => $nowtime,
                );

            $insert = DI()->notorm->pd_cash->insert($data);
            if (!$insert) {
                throw new Exception("申请记录添加失败，请重试");
            }

            // 扣除可用余额，增加冻结余额
            $data_log = array();
            $data_log['uid'] = $uid;
            $data_log['nickname'] = $now_user_info['user_nicename'];
            $data_log['amount'] = $amount;
            $data_log['order_sn'] = $data['cash_sn'];
            $pdResult = $this->changePd('cash_apply',$data_log);
            if(!$pdResult){
                throw new Exception("余额更新失败，请重试");
            }

            DI()->notorm->commit('db_appapi');
            $result = true;
        } catch (Exception $e) {
            DI()->notorm->rollback('db_appapi');
            $result = false;
            throw new Exception($e->getMessage());
        }

        return $result;
    }

    public function getPdData($uid) {
        $result = DI()->notorm->user
            ->select('available_predeposit,freeze_predeposit')
            ->where('id = ?', $uid)
            ->fetchOne();
        return $result;
    }

}