<?php
class Model_Expert extends PhalApi_Model_NotORM {
	public function application($data)
	{
		try {
			DI()->notorm->beginTransaction('db_appapi');

			// 去除 分类字段
			if(isset($data['assort_id'])){
				$assort_id = $data['assort_id'];

				$assort_data = array();
				foreach ($assort_id as $key => $value) {
					$assort_item_data = array();
					$assort_item_data['uid'] = $data['uid'];
					$assort_item_data['assort_id'] = $value;

					$assort_data[] = $assort_item_data;
				}

				// 清除当前用户未启用的分类
				// DI()->notorm->user_expert_class->where("uid = ? and status = '0'",$data['uid'])->delete();

				// 添加新设置的分类
				if(!DI()->notorm->user_expert_class->insert_multi($assort_data)) throw new Exception('申请失败');

				unset($data['assort_id']);
			}

			if(!DI()->notorm->user_expert->insert($data)) throw new Exception('申请失败');

			DI()->notorm->commit('db_appapi');
			$result = true;
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			// $message = $e->getMessage();
			$result = false;
		}
		return $result;
	}

	public function application_update($data)
	{
		try {
			DI()->notorm->beginTransaction('db_appapi');

			$uid = $data['uid'];
			// 去除 分类字段
			if(isset($data['assort_id'])){
				$assort_id = $data['assort_id'];

				$assort_data = array();
				foreach ($assort_id as $key => $value) {
					$assort_item_data = array();
					$assort_item_data['uid'] = $data['uid'];
					$assort_item_data['assort_id'] = $value;

					$assort_data[] = $assort_item_data;
				}

				// 清除当前用户未启用的分类
				DI()->notorm->user_expert_class->where("uid = ? and status = '0'",$data['uid'])->delete();

				// 添加新设置的分类
				if(!DI()->notorm->user_expert_class->insert_multi($assort_data)) throw new Exception('申请失败');

				unset($data['assort_id']);
			}
			unset($data['uid']);
			if(!DI()->notorm->user_expert->where("uid=?",$uid)->update($data)) throw new Exception('申请失败');

			DI()->notorm->commit('db_appapi');
			$result = true;
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			// $message = $e->getMessage();
			$result = false;
		}
		return $result;
	}

	public function getApplyData($uid)
	{
		$result = array();
		// 获取申请信息
		$mainInfo=DI()->notorm->user_expert
				->select("*")
				->where('uid=?',$uid)
				->fetchOne();
		// 获取分类信息
		$cateInfo=DI()->notorm->user_expert_class
				->select("uid,assort_id")
				->where('uid=?',$uid)
				->fetchAll();
		// if (!empty($cateInfo)) {
		// 	$assort_ids = array_column($cateInfo, 'assort_id');
		// 	$categoryData = DI()->notorm->appraise_assort->select('id,name')->where('state=1 AND id',$assort_ids)->fetchAll();
		// 	// 获取分类名称
		// 	if(!empty($categoryData)){
		// 		$categoryData = array_column($categoryData,'name','id');
		// 		foreach ($cateInfo as $key => $value) {
		// 			$value['name'] = isset($categoryData[$value['assort_id']]) ? $categoryData[$value['assort_id']] : '';
		// 			$cateInfo[$key] = $value;
		// 		}
		// 	}
		// }

		if (!empty($mainInfo)) {
			if (!empty($cateInfo)) {
				$mainInfo['assort'] = $cateInfo;
			}
			$result = $mainInfo;
		}
		
		return $result;
	}

	public function getExpertUserData($assort_id)
	{
		$result = array();
		// 获取已启用的分类对应的专家ID集合
		$assort_data=DI()->notorm->user_expert_class
				->select("uid,assort_id")
				->where("assort_id=? and status='1'",$assort_id)
				->fetchAll();

		if (!empty($assort_data)) {
			foreach ($assort_data as $key => $value) {
				$item_user_info = DI()->notorm->user
						->select("user_nicename,avatar,avatar_thumb")
						->where('id=?  and user_type="2"',$value['uid'])
						->fetchOne();
				if(empty($item_user_info)){
					continue;
				}
				$item_user_expert_info = DI()->notorm->user_expert
						->select("realname,appraise_fee")
						->where('uid=?  and apply_status="1"',$value['uid'])
						->fetchOne();
				if(empty($item_user_expert_info)){
					continue;
				}

				// 当前专家所属分类
				$item_user_expert_class_data = DI()->notorm->user_expert_class
						->select("assort_id")
						->where('uid=? and status="1"',$value['uid'])
						->fetchAll();
				if (!empty($item_user_expert_class_data)) {
					$item_user_expert_class_data = array_column($item_user_expert_class_data,'assort_id');
					$item_assort_class_data = DI()->notorm->appraise_assort->select('id,name')->where('state=1 and id in ('.implode(',',$item_user_expert_class_data).')')->fetchAll();
					// if(!empty($item_assort_class_data)){
						// $item_assort_class_data = array_column($item_assort_class_data,'name','id');
					// }
					$value['user_expert_class_data'] = $item_assort_class_data;
				}
				$value['user_nicename'] = $item_user_info['user_nicename'];
				$value['avatar'] = get_upload_path($item_user_info['avatar']);
				$value['avatar_thumb'] = get_upload_path($item_user_info['avatar_thumb']);
				$value['realname'] = $item_user_expert_info['realname'];
				$value['appraise_fee'] = $item_user_expert_info['appraise_fee'];
				$result[] = $value;
			}
		}

		return $result;
	}

	public function appraise($order,$data)
	{
		try {
			DI()->notorm->beginTransaction('db_appapi');

			$where = "id=? and pay_status = '1' and status='0'";
			$whereData[] = $order['id'];

			// 鉴定状态更新
			$data['status'] = '1';
			$data['over_time'] = time();

			$result = DI()->notorm->appraise_apply
						->where($where,$whereData)
						->update($data);

			if ($result) {
				// 冻结的余额转入余额
				$amount = $order['expert_fee'];
				if(0<$amount*1){
					$now_user_info = DI()->notorm->user
									->select("user_nicename")
									->where('id=?',$order['touid'])
									->fetchOne();
					$data_log = array();
					$data_log['uid'] = $order['touid'];
					$data_log['nickname'] = $now_user_info['user_nicename'];
					$data_log['amount'] = $amount;
					$data_log['order_sn'] = $order['apply_order_sn'];

					$model = new Model_Pd();
					$rs = $model->changePd('appraise',$data_log);
				}
			}else{
				throw new Exception('鉴定状态更新失败');
			}

			DI()->notorm->commit('db_appapi');
			$result = true;
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			// $message = $e->getMessage();
			throw new Exception($e->getMessage());
			$result = false;
		}
		return $result;
	}

	public function getExpertUserInfo($uid)
	{

		$result = array();

		$expert_user_data=DI()->notorm->user_expert
				->select("uid,realname,appraise_fee,description")
				->where("uid=? and apply_status='1'",$uid)
				->fetchOne();

		if (!empty($expert_user_data)) {
			$item_user_info = DI()->notorm->user
					->select("user_nicename,avatar,avatar_thumb")
					->where('id=?  and user_type="2"',$expert_user_data['uid'])
					->fetchOne();
			if(!empty($item_user_info)){
				// 当前专家所属分类
				$item_user_expert_class_data = DI()->notorm->user_expert_class
						->select("assort_id")
						->where('uid=? and status="1"',$expert_user_data['uid'])
						->fetchAll();
				if (!empty($item_user_expert_class_data)) {
					$item_user_expert_class_data = array_column($item_user_expert_class_data,'assort_id');
					$item_assort_class_data = DI()->notorm->appraise_assort->select('id,name')->where('state=1 and id in ('.implode(',',$item_user_expert_class_data).')')->fetchAll();
					// if(!empty($item_assort_class_data)){
						// $item_assort_class_data = array_column($item_assort_class_data,'name','id');
					// }
				}
				$result['uid'] = $expert_user_data['uid'];
				$result['user_nicename'] = $item_user_info['user_nicename'];
				$result['avatar'] = get_upload_path($item_user_info['avatar']);
				$result['avatar_thumb'] = get_upload_path($item_user_info['avatar_thumb']);
				$result['realname'] = $expert_user_data['realname'];
				$result['appraise_fee'] = $expert_user_data['appraise_fee'];
				$result['description'] = $expert_user_data['description'];
				$result['user_expert_class_data'] = $item_assort_class_data;
			}
		}

		return $result;
	}

	// 是否为认证专家
	public function isExpert($uid)
	{
		// 未认证
		$result = 0;
        $isexist=DI()->notorm->user_expert
                    ->where('uid=?',$uid)
                    ->fetchOne();
        if($isexist){
        	if ($isexist['apply_status']=='1') {
        		// 已认证
            	$result = 2;
        	}else if($isexist['apply_status']=='0'){
        		// 认证中
				$result = 1;
        	}
        }
            
		return $result;
	}

}