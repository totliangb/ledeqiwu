<?php

class Model_Auction extends PhalApi_Model_NotORM {

	public function getNowAuctionGoods($liveuid,$showid)
	{
		$info = DI()->notorm->shop_auction->where("liveuid = ? AND showid = ? AND status = '0'",$liveuid,$showid)->fetchOne();

		if (!empty($info)) {
			$info['img'] = get_upload_path($info['img'],0);
		}

		return $info;
	}

	//生成唯一订单号
	private function trade_no()
	{
		$time = explode(' ', microtime());
		return $time[1].intval($time[0] * 1000000).mt_rand(10000, 99999);
	}

    public function createAuctionOrder($liveuid,$showid,$win_data)
    {
    	$result = array();

    	try {
    		DI()->notorm->beginTransaction('db_appapi');

    		// 竞拍成功用户头像
    		$win_userinfo = getUserInfo($win_data['win_uid']);
    		if (empty($win_userinfo)) {
    			throw new Exception("未找到竞拍成功用户信息");
    		}

	        // 获取 当前竞拍商品
	        $auctioninfo = DI()->notorm->shop_auction
	        				->where("liveuid = ? AND showid = ? AND status = '0'",$liveuid,$showid)
	        				->fetchOne();
	        if (empty($auctioninfo)) {
	        	throw new Exception("未找到竞拍商品信息");
	        }

	        // 创建竞拍商品订单
	        $pay = [
				'uid' => $win_data['win_uid'],
				'numbering' => $this->trade_no(),
				'original_price' => $win_data['win_price'],
				'addtime' => time(),
				'discount' => 0,
				'payment_amount' => $win_data['win_price']
			];

			if(!DI()->notorm->shop_pay->insert($pay)) throw new Exception('生成订单失败4');
			$pay_id = DI()->notorm->shop_pay->insert_id();

			$order_info = [
				'uid' => $win_data['win_uid'],
				'pay_id' => $pay_id,
				'shop_id' => $auctioninfo['shop_id'],
				'commodity_id' => $auctioninfo['id'],
				'title' => $auctioninfo['title'],
				'num' => 1,
				'price' => $win_data['win_price'],
				'img' => get_upload_path($auctioninfo['img'], 0)
			];
			$shop = [
					'shop_id' => $auctioninfo['shop_id'],
					'pay_id' => $pay_id,
					'numbering' => $pay['numbering'],
					'uid' => $win_data['win_uid'],
					'addtime' => time(),
					'payment_amount' => $win_data['win_price'],
					'original_price' => $win_data['win_price'],
					'is_logistic' => $auctioninfo['is_logistic'],
					'order_type'=>'1',
				];
			if(!DI()->notorm->shop_order->insert($shop)) throw new Exception('生成订单失败1');
			$order_id = DI()->notorm->shop_order->insert_id();

			$order_info['order_id'] = $order_id;
			if(!DI()->notorm->shop_order_info->insert($order_info)) throw new Exception('生成订单失败2');

	        // 更新 竞拍商品 状态及订单内容
	        $win_data['win_order_sn'] = $pay['numbering'];
	        $win_data['endtime'] = time();
	        $win_data['status'] = '1';
	        
	        DI()->notorm->shop_auction
	        			->where("liveuid = ? AND showid = ? AND status = '0'",$liveuid,$showid)
	        			->update($win_data);

			DI()->notorm->commit('db_appapi');

    	} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			return ['code' => 1005, 'msg' => $e->getMessage()];		
    	}

		return ['code' => 0, 'msg' => '', 'info' => [
			'order_id'=>$pay_id,
			'uid'=>$win_userinfo['id'],
			'nickname'=>$win_data['win_nickname'],
			'avatar'=>$win_userinfo['avatar'],
			'avatar_thumb'=>$win_userinfo['avatar_thumb'],
			'numbering' => $pay['numbering'],
			'price' => $pay['payment_amount']
		]];
    }

    public function withoutAuction($liveuid,$showid)
    {
    	$up_data = array(
    		'status' => '1',
    		'endtime' => time()
    	);
        // 获取 当前竞拍商品
        $up_flag = DI()->notorm->shop_auction
        				->where("liveuid = ? AND showid = ? AND status = '0'",$liveuid,$showid)
        				->update($up_data);

        return $up_flag;
    }

    public function completingAuctionOrderAddress($order_id,$address_info)
    {
    	$result = false;

    	$shop_pay = array();
    	$shop_pay['address'] = $address_info['id'];
    	$shop_order = array();
    	$shop_order['address'] = $address_info['id'];
    	$shop_order['address_info'] = $address_info['title'];
    	try {
    		DI()->notorm->beginTransaction('db_appapi');

	    	$payFlag = DI()->notorm->shop_pay->where('id = ?',$order_id)->update($shop_pay);
	    	$orderFlag = DI()->notorm->shop_order->where('id = ?',$order_id)->update($shop_order);
	    	if ($payFlag && $orderFlag) {
	    		$result = true;
	    	}
	    	
	    	DI()->notorm->commit('db_appapi');
    	} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
    	}

    	return $result;
    }

}