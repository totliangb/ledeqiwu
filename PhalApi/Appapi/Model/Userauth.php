<?php
/**
 * ${名称}
 * @author 姚翔（512282389@qq.com iaage2010@gmail.com）深圳市将才网络科技有限公司
 * @version 1.0.0
 * @desc ${描述}
 * @createTime 2020/5/1 22:02
 */

class Model_Userauth extends PhalApi_Model_NotORM
{

    /**
     * 新增认证
     * @param $data
     * @return bool
     */
    public function addAuth($data){

        /** @var  $userAuthModel */
        if(DI()->notorm->user_auth->where('uid=? AND status=?',$data['uid'],1)->count('uid')) {
            return false;
        }else{
            if(DI()->notorm->user_auth->where('uid=?',$data['uid'])->count()){
                DI()->notorm->user_auth->where('uid=?',$data['uid'])->delete();
            }
            return DI()->notorm->user_auth->insert($data);
        }
    }

    /**
     * 查询是否认证成功，并根据缴纳状态更新认证状态
     * @param $uid
     * @return bool
     */
    public function getAuthInfo($uid){
        $time = time();
        $configpri = getConfigPri();
        $authInfo = DI()->notorm->user_auth->where('uid=?',$uid)->fetchOne();
        $userInfo = DI()->notorm->user->select("is_bond,cert_overdue")->where('id=?',$uid)->fetchOne();
        //若保证金设置金额为0，且交了认证费，则自动把保证金改为1
        if ($configpri['bond_price'] == 0 && $userInfo['cert_overdue'] >= $time && $userInfo['is_bond'] == 0){
            $ret = DI()->notorm->user->where('id=?',$uid)->update(array("is_bond"=>1));
        }
        if ($authInfo){
            $authInfo['is_bond'] = $userInfo['is_bond'];
            $authInfo['is_cert'] = $userInfo['cert_overdue'];
        }
        
        return $authInfo;

    }

    public function addAuthPay($uid,$price,$order,$type,$reorder="",$reason="",$pay_code=""){
        $configpri = getConfigPri();
        $configpub = getConfigPub();
        $time = time();
        if ($type == "bond" || $type == "cert"){
            $data = [
                "uid" => $uid,
                "type" => $type,
                "num" => $price,
                "payment" => $order,
                "addtime" => time()
            ];
            $payment = DI()->notorm->user_auth_payment->insert($data);
            
            if ($type == "bond"){
                $name = "平台开播保证金";
            }else{
                $name = "平台开播认证费";
            }

            try {
                // 调整order数据
                $orderData['order_sn'] = $name;
                $orderData['pay_sn'] = $order;
                $orderData['amount'] = $price;

                $params = array(
                    'code' => $pay_code,
                    'notify_url' => '/Appapi/pay/cert_notify_'.$pay_code,
                );

                // 第三方支付
                $domainPay = new Domain_Pay();
                $result = $domainPay->_api_pay($orderData,$params);

            } catch (Exception $e) {
                $result = false;
            }

            return $result;
        }elseif ($type == "rebond"){
            return [];
        }
    }
    
    /**
     * 添加支付记录
     * @param $uid
     * @return int
     */
    public function addAuthWxPay($uid,$price,$order,$type,$reorder="",$reason=""){
        $configpri = getConfigPri();
        $configpub = getConfigPub();
        $time = time();
        if ($type == "bond" || $type == "cert"){
            $data = [
                "uid" => $uid,
                "type" => $type,
                "num" => $price,
                "payment" => $order,
                "addtime" => time()
            ];
            $payment = DI()->notorm->user_auth_payment->insert($data);
            
            if ($type == "bond"){
                $name = "平台开播保证金";
            }else{
                $name = "平台开播认证费";
            }
            $noceStr = md5(rand(100,1000).time());//获取随机字符串
    			
    		$paramarr = array(
    			"appid"       =>   $configpri['wx_appid'],
    			"body"        =>    $name,
    			"mch_id"      =>    $configpri['wx_mchid'],
    			"nonce_str"   =>    $noceStr,
    			"notify_url"  =>    $configpub['site'].'/Appapi/pay/cert_notify_wx',
    			"out_trade_no"=>    $order,
    			"total_fee"   =>    $price*100, 
    			"trade_type"  =>    "APP"
    		);
    		$sign = $this -> sign($paramarr,$configpri['wx_key']);//生成签名
    		$paramarr['sign'] = $sign;
    		$paramXml = "<xml>";
    		foreach($paramarr as $k => $v){
    			$paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
    		}
    		$paramXml .= "</xml>";
    			 
    		$ch = curl_init ();
    		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
    		@curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
    		@curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/pay/unifiedorder");
    		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		@curl_setopt($ch, CURLOPT_POST, 1);
    		@curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
    		@$resultXmlStr = curl_exec($ch);
    		if(curl_errno($ch)){
    			//print curl_error($ch);
    			file_put_contents('./wxpay.txt',date('y-m-d H:i:s').' 提交参数信息 ch:'.json_encode(curl_error($ch))."\r\n",FILE_APPEND);
    		}
    		curl_close($ch);
    
    		$result2 = $this->xmlToArray($resultXmlStr);
            
            if($result2['return_code']=='FAIL'){
                $rs['code']=1005;
    			$rs['msg']=$result2['return_msg'];
                return $rs;	
            }
    		$time2 = time();
    		$prepayid = $result2['prepay_id'];
    		$sign = "";
    		$noceStr = md5(rand(100,1000).time());//获取随机字符串
    		$paramarr2 = array(
    			"appid"     =>  $configpri['wx_appid'],
    			"noncestr"  =>  $noceStr,
    			"package"   =>  "Sign=WXPay",
    			"partnerid" =>  $configpri['wx_mchid'],
    			"prepayid"  =>  $prepayid,
    			"timestamp" =>  $time2
    		);
    		$paramarr2["sign"] = $this -> sign($paramarr2,$configpri['wx_key']);//生成签名
    
    		return $paramarr2;
        }elseif ($type == "rebond"){
            return [];
            $data = [
                "uid" => $uid,
                "type" => $type,
                "num" => $price,
                "payment" => $order,
                "addtime" => time(),
                "refund_payment" => $reorder,
                "reason" => $reason
            ];
            $payment = DI()->notorm->user_auth_refund->insert($data);
            
            $noceStr = md5(rand(100,1000).time());//获取随机字符串
    			
    		$paramarr = array(
    			"appid"       =>   $configpri['wx_appid'],
    			"mch_id"      =>    $configpri['wx_mchid'],
    			"nonce_str"   =>    $noceStr,
    			"notify_url"  =>    $configpub['site'].'/Appapi/pay/certrefund_notify_wx',
    			"out_refund_no" =>  $reorder,
    			"out_trade_no"=>    $order,
    			"refund_desc" =>    $reason,
    			"refund_fee"  =>    $price*100,
    			"total_fee"   =>    $price*100, 
    			"trade_type"  =>    "APP"
    		);
    		$sign = $this -> sign($paramarr,$configpri['wx_key']);//生成签名
    		$paramarr['sign'] = $sign;
    		$paramXml = "<xml>";
    		foreach($paramarr as $k => $v){
    			$paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
    		}
    		$paramXml .= "</xml>";
    			 
    		$ch = curl_init ();
    		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
    		@curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
    		@curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/secapi/pay/refund");
    		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		@curl_setopt($ch, CURLOPT_POST, 1);
    		@curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
    		@$resultXmlStr = curl_exec($ch);
    		if(curl_errno($ch)){
    			//print curl_error($ch);
    			file_put_contents('./wxpay.txt',date('y-m-d H:i:s').' 提交参数信息 ch:'.json_encode(curl_error($ch))."\r\n",FILE_APPEND);
    		}
    		curl_close($ch);
    
    		$result2 = $this->xmlToArray($resultXmlStr);
            
            if($result2['return_code']=='FAIL'){
                $rs['code']=1005;
    			$rs['msg']=$result2['return_msg'];
                return $rs;	
            }
    		$time2 = time();
    		$prepayid = $result2['out_trade_no'];
    		$prerefund = $result2['out_refund_no'];
    		$sign = "";
    		$noceStr = md5(rand(100,1000).time());//获取随机字符串
    		$paramarr2 = array(
    			"appid"     =>  $configpri['wx_appid'],
    			"noncestr"  =>  $noceStr,
    			"package"   =>  "Sign=WXPay",
    			"partnerid" =>  $configpri['wx_mchid'],
    			"out_trade_no"  =>  $prepayid,
    			"out_refund_no" =>  $prerefund,
    			"timestamp" =>  $time2
    		);
    		$paramarr2["sign"] = $this -> sign($paramarr2,$configpri['wx_key']);//生成签名
    		
    		return $paramarr2;
        }
    }
    
    public function getOrder($uid){
        $order1 = DI()->notorm->user_auth_payment->where("uid = ".$uid." AND status = 'SUCCESS' AND type = 'bond'")->fetchOne();
        $order2 = DI()->notorm->user_auth_payment->where("uid = ".$uid." AND status = 'SUCCESS' AND type = 'cert'")->fetchOne();
        if ($order1 && $order2){
            $order = [$order1,$order2];
        }else{
            if ($order1){
                $order = [$order1];
            }elseif ($order2){
                $order = [$order2];
            }else{
                $order = [];
            }
        }
        return $order;
    }
    
    protected function checkWxPay($ordernum){
        $configpri = getConfigPri();
        $noceStr = md5(rand(100,1000).time());
        
        $paramarr = array(
			"appid"       =>   $configpri['wx_appid'],
			"mch_id"      =>    $configpri['wx_mchid'],
			"nonce_str"   =>    $noceStr,
			"out_trade_no" =>   $ordernum,
		);
		$sign = $this -> sign($paramarr,$configpri['wx_key']);//生成签名
		$paramarr['sign'] = $sign;
		$paramXml = "<xml>";
		foreach($paramarr as $k => $v){
			$paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
		}
		$paramXml .= "</xml>";
			 
		$ch = curl_init ();
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
		@curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
		@curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/pay/orderquery");
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($ch, CURLOPT_POST, 1);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
		@$resultXmlStr = curl_exec($ch);

		curl_close($ch);

		$result2 = $this->xmlToArray($resultXmlStr);
		return $result2;
    }
    
    protected function checkWxRefund($ordernum){
        $configpri = getConfigPri();
        $noceStr = md5(rand(100,1000).time());
        
        $paramarr = array(
			"appid"       =>   $configpri['wx_appid'],
			"mch_id"      =>    $configpri['wx_mchid'],
			"nonce_str"   =>    $noceStr,
			"out_refund_no" =>   $ordernum,
		);
		$sign = $this -> sign($paramarr,$configpri['wx_key']);//生成签名
		$paramarr['sign'] = $sign;
		$paramXml = "<xml>";
		foreach($paramarr as $k => $v){
			$paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
		}
		$paramXml .= "</xml>";
			 
		$ch = curl_init ();
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
		@curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
		@curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/pay/refundquery");
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($ch, CURLOPT_POST, 1);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
		@$resultXmlStr = curl_exec($ch);

		curl_close($ch);

		$result2 = $this->xmlToArray($resultXmlStr);
		return $result2;
    }
    
    /**
	* sign拼装获取
	*/
	protected function sign($param,$key){
		$sign = "";
		foreach($param as $k => $v){
			$sign .= $k."=".$v."&";
		}
		$sign .= "key=".$key;
		$sign = strtoupper(md5($sign));
		return $sign;
	
	}
	/**
	* xml转为数组
	*/
	protected function xmlToArray($xmlStr){
		$msg = array(); 
		$postStr = $xmlStr; 
		$msg = (array)simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA); 
		return $msg;
	}
    
    /**
     * 查询支付记录并更改用户状态和订单状态
     * @param $uid,$type
     * @return
     */
    public function checkPayment($uid,$type){
        $configpri = getConfigPri();
        $mchid = $configpri['wx_mchid'];
        
        if($type == "bond" || $type == "cert"){
            $check = DI()->notorm->user_auth_payment->where("uid=".$uid." AND type='".$type."'")->order("addtime desc")->fetchOne();
            //查不到订单就算了
            if (!$check) return;
            $result = $this->checkWxPay($check['payment']);
            if ($result['trade_state'] == "SUCCESS"){
                if($check['status'] != "SUCCESS"){  //首次验证
                    $paytime = strtotime($result['time_end']);
                    $ret = DI()->notorm->user_auth_payment->where("id = ".$check['id'])->update(array("paytime"=>$paytime,"status"=>$result['trade_state']));
                    if ($type == "bond"){
                        $ret2 = DI()->notorm->user->where("id = ".$uid)->update(array("is_bond"=>1));
                    }elseif ($type == "cert"){
                        //设置过期时间
                        $overdue = strtotime("+1 year",strtotime(date("Y-m-d 24:00",$paytime)));
                        $ret2 = DI()->notorm->user->where("id = ".$uid)->update(array("cert_overdue"=>$overdue));
                    }
                }else{  //若已支付且状态已经是已支付，则无需更改
                    return;
                }
            }else{
                //创建订单未付款、订单关闭、产生退款等情况，一律视为未交保证金
                if ($type == "bond"){
                    $ret2 = DI()->notorm->user->where("id = ".$uid)->update(array("is_bond"=>0));
                }
                
                if($result['trade_state'] == "REFUND"){
                    $check = DI()->notorm->user_auth_refund->where("payment = '".$result['out_trade_no']."'")->order("addtime desc")->fetchOne();
                    if($check['status'] == "SUCCESS") return;   //退了款就可以了
                    $result = $this->checkWxRefund($check['refund_payment']);
                    if ($result['return_code'] == "SUCCESS"){
                        $refundtime = strtotime(str_replace("T"," ",substr($result['success_time'],0,-6)));
                        $ret = DI()->notorm->user_auth_payment->where("id = ".$check['id'])->update(array("refundtime"=>$refundtime,"status"=>$result['status']));
                    }
                }
            }
        }
        
    }
    
    public function addRefundBond($uid,$accountid,$reason=""){
        $select = DI()->notorm->user_auth_payment->where("uid = ? AND status = 'SUCCESS' AND type='bond'",$uid)->order("addtime desc")->fetchOne();
        if (!$select){
            return 0;
        }
        // $userbond = DI()->notorm->user->where("id = ?",$uid)->update(array("is_bond" => 0));
        $data = [
            "uid" => $uid,
            "type" => "rebond",
            "num" => $select['num'],
            "payment" => $select['payment'],
            "addtime" => time(),
            'accountid' => $accountid,
            "refund_payment" => "manual",
            "reason" => $reason
        ];
        $payment = DI()->notorm->user_auth_refund->insert($data);
        // if ($userbond && $payment){
        if ($payment){
            return 1;
        }else{
            return 0;
        }
        
    }
}