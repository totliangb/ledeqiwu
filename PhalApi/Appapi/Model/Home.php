<?php
if (!session_id()) session_start();
class Model_Home extends PhalApi_Model_NotORM {
    protected $live_fields='uid,title,city,stream,pull,thumb,isvideo,type,type_val,goodnum,anyway,starttime,isshop,game_action';
     
    
	/* 轮播 */
	public function getSlide(){

		$rs=DI()->notorm->slide_item
			->select("image as slide_pic,target,url as slide_url")
			->where("status='1' and slide_id='2' ")
			->order("list_order asc")
			->fetchAll();
		foreach($rs as $k=>$v){
			$rs[$k]['slide_pic']=get_upload_path($v['slide_pic']);
		}				

		return $rs;
	}

	/* 热门 */
    public function getHot($p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=" islive= '1' and ishot='1'";
        
        if($p==1){
			$_SESSION['hot_starttime']=time();
		}
        
		if($p!=0){
			$endtime=$_SESSION['hot_starttime'];
            if($endtime){
                $where.=" and starttime < {$endtime}";
            }
			
		}
        
        if($p!=1){
			$hotvotes=$_SESSION['hot_hotvotes'];
            if($hotvotes){
                $where.=" and hotvotes < {$hotvotes}";
            }else{
                $where.=" and hotvotes < 0";
            }
			
		}
		
		$result=DI()->notorm->live
                    ->select($this->live_fields.',hotvotes')
                    ->where($where.' and is_home = 0')
                    ->order('isrecommend desc,hotvotes desc,starttime desc')
                    ->limit(0,$pnum)
                    ->fetchAll();
        $result2=DI()->notorm->live
                    ->select($this->live_fields.',hotvotes')
                    ->where($where.' and is_home = 1')
                    ->order('isrecommend desc,hotvotes desc,starttime desc')
                    ->limit(0,$pnum)
                    ->fetchAll();
		foreach($result as $k=>$v){
            
			$v=handleLive($v);
            $result[$k]=$v;
			
		}	
		foreach($result2 as $k=>$v){
            
			$v=handleLive($v);
            
            $result2[$k]=$v;
			
		}


		if($result){
			$last=end($result);
			//$_SESSION['hot_starttime']=$last['starttime'];
			$_SESSION['hot_hotvotes']=$last['hotvotes'];
		}
        // echo json_encode([$result, $result2]);die;
		return [$result, $result2];
    }
	
		/* 关注列表 */
    public function getFollow($uid,$p) {
        $rs=array(
            'title'=>'你还没有关注任何主播',
            'des'=>'赶快去关注自己喜欢的主播吧~',
            'list'=>array(),
        );
        if($p<1){
            $p=1;
        }
		$result=array();
		$pnum=50;
		$start=($p-1)*$pnum;
		
		$touid=DI()->notorm->user_attention
				->select("touid")
				->where('uid=?',$uid)
				->fetchAll();
				
		if(!$touid){
            return $rs;
        }
        
        $rs['title']='你关注的主播没有开播';
        $rs['des']='赶快去看看其他主播的直播吧~';
        $where=" islive='1' ";					
        if($p!=1){
            $endtime=$_SESSION['follow_starttime'];
            if($endtime){
                $start=0;
                $where.=" and starttime < {$endtime}";
            }
            
        }	
    
        $touids=array_column($touid,"touid");
        $touidss=implode(",",$touids);
        $where.=" and uid in ({$touidss})";
        $result=DI()->notorm->live
                ->select($this->live_fields)
                ->where($where)
                ->order("starttime desc")
                ->limit(0,$pnum)
                ->fetchAll();
	
		foreach($result as $k=>$v){
            
			$v=handleLive($v);
            
            $result[$k]=$v;
		}	

		if($result){
			$last=end($result);
			$_SESSION['follow_starttime']=$last['starttime'];
		}
        
        $rs['list']=$result;

		return $rs;					
    }
		
		/* 最新 */
    public function getNew($lng,$lat,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=" islive='1' ";

		if($p!=1){
			$endtime=$_SESSION['new_starttime'];
            if($endtime){
                $where.=" and starttime < {$endtime}";
            }
		}
		
		$result=DI()->notorm->live
				->select($this->live_fields.',lng,lat')
				->where($where)
				->order("starttime desc")
				->limit(0,$pnum)
				->fetchAll();	
		foreach($result as $k=>$v){
            
			$v=handleLive($v);
			
			$distance='好像在火星';
			if($lng!='' && $lat!='' && $v['lat']!='' && $v['lng']!=''){
				$distance=getDistance($lat,$lng,$v['lat'],$v['lng']);
			}else if($v['city']){
				$distance=$v['city'];	
			}
			
			$v['distance']=$distance;
			unset($v['lng']);
			unset($v['lat']);
            
            $result[$k]=$v;
			
		}		
		if($result){
			$last=end($result);
			$_SESSION['new_starttime']=$last['starttime'];
		}

		return $result;
    }
		
		/* 搜索 */
    public function search($uid,$key,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=' user_type="2" and ( id=? or user_nicename like ?  or goodnum like ? ) and id!=?';
		if($p!=1){
			$id=$_SESSION['search'];
            if($id){
                $where.=" and id < {$id}";
            }
		}
		
		$result=DI()->notorm->user
				->select("id,user_nicename,avatar,sex,signature,consumption,votestotal")
				->where($where,$key,'%'.$key.'%','%'.$key.'%',$uid)
				->order("id desc")
				->limit($start,$pnum)
				->fetchAll();
		foreach($result as $k=>$v){
			$v['level']=(string)getLevel($v['consumption']);
			$v['level_anchor']=(string)getLevelAnchor($v['votestotal']);
			$v['isattention']=(string)isAttention($uid,$v['id']);
			$v['avatar']=get_upload_path($v['avatar']);
			unset($v['consumption']);
            
            $result[$k]=$v;
		}				
		
		if($result){
			$last=end($result);
			$_SESSION['search']=$last['id'];
		}
		
		return $result;
    }
	
	/* 附近 */
    public function getNearby($lng,$lat,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=" islive='1' and lng!='' and lat!='' ";
		
		$result=DI()->notorm->live
				->select($this->live_fields.",round(6378.138 * 2 * ASIN(SQRT(POW(SIN(( ".$lat." * PI() / 180 - lat * PI() / 180) / 2),2) + COS(".$lat." * PI() / 180) * COS(lat * PI() / 180) * POW(SIN((".$lng." * PI() / 180 - lng * PI() / 180) / 2),2))) * 1000) AS distance,province")//,getDistance('{$lat}','{$lng}',lat,lng) as distance
				->where($where)
                ->order("distance asc")
                ->limit($start,$pnum)
				->fetchAll();	
		foreach($result as $k=>$v){
            
			$v=handleLive($v);
            
            if($v['distance']>1000){
                $v['distance']=1000;
            }
            $v['distance']=$v['distance'].'km';

            $result[$k]=$v;
		}
		
		return $result;
    }


	/* 推荐 */
	public function getRecommend(){

		$result=DI()->notorm->user
				->select("id,user_nicename,avatar,avatar_thumb")
				->where("isrecommend='1'")
				->order("votestotal desc")
				->limit(0,12)
				->fetchAll();
		foreach($result as $k=>$v){
			$v['avatar']=get_upload_path($v['avatar']);
			$v['avatar_thumb']=get_upload_path($v['avatar_thumb']);
			$fans=getFans($v['id']);
			$v['fans']='粉丝 · '.$fans;
            
            $result[$k]=$v;
		}
		return  $result;
	}
	/* 关注推荐 */
	public function attentRecommend($uid,$touids){
		//$users=$this->getRecommend();
		//$users=explode(',',$touids);
        //file_put_contents('./attentRecommend.txt',date('Y-m-d H:i:s').' 提交参数信息 touids:'.$touids."\r\n",FILE_APPEND);
        $users=preg_split('/,|，/',$touids);
		foreach($users as $k=>$v){
			$touid=$v;
            //file_put_contents('./attentRecommend.txt',date('Y-m-d H:i:s').' 提交参数信息 touid:'.$touid."\r\n",FILE_APPEND);
			if($touid && !isAttention($uid,$touid)){
				DI()->notorm->user_black
					->where('uid=? and touid=?',$uid,$touid)
					->delete();
				DI()->notorm->user_attention
					->insert(array("uid"=>$uid,"touid"=>$touid));
			}
			
		}
		return 1;
	}

	/*获取收益排行榜*/

	public function profitList($uid,$type,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;

		switch ($type) {
			case 'day':
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";

			break;

			case 'week':
                $w=date('w'); 
                //获取本周开始日期，如果$w是0，则表示周日，减去 6 天 
                $first=1;
                //周一
                $week=date('Y-m-d H:i:s',strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days')); 
                $week_start=strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days'); 

                //本周结束日期 
                //周天
                $week_end=strtotime("{$week} +1 week")-1;
                
				$where=" addtime >={$week_start} and addtime<={$week_end} and ";

			break;

			case 'month':
                //本月第一天
                $month=date('Y-m-d',strtotime(date("Ym").'01'));
                $month_start=strtotime(date("Ym").'01');

                //本月最后一天
                $month_end=strtotime("{$month} +1 month")-1;

				$where=" addtime >={$month_start} and addtime<={$month_end} and ";

			break;

			case 'total':
				$where=" ";
			break;
			
			default:
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";
			break;
		}




		$where.=" type=0 and action in ('1','2')";
		
		$result=DI()->notorm->user_coinrecord
            ->select('sum(totalcoin) as totalcoin,touid as uid')
            ->where($where)
            ->group('touid')
            ->order('totalcoin desc')
            ->limit($start,$pnum)
            ->fetchAll();

		foreach ($result as $k => $v) {
            $userinfo=getUserInfo($v['uid']);
            $v['avatar']=$userinfo['avatar'];
			$v['avatar_thumb']=$userinfo['avatar_thumb'];
			$v['user_nicename']=$userinfo['user_nicename'];
			$v['sex']=$userinfo['sex'];
			$v['level']=$userinfo['level'];
			$v['level_anchor']=$userinfo['level_anchor'];
            
            $v['isAttention']=isAttention($uid,$v['uid']);//判断当前用户是否关注了该主播
            
            $result[$k]=$v;
		}


		return $result;
	}



	/*获取消费排行榜*/
	public function consumeList($uid,$type,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;

		switch ($type) {
			case 'day':
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";

			break;
            
            case 'week':
                $w=date('w'); 
                //获取本周开始日期，如果$w是0，则表示周日，减去 6 天 
                $first=1;
                //周一
                $week=date('Y-m-d H:i:s',strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days')); 
                $week_start=strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days'); 

                //本周结束日期 
                //周天
                $week_end=strtotime("{$week} +1 week")-1;
                
				$where=" addtime >={$week_start} and addtime<={$week_end} and ";

			break;

			case 'month':
                //本月第一天
                $month=date('Y-m-d',strtotime(date("Ym").'01'));
                $month_start=strtotime(date("Ym").'01');

                //本月最后一天
                $month_end=strtotime("{$month} +1 month")-1;

				$where=" addtime >={$month_start} and addtime<={$month_end} and ";

			break;

			case 'total':
				$where=" ";
			break;
			
			default:
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";
			break;
		}

		$where.=" type=0 and action in ('1','2')";
		
        $result=DI()->notorm->user_coinrecord
            ->select('sum(totalcoin) as totalcoin, uid')
            ->where($where)
            ->group('uid')
            ->order('totalcoin desc')
            ->limit($start,$pnum)
            ->fetchAll();

		foreach ($result as $k => $v) {
            $userinfo=getUserInfo($v['uid']);
            $v['avatar']=$userinfo['avatar'];
			$v['avatar_thumb']=$userinfo['avatar_thumb'];
			$v['user_nicename']=$userinfo['user_nicename'];
			$v['sex']=$userinfo['sex'];
			$v['level']=$userinfo['level'];
			$v['level_anchor']=$userinfo['level_anchor'];
            
            $v['isAttention']=isAttention($uid,$v['uid']);//判断当前用户是否关注了该主播
            
            $result[$k]=$v;
		}

		return $result;
	}
    
    /* 分类下直播 */
    public function getClassLive($p,$liveclassid,$nickname) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=" islive='1' ";
		if ($liveclassid) {
			$where .= " and liveclassid={$liveclassid} ";
		}

		if ($nickname) {
			// 用户昵称检索
			$userWhere=' user_type="2" and ( user_nicename like "%'.$nickname.'%")';
			$searchUids = DI()->notorm->user
				->select('id')
				->where($userWhere)
				->fetchAll();
			$searchUids = array_column($searchUids, 'id');
			if (!empty($searchUids)) {
				$where .= " and uid IN (".implode(',', $searchUids).") ";	
			}else{
				$where .= " and uid IN ('') ";	
			}
			
		}
        
		// if($p!=1){
		// 	$endtime=$_SESSION['getClassLive_starttime'];
  //           if($endtime){
  //               $where.=" and starttime < {$endtime}";
  //           }
			
		// }
		$last_starttime=0;
		$result=DI()->notorm->live
				->select($this->live_fields)
				->where($where)
				->order("hotvotes desc,starttime desc")
				->limit($start,$pnum)
				->fetchAll();	
		foreach($result as $k=>$v){
			$v=handleLive($v);
            $result[$k]=$v;
		}		
		// if($result){
  //           $last=end($result);
		// 	$_SESSION['getClassLive_starttime']=$last['starttime'];
		// }

		return $result;
    }


	/* 首页推荐直播间信息 */
    public function getIndexRecommend() {
		$where=" islive= '1' ";
        $result=DI()->notorm->live
                    ->select($this->live_fields)
                    ->where($where.' and is_home = 1')
                    ->fetchRow();

        if(empty($result)){
        	// 获取人气最高的直播间
        	$hvData = DI()->notorm->live
		        		->select('MAX(hotvotes) as hv')
		        		->where($where)
		        		->fetchRow();
        	$hv = $hvData['hv'] ? $hvData['hv'] : 0;
        	$result = DI()->notorm->live
		        		->select($this->live_fields)
		        		->where($where." AND hotvotes = ".$hv)
		        		->fetchRow();
        }
        if(!empty($result)){
        	$result=handleLive($result);
        }else{
        	$result = array();
        }

		return $result;
    }


	/* 搜索 根据版块 */
    public function searchMulti($type,$keywords,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;

		$result = array();
		if ($keywords) {
			switch ($type) {
				case 'goods':
					// 库存不能为0 
					$where .= " and amount > 0 AND title like '%".$keywords."%' ";

					$result = DI()->notorm->shop_commodity->select('id,shop_id,title,description,img,img_h,img_w,original_price,price,amount,sales')->order('sales desc,id desc')->limit($start,$pnum)->where('state=1'.$where)->fetchAll();
					foreach ($result as $key => &$value) 
					{
						$value['img'] = get_upload_path($value['img'], 0);
					}
					break;
				case 'live':
					$result = $this->getClassLive($p,0,$keywords);
					break;
				case 'shop':
					$where .= " AND name like '%".$keywords."%' ";
					$result = DI()->notorm->shop->select('id,uid,name,shop_img,description')->order('sort desc,id desc')->limit($start,$pnum)->where('state=1'.$where)->fetchAll();
					foreach ($result as $key => &$value) 
					{
			      		$value['follow']=DI()->notorm->user_attention->where('uid', $value['uid'])->count();
			      		$value['fans']=DI()->notorm->user_attention->where('touid', $value['uid'])->count();
						$value['shop_img'] = get_upload_path($value['shop_img'], 0);
					}
					break;
			}
		}
		
		return $result;
    }
    
}
