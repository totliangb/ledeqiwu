<?php

class Model_Appraise extends PhalApi_Model_NotORM {

	public function getAppraiseList($p)
	{
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		// $where=' 1 ';
		$where = "status = '1' AND pay_status = '1'";
		
		if($p!=1){
			$id=$_SESSION['appraise'];
            if($id){
                $where.=" and id < {$id}";
            }
		}
		
		$result=DI()->notorm->appraise_apply
				->select("id,uid,touid,apply_order_sn,pay_sn,pics,description,add_time,goods_id,type,appraise_fee,answer,view_num,anonymous")
				->where($where)
				->order("id desc,add_time desc")
				->limit($start,$pnum)
				->fetchAll();
		foreach($result as $k=>$v){
			// 确定商品
			switch ($v['type']) {
				case '1':
					// 商品鉴定
					break;
				case '0':
					// 普通鉴定
					break;
			}
			// 图片集合
			if(!empty($v['pics'])){
				$itemPics = json_decode($v['pics']);
				foreach ($itemPics as $key => $value) {
					$value=get_upload_path($value,0);
					$itemPics[$key] = $value;
				}
				$v['pics'] = $itemPics;
			}
			$v['add_time'] = date('Y-m-d H:i', $v['add_time']);
			// 是否回答
			$v['is_answered'] = !empty($v['answer']) ? 1 : 0;
			unset($v['answer']);
			unset($v['goods_id']);

			// 用户信息
	        $userinfo=getUserInfo($v['uid']);
	        $v['user_nicename']=$userinfo['user_nicename'];
	        $v['avatar']=$userinfo['avatar'];
	        $v['avatar_thumb']=$userinfo['avatar_thumb'];
			// 鉴定专家信息
	        $expertuserinfo=getUserInfo($v['touid']);
	        $v['expert']['user_nicename']=$expertuserinfo['user_nicename'];
	        $v['expert']['avatar']=$expertuserinfo['avatar'];
	        $v['expert']['avatar_thumb']=$expertuserinfo['avatar_thumb'];

            $result[$k]=$v;
		}				
		
		if($result){
			$last=end($result);
			$_SESSION['appraise']=$last['id'];
		}
		
		return $result;
	}

	// 检查用户是否有查看回复的权限
	public function checkAppraiseAnswerPower($appraiseInfo,$uid)
	{
		$hasPower = 0;
		if($uid){
			// 检查用户是否为当前鉴定的申请者
			if($appraiseInfo['uid'] == $uid){
				$hasPower = 1;
			}else{
				// 检查是否有付费查看记录且已付款完成
				$checkAnswer = DI()->notorm->appraise_answer_view
					->select("*")
					->where("apply_id=? and pay_status='1' and uid=?",$appraiseInfo['id'],$uid)
					->fetchOne();
				if (!empty($checkAnswer)) {
					$hasPower = 1;
				}
			}
		}
		return $hasPower;
	}

	public function getAppraiseInfo($id,$uid)
	{
		$info=DI()->notorm->appraise_apply
					->select("*")
					->where('id=?',$id)
					->fetchOne();
		// 是否回答
		$info['is_answered'] = ($info['status'] == '1' && !empty($info['answer'])) ? 1 : 0;
		// 图片集合
		if(!empty($info['pics'])){
			$itemPics = json_decode($info['pics']);
			foreach ($itemPics as $key => $value) {
				$value=get_upload_path($value,0);
				$itemPics[$key] = $value;
			}
			$info['pics'] = $itemPics;
		}
		$info['add_time'] = date('Y-m-d H:i', $info['add_time']);

		if($uid){
			// 检查用户是否有查看答案的权限
			$info['is_allow_view_answer'] = $this->checkAppraiseAnswerPower($info,$uid);
		}else{
			$info['is_allow_view_answer'] = 0;
		}
		
		// 获取查看答案所需支付的金额
		$configpri=getConfigPri();
		$view_fee = isset($configpri['appraise_answer_view_fee']) ? $configpri['appraise_answer_view_fee'] : 0;
		$info['view_fee'] = $view_fee;

		// 获取截止时间
		$overtime = 0;
		// 获取设定的超时时间
		$overtime_days = 0;
		if(isset($configpri['appraise_lock_days'])) $overtime_days = $configpri['appraise_lock_days'];
		if($overtime_days){
			$overtime = $info['pay_time'] + (86400*$overtime_days);
		}
		$info['overtime'] = $overtime;

		// 用户信息
        $userinfo=getUserInfo($info['uid']);
        $info['user_nicename']=$userinfo['user_nicename'];
        $info['avatar']=$userinfo['avatar'];
        $info['avatar_thumb']=$userinfo['avatar_thumb'];
		// 鉴定专家信息
        $expertuserinfo=getUserInfo($info['touid']);
        $info['expert']['user_nicename']=$expertuserinfo['user_nicename'];
        $info['expert']['avatar']=$expertuserinfo['avatar'];
        $info['expert']['avatar_thumb']=$expertuserinfo['avatar_thumb'];
        // 当前专家所属分类
		$item_user_expert_class_data = DI()->notorm->user_expert_class
				->select("assort_id")
				->where('uid=? and status="1"',$info['touid'])
				->fetchAll();
		if (!empty($item_user_expert_class_data)) {
			$item_user_expert_class_data = array_column($item_user_expert_class_data,'assort_id');
			$item_assort_class_data = DI()->notorm->shop_assort->select('id,name')->where('state=1 and id in ('.implode(',',$item_user_expert_class_data).')')->fetchAll();
		}
		$info['expert']['user_expert_class_data'] = $item_assort_class_data;

		return $info;

	}

	public function getViewAppraiseAnswerList($id,$p)
	{
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where = "pay_status = '1' and apply_id = ?";
		if($p!=1){
			$id=$_SESSION['appraise_view'];
            if($id){
                $where.=" and id < {$id}";
            }
		}

		$viewList=DI()->notorm->appraise_answer_view
				->select("*")
				->where($where, $id)
				->order("id desc,pay_time desc")
				->limit($start,$pnum)
				->fetchAll();

		foreach ($viewList as $key => $value) {
			// 用户信息
			$item_user_info = DI()->notorm->user
					->select("user_nicename,avatar,avatar_thumb")
					->where('id=?  and user_type="2"',$value['uid'])
					->fetchOne();
			if(empty($item_user_info)){
				unset($viewList[$key]);
				continue;
			}else{
				$value['user_nicename'] = $item_user_info['user_nicename'];
				$value['avatar'] = get_upload_path($item_user_info['avatar']);
				$value['avatar_thumb'] = get_upload_path($item_user_info['avatar_thumb']);
			}
			// 支付时间格式化
			$value['pay_time'] = date('Y-m-d H:i', $value['pay_time']);
		}

		if($result){
			$last=end($result);
			$_SESSION['appraise_view']=$last['id'];
		}
		
		return $viewList;

	}
	
	public function applyAppraise($data)
	{
		try {
			DI()->notorm->beginTransaction('db_appapi');
			// 获取鉴定费用
			$expertUserData=DI()->notorm->user_expert
				->select("appraise_fee")
				->where("uid=? and apply_status='1'",$data['touid'])
				->fetchOne();
			if (!empty($expertUserData) && isset($expertUserData['appraise_fee'])) {
				$data['appraise_fee'] = $expertUserData['appraise_fee'];
				// 获取分成比例,计算专家能获得的金额
				$configpri=getConfigPri();
				$amount = $data['appraise_fee'];
				if(isset($configpri['appraise_percentage']) && $configpri['appraise_percentage']){
					// 计算专家可以获得的钱
					// $amount = round($data['appraise_fee'] * ((100-$configpri['appraise_percentage'])/100));
					$amount = floatval(sprintf("%01.2f", $data['appraise_fee'] * ((100-$configpri['appraise_percentage'])/100)));
				}
				$data['expert_fee'] = $amount;
			}else{
				throw new Exception('未找到鉴定专家信息');
			}

			if(!$insertInfo = DI()->notorm->appraise_apply->insert($data)) throw new Exception('创建订单失败');

			DI()->notorm->commit('db_appapi');
			$returnData = array(
				'order_sn' => $insertInfo['apply_order_sn'],
				'id' => $insertInfo['id'],
			);
			$result = $returnData;
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			// $message = $e->getMessage();
			throw new Exception($e->getMessage());
			$result = false;
		}
		return $result;
	}

	public function myAppraiseList($uid,$p,$other)
	{
		$configpri = getConfigPri();

        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;

        $type = $other['type'];
        $pay_status = $other['pay_status'];
        $status = $other['status'];

		if($type == 1){
		    // 专家
		    $where = "touid = ? ";
		    $whereData = array($uid);
		}else if($type == 0){
		    // 普通用户
		    $where = "uid = ? ";
		    $whereData = array($uid);
		}

		if(isset($pay_status) && $pay_status !=''){
			$where .= " and pay_status = ? ";
			$whereData[] = $pay_status;
		}

		if(isset($status) && $status !=''){
			$where .= " and status = ? ";
			$whereData[] = $status;
		}

		if($p!=1){
			$id=$_SESSION['myappraise_view'];
            if($id){
                $where.=" and id < {$id}";
            }
		}

		$list=DI()->notorm->appraise_apply
				->select("*")
				->where($where, $whereData)
				->order("id desc")
				->limit($start,$pnum)
				->fetchAll();

		foreach ($list as $key => &$value) {
			// 图片地址
			if(!empty($value['pics'])){
				$itemPics = json_decode($value['pics']);
				foreach ($itemPics as $k => $v) {
					$v=get_upload_path($v,0);
					$itemPics[$k] = $v;
				}
				$value['pics'] = $itemPics;
			}
			// 用户信息
			$item_user_info = DI()->notorm->user
					->select("user_nicename,avatar,avatar_thumb")
					->where('id=?  and user_type="2"',$value['uid'])
					->fetchOne();
			// 鉴定专家信息
			$item_expert_info = DI()->notorm->user
					->select("user_nicename,avatar,avatar_thumb")
					->where('id=?  and user_type="2"',$value['touid'])
					->fetchOne();
			if(empty($item_user_info) || empty($item_expert_info)){
				unset($list[$key]);
				continue;
			}else{
				$value['user_nicename'] = $item_user_info['user_nicename'];
				$value['avatar'] = get_upload_path($item_user_info['avatar']);
				$value['avatar_thumb'] = get_upload_path($item_user_info['avatar_thumb']);
				
				$value['expert']['user_nicename'] = $item_expert_info['user_nicename'];
				$value['expert']['avatar'] = get_upload_path($item_expert_info['avatar']);
				$value['expert']['avatar_thumb'] = get_upload_path($item_expert_info['avatar_thumb']);
				// 当前专家所属分类
				$item_user_expert_class_data = DI()->notorm->user_expert_class
						->select("assort_id")
						->where('uid=? and status="1"',$value['touid'])
						->fetchAll();
				if (!empty($item_user_expert_class_data)) {
					$item_user_expert_class_data = array_column($item_user_expert_class_data,'assort_id');
					$item_assort_class_data = DI()->notorm->shop_assort->select('id,name')->where('state=1 and id in ('.implode(',',$item_user_expert_class_data).')')->fetchAll();
				}
				$value['expert']['user_expert_class_data'] = $item_assort_class_data;
			}

			// 鉴定超时-截止时间
			$timeout_time = 0;
			if($value['pay_status'] == '1' && $value['status'] == '0'){
				$overtime_days = 0;
				if(isset($configpri['appraise_lock_days'])) $overtime_days = $configpri['appraise_lock_days'];
				// 截止时间 = 支付时间 + 超时天数*86400
				$timeout_time = $value['pay_time'] + (86400*$overtime_days);
			}
			$value['timeout_time'] = $timeout_time;

			// 添加时间格式化
			$value['add_time'] = date('Y-m-d H:i', $value['add_time']);
		}

		if($result){
			$last=end($result);
			$_SESSION['myappraise_view']=$last['id'];
		}
		
		return $list;

	}

	public function updateAppraiseViewNum($id,$uid,$old_view_num)
	{
		$where = 'id=? and uid!=? and touid!=? and view_num=?';
		$whereData = array($id,$uid,$uid,$old_view_num);
		$new_view_num = $old_view_num+1;
		$data = array('view_num'=>$new_view_num);
		$updateResult = DI()->notorm->appraise_apply
				->where($where,$whereData)
				->update($data);

		$view_num = $updateResult ? $new_view_num : $old_view_num;

		return $view_num;
	}

	public function applyAppraiseAnswerView($data)
	{
		try {
			DI()->notorm->beginTransaction('db_appapi');
			
			if(!$insertInfo = DI()->notorm->appraise_answer_view->insert($data)) throw new Exception('创建订单失败');

			DI()->notorm->commit('db_appapi');
			$returnData = array(
				'order_sn' => $insertInfo['view_order_sn'],
				'id' => $insertInfo['id'],
			);
			$result = $returnData;
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			// $message = $e->getMessage();
			throw new Exception($e->getMessage());
			$result = false;
		}
		return $result;
	}

}