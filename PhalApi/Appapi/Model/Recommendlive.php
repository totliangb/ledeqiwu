<?php
if (!session_id()) session_start();
class Model_Recommendlive extends PhalApi_Model_NotORM {
    protected $live_fields='uid,title,city,stream,pull,thumb,isvideo,type,type_val,goodnum,anyway,starttime,isshop,game_action';
     
    
	/* 轮播 */
	public function getRecommendSlide(){

		$rs=DI()->notorm->slide_item
			->select("image as slide_pic,target,url as slide_url")
			->where("status='1' and slide_id='5' ")
			->order("list_order asc")
			->fetchAll();
		foreach($rs as $k=>$v){
			$rs[$k]['slide_pic']=get_upload_path($v['slide_pic']);
		}				

		return $rs;
	}

	/* 大师直播间列表 */
    public function getLiveRoomList($p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=" islive= '1'";

		$master_users=getcaches('masterUsers');

		if(!empty($master_users)){
			$where .= " and uid IN (".implode(',', $master_users).") ";	

			if($p!=1){
				$endtime=$_SESSION['master_starttime'];
	            if($endtime){
	                $where.=" and starttime < {$endtime}";
	            }
			}

			$result=DI()->notorm->live
					->select($this->live_fields.',lng,lat')
					->where($where)
					->order("starttime desc")
					->limit(0,$pnum)
					->fetchAll();	
		
			foreach($result as $k=>$v){
	            
				$v=handleLive($v);
				
				$distance='好像在火星';
				if($v['city']){
					$distance=$v['city'];	
				}
				
				$v['distance']=$distance;
				unset($v['lng']);
				unset($v['lat']);
	            
	            $result[$k]=$v;
				
			}		
			if($result){
				$last=end($result);
				$_SESSION['master_starttime']=$last['starttime'];
			}
		}else{
    		$result = array();
		}

		return $result;
    }
}