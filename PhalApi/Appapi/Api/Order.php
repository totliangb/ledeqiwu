<?php
/**
* 订单模块
*/
class Api_Order extends PhalApi_Api
{
	public function getRules() {
		return array(
			'orderInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'cart_id' => array('name' => 'cart_id', 'type' => 'string', 'require' => true, 'desc' => '购物车商品id'),
			),
			'confirmOrder' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'cart_id' => array('name' => 'cart_id', 'type' => 'string', 'require' => true, 'desc' => '购物车商品id'),
				'coupon_id' => array('name' => 'coupon_id', 'type' => 'int','min' => 0, 'default' => 0, 'require' => true, 'desc' => '优惠券id'),
				'address_id' => array('name' => 'address_id', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '收货地址id'),
			),
			'orderList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'type' => array('name' => 'type', 'type' => 'int',  'default' => -1,'require' => true, 'desc' => '分类'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			'orderInfo2' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '订单id'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 0 , 'default' => 1,'require' => true, 'desc' => '分类'),
			),
			'PlaceAnOrder' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 0, 'require' => true, 'desc' => '订单id'),
				'coupon_id' => array('name' => 'coupon_id', 'type' => 'int','min' => 0, 'default' => 0, 'require' => true, 'desc' => '优惠券id'),
				'address_id' => array('name' => 'address_id', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '收货地址id'),
			),
			'orderLogistics' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 0, 'require' => true, 'desc' => '订单id'),
			),
			'orderEvaluation' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 0, 'require' => true, 'desc' => '订单id'),
				'rating' => array('name' => 'rating', 'type' => 'int','min' => 1, 'max' => 5,'require' => true, 'desc' => '星级'),
				'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '评价内容'),
				'img' => array('name' => 'img', 'type' => 'string', 'desc' => '评价图片'),
			),
			'ConfirmReceipt' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 0, 'require' => true, 'desc' => '订单id'),
			),
			'commentList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 0, 'require' => true, 'desc' => '订单id'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			'commentReply' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 0, 'require' => true, 'desc' => '订单id'),
				'id' => array('name' => 'id', 'type' => 'int','min' => 0, 'require' => true, 'desc' => '评价id'),
				'reply_content' => array('name' => 'reply_content', 'type' => 'string','require' => true, 'desc' => '回复内容'),
			),
			'orderInfo3' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'commodity_id' => array('name' => 'commodity_id', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '订单id'),
				'num' => array('name' => 'num', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '商品数量'),
			),
			'confirmOrder2' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'commodity_id' => array('name' => 'commodity_id', 'type' => 'int', 'require' => true, 'desc' => '商品id'),
				'coupon_id' => array('name' => 'coupon_id', 'type' => 'int','min' => 0, 'default' => 0, 'require' => true, 'desc' => '优惠券id'),
				'address_id' => array('name' => 'address_id', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '收货地址id'),
				'num' => array('name' => 'num', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '数量'),
			),
			'refund' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'desc' => '订单id'),
				'refund_info_id' => array('name' => 'refund_info_id', 'type' => 'int', 'require' => true, 'desc' => '退款理由id'),
				'info' => array('name' => 'info', 'type' => 'string', 'require' => true, 'desc' => '退款说明'),
				'img' => array('name' => 'img', 'type' => 'string', 'desc' => '图片'),
			),
			'determineRefund' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'desc' => '订单id'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 0 , 'default' => 1,'require' => true, 'desc' => '分类'),
				'refund_info_id' => array('name' => 'refund_info_id', 'type' => 'int', 'default' => 0, 'desc' => '取消退款理由id'),
				'info' => array('name' => 'info', 'type' => 'string', 'default' => '',  'desc' => '取消退款说明'),
				'address_id' => array('name' => 'address_id', 'type' => 'int', 'default' => '',  'desc' => '退款收件地址ID'),
			),
			'refund_logistic' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'desc' => '订单id'),
				'logistics_id' => array('name' => 'logistics_id', 'type' => 'int', 'require' => true, 'desc' => '快递id'),
				'logistics_num' => array('name' => 'logistics_num', 'type' => 'string','require' => true, 'desc' => '快递单号'),
			),
			'platformIntervention' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'desc' => '订单id'),
				'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '申请标题'),
				'text' => array('name' => 'text', 'type' => 'string', 'require' => true, 'desc' => '申请内容'),
				'img' => array('name' => 'img', 'type' => 'string', 'require' => true, 'desc' => '图片'),
			),
			'disputeList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			'disputeInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'dispute_id' => array('name' => 'dispute_id', 'type' => 'int','require' => true, 'desc' => '纠纷id'),
			),
			'distribution' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
			),
			'reasonForReturn' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'type' => array('name' => 'type', 'type' => 'int','require' => true, 'default'=>1, 'min'=>0, 'desc' => '分类'),
			),
			'refundReceive' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int' ,'require' => true, 'desc'=> '订单ID')
			),
			'deleteOrder'=>array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','require' => true, 'desc' => '订单id'),
			),
			'refundInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','require' => true, 'min'=>0, 'desc' => '订单id'),
			),
			'jpush' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','require' => true, 'min'=>0, 'desc' => '订单id')
			)
		);
	}

	//退款退货详情
	public function refundInfo()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$refund = DI()->notorm->shop_refund->where('order_id = ?', $this->order_id)->fetchOne();
		$order = DI()->notorm->shop_order->select('shop_id,state,payment_amount,discount')->where('id = ?', $this->order_id)->fetchOne();
        
        
		if(empty($order['discount'])) $order['discount'] = '0';

		if(!$refund) return ['code'=>1000, 'msg' => '退款退货信息不存在'];
		
		if($refund['uid'] != $this->uid){
		    $shop = DI()->notorm->shop->where("uid = ?",$this->uid)->fetchOne();
		    if ($shop && $shop['id'] != $refund['shop_id']){
		        return ['code'=>1000, 'msg' => '参数错误'];
		    }
		}
		$order_list = DI()->notorm->shop_order_info->select('commodity_id,title,img,num,price')->where('id = ?', $this->order_id)->fetchAll();
		foreach ($order_list as $key => &$value) 
		{
			$value['price_num'] = $value['price']*$value['num'];
		}

		$refund['addtime'] = date('Y-m-d H:i:s', $refund['addtime']);

		$refund['img'] = empty($refund['img']) ? [] : explode(',', $refund['img']);
		foreach ($refund['img'] as &$value){
		    $value = get_upload_path($value,0);
		}
		
		$refund['logistics_name'] = DI()->notorm->shop_logistics->where("id = ?",$refund['logistics_id'])->fetchOne()["name"];

		$refund_info_id = [$refund['refund_info_id']];

		if(2 == $refund['state']) $refund_info_id[] = $refund['refund_info_id2'];

		$refund_info = DI()->notorm->shop_refund_info->select('id,title')->where('id in ('.implode(',', $refund_info_id).')')->limit(count($refund_info_id))->fetchAll();
		$refund_info = replaceArrayKey($refund_info);

		$refund['refund_info_name'] = $refund_info[$refund['refund_info_id']]['title'];

		$refund['refund_info_name2'] = (2 == $refund['state']) ? $refund_info[$refund['refund_info_id2']]['title'] : '';

		if(empty($refund['refund_info_id2'])) $refund['refund_info_id2'] = '';

		$refund['check_time'] = empty($refund['check_time']) ? '' : date('Y-m-d H:i:s', $refund['check_time']);

		if(empty($refund['info2'])) $refund['info2'] = '';

		return ['code' => 0, 'msg' => '', 'info' => ['order' => $order, 'refund' => $refund, 'order_list' => $order_list]];
	}

	//删除订单
	public function deleteOrder()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!DI()->notorm->shop_order->where('state < 20 and uid = ? and id = ?', $this->uid, $this->order_id)->update(['deletetime' => time()]))
		{
			return ['code'=>1000, 'msg' => '删除订单失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['删除订单成功']];
	}

	//退款退货原因列表
	public function reasonForReturn()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$data = DI()->notorm->shop_refund_info->select('id,title')->where('type = ? and state = 1', $this->type)->fetchAll();

		return ['code' => 0, 'msg' => '', 'info' => $data ? $data : []];
	}

	//分销
	public function distribution()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		//收益
		$Agent_profit = DI()->notorm->agent_profit->select('one_profit')->where('uid = ?', $this->uid)->fetchOne();

		$codeinfo = DI()->notorm->agent_code->select('code,img')->where('uid = ?', $this->uid)->fetchOne();

		$url = 'http://'.$_SERVER['HTTP_HOST'].'/index.php/Appapi/Agent/index2/uid/'.$this->uid;
		if(!$codeinfo || empty($codeinfo['code'])){
			$codeinfo['code']=$this->createCode();
			$codeinfo['img']=DI()->phpqrcode->flies($url);
			DI()->notorm->agent_code->insert(['uid'=>$this->uid,"code"=>$codeinfo['code'], 'img' => $codeinfo['img']]);
		}
		else if(empty($codeinfo['img']))
		{
			$codeinfo['img']=DI()->phpqrcode->flies($url);
			DI()->notorm->agent_code->where('uid = ?', $this->uid)->update(['img' => $codeinfo['img']]);
		}
// 		echo json_encode($codeinfo['img']);die;

		if(empty($Agent_profit['one_profit'])) $Agent_profit['one_profit'] = '0';
		if(empty($Agent_profit['two_profit'])) $Agent_profit['two_profit'] = '0';
        
        $nums = DI()->notorm->agent->where("one_uid = ".$this->uid)->count();
        if (!$nums){
            $nums = 0;
        }
        
        //分销余额
        $distribute = DI()->notorm->user->select("distribute")->where("id = ?",$this->uid)->fetchOne()['distribute'];
        $distribute_total = DI()->notorm->user_distributerecord->select("sum(distribute_total) as total")->where("one_uid = ?",$this->uid)->fetchOne();
		$data = [
			'user_nicename' => empty($users_agent['user_nicename']) ? '' : $users_agent['user_nicename'],
			'one_profit' => $Agent_profit['one_profit'],
			'distribute' => $distribute,
			'distribute_total' => number_format($distribute_total["total"]*1,2),
			'nums' => $nums,
			'code' => $codeinfo['code'],
			'img' => get_upload_path($codeinfo['img'], 0),
			'uid' => $this->uid,
			'url' => $url,
			'notice' => getConfigPri()['share_notice']
		];

		return ['code' => 0, 'msg' => '', 'info' => $data];
	}

	/* 生成邀请码 */
	private function createCode($len=6,$format='ALL2'){
        $is_abc = $is_numer = 0;
        $password = $tmp =''; 
        switch($format){
            case 'ALL':
                $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                break;
            case 'ALL2':
                $chars='ABCDEFGHJKLMNPQRSTUVWXYZ0123456789';
                break;
            case 'CHAR':
                $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                break;
            case 'NUMBER':
                $chars='0123456789';
                break;
            default :
                $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                break;
        }
        
        while(strlen($password)<$len){
            $tmp =substr($chars,(mt_rand()%strlen($chars)),1);
            if(($is_numer <> 1 && is_numeric($tmp) && $tmp > 0 )|| $format == 'CHAR'){
                $is_numer = 1;
            }
            if(($is_abc <> 1 && preg_match('/[a-zA-Z]/',$tmp)) || $format == 'NUMBER'){
                $is_abc = 1;
            }
            $password.= $tmp;
        }
        if($is_numer <> 1 || $is_abc <> 1 || empty($password) ){
            $password = $this->createCode($len,$format);
        }
        if($password!=''){
            
            $oneinfo=DI()->notorm->agent_code->select('uid')->where('code = ?', $password)->fetchOne();
            if(!$oneinfo){
                return $password;
            }            
        }
        $password = $this->createCode($len,$format);
        return $password;
    }

	//申请平台介入详情
	public function disputeInfo()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$dispute = DI()->notorm->shop_dispute->where('id = ? AND uid = ?', $this->dispute_id,$this->uid)->fetchOne();
        
		if(!$dispute)  return ['code'=>1000, 'msg' => '记录不存在'];

		$order = DI()->notorm->shop_order->select('id,numbering,payment_amount')->where('id=?', $dispute['order_id'])->fetchOne();
        
		$dispute['numbering'] = $order['numbering'];
		$dispute['payment_amount'] = $order['payment_amount'];
		$dispute['addtime'] = date('Y-m-d H:i:s', $dispute['addtime']);
		$dispute['img'] = json_decode($dispute['img'], true);
		if(empty($dispute['reply'])) $dispute['reply'] = '';
		$dispute['checktime'] = empty($dispute['checktime']) ? '' : date('Y-m-d H:i:s', $dispute['checktime']);

		return ['code' => 0, 'msg' => '', 'info' => $dispute];
	}

	//用户申请平台记录
	public function disputeList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if($this->p<1){
            $this->p=1;
        }
		$nums=20;
		$start=($this->p-1)*$nums;

		$list = DI()->notorm->shop_dispute->select('id,order_id,title,text,addtime,state')->where('uid = ?', $this->uid)->limit($start,$nums)->fetchAll();

		if($list)
		{
			$order_id = [];
			foreach ($list as $key => &$value) 
			{
				$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
				$order_id[] = $value['order_id'];
			}

			$order = DI()->notorm->shop_order->select('id,numbering,payment_amount')->where('id in ('.implode(',', $order_id).')')->limit(count($order_id))->fetchAll();
			$order = replaceArrayKey($order);

			foreach ($list as $key => &$v) 
			{
				if(isset($order[$v['order_id']]))
				{
					$v['numbering'] = $order[$v['order_id']]['numbering'];
					$v['payment_amount'] = $order[$v['order_id']]['payment_amount'];
				}
			}
		}

		return ['code' => 0, 'msg' => '', 'info' => $list];
	}

	//订单纠纷，申请平台介入
	public function platformIntervention()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(mb_strlen($this->title) > 100 || mb_strlen($this->title) <= 0) return ['code'=>1000, 'msg' => '申请标题字数应在1~100之间'];

		if(mb_strlen($this->text) > 500 || mb_strlen($this->text) <= 0) return ['code'=>1001, 'msg' => '申请内容字数应在1~500之间'];

		if(mb_strlen($this->img) > 500 || mb_strlen($this->img) <= 0) return ['code'=>1002, 'msg' => '图片异常'];

		$this->img = json_encode(explode(',', $this->img));

        $order = DI()->notorm->shop_order->where('state >= 20 and id=?', $this->order_id)->fetchOne();
		if(!$order) return ['code'=>1000, 'msg' => '订单不存在'];

		$add = [
			'uid' => $this->uid,
			'order_id' => $this->order_id,
			'shop_id' => $order['shop_id'],
			'title' => $this->title,
			'text' => $this->text,
			'img' => $this->img,
			'addtime' => time()
		];
        
        // $dispute = DI()->notorm->shop_dispute->where("order_id = ?",$this->order_id)->fetchOne();
        // if ($dispute){
        //     $r = DI()->notorm->shop_dispute->where("order_id = ?",$this->order_id)->update($add);
        // }else{
            $r = DI()->notorm->shop_dispute->insert($add);
        // }
        
		if(!$r) return ['code'=>1000, 'msg' => '申请失败'];

		return ['code' => 0, 'msg' => '', 'info' => ['申请成功']];
	}

	//确认、取消退款
	public function determineRefund()
	{

		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$order = DI()->notorm->shop_order->where('state =40 and id=?', $this->order_id)->fetchOne();
		if(!$order) return ['code'=>1000, 'msg' => '订单状态不正确或订单不存在'];

		$shop = DI()->notorm->shop->where('id=?', $order['shop_id'])->fetchOne();

		if(!$shop) return ['code'=>1001, 'msg' => '店铺不存在'];

		$shop_refund = DI()->notorm->shop_refund->where('order_id=? and state = 0', $order['id'])->fetchOne();

		if(!$shop_refund) return ['code'=>1002, 'msg' => '退货信息已处理'];

		//1退款 0取消
		if($this->type)
		{
			try {
				DI()->notorm->beginTransaction('db_appapi');
				
				if($shop_refund['order_state'] == 30){
				    $state = 42;    //等待买家退货
				    $address = $this->address_id;
                    if ($address) {
                        $address_info = DI()->notorm->shop_address->where("id = ?",$address)->fetchOne();
                        $updaterefund = DI()->notorm->shop_refund->where('order_id =? and state=0', $order['id'])->update(['state'=>1,'check_time'=>time(),'address'=>$address_info['title'],'tel'=>$address_info['phone'],'name'=>$address_info['name']]);
                    }else{
                        $updaterefund = 0;
                    }
				}elseif($shop_refund['order_state'] == 20){
				    $state = 41;    //直接退款
				    if(!DI()->notorm->shop->where('id=?', $order['shop_id'])->update(['balance_freeze' => new NotORM_Literal("balance_freeze - ".$order['payment_amount'])])) throw new Exception('退款失败');
				    $updaterefund = DI()->notorm->shop_refund->where('order_id =? and state=0', $order['id'])->update(['state'=>1,'check_time'=>time()]);
				}
				
				if(!$updaterefund) throw new Exception('退款失败');
                
				if(!DI()->notorm->shop_order->where('id=? and shop_id=? and deletetime = 0', $this->order_id, $order['shop_id'])->update(['state' => $state])) throw new Exception('退款失败');

				if(!DI()->notorm->shop_order_log->insert([
					'uid' => $this->uid,
					'order_id' => $this->order_id,
					'type' => 1,
					'addtime' => time(),
					'text' => '商家确认退款'
				])) throw new Exception('退款失败');

				if(!DI()->notorm->shop_amount_log->insert([
					'shop_id' => $order['shop_id'],
					'order_id' => $order['id'],
					'type' => 0,
					'amount' => $order['payment_amount'],
					'type2' => 0,
					'addtime' => time(),
					'text' => '商家确认退款'
				])) throw new Exception("确认收货失败");
                
                if($state == 41){
                    if ($order['is_pay_type'] == 1){
                        if($this->wxRefund($order) != 1)  throw new Exception("退款失败");
                    }else{
                        if(!DI()->Alipay->setCofig()->TradeRefundRequest($order))  throw new Exception("退款失败");
                    }
                    jpushNotice($order['uid'],"订单号:".$order['numbering']."订单商家同意退款");
                }else{
                    jpushNotice($order['uid'],"订单号:".$order['numbering']."订单商家同意退货，请及时发货");
                }
                

				DI()->notorm->commit('db_appapi');
			} catch (Exception $e) {
				DI()->notorm->rollback('db_appapi');
				return ['code' => 1008, 'msg' => '退款失败'];
			}

			return ['code' => 0, 'msg' => '', 'info' => ['退款成功']];
		}

		if(empty($this->refund_info_id) || empty($this->info)) return ['code' => 1003, 'msg' => '参数错误'];

		if(!DI()->notorm->shop_refund_info->select('id')->where('id=? and state = 1 and type=0', $this->refund_info_id)->fetchOne())  return ['code' => 1004, 'msg' => '参数错误'];
		
		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->shop_order->where('id=? and shop_id=?', $this->order_id, $order['shop_id'])->update(['state' => $shop_refund['order_state']])) throw new Exception('取消退款失败');

			if(!DI()->notorm->shop_order_log->insert([
				'uid' => $this->uid,
				'order_id' => $this->order_id,
				'type' => 2,
				'addtime' => time(),
				'text' => '商家取消退款'
			])) throw new Exception('取消退款失败');

			if(!DI()->notorm->shop_refund->where('order_id =? and state=0', $order['id'])->update([
				'state'=>2,
				'check_time'=>time(),
				'refund_info_id2' => intval($this->refund_info_id),
				'info2' => htmlspecialchars($this->info)
			])) throw new Exception('取消退款失败');
			
			jpushNotice($order['uid'],"订单号:".$order['numbering']."订单商家取消退款");

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');

			return ['code' => 1008, 'msg' => '取消退款失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['取消退款成功']];
	}
	
	//退货快递
	public function refund_logistic(){
	    $checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		
		$info = DI()->notorm->shop_refund->where('order_id = ? AND state = 1 AND uid = ?', $this->order_id,$this->uid)->fetchOne();
		if(!$info)
		{
			return ['code'=>1001, 'msg' => '未查询到该退款订单'];
		}
		
		$shop = DI()->notorm->shop->where("id = ?",$info['shop_id'])->fetchOne();
		$order = DI()->notorm->shop_order->where("id = ?",$this->order_id)->fetchOne();
		
		$logistics_id = $this->logistics_id;
		$logistics_num = $this->logistics_num;
		
		DI()->notorm->beginTransaction('db_appapi');
		$update = DI()->notorm->shop_refund->where('order_id = ? AND state = 1',$this->order_id)->update(array("logistics_id" => $logistics_id,"logistics_num" => $logistics_num));
		$update2 = DI()->notorm->shop_order->where("id = ?",$this->order_id)->update(array("state"=>43));   //卖家等待收货
		
		
		if($update && $update2){
		    jpushNotice($shop['uid'],"订单号:".$order['numbering']."买家已退货，请及时查收");
		    
		    DI()->notorm->commit('db_appapi');
		    return ['code' => 0, 'msg' => "提交成功"];
		}else{
		    DI()->notorm->rollback('db_appapi');
		    return ['code' => 1008, 'msg' => "提交失败"];
		}
	}
	
	//商家收到退款
	public function refundReceive(){
	    $checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		
		$shop = DI()->notorm->shop->where("uid = ?",$this->uid)->fetchOne();
		
		$info = DI()->notorm->shop_refund->where('order_id = ? AND state = 1 AND shop_id = ?', $this->order_id,$shop['id'])->fetchOne();
		if(!$info)
		{
			return ['code'=>1001, 'msg' => '未查询到该退款订单'];
		}
		
		$order = DI()->notorm->shop_order->where("id = ? AND state = 43",$this->order_id)->fetchOne();
		if(!$order)
		{
			return ['code'=>1001, 'msg' => '未查询到该退款订单'];
		}
		//开始操作

	    DI()->notorm->beginTransaction('db_appapi');
	    
	    $ret1 = DI()->notorm->shop->where('id=?',$order['shop_id'])->update([
			'balance_freeze' => new NotORM_Literal("balance_freeze - ".$order['payment_amount']),
		]);
		
		$ret2 = DI()->notorm->shop_order->where("id = ?",$this->order_id)->update(["state"=>41]);
		
		$ret3 = DI()->notorm->shop_amount_log->insert([
		    'shop_id' => $order['shop_id'],
			'order_id' => $order['id'],
			'type' => 0,
			'amount' => $order['payment_amount'],
			'type2' => 0,
			'addtime' => time(),
			'text' => '商家确认收到退货'
		]);
		
		$ret4 = DI()->notorm->shop_order_log->insert([
			'uid' => $this->uid,
			'order_id' => $this->order_id,
			'type' => 0,
			'addtime' => time(),
			'text' => '商家确认收到退货'
		]);

		if ($ret1 && $ret2 && $ret3 && $ret4){
		    if ($order['is_pay_type'] == 1){
                $ret5 = $this->wxRefund($order);
            }else{
                $ret5 = DI()->Alipay->setCofig()->TradeRefundRequest($order);
            }
            
            if (!$ret5){
                DI()->notorm->rollback('db_appapi');
                return ['code'=>1002, 'msg' => '确认收货失败，该订单已退款'];
            }
            
            DI()->notorm->commit('db_appapi');
		    jpushNotice($order['uid'],"订单号:".$order['numbering']."卖家已收货，交易已关闭");
		    return ['code'=>0, 'msg' => '确认收货成功，订单已关闭'];
		}else{
		    DI()->notorm->rollback('db_appapi');
		    return ['code'=>1003, 'msg' => '确认收货失败'];
		}
	}

	//退款、退货
	public function refund()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!DI()->notorm->shop_refund_info->select('id')->where('id=? and type=1 and state=1', $this->refund_info_id)->fetchOne())
		{
			return ['code'=>1001, 'msg' => '退款理由错误'];
		}

		$refund['info'] = htmlspecialchars(trim($this->info));

		if(mb_strlen($refund['info']) > 100 || mb_strlen($refund['info']) <= 0) return ['code'=>1002, 'msg' => '退款说明字数应在1~100位之间'];

		if(!empty($this->img))
		{
			$refund['img'] = htmlspecialchars(trim($this->img));
		}


		$order = DI()->notorm->shop_order->where('state in (20,30) and id=? and uid=? and deletetime = 0', $this->order_id, $this->uid)->fetchOne();

		if(!$order) return ['code'=>1000, 'msg' => '订单状态不正确或订单不存在'];

		$refund['shop_id'] = $order['shop_id'];
		$refund['order_id'] = $this->order_id;
		$refund['uid'] = $this->uid;
		$refund['order_state'] = $order['state'];
		$refund['refund_info_id'] = $this->refund_info_id;
		$refund['addtime'] = time();

		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->shop_order->where('id=? and uid=?', $this->order_id, $this->uid)->update(['state' => 40])) throw new Exception('申请失败');

			if(!DI()->notorm->shop_refund->insert($refund)) throw new Exception("添加退款信息失败");
			

			if(!DI()->notorm->shop_order_log->insert([
				'uid' => $this->uid,
				'order_id' => $this->order_id,
				'type' => 0,
				'addtime' => time(),
				'text' => '用户发起退款申请'
			])) throw new Exception('申请失败');

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');

			return ['code' => 1008, 'msg' => $e->getMessage()];//'申请失败'
		}

		return ['code' => 0, 'msg' => '', 'info' => ['申请成功']];
	}

	//立即支付确认订单
	public function confirmOrder2()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!DI()->notorm->shop_address->select('id')->where('id=? and uid = ?', $this->address_id, $this->uid)->fetchOne()) return ['code'=>1000, 'msg' => '收货地址不存在'];


		//获取商品信息
		$commodity_info = DI()->notorm->shop_commodity->select('id,img,shop_id,is_logistic,price,amount,title')->where('id=? and state = 1', $this->commodity_id)->fetchOne();

		if(!$commodity_info) return ['code'=>1002, 'msg' => '您选择的商品已下架'];

		if($commodity_info['amount'] < $this->num) return ['code'=>1003, 'msg' => '您选择的商品库存不足'];
		$TotalPrice = $this->num*$commodity_info['price'];
		
		//快递费用
		$is_logistic = $commodity_info['is_logistic'];

		//获取店铺信息
		$shop_info = DI()->notorm->shop->select('id')->where('id=? and state = 1', $commodity_info['shop_id'])->fetchOne();

		if(!$shop_info) return ['code'=>1004, 'msg' => '您选择商品的店铺已关闭'];

		//添加订单表数据
		$pay = [
			'uid' => $this->uid,
			'numbering' => $this->trade_no(),
			'original_price' => $TotalPrice,
			'addtime' => time(),
			'address' => $this->address_id,
			'discount' => 0,
			'payment_amount' => $TotalPrice
		];

		//优惠券
		if($this->coupon_id)
		{
			$coupon_receive = DI()->notorm->shop_coupon_receive->select('id,shop_id,coupon_id')
				->where('id=? and uid = ? and state=0', $this->coupon_id, $this->uid)->fetchOne();
			if(!$coupon_receive)  return ['code'=>1001, 'msg' => '优惠券不存在或已使用'];

			$coupon = DI()->notorm->shop_coupon->select('discounted_price,use_amount,start_time,end_time')
				->where('id=? and state =1', $coupon_receive['coupon_id'])->fetchOne();
			if(!$coupon)  return ['code'=>1002, 'msg' => '优惠券不存在或已使用'];

			if($coupon['start_time'] > time() || $coupon['end_time'] < time()) return ['code'=>1002, 'msg' => '优惠券不在使用期'];

			if($TotalPrice < $coupon['use_amount']) return ['code'=>1002, 'msg' => '优惠券未到使用金额'];

			$pay['coupon_id'] = $this->coupon_id;
			$pay['discount'] = $coupon['discounted_price'];
			$pay['payment_amount'] = $pay['original_price'] - $coupon['discounted_price'];
		}

		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->shop_pay->insert($pay)) throw new Exception('生成订单失败4');
			$pay_id = DI()->notorm->shop_pay->insert_id();

			$shop = [
				'shop_id' => $commodity_info['shop_id'],
				'pay_id' => $pay_id,
				'numbering' => $pay['numbering'],
				'uid' => $this->uid,
				'addtime' => time(),
				'payment_amount' => $TotalPrice,
				'is_logistic' => $is_logistic,
				'original_price' => $TotalPrice,
				'address' => $this->address_id
			];
			if($this->coupon_id)
			{
				$shop['payment_amount'] -= $coupon['discounted_price'];
				$shop['discount'] = $coupon['discounted_price'];
				$shop['coupon_id'] = $this->coupon_id;
			}
			if(!DI()->notorm->shop_order->insert($shop)) throw new Exception('生成订单失败1');

			$order_info = [
				'uid' => $this->uid,
				'pay_id' => $pay_id,
				'shop_id' => $commodity_info['shop_id'],
				'commodity_id' => $commodity_info['id'],
				'title' => $commodity_info['title'],
				'num' => $this->num,
				'price' => $commodity_info['price'],
				'img' => get_upload_path($commodity_info['img'], 0),
				'order_id' => DI()->notorm->shop_order->insert_id()
			];

			if(!DI()->notorm->shop_order_info->insert($order_info)) throw new Exception('生成订单失败2');

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1005, 'msg' => $e->getMessage()];
			return ['code' => 1005, 'msg' => '生成订单失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['order_id' => $pay_id, 'numbering' => $pay['numbering'], 'price' => $pay['payment_amount']]];
	}

	//直接支付确认订单页面
	public function orderInfo3()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		//获取商品信息
		$commodity_info = DI()->notorm->shop_commodity->select('id,img,shop_id,is_logistic,price,amount,title')->where('id=? and state = 1', $this->commodity_id)->fetchOne();

		if(!$commodity_info) return ['code'=>1000, 'msg' => '您选择的商品已下架'];


		if($commodity_info['amount'] < $this->num) return ['code'=>1001, 'msg' => '您选择的商品有商品库存不足'];
		$commodity_info['TotalPrice'] = $commodity_info['price']*$this->num;
		$commodity_info['num'] = $this->num;
		$commodity_info['img'] = get_upload_path($commodity_info['img'], 0);

		$address = DI()->notorm->shop_address->select('id,title,name,phone,province,city,area,info,is_default')->where('uid =?', $this->uid)->order('is_default desc')->fetchOne();
		if($address)
		{
			$area = DI()->notorm->shop_area->select('coding,name')
				->where('coding in ('.implode(',', [$address['province'],$address['city'],$address['area']]).')')
				->fetchAll();
			$area = replaceArrayKey($area,'coding');
			$address['province'] = $area[$address['province']]['name'];
			$address['city'] = $area[$address['city']]['name'];
			$address['area'] = $area[$address['area']]['name'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['list' => $commodity_info, 'address'=> empty($address) ? [] : [$address]]];
	}

	//回复评价
	public function commentReply()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop_id = DI()->notorm->shop->select('id')->where('uid = ?', $this->uid)->fetchOne();

		if(!$shop_id) return ['code'=>1000, 'msg' => '店铺不存在'];

		if(!DI()->notorm->shop_evaluation->where('id=? and order_id =?',$this->id,$this->order_id)->update(['reply_content'=>htmlspecialchars($this->reply_content),
			'reply_time' => time()]))
		{
			return ['code'=>1000, 'msg' => '回复评价失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['回复评价成功']];
	}

	//评价列表
	public function commentList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if($this->p<1){
            $this->p=1;
        }
		$nums=20;
		$start=($this->p-1)*$nums;

		$list = DI()->notorm->shop_evaluation->select('id,uid,rating,content,img,reply_content,reply_time,addtime')->where('order_id = ?', $this->order_id)->limit($start,$nums)->fetchAll();

		if($list)
		{
			$uid = [];
			foreach ($list as $key => &$value) 
			{
				$value['img'] = json_decode($value['img'], true);
				foreach ($value['img'] as $k => &$v) 
				{
					$v = get_upload_path($v, 0);
				}
				$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
				$value['reply_time'] = date('Y-m-d H:i:s', $value['reply_time']);
				$uid[$value['uid']] = $value['uid'];
			}

			$user_list = DI()->notorm->user->select('id,user_nicename')->where('id in ('.implode(',', $uid).')')->fetchAll();
			$user_list = replaceArrayKey($user_list);

			foreach ($list as $key => &$value) 
			{
				$value['user_name'] = $user_list[$value['uid']]['user_nicename'];
			}
		}
		


		return ['code' => 0, 'msg' => '', 'info' => $list ? $list : []];
	}

	//确认收货
	public function ConfirmReceipt()
	{   
	    $configpri = getConfigPri();
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$order = DI()->notorm->shop_order->where('id=? and uid = ? and state = 30', $this->order_id, $this->uid)->fetchOne();
		if(!$order) return ['code'=>1000, 'msg' => '订单不存在或已收货'];
		
		$agent = DI()->notorm->agent->where("uid = ?",$this->uid)->fetchOne();

		try {
			DI()->notorm->beginTransaction('db_appapi');
            
            $platform_amount = $order['payment_amount'] * $configpri['shop_percentage'] * 0.01;
            
			if(!DI()->notorm->shop_order->where('id=? and uid = ?', $this->order_id, $this->uid)->update(['state' => 50,'platform_amount' => $platform_amount])) throw new Exception("确认收货失败");

			if(!DI()->notorm->shop->where('id=?',$order['shop_id'])->update([
				'balance_freeze' => new NotORM_Literal("balance_freeze - ".$order['payment_amount']),
				'balance' => new NotORM_Literal("balance + ".($order['payment_amount'] - $platform_amount)),    //平台抽成
			])) throw new Exception("确认收货失败");

			if(!DI()->notorm->shop_amount_log->insert_multi([[
				'shop_id' => $order['shop_id'],
				'order_id' => $order['id'],
				'type' => 1,
				'amount' => $order['payment_amount'],
				'type2' => 1,
				'addtime' => time(),
				'text' => '用户确认收货'
			],[
				'shop_id' => $order['shop_id'],
				'order_id' => $order['id'],
				'type' => 0,
				'amount' => $order['payment_amount'] - $platform_amount,
				'type2' => 0,
				'addtime' => time(),
				'text' => '用户确认收货'
			]])) {echo 0;throw new Exception("确认收货失败");}
			
			if(!DI()->notorm->shop_order_log->insert([
				'uid' => $this->uid,
				'order_id' => $order['id'],
				'addtime' => time(),
				'type' => 4,
				'text' => '用户确认收货'
			])) {echo 1;throw new Exception("确认收货失败");}
			
			if ($agent){
			    if(!DI()->notorm->user_distributerecord->insert([
    				'uid' => $this->uid,
    				'one_uid' => $agent['one_uid'],
    				'addtime' => time(),
    				'payment_total' => $order['payment_amount'],
    				'numbering' => $order['numbering'],
    				'distribute_total' => round($order['payment_amount'] * $configpri['distribut1'] * 0.01,2),
    				'source' => '订单'.$order['numbering']."获得佣金"
    			])) {echo 2;throw new Exception("确认收货失败");}
    			
    			
    			if(!DI()->notorm->user->where("id = ?",$agent['one_uid'])->update([
    				'distribute' => new NotORM_Literal("distribute + ".($order['payment_amount'] * $configpri['distribut1'] * 0.01))
    			])) {echo 3;throw new Exception("确认收货失败");}
			}

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			return ['code'=>1000, 'msg' => $e->getMessage()];
		}

		
		return ['code' => 0, 'msg' => '', 'info' => ['确认收货成功']];
	}

	//添加评价
	public function orderEvaluation()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$order = DI()->notorm->shop_order->where('id=? and uid = ? and state >= 50', $this->order_id, $this->uid)->fetchOne();
		if(!$order) return ['code'=>1000, 'msg' => '订单不存在或未签收'];

		if(mb_strlen($this->content) > 100 || mb_strlen($this->content) <= 0) return ['code'=>1000, 'msg' => '评价内容字数应在1~100位之间'];

		$evaluation = DI()->notorm->shop_evaluation->where('order_id=? and uid = ?', $this->order_id, $this->uid)->fetchOne();
		if($evaluation) //如果有数据就修改评价
		{
			$updata = [
				'rating' => $this->rating,
				'content' => htmlspecialchars($this->content)
			];
			if($this->img) $updata['img'] = json_encode(explode(',', $this->img));

			if(!DI()->notorm->shop_evaluation->where('id = ?', $evaluation['id'])->update($updata)) return ['code'=>1000, 'msg' => '评价失败'];
		}
		else
		{
			$add = [
				'shop_id' => $order['shop_id'],
				'order_id' => $this->order_id,
				'uid' => $this->uid,
				'rating' => $this->rating,
				'content' => htmlspecialchars($this->content),
				'addtime' => time()
			];
			if($this->img) $add['img'] = json_encode(explode(',', $this->img));

			if(!DI()->notorm->shop_evaluation->insert($add)) return ['code'=>1000, 'msg' => '评价失败'];

			if(!DI()->notorm->shop_order->where('id=? and uid = ? and state = 50', $this->order_id, $this->uid)->update(['state' => 60]))
			{
				return ['code'=>1000, 'msg' => '评价失败'];
			}
		}

		return ['code' => 0, 'msg' => '', 'info' => ['评价成功']];
	}

	//查看物流
	public function orderLogistics()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$order = DI()->notorm->shop_order->where('id=? and uid = ? and state >= 30 and deletetime = 0', $this->order_id, $this->uid)->fetchOne();

		if(!$order) return ['code'=>1000, 'msg' => '订单不存在或未发货'];

		$logistics = DI()->notorm->shop_logistics->where('id=? and state = 1', $order['logistics_id'])->fetchOne();

		if(!$logistics) return ['code'=>1001, 'msg' => '数据错误请联系客服'];

		$config=getConfigPri();

		$key = $config['logistics_key'];						//客户授权key
		$customer = $config['logistics_customer'];				//查询公司编号
	    $param = array (
			'com' => $logistics['nickname'],	//快递公司编码
			'num' => $order['single_number'],	//快递单号
		);
		
		//请求参数
	    $post_data = array();
	    $post_data["customer"] = $customer;
	    $post_data["param"] = json_encode($param);
	    $sign = md5($post_data["param"].$key.$post_data["customer"]);
	    $post_data["sign"] = strtoupper($sign);
		
	    $url = 'http://poll.kuaidi100.com/poll/query.do';	//实时查询请求地址
	    
		$params = "";
	    foreach ($post_data as $k=>$v) {
	        $params .= "$k=".urlencode($v)."&";		//默认UTF-8编码格式
	    }
	    $post_data = substr($params, 0, -1);
		
		//发送post请求
	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		return ['code' => 0, 'msg' => '', 'info' => [
					'nickname' => $logistics['nickname'], 
					'single_number'=>$order['single_number'],
					'name' => $logistics['name'],
					'address_info' => $order['address_info'] ? $order['address_info'] : '',
					'list' => json_decode($result, true)
				]];
	}

	//确认订单
	public function PlaceAnOrder()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!DI()->notorm->shop_address->select('id')->where('id=? and uid = ?', $this->address_id, $this->uid)->fetchOne()) return ['code'=>1000, 'msg' => '收货地址不存在'];

		$shop_order = DI()->notorm->shop_order->where('id=? and uid = ? and state = 0 and deletetime = 0', $this->order_id, $this->uid)->fetchOne();
		if(!$shop_order) return ['code'=>1001, 'msg' => '订单已支付或已取消'];

		$order = DI()->notorm->shop_order_info->select('num,shop_id,commodity_id')->where('order_id=? and uid = ?', $this->order_id, $this->uid)->fetchAll();
		if(!$order) return ['code'=>1002, 'msg' => '订单不存在'];



		$commodity_id = $shop_id = $shop_price = [];
		foreach ($order as $key => $value) 
		{
			$commodity_id[] = $value['commodity_id'];
			$shop_id[$value['shop_id']] = $value['shop_id'];
		}
        
		//获取商品并判断商品
		$commodity = DI()->notorm->shop_commodity->select('id,price,amount')->where('id in ('.implode(',', $commodity_id).') and state = 1')->fetchAll();
		
		if(count($commodity) != count($commodity_id)) return ['code'=>1002, 'msg' => '订单中有商品已经停止出售'];

		$commodity = replaceArrayKey($commodity);
        
		//判断库存及添加各个店铺的金额
		foreach ($order as $key => $value) 
		{
			if($commodity[$value['commodity_id']]['amount'] < $value['num']) return ['code'=>1002, 'msg' => '订单中有商品库存不足'];

			$shop_price[$value['shop_id']] += $value['num']*$commodity[$value['commodity_id']]['price'];
		}		

		//判断店铺
		$shop = DI()->notorm->shop->select('id')->where('id in ('.implode(',', $shop_id).') and state = 1')->fetchAll();

		if(count($shop) != count($shop_id)) return ['code'=>1002, 'msg' => '订单中有店铺已经关闭'];

		$up_data['original_price'] = array_sum($shop_price);
		$up_data['payment_amount'] = $up_data['original_price'];
		$up_data['address'] = $this->address_id;
		$up_data['numbering'] = $this->trade_no();

		//优惠券
		if($this->coupon_id)
		{
			$coupon_receive = DI()->notorm->shop_coupon_receive->select('id,shop_id,coupon_id')
				->where('id=? and uid = ? and state=0', $this->coupon_id, $this->uid)->fetchOne();
			if(!$coupon_receive)  return ['code'=>1001, 'msg' => '优惠券不存在或已使用'];

			$coupon = DI()->notorm->shop_coupon->select('discounted_price,use_amount,start_time,end_time')
				->where('id=? and state =1', $coupon_receive['coupon_id'])->fetchOne();
			if(!$coupon)  return ['code'=>1002, 'msg' => '优惠券不存在或已使用'];

			if($coupon['start_time'] > time() || $coupon['end_time'] < time()) return ['code'=>1002, 'msg' => '优惠券不在使用期'];

			if($shop_price[$coupon_receive['shop_id']] < $coupon['use_amount']) return ['code'=>1002, 'msg' => '优惠券未到使用金额'];

			$up_data['coupon_id'] = $this->coupon_id;
			$up_data['discount'] = $coupon['discounted_price'];
			$up_data['payment_amount'] = $up_data['original_price'] - $coupon['discounted_price'];
		}

		if(!DI()->notorm->shop_order->where('id=? and uid = ?', $this->order_id, $this->uid)->update($up_data)) return ['code'=>1002, 'msg' => '生成订单失败'];

		return ['code' => 0, 'msg' => '', 'info' => ['numbering' => $up_data['numbering'], 'id' => $this->order_id, 'price' => $up_data['payment_amount']]];
	}

	//获取订单详情
	public function orderInfo2()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$order = DI()->notorm->shop_order->select('id,original_price,uid,is_logistic,numbering,state,address,order_type,addtime,deliverytime')->where('deletetime = 0 and id=? and uid = ?', $this->order_id, $this->uid)->fetchOne();

		if(!$order) return ['code'=>1000, 'msg' => '订单不存在'];

		$order_info = DI()->notorm->shop_order_info->select('commodity_id,num,price,title,price')->where('order_id=? and uid = ?', $this->order_id, $this->uid)->fetchAll();

		if(!$order_info) return ['code'=>1001, 'msg' => '订单商品不存在'];

		$shop_id = $commodity_id = [];

		foreach ($order_info as $key => $value) 
		{
			$shop_id[$value['shop_id']] = $value['shop_id'];
			$commodity_id[] = $value['commodity_id'];
		}

		/*$shop_info = DI()->notorm->shop->select('id,name,shop_img,state')->where('id in ('.implode(',', $shop_id).')')->fetchAll();
		$shop_info = replaceArrayKey($shop_info);*/
		if($order['order_type'] == '1'){
			$commodity_info = DI()->notorm->shop_auction->select('id,title,img,is_logistic,is_noreasonretrun,win_price as price,length,width,height')->where("id in (".implode(',', $commodity_id).") and status = '1'")->fetchAll();
		}else{
			$commodity_info = DI()->notorm->shop_commodity->select('id,title,img,is_logistic,price,state')->where('id in ('.implode(',', $commodity_id).')')->fetchAll();
		}
		$commodity_info = replaceArrayKey($commodity_info);

		foreach ($order_info as $key => &$value) 
		{
			if(isset($commodity_info[$value['commodity_id']])) //type :1支付查看  0其他
			{
				$value['img'] = get_upload_path($commodity_info[$value['commodity_id']]['img'], 0);
				if($this->type)
				{
					$value['price'] = $commodity_info[$value['commodity_id']]['price'];
					$value['title'] = $commodity_info[$value['commodity_id']]['title'];
					$value['length'] = $commodity_info[$value['commodity_id']]['length'] ? $commodity_info[$value['commodity_id']]['length'] : '';
					$value['width'] = $commodity_info[$value['commodity_id']]['width'] ? $commodity_info[$value['commodity_id']]['width'] : '';
					$value['height'] = $commodity_info[$value['commodity_id']]['height'] ? $commodity_info[$value['commodity_id']]['height'] : '';
				}
			}
			else
			{
				$value['img'] = '';
			}
			$value['TotalPrice'] = $value['price']*$value['num'];
		}

		$address = DI()->notorm->shop_address->select('id,title,name,phone,province,city,area,info,is_default')->where('uid =? and id=?', $this->uid, $order['address'])->order('is_default desc')->fetchOne();
		if($address)
		{
			$area = DI()->notorm->shop_area->select('coding,name')
				->where('coding in ('.implode(',', [$address['province'],$address['city'],$address['area']]).')')
				->fetchAll();
			$area = replaceArrayKey($area,'coding');
			$address['province'] = $area[$address['province']]['name'];
			$address['city'] = $area[$address['city']]['name'];
			$address['area'] = $area[$address['area']]['name'];
		}

		// 自动取消订单及自动确认收货 截止时间
		$order['auto_cancel_order_time'] = $order['addtime'] + 86400; // 24小时自动取消
		$order['auto_confirm_delivery_time'] = $order['deliverytime'] ? $order['deliverytime'] + 604800 : 0; // 7天自动确认收货

		return ['code' => 0, 'msg' => '', 'info' => ['list' => $order_info, 'address'=> empty($address) ? [] : [$address], 'order' => $order]];
	}

	//订单列表
	public function orderList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		//0待支付 10取消  20支付  30已发货 40退货/退款申请 41确认退货 50已收货 60已评价
		if ($this->type == 40){
		    $where = " and state IN (40,41,42,43)";
		}else{
		    $where = (-1 == $this->type) ? '' : ' and state='.intval($this->type);
		}
		

		if($this->p<1){
            $this->p=1;
        }
		$nums=20;
		$start=($this->p-1)*$nums;

		$list = DI()->notorm->shop_order->select('id,numbering,shop_id,state,is_logistic,payment_amount,address,address_info,order_type,addtime,deliverytime')->where('deletetime = 0 and uid = ?'.$where, $this->uid)->limit($start,$nums)->order('addtime desc')->fetchAll();

		if($list)
		{
			$list = replaceArrayKey($list);
			$order_id = array_keys($list);
			$order_info = DI()->notorm->shop_order_info->select('price,order_id,title,img,num,commodity_id')->where('order_id in ('.implode(',', $order_id).') and uid=?', $this->uid)->fetchAll();

			$shop_refund = DI()->notorm->shop_refund->select('id,order_id')->where('order_id in ('.implode(',', $order_id).') and uid=?', $this->uid)->fetchAll();

			$shop_refund_count = count($shop_refund);

			if($shop_refund_count) $shop_refund = replaceArrayKey($shop_refund,'order_id');

			foreach ($order_info as $key => $value)
			{
				if(isset($list[$value['order_id']]))
				{
					$length = $width = $height = '';
					if ($list[$value['order_id']]['order_type'] == '1') {
						// 竞拍订单 获取竞拍商品 长宽高
						$item_auction_info = DI()->notorm->shop_auction
								->select("length,width,height")
								->where('id = ?',$value['commodity_id'])
								->fetchOne();
						if (!empty($item_auction_info)) {
							$length = $item_auction_info['length'];
							$width = $item_auction_info['width'];
							$height = $item_auction_info['height'];
						}
					}
					$value['length'] = $length;
					$value['width'] = $width;
					$value['height'] = $height;
					$value['img'] = get_upload_path($value['img'], 0);
					if($shop_refund_count && isset($shop_refund[$value['order_id']])) $value['is_refund'] = 1;
					$list[$value['order_id']]['shoplist'][] = $value;
				}
			}

			foreach ($list as $key => &$value) 
			{
				if(!isset($value['is_refund'])) $value['is_refund'] = 0;
				if ($value['address']){
				    $list[$key]['address_info'] = DI()->notorm->shop_address->select("uid,title,name,phone")->where("id = ?",$value['address'])->fetchOne();
				}else{
				    $list[$key]['address_info'] = [];
				}

				$value['auto_cancel_order_time'] = $value['addtime'] + 86400; // 24小时自动取消
				$value['auto_confirm_delivery_time'] = $value['deliverytime'] ? $value['deliverytime'] + 604800 : 0; // 7天自动确认收货
			}
		}
		return ['code' => 0, 'msg' => '', 'info' => array_values($list)];
	}


	//确认订单页面
	public function orderInfo()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		//获取到购物车id
		$this->cart_id = explode(',', $this->cart_id);
		$cart_id = [];
		foreach ($this->cart_id as $key => &$value) 
		{
			if(!intval($value)) return ['code'=>1000, 'msg' => '参数错误'];
			$cart_id[] = intval($value);
		}

		if(!$cart_id) return ['code'=>1000, 'msg' => '参数错误'];

		$cart = DI()->notorm->shop_car->select('id,commodity_id,shop_id,num')->where('id in ('.implode(',', $cart_id).') and uid = ?', $this->uid)->fetchAll();

		if(!$cart) return ['code'=>1001, 'msg' => '购物车中没有您选中的商品'];

		//取得商品id及店铺id
		$commodity_id = $shop_id = [];
		foreach ($cart as $key => $value) 
		{
			$commodity_id[] = $value['commodity_id'];
			$shop_id[$value['shop_id']] = $value['shop_id'];
		}

		//获取商品信息
		$commodity_info = DI()->notorm->shop_commodity->select('id,img,shop_id,price,is_logistic,amount,title')->where('id in ('.implode(',', $commodity_id).') and state = 1')->fetchAll();

		if(count($commodity_info) != count($commodity_id)) return ['code'=>1002, 'msg' => '您选择的商品当中有已下架商品'];
		$commodity_info = replaceArrayKey($commodity_info);


		foreach ($cart as $key => &$value) 
		{
			if($commodity_info[$value['commodity_id']]['amount'] < $value['num']) return ['code'=>1001, 'msg' => '您选择的商品有商品库存不足'];
			$value['price'] = $commodity_info[$value['commodity_id']]['price'];
			$value['title'] = $commodity_info[$value['commodity_id']]['title'];
			$value['TotalPrice'] = $commodity_info[$value['commodity_id']]['price']*$value['num'];
			$value['img'] = get_upload_path($commodity_info[$value['commodity_id']]['img'], 0);
		}

		$address = DI()->notorm->shop_address->select('id,title,name,phone,province,city,area,info,is_default')->where('uid =?', $this->uid)->order('is_default desc')->fetchOne();
		if($address)
		{
			$area = DI()->notorm->shop_area->select('coding,name')
				->where('coding in ('.implode(',', [$address['province'],$address['city'],$address['area']]).')')
				->fetchAll();
			$area = replaceArrayKey($area,'coding');
			$address['province'] = $area[$address['province']]['name'];
			$address['city'] = $area[$address['city']]['name'];
			$address['area'] = $area[$address['area']]['name'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['list' => $cart, 'address'=> empty($address) ? [] : [$address]]];
	}

	//确认订单
	public function confirmOrder()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		//获取到购物车id
		$this->cart_id = explode(',', $this->cart_id);
		$cart_id = [];
		foreach ($this->cart_id as $key => &$value) 
		{
			if(!intval($value)) return ['code'=>1000, 'msg' => '参数错误'];
			$cart_id[] = intval($value);
		}

		if(!$cart_id) return ['code'=>1000, 'msg' => '参数错误'];

		$cart = DI()->notorm->shop_car->select('id,commodity_id,shop_id,num')->where('id in ('.implode(',', $cart_id).') and uid = ?', $this->uid)->fetchAll();

		if(!$cart) return ['code'=>1001, 'msg' => '购物车中没有您选中的商品'];
        
        $address_info = DI()->notorm->shop_address->select('id')->where('id=? and uid = ?', $this->address_id, $this->uid)->fetchOne();
		if(!$address_info) return ['code'=>1000, 'msg' => '收货地址不存在'];

		//取得商品id及店铺id
		$commodity_id = $shop_id = $is_logistics = [];
		foreach ($cart as $key => $value) 
		{
			$commodity_id[] = $value['commodity_id'];
		}

		//获取商品信息
		$commodity_info = DI()->notorm->shop_commodity->select('id,img,shop_id,is_logistic,price,amount,title')->where('id in ('.implode(',', $commodity_id).') and state = 1')->fetchAll();

		if(count($commodity_info) != count($commodity_id)) return ['code'=>1002, 'msg' => '您选择的商品当中有已下架商品'];

		$cart = replaceArrayKey($cart, 'commodity_id');

		foreach ($commodity_info as $key => $value)
		{
			if($value['amount'] < $cart[$value['id']]['num']) return ['code'=>1003, 'msg' => '您选择的商品有商品库存不足'];
			$shop_id[$value['shop_id']] += $cart[$value['id']]['num']*$value['price'];
			$is_logistics[$value['shop_id']] = $value['is_logistic'];
		}

		//获取店铺信息
		$shop_info = DI()->notorm->shop->select('id')->where('id in ('.implode(',', array_keys($shop_id)).') and state = 1')->fetchAll();

		if(count($shop_info) != count($shop_id)) return ['code'=>1004, 'msg' => '您选择的商品有店铺已关闭'];
        
		//添加订单表数据
		$pay = [
			'uid' => $this->uid,
			'numbering' => $this->trade_no(),
			'original_price' => array_sum($shop_id),
			'addtime' => time(),
			'address' => $this->address_id,
			'address_info' => $address_info['title'],
			'discount' => 0,
			'payment_amount' => array_sum($shop_id)
		];

		//优惠券
		if($this->coupon_id)
		{
			$coupon_receive = DI()->notorm->shop_coupon_receive->select('id,shop_id,coupon_id')
				->where('id=? and uid = ? and state=0', $this->coupon_id, $this->uid)->fetchOne();
			if(!$coupon_receive)  return ['code'=>1001, 'msg' => '优惠券不存在或已使用'];

			$coupon = DI()->notorm->shop_coupon->select('discounted_price,use_amount,start_time,end_time')
				->where('id=? and state =1', $coupon_receive['coupon_id'])->fetchOne();
			if(!$coupon)  return ['code'=>1002, 'msg' => '优惠券不存在或已使用'];

			if($coupon['start_time'] > time() || $coupon['end_time'] < time()) return ['code'=>1002, 'msg' => '优惠券不在使用期'];

			if($shop_id[$coupon_receive['shop_id']] < $coupon['use_amount']) return ['code'=>1002, 'msg' => '优惠券未到使用金额'];

			$pay['coupon_id'] = $this->coupon_id;
			$pay['discount'] = $coupon['discounted_price'];
			$pay['payment_amount'] = $pay['original_price'] - $coupon['discounted_price'];
		}

		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->shop_pay->insert($pay)) throw new Exception('生成订单失败4');
			$pay_id = DI()->notorm->shop_pay->insert_id();

			$order_info = $order = [];
			foreach ($commodity_info as $key => $value)
			{
				$order_info[$value['shop_id']][] = [
					'uid' => $this->uid,
					'pay_id' => $pay_id,
					'shop_id' => $value['shop_id'],
					'commodity_id' => $value['id'],
					'title' => $value['title'],
					'num' => $cart[$value['id']]['num'],
					'price' => $value['price'],
					'img' => get_upload_path($value['img'], 0)
				];
			}

			foreach ($shop_info as $key => $value) 
			{
				$shop = [
					'shop_id' => $value['id'],
					'pay_id' => $pay_id,
					'numbering' => $pay['numbering'],
					'uid' => $this->uid,
					'addtime' => time(),
					'payment_amount' => $shop_id[$value['id']],
					'original_price' => $shop_id[$value['id']],
					'is_logistic' => $is_logistics[$value['id']],
					'address' => $this->address_id,
					'address_info' => $address_info['title']
				];
				if($this->coupon_id && $coupon_receive['shop_id'] == $value['id'])
				{
					$shop['payment_amount'] -= $coupon['discounted_price'];
					$shop['discount'] = $coupon['discounted_price'];
					$shop['coupon_id'] = $this->coupon_id;
				}

				if(!DI()->notorm->shop_order->insert($shop)) throw new Exception('生成订单失败1');

				$order_id = DI()->notorm->shop_order->insert_id();
				foreach ($order_info[$value['id']] as $k => &$v) 
				{
					$v['order_id'] = $order_id;
					if(!DI()->notorm->shop_order_info->insert($v)) throw new Exception('生成订单失败2');
				}
			}
			

			if(!DI()->notorm->shop_car->select('id,commodity_id,shop_id,num')->where('id in ('.implode(',', $cart_id).') and uid = ?', $this->uid)->delete())
			{
				throw new Exception('生成订单失败3');
			}

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1005, 'msg' => $e->getMessage()];
			return ['code' => 1005, 'msg' => '生成订单失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['order_id' => $pay_id, 'numbering' => $pay['numbering'], 'price' => $pay['payment_amount']]];
	}
	
	public function jpush(){
	    $checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		
	    $configpri=getConfigPri();
        $app_key = $configpri['jpush_key'];
        $master_secret = $configpri['jpush_secret'];
        
        if(!$app_key || !$master_secret) {
            return ['code' => 800, 'msg' => '推送设置有误'];
        }
        
        $order = DI()->notorm->shop_order->select("id,uid,numbering,shop_id,pay_id")->where("id = ? AND uid = ?",$this->order_id,$this->uid)->fetchOne();
        $shop = DI()->notorm->shop->where("id = ?",$order['shop_id'])->fetchOne();
        
        if (!$order){
            return ['code' => 1001, 'msg' => '订单状态异常'];
        }
        
        if($app_key && $master_secret ){
             
            require API_ROOT.'/../sdk/JPush/autoload.php';

            // 初始化
            $client = new \JPush\Client($app_key, $master_secret,null);
            $anthorinfo=array();

            $pushids = DI()->notorm->user_pushid->select("pushid")->where("uid = ?",$shop['uid'])->fetchAll();

            $pushids=array_column($pushids,'pushid');
            $pushids=array_filter($pushids);

            $nums=count($pushids);
            
            $apns_production=false;
            if($configpri['jpush_sandbox']){
                $apns_production=true;
            }
            $title='您有一笔订单未发货，请及时发货';
            for($i=0;$i<$nums;){
                $alias=array_slice($pushids,$i,900);
                $i+=900;
                try{
                    $result = $client->push()
                        ->setPlatform('all')
                        ->addRegistrationId($alias)
                        ->setNotificationAlert($title)
                        ->iosNotification($title, array(
                            'sound' => 'sound.caf',
                            'category' => 'jiguang',
                            'extras' => array(
                                'type' => '2',
                                'userinfo' => $anthorinfo
                            ),
                        ))
                        ->androidNotification('', array(
                            'extras' => array(
                                'type' => '2',
                                'title' => $title,
                                'userinfo' => $anthorinfo
                            ),
                        ))
                        ->options(array(
                            'sendno' => 100,
                            'time_to_live' => 0,
                            'apns_production' =>  $apns_production,
                        ))
                        ->send();
                    if($result['code']==0){
                        $issuccess=1;
                    }else{
                        $error=$result['msg'];
                    }
                } catch (Exception $e) {   
                    file_put_contents(CMF_ROOT.'data/jpush.txt',date('y-m-d h:i:s').'提交参数信息 设备名:'.json_encode($alias)."\r\n",FILE_APPEND);
                    file_put_contents(CMF_ROOT.'data/jpush.txt',date('y-m-d h:i:s').'提交参数信息:'.$e."\r\n",FILE_APPEND);
                }					
            }			
        }
        /* 极光推送 */
        
        return ["code" => 0, "msg" => "发送成功"];
	}

	//生成唯一订单号
	private function trade_no()
	{
		$time = explode(' ', microtime());
		return $time[1].intval($time[0] * 1000000).mt_rand(10000, 99999);
	}
	
	/**
	* sign拼装获取
	*/
	private function sign($param,$key){
		$sign = "";
		foreach($param as $k => $v){
			$sign .= $k."=".$v."&";
		}
		$sign .= "key=".$key;
		$sign = strtoupper(md5($sign));
		return $sign;
	
	}
	
	//微信退款
	private function wxRefund($order){
		$configpri = getConfigPri(); 
		$configpub = getConfigPub(); 

		 //配置参数检测
					
		if($configpri['wx_appid']== "" || $configpri['wx_mchid']== "" || $configpri['wx_key']== ""){
			$rs['code'] = 1002;
			$rs['msg'] = '微信未配置';
			return $rs;					 
		}
        require_once(API_ROOT."/Library/WxPay/Lite.php");
        $wxpay = new Wxpay_Lite();
        $result = $wxpay->RefundRequest($order);
        return $result;
	}
	
	/**
	* xml转为数组
	*/
	protected function xmlToArray($xmlStr){
		$msg = array(); 
		$postStr = $xmlStr; 
		$msg = (array)simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA); 
		return $msg;
	}

}