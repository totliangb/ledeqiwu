<?php
/**
 * 福利商城
 */
if (!session_id()) session_start();
class Api_FuliMarket extends PhalApi_Api {

	public function getRules() {
        return array(
            'getFuliLevelData' => array(
            ),
            'getFuliGoodsDataByLevel' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'level' => array('name' => 'level', 'type' => 'int', 'require' => true, 'desc' => '邀请等级'),
            ),
            'getFuliGoodsSharedData' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'goods_id' => array('name' => 'goods_id', 'type' => 'int', 'require' => true, 'desc' => '福利商品ID'),
            ),
            'orderConfirm' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'goods_id' => array('name' => 'goods_id', 'type' => 'string', 'require' => true, 'desc' => '福利商品id'),
            ),
            'orderCreate' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'goods_id' => array('name' => 'goods_id', 'type' => 'string', 'require' => true, 'desc' => '福利商品id'),
                'address_id' => array('name' => 'address_id', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '收货地址id'),
            ),
            'orderList' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'type' => array('name' => 'type', 'type' => 'int',  'default' => -1,'require' => true, 'desc' => '分类'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
            'confirmReceipt' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 0, 'require' => true, 'desc' => '订单id'),
            ),
            'orderLogistics' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 0, 'require' => true, 'desc' => '订单id'),
            ),
        );
    }

    /**
     * 获取福利版块邀请等级
     */
    public function getFuliLevelData()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_FuliMarket();
        $key1='fulilevel';
        $level=getcaches($key1);
        if(!$level){
            $level = $domain->getLevel();
            setcaches($key1,$level);
        }

        $rs['info'] = $level;

        return $rs;
    }

    /**
     * 获取福利商品数据根据邀请等级
     */
    public function getFuliGoodsDataByLevel()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        // 获取数据
        $domain = new Domain_FuliMarket();
        $goodsData = $domain->getFuliGoodsDataByLevel($this->level);

        if (!empty($goodsData)) {
            foreach ($goodsData as $key => $value) {
                $shareLogData = $domain->getFuliGoodsSharedData($this->uid,$value['id']);
                $value['has_share_log'] = $hasShareLog = !empty($shareLogData) ? true : false;
                $goodsData[$key] = $value;
            }
        }
        
        $rs['info'] = $goodsData;

        return $rs;
    }

    /**
     * 获取福利商品 可兑换福利商品数量
     */
    public function getFuliGoodsCount()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        // 获取弹窗商品数据
        $domain = new Domain_FuliMarket();
        $count = $domain->getFuliGoodsCount();
        
        $rs['info'] = $count;

        return $rs;
    }

    /**
     * 获取福利商品 弹窗 推荐福利商品
     */
    public function getFuliGoodsIndexData()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        // 获取弹窗商品数据
        $domain = new Domain_FuliMarket();
        $goodsData = $domain->getFuliGoodsIndexData();
        
        $rs['info'] = $goodsData;

        return $rs;
    }

    /**
     * 获取福利商品邀请记录
     */
    public function getFuliGoodsSharedData()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        $configpub = getConfigPub();

        if(!$this->goods_id){
            return ['code'=>1001, 'msg' => '信息错误'];
        }

        // 分享链接地址
        $share_url = '';
        $share_url = $configpub['site'].'/appapi/agent/fuliSharePage?uid='.$this->uid.'&gid='.$this->goods_id;

        // 获取数据
        $domain = new Domain_FuliMarket();
        $logData = $domain->getFuliGoodsSharedData($this->uid,$this->goods_id);

        if(!empty($logData)){
            foreach ($logData as $key => $value) {
                $sharedUserItemData = getUserInfo($value['shared_uid'],0);
                $value['user_nicename'] = $sharedUserItemData['user_nicename'];
                $value['avatar'] = $sharedUserItemData['avatar'];
                $value['avatar_thumb'] = $sharedUserItemData['avatar_thumb'];
                $logData[$key] = $value;
            }
        }
        
        $rs['info'][0]['list'] = $logData;
        $rs['info'][0]['share_url'] = $share_url;

        return $rs;
    }

    /**
     * 福利商品确认兑换页面
     */
    public function orderConfirm()
    {
        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        // 福利商品ID
        $goods_id = $this->goods_id;

        $goods_list = array();

        if(!$goods_id) return ['code'=>1000, 'msg' => '参数错误'];

        // 校验兑换的商品是否有库存
        $commodity_info = DI()->notorm->fuli_goods->select('id,goods_name,goods_price,goods_shared_num,goods_image')->where('goods_storage>0 and id = ?', $goods_id)->fetchOne();

        if(!$commodity_info) return ['code'=>1001, 'msg' => '当前商品不存在或已无库存'];

        if($commodity_info){
            // 确认当前商品邀请记录 是否已达标
            $sharedData = DI()->notorm->fuli_share_log
                        ->select("count(shared_uid) as c")
                        ->where("uid=? AND goodsid=? AND is_used = 0",$this->uid,$goods_id)
                        ->fetchOne();
            if($commodity_info['goods_shared_num'] != $sharedData['c']){
                return ['code'=>1001, 'msg' => '当前用户还未完成当前福利商品的兑换条件'];
            }
            $commodity_info['goods_image'] = get_upload_path($commodity_info['goods_image'], 0);
        }

        $address = DI()->notorm->shop_address->select('id,title,name,phone,province,city,area,info,is_default')->where('uid =?', $this->uid)->order('is_default desc')->fetchOne();
        if($address)
        {
            $area = DI()->notorm->shop_area->select('coding,name')
                ->where('coding in ('.implode(',', [$address['province'],$address['city'],$address['area']]).')')
                ->fetchAll();
            $area = replaceArrayKey($area,'coding');
            $address['province'] = $area[$address['province']]['name'];
            $address['city'] = $area[$address['city']]['name'];
            $address['area'] = $area[$address['area']]['name'];
        }

        return ['code' => 0, 'msg' => '', 'info' => ['list' => $commodity_info, 'address'=> empty($address) ? [] : [$address]]];
    }

    /**
     * 福利订单下单操作
     */
    public function orderCreate()
    {
        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        $address_info = DI()->notorm->shop_address->select('id')->where('id=? and uid = ?', $this->address_id, $this->uid)->fetchOne();
        if(!$address_info) return ['code'=>1000, 'msg' => '收货地址不存在'];

        //获取商品信息
        $commodity_info = DI()->notorm->fuli_goods->select('id,goods_name,goods_price,goods_shared_num,goods_image')->where('goods_storage>0 and id = ?', $this->goods_id)->fetchOne();

        if(!$commodity_info) return ['code'=>1002, 'msg' => '当前商品不存在或已无库存'];

        // 确认当前商品邀请记录 是否已达标
        $sharedData = DI()->notorm->fuli_share_log
                    ->select("count(shared_uid) as c")
                    ->where("uid=? AND goodsid=? AND is_used = 0",$this->uid,$this->goods_id)
                    ->fetchOne();
        if($commodity_info['goods_shared_num'] != $sharedData['c']){
            return ['code'=>1001, 'msg' => '当前用户还未完成当前福利商品的兑换条件'];
        }

        // 创建福利订单
        $domain = new Domain_FuliMarket();
        $result = $domain->orderCreate($commodity_info,$this->uid,$address_info);

        return $result;
    }

    /**
     * 福利订单 列表(用户中心)
     */
    public function orderList()
    {   
        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
        //10待发货  30已发货 35纠纷订单 40已收货 50完成
        if ($this->type == 35){
            $where = " and status IN (35)";
        }else{
            $where = intval($this->type) ? ' and status='.intval($this->type) : '';
        }
        

        if($this->p<1){
            $this->p=1;
        }
        $nums=20;
        $start=($this->p-1)*$nums;

        $list = DI()->notorm->fuli_order->select('id,order_sn,status,address,address_info,add_time,shippingtime,shippingcode')->where('uid = ?'.$where, $this->uid)->limit($start,$nums)->order('add_time desc')->fetchAll();

        if($list)
        {
            $list = replaceArrayKey($list);
            $order_id = array_keys($list);
            $order_info = DI()->notorm->fuli_ordergoods->select('orderid,goodsname,goodsimage,goodsnum,goodsid')->where('orderid in ('.implode(',', $order_id).')')->fetchAll();

            foreach ($order_info as $key => $value)
            {
                if(isset($list[$value['orderid']]))
                {
                    $value['goodsimage'] = get_upload_path($value['goodsimage'], 0);
                    $list[$value['orderid']]['shoplist'][] = $value;
                }
            }

            foreach ($list as $key => &$value) 
            {
                if ($value['address']){
                    $list[$key]['address_info'] = DI()->notorm->shop_address->select("uid,title,name,phone")->where("id = ?",$value['address'])->fetchOne();
                }else{
                    $list[$key]['address_info'] = [];
                }

                $value['auto_confirm_delivery_time'] = $value['shippingtime'] ? $value['shippingtime'] + 604800 : 0; // 7天自动确认收货
            }
        }
        return ['code' => 0, 'msg' => '', 'info' => array_values($list)];
    }

    /**
     * 福利订单 确认收货(用户中心)
     */
    public function confirmReceipt()
    {   
        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        $order = DI()->notorm->fuli_order->where('id=? and uid = ? and status = 30', $this->order_id, $this->uid)->fetchOne();
        if(!$order) return ['code'=>1000, 'msg' => '订单不存在或已收货'];

        try {
            DI()->notorm->beginTransaction('db_appapi');
            
            if(!DI()->notorm->fuli_order->where('id=? and uid = ?', $this->order_id, $this->uid)->update(['status' => 50,'finnshedtime' => time()])) throw new Exception("确认收货失败");

            DI()->notorm->commit('db_appapi');
        } catch (Exception $e) {
            DI()->notorm->rollback('db_appapi');
            return ['code'=>1000, 'msg' => $e->getMessage()];
        }

        
        return ['code' => 0, 'msg' => '', 'info' => ['确认收货成功']];
    }

    /**
     * 福利订单 查看物流(用户中心)
     */
    public function orderLogistics()
    {
        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        $order = DI()->notorm->fuli_order->where('id=? and uid = ? and status >= 30', $this->order_id, $this->uid)->fetchOne();

        if(!$order) return ['code'=>1000, 'msg' => '订单不存在或未发货'];

        $logistics = DI()->notorm->shop_logistics->where('id=? and state = 1', $order['logistics_id'])->fetchOne();

        if(!$logistics) return ['code'=>1001, 'msg' => '数据错误请联系客服'];

        $config=getConfigPri();

        $key = $config['logistics_key'];                        //客户授权key
        $customer = $config['logistics_customer'];              //查询公司编号
        $param = array (
            'com' => $logistics['nickname'],    //快递公司编码
            'num' => $order['shippingcode'],   //快递单号
        );
        
        //请求参数
        $post_data = array();
        $post_data["customer"] = $customer;
        $post_data["param"] = json_encode($param);
        $sign = md5($post_data["param"].$key.$post_data["customer"]);
        $post_data["sign"] = strtoupper($sign);
        
        $url = 'http://poll.kuaidi100.com/poll/query.do';   //实时查询请求地址
        
        $params = "";
        foreach ($post_data as $k=>$v) {
            $params .= "$k=".urlencode($v)."&";     //默认UTF-8编码格式
        }
        $post_data = substr($params, 0, -1);
        
        //发送post请求
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        return ['code' => 0, 'msg' => '', 'info' => [
                    'nickname' => $logistics['nickname'], 
                    'single_number'=>$order['shippingcode'],
                    'name' => $logistics['name'],
                    'address_info' => $order['address_info'] ? $order['address_info'] : '',
                    'list' => json_decode($result, true)
                ]];
    }

}