<?php
/**
 * 鉴宝专家
 */
if (!session_id()) session_start();
class Api_Expert extends PhalApi_Api {

	public function getRules() {
        return array(
            'application' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'realname' => array('name' => 'realname', 'type' => 'string', 'require' => true, 'desc' => '姓名'),
                'card_no' => array('name' => 'card_no', 'type' => 'string','require' => true, 'desc' => '身份证号'),
                'phone' => array('name' => 'phone', 'type' => 'string','require' => true, 'desc' => '电话'),
                'front_card_view'=>array('name' => 'front_card_view', 'type' => 'string', 'require' => true, 'desc' => '身份证正面照'),
                'back_card_view'=>array('name' => 'back_card_view', 'type' => 'string', 'require' => true, 'desc' => '身份证反面照'),
                'hand_card_view'=>array('name' => 'hand_card_view', 'type' => 'string', 'require' => true, 'desc' => '手持身份证照'),
                'hand_card_view'=>array('name' => 'hand_card_view', 'type' => 'string', 'require' => true, 'desc' => '手持身份证照'),
                'cert_view'=>array('name' => 'cert_view', 'type' => 'string', 'desc' => '证件照集合'),
                'description' => array('name' => 'description', 'type' => 'string', 'require' => true, 'desc' => '自我介绍'),
                'assort_id' => array('name' => 'assort_id', 'type' => 'string', 'require' => true, 'desc' => '分类'),
            ),
            'getApplyData' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
            ),
            'getExpertUserData' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'assort_id' => array('name' => 'assort_id', 'type' => 'string', 'require' => true, 'desc' => '分类'),
            ),
            'appraise' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'apply_id' => array('name' => 'apply_id', 'type' => 'int', 'require' => true, 'desc' => '鉴定申请ID'),
                'answer_real' => array('name' => 'answer_real', 'type' => 'string', 'require' => true, 'desc' => '鉴定结果'),
                'answer' => array('name' => 'answer', 'type' => 'string', 'require' => true, 'desc' => '鉴定答复语音'),
                'answer_length' => array('name' => 'answer_length', 'type' => 'string', 'require' => true, 'desc' => '鉴定答复语音时长'),
            ),
            'getExpertUserInfo' => array(
                'expert_uid' => array('name' => 'expert_uid', 'type' => 'int','require' => true, 'desc' => '专家用户ID'),
            ),
        );
    }


    /**
     * 申请成为专家 
     */
    public function application()
    {
        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        $data = [];

        if(isset($this->description))
        {
            if(mb_strlen($this->description) > 200) return ['code'=>1001, 'msg' => '自我介绍字数不能大于200个字符'];
            $data['description'] = $this->description;
        }
        if (empty($this->realname)) {
            return ['code'=>1002, 'msg' => '信息不全，请补充姓名'];
        }
        if (empty($this->card_no)) {
            return ['code'=>1003, 'msg' => '信息不全，请补充身份证号'];
        }
        if (empty($this->phone)) {
            return ['code'=>1003, 'msg' => '信息不全，请补充电话'];
        }
        if (empty($this->front_card_view)) {
            return ['code'=>1004, 'msg' => '信息不全，请补充身份证正面照'];
        }
        if (empty($this->back_card_view)) {
            return ['code'=>1005, 'msg' => '信息不全，请补充身份证反面照'];
        }
        if (empty($this->hand_card_view)) {
            return ['code'=>1006, 'msg' => '信息不全，请补充手持身份证照'];
        }
        if (empty($this->description)) {
            return ['code'=>1007, 'msg' => '信息不全，请补充自我介绍'];
        }
        if (empty($this->assort_id)) {
            return ['code'=>1008, 'msg' => '信息不全，请补充分类'];
        }

        $data['uid'] = checkNull($this->uid);
        $data['realname'] = checkNull($this->realname);
        $data['card_no'] = checkNull($this->card_no);
        $data['phone'] = checkNull($this->phone);
        $data['front_card_view'] = checkNull($this->front_card_view);
        $data['back_card_view'] = checkNull($this->back_card_view);
        $data['hand_card_view'] = checkNull($this->hand_card_view);
        $data['assort_id'] = json_decode($this->assort_id);
        $data['cert_view'] = $this->cert_view;

        // 校验当前Uid是否已存在
        $checkUserData = DI()->notorm->user_expert->where('uid = ?',$this->uid)->fetchOne();
        if(empty($checkUserData)){
            // 用户第一次提交申请
            $data['apply_time'] = time();
            $domain = new Domain_Expert();
            if($domain->application($data))
            {
                return ['code' => 0, 'msg' => '', 'info' => ['申请成功']];
            }
        }else{
            // 校验状态是否为未通过
            if($checkUserData['apply_status'] == '2'){
                // 执行更新数据操作
                $data['apply_status'] = '0';
                $data['apply_time'] = time();
                $data['unpass_reason'] = '';
                $domain = new Domain_Expert();
                if($domain->application_update($data))
                {
                    return ['code' => 0, 'msg' => '', 'info' => ['申请成功']];
                }
            }
        }
        

        return ['code' => 1017, 'msg' => '申请失败'];
    }

    /**
     * 申请数据 
     */
    public function getApplyData()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        // 获取专家申请数据
        $domain = new Domain_Expert();
        $applyData = $domain->getApplyData($this->uid);
        
        $rs['info'] = $applyData;

        return $rs;
    }

    /**
     * 根据分类获取专家列表 
     */
    public function getExpertUserData()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        if (empty($this->assort_id)) {
            return ['code'=>1003, 'msg' => '信息不全，请指定鉴宝分类'];
        }

        // 获取专家申请数据
        $domain = new Domain_Expert();
        $applyData = $domain->getExpertUserData($this->assort_id);
        
        $rs['info'] = $applyData;

        return $rs;
    }

    /**
     * 鉴定操作 
     */
    public function appraise()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        $data['answer_real'] = $answer_real = checkNull($this->answer_real);
        $data['answer'] = $answer = checkNull($this->answer);
        $data['answer_length'] = $answer_length = checkNull($this->answer_length);

        if (empty($this->apply_id)) {
            return ['code'=>1001, 'msg' => '信息不全，请填写需要进行鉴定的鉴定申请'];
        }else{
            // 校验订单ID是否正确
            $applyInfo = DI()->notorm->appraise_apply->where('id = ?',$this->apply_id)->fetchOne();
            if (empty($applyInfo)) {
                // 未找到鉴定申请订单信息
                return ['code'=>1002, 'msg' => '信息错误'];
            }else{
                //校验鉴定订单状态是否正确、指定鉴定人是否为当前用户
                if($applyInfo['pay_status'] == '1' && $applyInfo['status'] == '0' && $appInfo['touid'] == $this->uid)
                {
                    return ['code'=>1003, 'msg' => '信息错误'];
                }
            }
        }

        try {
            $domain = new Domain_Expert();
            $applyData = $domain->appraise($applyInfo,$data);

            $rs['info'] = $applyData;
        } catch (Exception $e) {
            $rs['code'] = 1005;
            $rs['msg'] = $e->getMessage();
        }

        return $rs;
    }

    /**
     * 获取专家详细信息
     */
    public function getExpertUserInfo()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        if (empty($this->expert_uid)) {
            return ['code'=>1001, 'msg' => '信息错误'];
        }else{

            $domain = new Domain_Expert();
            $expertUserData = $domain->getExpertUserInfo($this->expert_uid);

            $rs['info'] = $expertUserData;
        }

        return $rs;
    }

}