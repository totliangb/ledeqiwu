<?php
/**
* 通用接口
*/
class Api_Universal extends PhalApi_Api
{
	public function getRules() {
		return array(
            'getClassification' => array(),
            'shop_index' => array(),
            'announcement' => array(
            	'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'min' =>1, 'desc' => '公告id'),
            ),
            'paymentTypes' => array(
            	'version_ios' => array('name' => 'version_ios', 'type' => 'string', 'desc' => 'IOS版本号'),
            ),
            'redCodeDecode' => array(
            	'code' => array('name' => 'code', 'type' => 'string', 'require' => true,'desc' => 'IOS版本号'),
            ),
            'getUnRealUserData' => array(
            	'num' => array('name' => 'num', 'type' => 'int', 'require' => true,'desc' => '数量'),
            	'key' => array('name' => 'key', 'type' => 'string', 'require' => true,'desc' => '访问密钥'),
            ),
		);
	}

	//分类接口
	public function getClassification()
	{
		$list = DI()->notorm->shop_assort->select('id,name,img')->where('state=1')->fetchAll();
		foreach ($list as $key => &$value) 
		{
			$value['img'] = empty($value['img']) ? '' : get_upload_path($value['img'], 0);
		}

		return ['code' => 0, 'msg' => '', 'info' => $list];
	}

	//商城首页
	public function shop_index()
	{
		$assort_list = DI()->notorm->shop_assort->select('id,name,img')->where('state=1')->fetchAll();
		foreach ($assort_list as $key => &$value) 
		{
			$value['img'] = empty($value['img']) ? '' : get_upload_path($value['img'], 0);
		}

		$banner =  DI()->notorm->slide_item->select('title slide_name,image slide_pic,url slide_url')->where('status=1 and slide_id=3')->order('list_order desc')->fetchAll();

		return ['code' => 0, 'msg' => '', 'info' => ['assort' => $assort_list ? $assort_list : [], 'banner' => $banner ? $banner : []]];
	}

	//公告
	public function announcement()
	{
		$info = DI()->notorm->portal_post->select('id,post_title')->where('post_status=1 and id=?', $this->id)->fetchOne();
		if($info) $info['url'] = 'http://'.$_SERVER['HTTP_HOST'].'/portal/page/index?id='.$info['id']; 

		return ['code' => 0, 'msg' => '', 'info' => empty($info) ? [] : [$info]];
	}

	//鉴宝分类接口
	public function getAppraiseClass()
	{
		$list = DI()->notorm->appraise_assort->select('id,name,img')->where('state=1')->fetchAll();
		foreach ($list as $key => &$value) 
		{
			$value['img'] = empty($value['img']) ? '' : get_upload_path($value['img'], 0);
		}

		return ['code' => 0, 'msg' => '', 'info' => $list];
	}

	//支付方式
	public function paymentTypes()
	{
		$rs = array('code' => 0, 'msg' => '', 'info' => array());

		$version_ios=$this->version_ios;

		$configpub=getConfigPub();
		$configpri=getConfigPri();
		
		$aliapp_switch=$configpri['aliapp_switch'];
        $wx_switch=$configpri['wx_switch'];
        $aliscan_switch=$configpri['aliscan_switch'];

        /* 支付方式列表 */
        $shelves=1;
        $ios_shelves=$configpub['ios_shelves'];
        if($version_ios && $version_ios==$ios_shelves){
			$shelves=0;
		}
        
        $paylist=[];
        
        if($aliapp_switch && $shelves){
            $paylist[]=[
                'id'=>'ali',
                'name'=>'支付宝支付',
                'thumb'=>get_upload_path("/static/app/pay/ali.png"),
                'href'=>'',
            ];
        }
        
        if($wx_switch && $shelves){
            $paylist[]=[
                'id'=>'wx',
                'name'=>'微信支付',
                'thumb'=>get_upload_path("/static/app/pay/wx.png"),
                'href'=>'',
            ];
        }

        if($aliscan_switch && $shelves){
            $paylist[]=[
                'id'=>'aliscan',
                'name'=>'当面付',
                'thumb'=>get_upload_path("/static/app/pay/ali.png"),
                'href'=>get_upload_path("/appapi/aliscan/index"),
            ];
        }

        $ios_switch=$configpri['ios_switch'];
        
        if( ($ios_switch || $shelves==0) ){
            $paylist[]=[
                'id'=>'apple',
                'name'=>'苹果支付',
                'thumb'=>get_upload_path("/static/app/pay/apple.png"),
                'href'=>'',
            ];
        }

		$rs['info'][0]=$paylist;
		return $rs;
	}

	// 红包分享码解密
	public function redCodeDecode()
	{
		$rs = array('code' => 0, 'msg' => '', 'info' => array());

		$code=checkNull($this->code);

        // 解密分享码
        $codeData = LRSCDecode($code);
        $codeData = explode("_",$codeData);

        // 发起分享的用户ID
        $result['uid'] = $uid = $codeData['0'];
        $result['liveuid'] = $liveuid = $codeData['1'];
        $result['show_id'] = $show_id = $codeData['2'];
        $result['red_id'] = $red_id = $codeData['3'];
        $result['stream'] = $stream = $liveuid."_".$show_id;

        // 分享人 的 昵称 + 头像
        $shareuserinfo = getUserInfo($uid);
		$result['share_from_user']['user_nicename'] = $shareuserinfo['user_nicename'];
		$result['share_from_user']['avatar'] = $shareuserinfo['avatar'];
		$result['share_from_user']['avatar_thumb'] = $shareuserinfo['avatar_thumb'];
        // 直播间 图片+title
		$liveinfo=DI()->notorm->live
				->select("uid,title,thumb")
				->where('uid=? and showid=?',$liveuid,$show_id)
				->fetchOne();
		$result['live'] = $liveinfo;
        // 直播间 主播用的  昵称 头像
        $liveuserinfo = getUserInfo($liveuid);
		$result['live_user']['user_nicename'] = $liveuserinfo['user_nicename'];
		$result['live_user']['avatar'] = $liveuserinfo['avatar'];
		$result['live_user']['avatar_thumb'] = $liveuserinfo['avatar_thumb'];
        
        $rs['info']=$result;

        return $rs;
	}

	// 获取虚拟用户
	public function getUnRealUserData()
	{
		if( !($this->key && $this->key == 'WC34WISQUMXFTBG9') ){
			return '';
		}
		$num = $this->num;

		$unRealUserPool = array(
			array(10000,36000),
			array(39000,98224),
		);

		$his_where = " id != 1 ";

		if($unRealUserPool){
			$his_where_item = array();
			foreach ($unRealUserPool as $key => $value) {
				$his_where_item[] = "(id > ".$value[0]." AND id < ".$value[1].")";
			}
			if(!empty($his_where_item)){
				$his_where .= " AND (";
				$his_where .= implode(" OR ", $his_where_item);
				$his_where .= ")";
			}
		}


		$userData = DI()->notorm->user
						->select("id,user_login")
						->where($his_where)
						->order("rand()")
						->limit($num)
						->fetchAll();
		$model = new Model_Login();
		if (!empty($userData)) {
			foreach ($userData as $key => $value) {
				// 校验token 是否存在
				$has_token_flag = false;
				$token = "";
				$hasToken = getcaches('token_'.$value['id']);
				if($hasToken){
					// 校验token 是否存在数据
					$hasTokenInfo = getcaches($hasToken['token']);
					if($hasTokenInfo){
						$token = $hasToken['token'];
						$has_token_flag = true;
					}
				}

				if(!$has_token_flag){
					$token = md5(md5($value['id'].$value['user_login'].time()));
					$model->updateToken($value['id'],$token);

					$info=getUserInfo($value['id']);
					$info['contribution']='0';
					$info['usertype']='30';
					$info['guard_type']='0';
					$info['sign']=$info['contribution'].'.'.($info['level']+100).'1';
					setcaches($token,$info);
				}

				$value['token'] = $token;
				unset($value['user_login']);

				$userData[$key] = $value;
			}
		}

		return $userData;
	}
}