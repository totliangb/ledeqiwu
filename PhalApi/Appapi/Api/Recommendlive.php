<?php
/**
 * 大师来了
 */
class Api_Recommendlive extends PhalApi_Api {  

	public function getRules() {
		return array(
			'getLiveRoomList' => array(
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			)
		);
	}

	/**
	 * 获取轮播图
	 */
	public function getRecommendSlide()
	{
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Recommendlive();
		$key='getRecommendSlide';
		$slide=getcaches($key1);
		if(!$slide){
			$slide = $domain->getRecommendSlide();
			setcaches($key,$slide);
		}
		$rs['info'] = $slide;

		return $rs;
	}

	/**
	 * 获取直播间列表
	 */
	public function getLiveRoomList()
	{
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

		$key="getRecommend_".$this->p;
		$list=getcaches($key);
		if(!$list){
			$domain = new Domain_Recommendlive();
		    $userdomain = new Domain_User();
			$list = $domain->getLiveRoomList($this->p);
			if (!empty($list)) {
			    foreach ($list as &$v){
			        $v['bond_num'] = $userdomain->getBond($v['uid']);
			    }
			}
			setcaches($key,$list,2); 
		}

		$rs['info'] = $list;

		return $rs;
	}

}