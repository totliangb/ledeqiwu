<?php
/**
 * 充值
 */
class Api_Charge extends PhalApi_Api {

	public function getRules() {
		return array(
			'getOrder' => array( 
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'changeid' => array('name' => 'changeid', 'type' => 'string',  'require' => true, 'desc' => '充值规则ID'),
				'coin' => array('name' => 'coin', 'type' => 'string',  'require' => true, 'desc' => '钻石'),
				'money' => array('name' => 'money', 'type' => 'string', 'require' => true, 'desc' => '充值金额'),
                'pay_code' => array('name' => 'pay_code', 'type' => 'string', 'require' => true, 'desc' => '支付方式CODE'),
			),
			'getAliOrder' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'changeid' => array('name' => 'changeid', 'type' => 'int',  'require' => true, 'desc' => '充值规则ID'),
				'coin' => array('name' => 'coin', 'type' => 'string',  'require' => true, 'desc' => '钻石'),
				'money' => array('name' => 'money', 'type' => 'string', 'require' => true, 'desc' => '充值金额'),
			),
			'getWxOrder' => array( 
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'changeid' => array('name' => 'changeid', 'type' => 'string',  'require' => true, 'desc' => '充值规则ID'),
				'coin' => array('name' => 'coin', 'type' => 'string',  'require' => true, 'desc' => '钻石'),
				'money' => array('name' => 'money', 'type' => 'string', 'require' => true, 'desc' => '充值金额'),
			),
			'getIosOrder' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'changeid' => array('name' => 'changeid', 'type' => 'string',  'require' => true, 'desc' => '充值规则ID'),
				'coin' => array('name' => 'coin', 'type' => 'string',  'require' => true, 'desc' => '钻石'),
				'money' => array('name' => 'money', 'type' => 'string', 'require' => true, 'desc' => '充值金额'),
			),
			'getShopPay' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '订单id'),
				'numbering' => array('name' => 'numbering', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
                'pay_code' => array('name' => 'pay_code', 'type' => 'string', 'require' => true, 'desc' => '支付方式CODE'),
			),
			'getShopWxPay' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '订单id'),
				'numbering' => array('name' => 'numbering', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
			),
			'getShopWxPay2' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'numbering' => array('name' => 'numbering', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
			),
			'checkWxPay' => array(
				'numbering' => array('name' => 'numbering', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
			),
			'createRechargeOrder' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'money' => array('name' => 'money', 'type' => 'string', 'require' => true, 'desc' => '充值金额'),
			),
			'rechargePay' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'recharge_id' => array('name' => 'recharge_id', 'type' => 'int','min' => 1, 'require' => true, 'desc' => '订单id'),
				'recharge_sn' => array('name' => 'recharge_sn', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
                'pay_code' => array('name' => 'pay_code', 'type' => 'string', 'require' => true, 'desc' => '支付方式CODE'),
			),
		);
	}

	public function getShopWxPay2()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop_order = DI()->notorm->shop_order->where('numbering=? and uid = ? and state = 0', $this->numbering, $this->uid)->fetchAll();
		if(!$shop_order) return ['code'=>1001, 'msg' => '订单已支付或已取消'];

		$order_id = array_values(array_filter(array_column($shop_order, 'order_id')));

		$order = DI()->notorm->shop_order_info->select('num,shop_id,commodity_id')->where('order_id in (?) and uid = ?', implode(',', $order_id), $this->uid)->fetchAll();
		if(!$order) return ['code'=>1002, 'msg' => '订单不存在'];

		$shop_id = array_values(array_filter(array_column($order, 'shop_id')));
		$commodity_id = array_values(array_filter(array_column($order, 'commodity_id')));

		$shop = DI()->notorm->shop->select('id')->where('id in ('.implode(',', $shop_id).') and state = 1')->fetchAll();
		if(count($shop) != count($shop_id)) return ['code'=>1002, 'msg' => '订单中有店铺已经关闭'];

		$commodity = DI()->notorm->shop_commodity->select('id,price,amount')->where('id in ('.implode(',', $commodity_id).') and state = 1')->fetchAll();
		if(count($commodity) != count($commodity_id)) return ['code'=>1002, 'msg' => '订单中有商品已经停止出售'];

		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		$configpri = getConfigPri(); 
		$configpub = getConfigPub(); 

		 //配置参数检测
					
		if($configpri['wx_appid']== "" || $configpri['wx_mchid']== "" || $configpri['wx_key']== ""){
			$rs['code'] = 1002;
			$rs['msg'] = '微信未配置';
			return $rs;					 
		}

		$noceStr = md5(rand(100,1000).time());//获取随机字符串
		$time = time();
			
		$paramarr = array(
			"appid"       =>   $configpri['wx_appid'],
			"body"        =>    $shop_order['numbering'],
			"mch_id"      =>    $configpri['wx_mchid'],
			"nonce_str"   =>    $noceStr,
			"notify_url"  =>    $configpub['site'].'/Appapi/pay/shop_notify_wx',
			"out_trade_no"=>    $this->numbering,
			"total_fee"   =>    array_sum(array_column($shop_order, 'payment_amount')), 
			"trade_type"  =>    "APP"
		);
		$sign = $this -> sign($paramarr,$configpri['wx_key']);//生成签名
		$paramarr['sign'] = $sign;
		$paramXml = "<xml>";
		foreach($paramarr as $k => $v){
			$paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
		}
		$paramXml .= "</xml>";
			 
		$ch = curl_init ();
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
		@curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
		@curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/pay/unifiedorder");
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($ch, CURLOPT_POST, 1);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
		@$resultXmlStr = curl_exec($ch);
		if(curl_errno($ch)){
			//print curl_error($ch);
			file_put_contents('./shopwxpay.txt',date('y-m-d H:i:s').' 提交参数信息 ch:'.json_encode(curl_error($ch))."\r\n",FILE_APPEND);
		}
		curl_close($ch);

		$result2 = $this->xmlToArray($resultXmlStr);
        
        if($result2['return_code']=='FAIL'){
            $rs['code']=1005;
			$rs['msg']=$result2['return_msg'];
            return $rs;	
        }
		$time2 = time();
		$prepayid = $result2['prepay_id'];
		$sign = "";
		$noceStr = md5(rand(100,1000).time());//获取随机字符串
		$paramarr2 = array(
			"appid"     =>  $configpri['wx_appid'],
			"noncestr"  =>  $noceStr,
			"package"   =>  "Sign=WXPay",
			"partnerid" =>  $configpri['wx_mchid'],
			"prepayid"  =>  $prepayid,
			"timestamp" =>  $time2
		);
		$paramarr2["sign"] = $this -> sign($paramarr2,$configpri['wx_key']);//生成签名
		
		$rs['info'][0]=$paramarr2;
		return $rs;	
	}

	//商城支付
	public function getShopPay()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        if(empty($this->pay_code)){
            return ['code'=>1011, 'msg' => '传入参数：pay_code'];
        }
        $shop_order_data = DI()->notorm->shop_order->where('id=? and uid = ? and state = 0', $this->order_id, $this->uid)->fetchOne();

		$shop_order = DI()->notorm->shop_pay->where('id=? and uid = ? and state = 0', $this->order_id, $this->uid)->fetchOne();
		if(!$shop_order) return ['code'=>1001, 'msg' => '订单已支付或已取消'];

		if(!DI()->notorm->shop_address->select('id')->where('id=? and uid = ?', $shop_order['address'], $this->uid)->fetchOne()) return ['code'=>1000, 'msg' => '收货地址不存在'];

		$order = DI()->notorm->shop_order_info->select('num,shop_id,commodity_id')->where('pay_id=? and uid = ?', $this->order_id, $this->uid)->fetchAll();
		if(!$order) return ['code'=>1002, 'msg' => '订单不存在'];

		$commodity_id = $shop_id = $shop_price = [];
		foreach ($order as $key => $value) 
		{
			$commodity_id[] = $value['commodity_id'];
			$shop_id[$value['shop_id']] = $value['shop_id'];
		}

		if($shop_order_data['order_type'] == '1'){
			// 竞拍商品
			//获取商品并判断商品
			$commodity = DI()->notorm->shop_auction->select('id,win_price as price')->where("id in (".implode(',', $commodity_id).") and status = '1'")->fetchAll();
			foreach ($commodity as $ck => &$cv) {
				$cv['amount'] = 1;
			}
		}else{
			//获取商品并判断商品
			$commodity = DI()->notorm->shop_commodity->select('id,price,amount')->where('id in ('.implode(',', $commodity_id).') and state = 1')->fetchAll();
		}

		if(count($commodity) != count($commodity_id)) return ['code'=>1002, 'msg' => '订单中有商品已经停止出售'];

		$commodity = replaceArrayKey($commodity);
		//判断库存及添加各个店铺的金额
		foreach ($order as $key => $value) 
		{
			if($commodity[$value['commodity_id']]['amount'] < $value['num']) return ['code'=>1002, 'msg' => '订单中有商品库存不足'];

			$shop_price[$value['shop_id']] += $value['num']*$commodity[$value['commodity_id']]['price'];
		}		

		//判断店铺
		$shop = DI()->notorm->shop->select('id')->where('id in ('.implode(',', $shop_id).') and state = 1')->fetchAll();

		if(count($shop) != count($shop_id)) return ['code'=>1002, 'msg' => '订单中有店铺已经关闭'];

		$up_data['original_price'] = array_sum($shop_price);
		$up_data['payment_amount'] = $up_data['original_price'];

		//优惠券
		if($shop_order['coupon_id'])
		{
			$coupon_receive = DI()->notorm->shop_coupon_receive->select('id,shop_id,coupon_id')
				->where('id=? and uid = ? and state=0', $shop_order['coupon_id'], $this->uid)->fetchOne();
			if(!$coupon_receive)  return ['code'=>1001, 'msg' => '优惠券不存在或已使用'];

			$coupon = DI()->notorm->shop_coupon->select('discounted_price,use_amount,start_time,end_time')
				->where('id=? and state =1', $coupon_receive['coupon_id'])->fetchOne();
			if(!$coupon)  return ['code'=>1002, 'msg' => '优惠券不存在或已使用'];

			if($coupon['start_time'] > time() || $coupon['end_time'] < time()) return ['code'=>1002, 'msg' => '优惠券不在使用期'];

			if($shop_price[$coupon_receive['shop_id']] < $coupon['use_amount']) return ['code'=>1002, 'msg' => '优惠券未到使用金额'];

			$up_data['coupon_id'] = $shop_order['coupon_id'];
			$up_data['discount'] = $coupon['discounted_price'];
			$up_data['payment_amount'] = $up_data['original_price'] - $coupon['discounted_price'];
		}

		if($up_data['payment_amount'] != $shop_order['payment_amount'])
		{
			if(!DI()->notorm->shop_order->where('id=? and uid = ?', $this->order_id, $this->uid)->update($up_data)) return ['code'=>1002, 'msg' => '支付失败'];
		}

		$rs = array('code' => 0, 'msg' => '', 'info' => array());

        try {
            // 调整order数据
            if(!empty($shop_order)){
                $shop_order['order_sn'] = $shop_order['numbering'];
                $shop_order['pay_sn'] = $shop_order['numbering'];
                $shop_order['amount'] = $up_data['payment_amount'];
            }

            $params = array(
                'code' => $this->pay_code,
                'notify_url' => '/Appapi/pay/shop_notify_'.$this->pay_code,
            );
            if($this->pay_code == 'ali'){
        		$params['notify_url'] = '/Appapi/pay/notify_ali2';
            }

            // 第三方支付
            $domainPay = new Domain_Pay();
            $result = $domainPay->_api_pay($shop_order,$params);

            $rs['info'][0]=$result;
        } catch (Exception $e) {
            $rs['code'] = 1002;
            $rs['msg'] = $e->getMessage();
        }

        return $rs;
	}

	//商城微信支付
	public function getShopWxPay()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop_order = DI()->notorm->shop_pay->where('id=? and uid = ? and state = 0', $this->order_id, $this->uid)->fetchOne();
		if(!$shop_order) return ['code'=>1001, 'msg' => '订单已支付或已取消'];

		if(!DI()->notorm->shop_address->select('id')->where('id=? and uid = ?', $shop_order['address'], $this->uid)->fetchOne()) return ['code'=>1000, 'msg' => '收货地址不存在'];

		$order = DI()->notorm->shop_order_info->select('num,shop_id,commodity_id')->where('pay_id=? and uid = ?', $this->order_id, $this->uid)->fetchAll();
		if(!$order) return ['code'=>1002, 'msg' => '订单不存在'];

		$commodity_id = $shop_id = $shop_price = [];
		foreach ($order as $key => $value) 
		{
			$commodity_id[] = $value['commodity_id'];
			$shop_id[$value['shop_id']] = $value['shop_id'];
		}

		//获取商品并判断商品
		$commodity = DI()->notorm->shop_commodity->select('id,price,amount')->where('id in ('.implode(',', $commodity_id).') and state = 1')->fetchAll();

		if(count($commodity) != count($commodity_id)) return ['code'=>1002, 'msg' => '订单中有商品已经停止出售'];

		$commodity = replaceArrayKey($commodity);
		//判断库存及添加各个店铺的金额
		foreach ($order as $key => $value) 
		{
			if($commodity[$value['commodity_id']]['amount'] < $value['num']) return ['code'=>1002, 'msg' => '订单中有商品库存不足'];

			$shop_price[$value['shop_id']] += $value['num']*$commodity[$value['commodity_id']]['price'];
		}		

		//判断店铺
		$shop = DI()->notorm->shop->select('id')->where('id in ('.implode(',', $shop_id).') and state = 1')->fetchAll();

		if(count($shop) != count($shop_id)) return ['code'=>1002, 'msg' => '订单中有店铺已经关闭'];

		$up_data['original_price'] = array_sum($shop_price);
		$up_data['payment_amount'] = $up_data['original_price'];

		//优惠券
		if($shop_order['coupon_id'])
		{
			$coupon_receive = DI()->notorm->shop_coupon_receive->select('id,shop_id,coupon_id')
				->where('id=? and uid = ? and state=0', $shop_order['coupon_id'], $this->uid)->fetchOne();
			if(!$coupon_receive)  return ['code'=>1001, 'msg' => '优惠券不存在或已使用'];

			$coupon = DI()->notorm->shop_coupon->select('discounted_price,use_amount,start_time,end_time')
				->where('id=? and state =1', $coupon_receive['coupon_id'])->fetchOne();
			if(!$coupon)  return ['code'=>1002, 'msg' => '优惠券不存在或已使用'];

			if($coupon['start_time'] > time() || $coupon['end_time'] < time()) return ['code'=>1002, 'msg' => '优惠券不在使用期'];

			if($shop_price[$coupon_receive['shop_id']] < $coupon['use_amount']) return ['code'=>1002, 'msg' => '优惠券未到使用金额'];

			$up_data['coupon_id'] = $shop_order['coupon_id'];
			$up_data['discount'] = $coupon['discounted_price'];
			$up_data['payment_amount'] = $up_data['original_price'] - $coupon['discounted_price'];
		}

		if($up_data['payment_amount'] != $shop_order['payment_amount'])
		{
			if(!DI()->notorm->shop_order->where('id=? and uid = ?', $this->order_id, $this->uid)->update($up_data)) return ['code'=>1002, 'msg' => '支付失败'];
		}

		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		$configpri = getConfigPri(); 
		$configpub = getConfigPub(); 

		 //配置参数检测
					
		if($configpri['wx_appid']== "" || $configpri['wx_mchid']== "" || $configpri['wx_key']== ""){
			$rs['code'] = 1002;
			$rs['msg'] = '微信未配置';
			return $rs;					 
		}

		$noceStr = md5(rand(100,1000).time());//获取随机字符串
		$time = time();
			
		$paramarr = array(
			"appid"       =>   $configpri['wx_appid'],
			"body"        =>    $shop_order['numbering'],
			"mch_id"      =>    $configpri['wx_mchid'],
			"nonce_str"   =>    $noceStr,
			"notify_url"  =>    $configpub['site'].'/Appapi/pay/shop_notify_wx',
			"out_trade_no"=>    $this->numbering,
			"total_fee"   =>    $up_data['payment_amount']*100, 
			"trade_type"  =>    "APP"
		);
		$sign = $this -> sign($paramarr,$configpri['wx_key']);//生成签名
		$paramarr['sign'] = $sign;
		$paramXml = "<xml>";
		foreach($paramarr as $k => $v){
			$paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
		}
		$paramXml .= "</xml>";
			 
		$ch = curl_init ();
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
		@curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
		@curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/pay/unifiedorder");
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($ch, CURLOPT_POST, 1);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
		@$resultXmlStr = curl_exec($ch);
		if(curl_errno($ch)){
			//print curl_error($ch);
			file_put_contents('./shopwxpay.txt',date('y-m-d H:i:s').' 提交参数信息 ch:'.json_encode(curl_error($ch))."\r\n",FILE_APPEND);
		}
		curl_close($ch);

		$result2 = $this->xmlToArray($resultXmlStr);
        
        if($result2['return_code']=='FAIL'){
            $rs['code']=1005;
			$rs['msg']=$result2['return_msg'];
            return $rs;	
        }
		$time2 = time();
		$prepayid = $result2['prepay_id'];
		$sign = "";
		$noceStr = md5(rand(100,1000).time());//获取随机字符串
		$paramarr2 = array(
			"appid"     =>  $configpri['wx_appid'],
			"noncestr"  =>  $noceStr,
			"package"   =>  "Sign=WXPay",
			"partnerid" =>  $configpri['wx_mchid'],
			"prepayid"  =>  $prepayid,
			"timestamp" =>  $time2
		);
		$paramarr2["sign"] = $this -> sign($paramarr2,$configpri['wx_key']);//生成签名
		
		$rs['info'][0]=$paramarr2;
		return $rs;	
	}
	
	/* 获取订单号 */
	protected function getOrderid($uid){
		$orderid=$uid.'_'.date('YmdHis').rand(100,999);
		return $orderid;
	}

	/* 充值余额-创建充值订单 */
	public function createRechargeOrder() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$uid = $this->uid;
		$money = checkNull($this->money);

		$recharge_sn = $this->getOrderid($uid);
		
		if($money==0){
			$rs['code']=1002;
			$rs['msg']='信息错误';		
			return $rs;						
		}		
		$now_user_info=DI()->notorm->user
            ->select('user_nicename,available_predeposit,freeze_predeposit')
            ->where('id = ?', $uid)
            ->fetchOne();

		$orderinfo=array(
			"uid"=>$uid,
			"nickname"=>$now_user_info['user_nicename'],
			"recharge_sn"=>$recharge_sn,
			"amount"=>$money,
			"add_time"=>time()
		);

		$domain = new Domain_Charge();
		$info = $domain->createRechargeOrder($orderinfo);
		if(!$info){
			$rs['code']=1001;
			$rs['msg']='订单生成失败';
            return $rs;	
		}

		$rs['info'] = $info;

		return $rs;
	}

	/* 充值余额-支付充值订单 */
	public function rechargePay() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        if(empty($this->pay_code)){
            return ['code'=>1011, 'msg' => '传入参数：pay_code'];
        }

		$uid=$this->uid;
		$recharge_id=$this->recharge_id;
		$recharge_sn=$this->recharge_sn;

		// 查询订单
		$order = DI()->notorm->pd_recharge->where("id=? and uid = ? and recharge_sn = ? and payment_state = '0'", $recharge_id, $uid,$recharge_sn)->fetchOne();
        if(!$order) return ['code'=>1001, 'msg' => '订单已支付或已取消'];

        try {
            // 调整order数据
            if(!empty($order)){
                $info['order_sn'] = "充值".$order['amount']."金额";
                $info['pay_sn'] = $order['recharge_sn'];
                $info['amount'] = $order['amount'];
            }

            $params = array(
                'code' => $this->pay_code,
                'notify_url' => '/Appapi/pay/pd_notify_'.$this->pay_code,
            );

            // 第三方支付
            $domainPay = new Domain_Pay();
            $result = $domainPay->_api_pay($info,$params);

            $rs['info'][0]=$result;
        } catch (Exception $e) {
            $rs['code'] = 1002;
            $rs['msg'] = $e->getMessage();
        }
		
		return $rs;
	}

	/* 充值钻石-获取订单号-发起支付 */
	public function getOrder() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        if(empty($this->pay_code)){
            return ['code'=>1011, 'msg' => '传入参数：pay_code'];
        }

		$uid=$this->uid;
		$changeid=$this->changeid;
		$coin=checkNull($this->coin);
		$money=checkNull($this->money);

		$orderid=$this->getOrderid($uid);
		switch ($this->pay_code) {
			case 'wx':
				$type=2;
				break;
			case 'ali':
				$type=1;
				break;
		}
		
		if($coin==0){
			$rs['code']=1002;
			$rs['msg']='信息错误';		
			return $rs;						
		}		
		
		$orderinfo=array(
			"uid"=>$uid,
			"touid"=>$uid,
			"money"=>$money,
			"coin"=>$coin,
			"orderno"=>$orderid,
			"type"=>$type,
			"status"=>0,
			"addtime"=>time()
		);

		
		$domain = new Domain_Charge();
		$info = $domain->getOrderId($changeid,$orderinfo);
		if($info==1003){
			$rs['code']=1003;
			$rs['msg']='订单信息有误，请重新提交';
            return $rs;	
		}else if(!$info){
			$rs['code']=1001;
			$rs['msg']='订单生成失败';
            return $rs;	
		}

        try {
            // 调整order数据
            if(!empty($info)){
                $info['order_sn'] = "充值{$coin}虚拟币";
                $info['pay_sn'] = $orderid;
                $info['amount'] = $money;
            }

            $params = array(
                'code' => $this->pay_code,
                'notify_url' => '/Appapi/pay/notify_'.$this->pay_code,
            );

            // 第三方支付
            $domainPay = new Domain_Pay();
            $result = $domainPay->_api_pay($info,$params);

            $rs['info'][0]=$result;
        } catch (Exception $e) {
            $rs['code'] = 1002;
            $rs['msg'] = $e->getMessage();
        }
		
		return $rs;
	}

	/**
	 * 微信支付
	 * @desc 用于 微信支付 获取订单号
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0] 支付信息
	 * @return string msg 提示信息
	 */
	public function getWxOrder() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$changeid=$this->changeid;
		$coin=checkNull($this->coin);
		$money=checkNull($this->money);

		$orderid=$this->getOrderid($uid);
		$type=2;
		
		if($coin==0){
			$rs['code']=1002;
			$rs['msg']='信息错误';		
			return $rs;						
		}					
		
		$configpri = getConfigPri(); 
		$configpub = getConfigPub(); 

		 //配置参数检测
					
		if($configpri['wx_appid']== "" || $configpri['wx_mchid']== "" || $configpri['wx_key']== ""){
			$rs['code'] = 1002;
			$rs['msg'] = '微信未配置';
			return $rs;					 
		}
		
		$orderinfo=array(
			"uid"=>$uid,
			"touid"=>$uid,
			"money"=>$money,
			"coin"=>$coin,
			"orderno"=>$orderid,
			"type"=>$type,
			"status"=>0,
			"addtime"=>time()
		);

		
		$domain = new Domain_Charge();
		$info = $domain->getOrderId($changeid,$orderinfo);
		if($info==1003){
			$rs['code']=1003;
			$rs['msg']='订单信息有误，请重新提交';
            return $rs;	
		}else if(!$info){
			$rs['code']=1001;
			$rs['msg']='订单生成失败';
            return $rs;	
		}

			 
		$noceStr = md5(rand(100,1000).time());//获取随机字符串
		$time = time();
			
		$paramarr = array(
			"appid"       =>   $configpri['wx_appid'],
			"body"        =>    "充值{$coin}虚拟币",
			"mch_id"      =>    $configpri['wx_mchid'],
			"nonce_str"   =>    $noceStr,
			"notify_url"  =>    $configpub['site'].'/Appapi/pay/notify_wx',
			"out_trade_no"=>    $orderid,
			"total_fee"   =>    $money*100, 
			"trade_type"  =>    "APP"
		);
		$sign = $this -> sign($paramarr,$configpri['wx_key']);//生成签名
		$paramarr['sign'] = $sign;
		$paramXml = "<xml>";
		foreach($paramarr as $k => $v){
			$paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
		}
		$paramXml .= "</xml>";
			 
		$ch = curl_init ();
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
		@curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
		@curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/pay/unifiedorder");
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($ch, CURLOPT_POST, 1);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
		@$resultXmlStr = curl_exec($ch);
		if(curl_errno($ch)){
			//print curl_error($ch);
			file_put_contents('./wxpay.txt',date('y-m-d H:i:s').' 提交参数信息 ch:'.json_encode(curl_error($ch))."\r\n",FILE_APPEND);
		}
		curl_close($ch);

		$result2 = $this->xmlToArray($resultXmlStr);
        
        if($result2['return_code']=='FAIL'){
            $rs['code']=1005;
			$rs['msg']=$result2['return_msg'];
            return $rs;	
        }
		$time2 = time();
		$prepayid = $result2['prepay_id'];
		$sign = "";
		$noceStr = md5(rand(100,1000).time());//获取随机字符串
		$paramarr2 = array(
			"appid"     =>  $configpri['wx_appid'],
			"noncestr"  =>  $noceStr,
			"package"   =>  "Sign=WXPay",
			"partnerid" =>  $configpri['wx_mchid'],
			"prepayid"  =>  $prepayid,
			"timestamp" =>  $time2
		);
		$paramarr2["sign"] = $this -> sign($paramarr2,$configpri['wx_key']);//生成签名
		
		$rs['info'][0]=$paramarr2;
		return $rs;
	}		
	
	/**
	* sign拼装获取
	*/
	protected function sign($param,$key){
		$sign = "";
		foreach($param as $k => $v){
			$sign .= $k."=".$v."&";
		}
		$sign .= "key=".$key;
		$sign = strtoupper(md5($sign));
		return $sign;
	
	}
	/**
	* xml转为数组
	*/
	protected function xmlToArray($xmlStr){
		$msg = array(); 
		$postStr = $xmlStr; 
		$msg = (array)simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA); 
		return $msg;
	}	
		
	/**
	 * 支付宝支付
	 * @desc 用于支付宝支付 获取订单号
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].orderid 订单号
	 * @return string msg 提示信息
	 */
	public function getAliOrder() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$changeid=$this->changeid;
		$coin=checkNull($this->coin);
		$money=checkNull($this->money);
		
		$orderid=$this->getOrderid($uid);
		$type=1;
		
		if($coin==0){
			$rs['code']=1002;
			$rs['msg']='信息错误';		
			return $rs;						
		}	
		
		$orderinfo=array(
			"uid"=>$uid,
			"touid"=>$uid,
			"money"=>$money,
			"coin"=>$coin,
			"orderno"=>$orderid,
			"type"=>$type,
			"status"=>0,
			"addtime"=>time()
		);
		
		$domain = new Domain_Charge();
		$info = $domain->getOrderId($changeid,$orderinfo);
		if($info==1003){
			$rs['code']=1003;
			$rs['msg']='订单信息有误，请重新提交';
		}else if(!$info){
			$rs['code']=1001;
			$rs['msg']='订单生成失败';
		}
		
		$rs['info'][0]['orderid']=$orderid;
		return $rs;
	}		

	/**
	 * 苹果支付
	 * @desc 用于苹果支付 获取订单号
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].orderid 订单号
	 * @return string msg 提示信息
	 */
	public function getIosOrder() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$changeid=$this->changeid;
		$coin=checkNull($this->coin);
		$money=checkNull($this->money);
		
		$orderid=$this->getOrderid($uid);
		$type=3;
		
		if($coin==0){
			$rs['code']=1002;
			$rs['msg']='信息错误';		
			return $rs;						
		}

		$configpri = getConfigPri(); 
		
		$orderinfo=array(
			"uid"=>$uid,
			"touid"=>$uid,
			"money"=>$money,
			"coin"=>$coin,
			"orderno"=>$orderid,
			"type"=>$type,
			"status"=>0,
			"addtime"=>time()
		);
		
		$domain = new Domain_Charge();
		$info = $domain->getOrderId($changeid,$orderinfo);
		if($info==1003){
			$rs['code']=1003;
			$rs['msg']='订单信息有误，请重新提交';
		}else if(!$info){
			$rs['code']=1001;
			$rs['msg']='订单生成失败';
		}

		$rs['info'][0]['orderid']=$orderid;
		return $rs;
	}		
	
    public function checkWxPay(){
        $configpri = getConfigPri();
        $noceStr = md5(rand(100,1000).time());
        
        $paramarr = array(
			"appid"       =>   $configpri['wx_appid'],
			"mch_id"      =>    $configpri['wx_mchid'],
			"nonce_str"   =>    $noceStr,
			"out_trade_no" =>   $this->numbering,
		);
		$sign = $this -> sign($paramarr,$configpri['wx_key']);//生成签名
		$paramarr['sign'] = $sign;
		$paramXml = "<xml>";
		foreach($paramarr as $k => $v){
			$paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
		}
		$paramXml .= "</xml>";
			 
		$ch = curl_init ();
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
		@curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
		@curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/pay/orderquery");
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($ch, CURLOPT_POST, 1);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
		@$resultXmlStr = curl_exec($ch);
// 		if(curl_errno($ch)){
// 			//print curl_error($ch);
// 			file_put_contents('./wxpay.txt',date('y-m-d H:i:s').' 提交参数信息 ch:'.json_encode(curl_error($ch))."\r\n",FILE_APPEND);
// 		}
		curl_close($ch);

		$result2 = $this->xmlToArray($resultXmlStr);
		echo json_encode($result2);die;
    }

}
