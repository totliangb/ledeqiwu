<?php
/**
* 购物车
*/
class Api_Shoppingcart extends PhalApi_Api
{
	public function getRules() {
		return array(
            'addShoppingCart' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'commodity_id' => array('name' => 'commodity_id', 'min' => 1, 'type' => 'int','require' => true, 'desc' => '商品id'),
				'num' => array('name' => 'num', 'type' => 'int', 'min' => 1,'require' => true, 'desc' => '商品数量'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 0 , 'default' => 1,'require' => true, 'desc' => '分类'),
			),
			'delShoppingCart' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'cart_id' => array('name' => 'cart_id', 'type' => 'int', 'min' => 1,'require' => true, 'desc' => '商品id'),
			),
			'ShoppingCartList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token')
			),
		);
	}



	//购物车列表
	public function ShoppingCartList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$list = DI()->notorm->shop_car->select('id,commodity_id,shop_id,num')->where('uid=?', $this->uid)->fetchAll();

		if(!$list) return ['code' => 0, 'msg' => '', 'info' => []];
		$shop_id = $commodity_id = [];

		foreach ($list as $key => $value) 
		{
			$commodity_id[] = $value['commodity_id'];
		}

		//读取商品名称
		$commodity_list = DI()->notorm->shop_commodity->select('id commodity_id,title,description,img,original_price,price,state')->where('id in ('.implode(',', $commodity_id).') and state = 1')->fetchAll();
		$commodity_list = replaceArrayKey($commodity_list, 'commodity_id');
		//组合商品名称及购物车信息
		foreach ($list as $key => $value) 
		{
			if(isset($commodity_list[$value['commodity_id']]))
			{
				$shop_id[$value['shop_id']] = $value['shop_id'];
				$commodity_list[$value['commodity_id']]['img'] = get_upload_path($commodity_list[$value['commodity_id']]['img'], 0);
				$list[$key] = array_merge($value, $commodity_list[$value['commodity_id']]);
				$value['statename'] = $this->cartState($value['state']);
			}
			else
			{
				unset($list[$key]);
			}
		}

		$shop_data = DI()->notorm->shop->select('id,name,shop_img,state')->where('id in ('.implode(',', $shop_id).')')->fetchAll();
		$shop_data = replaceArrayKey($shop_data);

		foreach ($list as $key => $value) 
		{
			if(isset($shop_data[$value['shop_id']]))
			{
				if(!isset($shop_data[$value['shop_id']]['statename'])) $shop_data[$value['shop_id']]['statename'] = $this->cartState($shop_data[$value['shop_id']]['state']);
				$shop_data[$value['shop_id']]['list'][] = $value;
			}
		}

		foreach ($shop_data as $key => &$value) 
		{
			$value['shop_img'] = get_upload_path($value['shop_img'], 0);
		}

		return ['code' => 0, 'msg' => '', 'info' => array_values($shop_data)];
	}



	//删除购物车商品
	public function delShoppingCart()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		$car = DI()->notorm->shop_car->select('id')->where('id=? and uid=?', $this->cart_id,$this->uid)->fetchOne();
		if(!$car) return ['code'=>1001, 'msg' => '商品不存在'];

		if(!DI()->notorm->shop_car->where('id=? and uid=?', $this->cart_id,$this->uid)->delete()) return ['code'=>1002, 'msg' => '删除购物车商品失败'];
		return ['code' => 0, 'msg' => '', 'info' => ['删除购物车商品成功']];
	}

	//加购物车
	public function addShoppingCart()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		$commodity_info = DI()->notorm->shop_commodity->where('id=? and state=1', $this->commodity_id)->fetchOne();
		if(!$commodity_info) return ['code'=>1000, 'msg' => '商品不存在或已下架'];
		if(!DI()->notorm->shop->select('id')->where('id=? and state=1', $commodity_info['shop_id'])->fetchOne())  return ['code'=>1001, 'msg' => '店铺不存在或已关闭'];

		$car = DI()->notorm->shop_car->select('num,id')->where('shop_id=? and commodity_id=? and uid=?', $commodity_info['shop_id'], $this->commodity_id,$this->uid)->fetchOne();

		if($car) //如果购物车有订单就加或减数量
		{
			/**
			 * type:0减  1加
			 */
			if($this->type) 
			{
				if(!DI()->notorm->shop_car->where('id=?', $car['id'])->update(['num' => ($this->num+$car['num'])])) return ['code'=>1002, 'msg' => '加入购物车失败'];
			}
			else
			{
				if(0 >= ($car['num']-$this->num)) //如果购物车的商品数量小于等于0那就删除该列
				{
					if(!DI()->notorm->shop_car->where('id=?', $car['id'])->delete()) return ['code'=>1002, 'msg' => '删除购物车商品失败'];
					return ['code' => 0, 'msg' => '', 'info' => ['删除购物车商品成功']];
				}
				else
				{
					if(!DI()->notorm->shop_car->where('id=?', $car['id'])->update(['num' => ($car['num']-$this->num)])) return ['code'=>1002, 'msg' => '减少购物车数量失败'];
					return ['code' => 0, 'msg' => '', 'info' => ['减少购物车数量成功']];
				}
			}
		}
		else
		{
			$add = [
				'num' => $this->num,
				'shop_id' => $commodity_info['shop_id'],
				'commodity_id' => $this->commodity_id,
				'uid' => $this->uid
			];
			if(!DI()->notorm->shop_car->insert($add)) return ['code'=>1002, 'msg' => '加入购物车失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['加入购物车成功']];
	}

	//状态
	private function cartState($num)
	{
		$arr = ['待审核', '正常', '取消'];
		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}
}