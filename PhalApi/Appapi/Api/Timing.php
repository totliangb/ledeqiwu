<?php

/**
* 定时任务
*/
class Api_Timing extends PhalApi_Api
{
	public function getRules() {

	}

	//取消订单，每分钟执行一次
	public function cancelOrder()
	{
		// $time = time()-900; //15分钟后取消订单
		$time = time()-86400; //24小时后取消订单
		$list = DI()->notorm->shop_order->select('id')->where('state=0 and addtime <= ?', $time)->fetchAll();

		if($list)
		{
			$list = replaceArrayKey($list);
			DI()->notorm->shop_order->where('id in ('.implode(',', array_keys($list)).')')->update(['state'=>10]);
		}
	}

	// 福利订单 确认收货，每天执行一次
	public function ConfirmFuliOrderReceipt()
	{
		$time = time()-604800; //7天后确认收货
		$list = DI()->notorm->fuli_order->select('id,uid,order_sn')->where('status=30 and shippingtime <= ?', $time)->fetchAll();

		if($list)
		{
			$list = replaceArrayKey($list);

			$rs = array();
			foreach ($list as $key => $value) 
			{
				try {
					DI()->notorm->beginTransaction('db_appapi');

					$upFlag = DI()->notorm->fuli_order
									->where('id=?', $value['id'])
									->update(['status' => 50,'finnshedtime'=>time()]);

					if(!$upFlag){
						throw new Exception("确认收货失败");
					}

					DI()->notorm->commit('db_appapi');
				} catch (Exception $e) {
					DI()->notorm->rollback('db_appapi');
		            $rs_item['id']=$value['id'];
		            $rs_item['code']=1005;
		            $rs_item['msg']=$e->getMessage();
		            $rs[] = $rs_item;
					continue;
				}
			}

			// foreach ($rs as $key => $value) {
			// 	echo "ID:".$value['id']."|"."CODE:".$value['code']."|".$value['msg']."\n";
			// }

		}
	}

	//确认收货，每天执行一次
	public function ConfirmReceipt()
	{
		$time = time()-604800; //7天后确认收货
		$list = DI()->notorm->shop_order->select('id,uid,shop_id,numbering,payment_amount')->where('state=30 and deliverytime <= ?', $time)->fetchAll();

		if($list)
		{
			$list = replaceArrayKey($list);

			$configpri = getConfigPri();

			$rs = array();
			foreach ($list as $key => $value) 
			{
				try {
					DI()->notorm->beginTransaction('db_appapi');

					// 计算平台抽成
					$platform_amount = $value['payment_amount'] * floatval($configpri['shop_percentage']) * 0.01;
					$upFlag = DI()->notorm->shop_order
									->where('id=?', $value['id'])
									->update(['state' => 50,'platform_amount' => $platform_amount]);

					if(!$upFlag){
						throw new Exception("确认收货失败");
					}

					$add = [
						'uid' => $value['uid'],
						'order_id' => $value['id'],
						'addtime' => time(),
						'type' => 4,
						'text' => '用户7天未确认收货，系统自动确认收货'
					];
					if(!DI()->notorm->shop_order_log->insert($add)){
						throw new Exception("确认收货失败");
					}
					if(!DI()->notorm->shop->where('id=?',$value['shop_id'])->update([
						'balance_freeze' => new NotORM_Literal("balance_freeze - ".$value['payment_amount']),
						'balance' => new NotORM_Literal("balance + ".$value['payment_amount']),
						])){
						throw new Exception("确认收货失败");
					}

					if(!DI()->notorm->shop_amount_log->insert([
						'shop_id' => $value['shop_id'],
						'order_id' => $value['id'],
						'type' => 1,
						'amount' => $value['payment_amount'],
						'type2' => 1,
						'addtime' => time(),
						'text' => '系统自动收货'
					])){
						throw new Exception("确认收货失败");
					}

					if(!DI()->notorm->shop_amount_log->insert([
						'shop_id' => $value['shop_id'],
						'order_id' => $value['id'],
						'type' => 0,
						'amount' => $value['payment_amount'],
						'type2' => 0,
						'addtime' => time(),
						'text' => '系统自动收货'
					])){
						throw new Exception("确认收货失败");
					}

					$agent = DI()->notorm->agent->where("uid = ?",$value['uid'])->fetchOne();
					if ($agent){
					    if(!DI()->notorm->user_distributerecord->insert([
		    				'uid' => $value['uid'],
		    				'one_uid' => $agent['one_uid'],
		    				'addtime' => time(),
		    				'payment_total' => $value['payment_amount'],
		    				'numbering' => $value['numbering'],
		    				'distribute_total' => round($value['payment_amount'] * intval($configpri['distribut1']) * 0.01,2),
		    				'source' => '订单'.$value['numbering']."获得佣金"
		    			])) {throw new Exception("确认收货失败");}
		    			
		    			
		    			if(!DI()->notorm->user->where("id = ?",$agent['one_uid'])->update([
		    				'distribute' => new NotORM_Literal("distribute + ".($value['payment_amount'] * intval($configpri['distribut1']) * 0.01))
		    			])) {throw new Exception("确认收货失败");}
					}

					DI()->notorm->commit('db_appapi');
				} catch (Exception $e) {
					DI()->notorm->rollback('db_appapi');
		            $rs_item['id']=$value['id'];
		            $rs_item['code']=1005;
		            $rs_item['msg']=$e->getMessage();
		            $rs[] = $rs_item;
					continue;
				}
			}

			// foreach ($rs as $key => $value) {
			// 	echo "ID:".$value['id']."|"."CODE:".$value['code']."|".$value['msg']."\n";
			// }

		}
	}

	// 用户下单五分钟内未付款，提醒用户付款, 每分钟执行一次
	public function unpayOrderNotice()
	{
		$time = time()-(5*60); //5分钟后未付款订单提醒
		$list = DI()->notorm->shop_order->select('uid')->where('state=10 and addtime <= ?', $time)->fetchAll();
		if (!empty($list)) {
			$title = "您有未付款订单，请查看。";
			$uids = array();
			foreach ($list as $key => $value) {
				$uids[] = $value['uid'];
			}
			$uids = array_flip($uids);
            $uids = array_flip($uids);
            $uids = array_values($uids);
			
			jpushNoticeMulti($title,$uids);
		}

	}

	// 鉴定订单自动退款, 每天执行一次
	public function appraiseOrderOvertimeRefund()
	{
		// 获取设定的超时时间
		$configpri = getConfigPri();
		$overtime_days = 0;
		if(isset($configpri['appraise_lock_days'])) $overtime_days = $configpri['appraise_lock_days'];
		if ($overtime_days) {
			// 后台设置了鉴定有效期
			$time = time() - (86400*$overtime_days);
			$where = 'pay_status = ? and status = ? and pay_time <= ?';
			$whereData = array('1','0',$time);
			$list = DI()->notorm->appraise_apply
						->select('*')
						->where($where, $whereData)
						->fetchAll();
			if (!empty($list)) {
				$rs = array();
				foreach ($list as $key => $value) {
			        try {
			        	DI()->notorm->beginTransaction('db_appapi');
			        	// 鉴定专家的冻结金额扣除
						$amount = $value['expert_fee'];
						if($amount*1){
							$now_user_info = DI()->notorm->user
											->select("user_nicename")
											->where('id=?',$value['touid'])
											->fetchOne();
							$data_log = array();
							$data_log['uid'] = $value['touid'];
							$data_log['nickname'] = $now_user_info['user_nicename'];
							$data_log['amount'] = $amount;
							$data_log['order_sn'] = $value['apply_order_sn'];
				            $pdModel = new Model_Pd();
				            $refundPdFlag = $pdModel->changePd('appraise_refund',$data_log);
				            if(!$refundPdFlag) throw new Exception('扣除专家冻结金额失败');
						}

						// 退款操作
						$refundThreadFlag = false;
						// 构建退款参数
						$item_data['numbering'] = $value['pay_sn'];
						$item_data['payment_amount'] = $value['appraise_fee'];

			            $params = array(
			                'code' => $value['pay_type'],
			            );

			            // 第三方退款
			            $domainPay = new Domain_Pay();
			            $refundThreadFlag = $domainPay->_api_refund($item_data,$params);
			            if(!$refundThreadFlag) throw new Exception('退款失败');

			            // 更新订单状态
			            $where = 'id=?';
						$whereData = array($value['id']);
						$data = array('status'=>'2',"over_time"=>time());
						$updateResult = DI()->notorm->appraise_apply
								->where($where,$whereData)
								->update($data);
						if($updateResult === false)  throw new Exception('更新订单状态失败');

			            DI()->notorm->commit('db_appapi');
			        } catch (Exception $e) {
			        	DI()->notorm->rollback('db_appapi');
			            $rs_item['id']=$value['id'];
			            $rs_item['code']=1005;
			            $rs_item['msg']=$e->getMessage();
			            $rs[] = $rs_item;
			            continue;
			        }
				}
				// foreach ($rs as $key => $value) {
				// 	echo "ID:".$value['id']."|"."CODE:".$value['code']."|".$value['msg']."\n";
				// }
			}
		}

	}

	// 红包结束时间到了后退款，更改红包状态（1分钟执行一次）
	public function redEndtimeRefund()
	{
        $configpri=getConfigPri();
        $red_end_mins = isset($configpri['red_end_mins']) ? $configpri['red_end_mins'] : 1;
		$time = time() - ($red_end_mins*60);
		// 获取红包未抢完，结束时间到了的红包信息
		$where = 'status = ? and effecttime <= ?';
		$whereData = array('0',$time);
		$list = DI()->notorm->red
					->select('*')
					->where($where, $whereData)
					->fetchAll();
		if(!empty($list)){
			foreach ($list as $key => $value) {
				try {
					DI()->notorm->beginTransaction('db_appapi');

					// 计算需要退回的金额
					$other_amount = $value['amount'] - $value['coin_rob'];

					// 退回余额
		            $now_user_info = DI()->notorm->user
		                            ->select("user_nicename")
		                            ->where('id=?',$value['uid'])
		                            ->fetchOne();
		            $data_log = array();
		            $data_log['uid'] = $value['uid'];
		            $data_log['nickname'] = $now_user_info['user_nicename'];
		            $data_log['amount'] = $other_amount;
		            $data_log['order_sn'] = $value['red_sn'];

		            $pdModel = new Model_Pd();
		            $pdFlag = $pdModel->changePd('red_refund',$data_log);
		            if(!$pdFlag) throw new Exception('退回红包金额失败');

		            $item_update_data = array(
		            	'status' => '1',
		            );

		            // 更改状态
		            DI()->notorm->red
						->where('id = ? and red_sn = ?',$value['id'],$value['red_sn'])
						->update($item_update_data);

					DI()->notorm->commit('db_appapi');
				} catch (Exception $e) {
					DI()->notorm->rollback('db_appapi');
		            $rs[]['id']=$value['id'];
		            $rs[]['code']=1005;
		            $rs[]['msg']=$e->getMessage();
		            continue;
				}
			}
		}
	}
}