<?php
/**
* 竞拍商品
*/
class Api_Auction extends PhalApi_Api
{
	public function getRules() {
		return array(
			'add' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
				'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '商品名称'),
				'img' => array('name' => 'img', 'require' => true, 'type' => 'string', 'desc' => '商品图'),
				'is_noreasonretrun' => array('name' => 'is_noreasonretrun', 'type' => 'int', 'default' => 0, 'desc' => '是否7天无理由退货，0为否'),
				'is_logistic' => array('name' => 'is_logistic', 'type' => 'int', 'default' => 0, 'desc' => '是否需要邮费，0为包邮'),
				'start_price' => array('name' => 'start_price', 'require' => true, 'type' => 'float', 'desc' => '起拍价'),
				'each_price' => array('name' => 'each_price', 'require' => true, 'type' => 'float', 'desc' => '每次加价'),
				'duration_mins' => array('name' => 'duration_mins', 'require' => true, 'type' => 'float', 'desc' => '持续时长分钟数'),
				'delay_seconds' => array('name' => 'delay_seconds', 'require' => true, 'type' => 'float', 'desc' => '延时拍时长秒数'),
				'length' => array('name' => 'length', 'require' => true, 'type' => 'string', 'desc' => '长'),
				'width' => array('name' => 'width', 'require' => true, 'type' => 'string', 'desc' => '宽'),
				'height' => array('name' => 'height', 'require' => true, 'type' => 'string', 'desc' => '高'),
			),
			'getNowAuctionGoods' => array(
				'stream' => array('name' => 'stream', 'type' => 'string','require' => true, 'desc' => '流名'),
			),
			'createAuctionOrder' => array(
				'stream' => array('name' => 'stream', 'type' => 'string','require' => true, 'desc' => '流名'),
				'win_uid' => array('name' => 'win_uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'win_nickname' => array('name' => 'win_nickname', 'type' => 'string','require' => true, 'desc' => '昵称'),
				'win_price' => array('name' => 'win_price', 'type' => 'float','require' => true, 'desc' => '价格'),
			),
			'getAuctionOrderInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','require' => true, 'desc' => '订单ID'),
			),
			'completingAuctionOrderAddress' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'address_id' => array('name' => 'address_id', 'type' => 'int','require' => true, 'desc' => '地址ID'),
				'order_id' => array('name' => 'order_id', 'type' => 'int','require' => true, 'desc' => '订单ID'),
			),
			'withoutAuction' => array(
				'stream' => array('name' => 'stream', 'type' => 'string','require' => true, 'desc' => '流名'),
			),

		);
	}

	public function add()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$check_post = $this->check_post();
		if($check_post['code']) return $check_post;

		$stream = $this->stream;

        $stream_a=explode("_",$stream);
        $liveuid=$stream_a[0];
        $showid=$stream_a[1];
        if((int)$liveuid==0 || (int)$showid==0){
            $rs['code']=1007;
            $rs['msg']='信息错误';		
            return $rs;
        }

		// 校验当前主播是否存在进行中的竞拍商品
		$hasGoods = DI()->notorm->shop_auction->where("liveuid = ? AND showid = ? AND status = '0'",$liveuid,$showid)->fetchOne();
		if(!empty($hasGoods)) return ['code' => 1005, 'msg' => '当前存在进行中的竞拍商品'];

		$add = [
			'showid' => $showid,
			'shop_id' => $check_post['data']['id'],
			'liveuid' => $this->uid,
			'title' => $this->title,
			'start_price' => round($this->start_price, 2),
			'each_price' => round($this->each_price, 2),
			'is_noreasonretrun' => $this->is_noreasonretrun,
			'is_logistic' => $this->is_logistic,
			'img' => $this->img,
			'length' => $this->length,
			'width' => $this->width,
			'height' => $this->height,
			'duration_mins' => $this->duration_mins,
			'delay_seconds' => $this->delay_seconds,
			'status' => '0', // 进行中
			'addtime' => time(),
		];

		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->shop_auction->insert($add)) throw new Exception('商品添加失败1');

			$domain = new Domain_Auction();
        	$info = $domain->getNowAuctionGoods($liveuid,$showid);
        	$info_data = json_encode($info);

        	$info['overtime'] = ($info['addtime'] + ($info['duration_mins']*60))*1000;

			// 添加redis 信息
			$key = 'auc_goods_'.$stream;
			$key2 = 'auc_goods_'.$stream.'_price'; // 当前竞拍价格
			$key3 = 'auc_goods_'.$stream.'_overtime'; // 当前竞拍截止时间
			DI()->redis->set($key,$info_data);
			DI()->redis->set($key2,$info['start_price']);
			DI()->redis->set($key3,$info['overtime']);

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');

			return ['code' => 1008, 'msg' => '商品添加失败3'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['添加成功']];
	}

	/**
	 * 获取当前主播当前竞价商品
	 */
	public function getNowAuctionGoods()
	{
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

		$stream = $this->stream;

        $stream_a=explode("_",$stream);
        $liveuid=$stream_a[0];
        $showid=$stream_a[1];
        if((int)$liveuid==0 || (int)$showid==0){
            $rs['code']=1007;
            $rs['msg']='信息错误';		
            return $rs;
        }

		// 当前直播间 进行中的竞拍商品
        $domain = new Domain_Auction();
        $info = $domain->getNowAuctionGoods($liveuid,$showid);

        if(!empty($info)){
			$key2 = 'auc_goods_'.$stream.'_price'; // 当前竞拍价格
			$key3 = 'auc_goods_'.$stream.'_overtime'; // 当前竞拍截止时间
			$key4 = 'auc_goods_'.$stream.'_user'; // 当前竞拍最高出价人
        	// 当前最高价格
        	$info['now_price'] = $info['start_price'] = $now_price = DI()->redis->get($key2);
        	// 当前最高出价人
        	$now_user = DI()->redis->get($key4);
        	// 当前预计结束时间
        	$now_overtime = DI()->redis->get($key3);

        	// 获取当前商品的预计结束时间
        	$info['overtime'] = $now_overtime ? $now_overtime : ($info['addtime'] + ($info['duration_mins']*60))*1000;
        	// 剩余时间
        	$resttime = $info['overtime']-(time()*1000);
        	$info['resttime'] = $resttime>=0 ? $resttime*1 : 0;
        	// 什么时候显示倒计时
        	$info['showlasttimeseconds'] = 30;

        	$now_user_info = array();
        	if ($now_user) {
        		$now_user = json_decode($now_user,true);
        		$user_info = getUserInfo($now_user['uid'],1);
        		if($user_info){
	                $now_user_info[0]['id'] = $user_info['id'];
	                $now_user_info[0]['user_nicename'] = $user_info['user_nicename'];
	                $now_user_info[0]['avatar'] = $user_info['avatar'];
	                $now_user_info[0]['avatar_thumb'] = $user_info['avatar_thumb'];
        		}
        	}
    		$info['now_userinfo'] = $now_user_info;

        }
		
		$rs['info'] = $info;

        return $rs;
	}

	/**
	 * 竞拍商品下单(不需要填写收货地址在付款界面中进行补齐)
	 */
	public function createAuctionOrder()
	{
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $stream = $this->stream; // 直播间流名

        $win_data = array();
        $win_data['win_uid'] = $win_uid = $this->win_uid; // 赢得竞拍的用户ID
        $win_data['win_nickname'] = $win_nickname = $this->win_nickname; // 赢得竞拍的用户昵称
        $win_data['win_price'] = $win_price = $this->win_price; // 赢得竞拍的价格


        $stream_a=explode("_",$stream);
        $liveuid=$stream_a[0];
        $showid=$stream_a[1];
        if((int)$liveuid==0 || (int)$showid==0){
            $rs['code']=1007;
            $rs['msg']='信息错误';		
            return $rs;
        }

        $domain = new Domain_Auction();
        $result = $domain->createAuctionOrder($liveuid,$showid,$win_data);

        return $result;
	}

	/**
	 * 流拍
	 */
	public function withoutAuction()
	{

        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $stream = $this->stream; // 直播间流名

        $win_data = array();


        $stream_a=explode("_",$stream);
        $liveuid=$stream_a[0];
        $showid=$stream_a[1];
        if((int)$liveuid==0 || (int)$showid==0){
            $rs['code']=1007;
            $rs['msg']='信息错误';		
            return $rs;
        }

        $domain = new Domain_Auction();
        $result = $domain->withoutAuction($liveuid,$showid);

        $rs['info'][0] = $result;

        return $result;
	}

	/**
	 * 确认订单 补全竞拍商品订单地址信息
	 */
	public function getAuctionOrderInfo()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];


		if(!$this->order_id) return ['code'=>1000, 'msg' => '参数错误'];

		$order_info = DI()->notorm->shop_order->where('pay_id = ? and uid = ?', $this->order_id,$this->uid)->fetchOne();

		if(!$order_info) return ['code'=>1001, 'msg' => '未找到订单信息'];

		// 获取订单商品信息
		$order_commodity_info = DI()->notorm->shop_order_info->where('pay_id = ? and uid = ?', $this->order_id,$this->uid)->fetchOne();

		if(!empty($order_commodity_info)){
			//获取商品信息
			$commodity_info = DI()->notorm->shop_auction->select('id,img,shop_id,is_noreasonretrun,is_logistic,title,length,width,height')->where("id =? AND status = '1'",$order_commodity_info['commodity_id'])->fetchOne();
		}
		if(empty($order_commodity_info) || empty($commodity_info)){
			return ['code'=>1002, 'msg' => '未找到商品信息'];
		}else{
			$commodity_info['price'] = $order_commodity_info['price'];
		}

		$commodity_info['img'] = get_upload_path($commodity_info['img'], 0);

		$address = DI()->notorm->shop_address->select('id,title,name,phone,province,city,area,info,is_default')->where('uid =?', $this->uid)->order('is_default desc')->fetchOne();
		if($address)
		{
			$area = DI()->notorm->shop_area->select('coding,name')
				->where('coding in ('.implode(',', [$address['province'],$address['city'],$address['area']]).')')
				->fetchAll();
			$area = replaceArrayKey($area,'coding');
			$address['province'] = $area[$address['province']]['name'];
			$address['city'] = $area[$address['city']]['name'];
			$address['area'] = $area[$address['area']]['name'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['commodity_info' => $commodity_info, 'address'=> empty($address) ? [] : [$address]]];
	}

	/**
	 * 补全竞拍商品订单的地址信息
	 */
	public function completingAuctionOrderAddress()
	{
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!$this->order_id) return ['code'=>1000, 'msg' => '参数错误'];

		$order_info = DI()->notorm->shop_order->where('pay_id = ? and uid = ?', $this->order_id,$this->uid)->fetchOne();

		if(!$order_info) return ['code'=>1001, 'msg' => '未找到订单信息'];

		$address_info = DI()->notorm->shop_address->select('id,title')->where('id=? and uid = ?', $this->address_id, $this->uid)->fetchOne();
		
		if(!$address_info) return ['code'=>1000, 'msg' => '收货地址不存在'];

		// 更新竞拍商品订单信息
        $domain = new Domain_Auction();
        $result = $domain->completingAuctionOrderAddress($this->order_id,$address_info);

        if(!$result) {
        	return ['code'=>1004, 'msg' => '更新订单地址信息失败'];
        }

        return $rs;
	}

	//判断提交的商品数据
	private function check_post()
	{
		if(mb_strlen($this->title) > 50 || mb_strlen($this->title) <= 0) return ['code' => 1000, 'msg' => '商品名称字数应在1~50个字之间'];
		if($this->start_price > 10000000 || $this->start_price < 0) return ['code' => 1001, 'msg' => '起拍价应在0~10000000之间'];
		if($this->each_price > 10000000 || $this->each_price < 0) return ['code' => 1002, 'msg' => '每次加价金额应在0~10000000之间'];

		$shop = DI()->notorm->shop->where('uid=? and state = 1', $this->uid)->fetchOne();
		if(!$shop) return ['code' => 1007, 'msg' => '您当前的店铺未审核通过或正在审核'];
		return ['code' => 0, 'data' => $shop];
	}
}