<?php
/**
* 商品模块
*/
class Api_Commodity extends PhalApi_Api
{
	public function getRules() {
		return array(
			'add' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '商品名称'),
				'original_price' => array('name' => 'original_price', 'type' => 'float', 'require' => true, 'desc' => '商品原价'),
				'assort_id' => array('name' => 'assort_id', 'type' => 'int', 'require' => true, 'desc' => '分类id'),
				'price' => array('name' => 'price', 'type' => 'float', 'require' => true, 'desc' => '商城价格'),
				'amount' => array('name' => 'amount', 'type' => 'int', 'require' => true, 'desc' => '库存'),
				'is_logistic' => array('name' => 'is_logistic', 'type' => 'int', 'default' => 0, 'desc' => '是否需要邮费，0为包邮'),
				'carousel' => array('name' => 'carousel', 'require' => true, 'type' => 'string', 'desc' => '商品轮播图'),
				'description' => array('name' => 'description', 'type' => 'string', 'desc' => '商品描述'),
				'img_h' => array('name' => 'img_h', 'type' => 'float', 'desc' => '主图高度'),
				'img_w' => array('name' => 'img_h', 'type' => 'float', 'desc' => '主图宽度'),
				'text' => array('name' => 'text', 'type' => 'string', 'desc' => '商品详情'),
			),
			'edit' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '商品名称'),
				'commodity_id' => array('name' => 'commodity_id', 'type' => 'int', 'require' => true, 'desc' => '商品id'),
				'assort_id' => array('name' => 'assort_id', 'type' => 'int', 'require' => true, 'desc' => '分类id'),
				'original_price' => array('name' => 'original_price', 'type' => 'float', 'require' => true, 'desc' => '商品原价'),
				'price' => array('name' => 'price', 'type' => 'float', 'require' => true, 'desc' => '商城价格'),
				'amount' => array('name' => 'amount', 'type' => 'int', 'require' => true, 'desc' => '库存'),
				'is_logistic' => array('name' => 'is_logistic', 'type' => 'int', 'default' => 0, 'desc' => '是否需要邮费，0为包邮'),
				'carousel' => array('name' => 'carousel', 'type' => 'string', 'require' => true, 'desc' => '商品轮播图'),
				'description' => array('name' => 'description', 'type' => 'string', 'desc' => '商品描述'),
				'img_h' => array('name' => 'img_h', 'type' => 'float', 'desc' => '主图高度'),
				'img_w' => array('name' => 'img_h', 'type' => 'float', 'desc' => '主图宽度'),
				'text' => array('name' => 'text', 'type' => 'string', 'desc' => '商品详情'),
			),
			'delete' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'commodity_id' => array('name' => 'commodity_id', 'type' => 'int', 'require' => true, 'desc' => '商品id'),
			),
			'getInfo' => array(
				'commodity_id' => array('name' => 'commodity_id', 'type' => 'int', 'require' => true, 'desc' => '商品id'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 0, 'default'=>1,'desc' => '分类')
			),
			'shopCommodityList' => array(
				'shop_id' => array('name' => 'shop_id', 'type' => 'int', 'require' => true, 'desc' => '店铺id')
			),
			'bestSeller' => array(
				'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1,'desc' => '页数'),
				'assort_id' => array('name' => 'assort_id', 'type' => 'int', 'require' => true, 'default' => 0, 'desc' => '分类id'),
			),
			'modifyState' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'commodity_id' => array('name' => 'commodity_id', 'type' => 'int', 'require' => true, 'desc' => '商品id'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 0 , 'default' => 1,'require' => true, 'desc' => '分类'),
			),
			'merchantCommodityList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 0 , 'default' => 2,'require' => true, 'desc' => '分类'),
			),
			'search' => array(
				'name' => array('name' => 'name', 'type' => 'string', 'require' => true, 'desc' => '商品名称'),
				'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1,'desc' => '页数'),
			),
			'getRecommendCommodity' => array(
				'anchor_id' => array('name' => 'anchor_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '视频发布者id'),
				'video_id' => array('name' => 'video_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '商品id'),
			),
			'getLiveCommodity' => array(
				'anchor_id' => array('name' => 'anchor_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '视频发布者id'),
			),
		);
	}

	//获取当前主播正在推荐的商品
	public function getLiveCommodity()
	{
		$shop = DI()->notorm->shop->select('id')->where('uid=?', $this->anchor_id)->fetchOne();

		$info = [];
		if($shop)
		{
			$live = DI()->notorm->live->select('uid')->where('uid = ? and islive = 1', $this->anchor_id)->fetchOne();

			if(!$live) return ['code'=>1001, 'msg' => '视频异常'];

			$info = DI()->notorm->shop_commodity->select('id,title,img,amount,price,original_price')->where('shop_id=? and state=1 and is_recommend = 1', $shop['id'])->fetchOne();
			
			if($info) $info['img'] = get_upload_path($info['img'], 0); 
		}

		return ['code' => 0, 'msg' => '', 'info' => empty($info) ? [] : [$info]];
	}

	//获取当前主播正在推荐的商品
	public function getRecommendCommodity()
	{
		$shop = DI()->notorm->shop->select('id')->where('uid=?', $this->anchor_id)->fetchOne();

		$list = [];
		if($shop)
		{
			$video = DI()->notorm->video->select('commodity_id')->where('uid = ? and id = ? and isdel = 0 and status = 1', $this->anchor_id, $this->video_id)->fetchOne();

			if(!$video) return ['code'=>1001, 'msg' => '视频异常'];

			if(!empty($video['commodity_id']))
			{
				$list = DI()->notorm->shop_commodity->select('id,shop_id,title,description,img,original_price,price,amount,sales')->order('sort desc')->where('shop_id=? and state=1 and id in ('.$video['commodity_id'].')', $shop['id'])->fetchAll();

				foreach ($list as $key => &$value) 
				{
					$value['img'] = get_upload_path($value['img'], 0);
				}
			}
		}

		return ['code' => 0, 'msg' => '', 'info' => $list];
	}

	//搜索
	public function search()
	{
		if($this->p<1){
            $this->p=1;
        }
		$nums=10;
		$start=($this->p-1)*$nums;

		if(mb_strlen($this->name) > 20) $this->name = mb_substr($this->name, 0, 20);

		$where = $this->name ? ' and title like  "%'.$this->name.'%"' : '';

		$list = DI()->notorm->shop_commodity->select('id,shop_id,title,description,img,original_price,price,amount,sales')->order('sales desc')->limit($start,$nums)->where('state=1'.$where)->fetchAll();
		foreach ($list as $key => &$value) 
		{
			$value['img'] = get_upload_path($value['img'], 0);
		}
		return ['code' => 0, 'msg' => '', 'info' => $list];
	}

	//商家商品列表
	public function merchantCommodityList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop = DI()->notorm->shop->select('id,name,shop_img,description')->where('uid=? and state=1', $this->uid)->fetchOne();

		if(!$shop) return ['code'=>1000, 'msg' => '店铺不存在'];
		if(2 == $this->type) //所有
		{
			$where = '';
		}
		else if(1 == $this->type)//上架
		{
			$where = ' and state=1';
		}
		else //下架或审核中
		{
			$where = ' and state in (0,2)';
		}

		$list = DI()->notorm->shop_commodity->select('id,shop_id,title,description,img,original_price,price,amount,sales,state')->order('sort desc')->where('shop_id=?'.$where, $shop['id'])->fetchAll();

		foreach ($list as $key => &$value) 
		{
			$value['img'] = get_upload_path($value['img'], 0);
		}

		return ['code' => 0, 'msg' => '', 'info' => ['list' => $list]];
	}

	//修改商品状态
	public function modifyState()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(DI()->notorm->shop_commodity->where('uid=? and id=? and state=0',$this->uid,$this->commodity_id)->fetchOne())
		{
			return ['code' => 1001, 'msg' => '商品审核期间不能上下架'];
		}

		$type = $this->type ? 1 : 2; //1上架  0下架
		$type_name = $this->type ? '上' : '下';
		if(!DI()->notorm->shop_commodity->where('uid=? and id=?',$this->uid,$this->commodity_id)->update(['state' => $type]))
		{
			return array('code' => 1000, 'msg' => $type_name.'架失败', 'info' => array());
		}

		return array('code' => 0, 'msg' => '', 'info' => [$type_name.'架成功']);
	}

	//热销产品
	public function bestSeller()
	{
		$where = $this->assort_id ? ' and assort_id ='.$this->assort_id : '';
		if($this->p<1){
            $this->p=1;
        }
		$nums=10;
		$start=($this->p-1)*$nums;

		// 库存不能为0 
		$where .= ' and amount > 0';

		$list = DI()->notorm->shop_commodity->select('id,shop_id,title,description,img,img_h,img_w,original_price,price,amount,sales')->order('sales desc,id desc')->limit($start,$nums)->where('state=1'.$where)->fetchAll();
		foreach ($list as $key => &$value) 
		{
			$value['img'] = get_upload_path($value['img'], 0);
		}
		return ['code' => 0, 'msg' => '', 'info' => $list];
	}

	//店铺的商品列表
	public function shopCommodityList()
	{
		$shop = DI()->notorm->shop->select('name,shop_img,description')->where('id=? and state=1', $this->shop_id)->fetchOne();

		if(!$shop) return ['code'=>1000, 'msg' => '店铺不存在'];

		$list = DI()->notorm->shop_commodity->select('id,assort_id,shop_id,title,description,img,original_price,price,amount,sales')->order('sort desc')->where('shop_id=? and state=1', $this->shop_id)->fetchAll();

		foreach ($list as $key => &$value) 
		{
			$value['img'] = get_upload_path($value['img'], 0);
		}

		return ['code' => 0, 'msg' => '', 'info' => ['shop_info' => $shop, 'list' => $list]];
	}

	//获取商品详情
	public function getInfo()
	{
		$where = ($this->type) ? ' and state=1' : '';
		$commodity = DI()->notorm->shop_commodity->where('id=?'.$where, $this->commodity_id)->fetchOne();

		if(!$commodity) return ['code'=>1000, 'msg' => '商品不存在'];

		$commodity_info = DI()->notorm->shop_commodity_info->select('carousel,text')->where('commodity_id=?', $this->commodity_id)->fetchOne();
		$commodity['carousel'] = json_decode($commodity_info['carousel'], true);
		$commodity['text'] = json_decode($commodity_info['text'], true);
		
		if(!empty($commodity['img'])) $commodity['img']= get_upload_path($commodity['img'], 0);
		
		if(!empty($commodity['carousel']))
		{
		    foreach ($commodity['carousel'] as $key => &$value) 
    		{
    			$value = get_upload_path($value, 0);
    		}
		}
		
		
		if(!empty($commodity['text']))
		{
		    foreach ($commodity['text'] as $k => &$v) 
    		{
    			$v = get_upload_path($v, 0);
    		}
		}
		

		unset($commodity['addtime'], $commodity['sort']);

		$commodity['statename'] = $this->state($commodity['state']);
		return ['code' => 0, 'msg' => '', 'info' => [$commodity]];
	}

	//删除商品接口
	public function delete()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop_id = DI()->notorm->shop->select('id')->where('uid=?', $this->uid)->fetchOne();

		if(!$shop_id)  return ['code' => 1001, 'msg' => '店铺不存在'];

		if(!DI()->notorm->shop_commodity->where('id=? and shop_id=?', $this->commodity_id,$shop_id['id'])->delete()) return ['code' => 1000, 'msg' => '删除商品失败'];
		return ['code' => 0, 'msg' => '', 'info' => ['删除商品成功']];
	}

	//修改商品
	public function edit()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$check_post = $this->check_post();
		if($check_post['code']) return $check_post;

		$commodity_info = DI()->notorm->shop_commodity->where('id=? and shop_id=?', $this->commodity_id, $check_post['data']['id'])->fetchOne();

		if(!$commodity_info) return ['code' => 1009, 'msg' => '商品不存在'];
		$this->carousel = explode(',', $this->carousel);

		$add = [
			'title' => $this->title,
			'original_price' => round($this->original_price, 2),
			'price' => round($this->price, 2),
			'amount' => intval($this->amount),
			'img' => $this->carousel[0],
			'is_logistic' => $this->is_logistic,
			'description' => $this->description,
			'img_h'=> $this->img_h,
			'img_w'=> $this->img_w,
			'addtime' => time(),
			'assort_id' => $this->assort_id
		];

		if((getConfigPri()['commodity_switch'])) $add['state'] = 0;

		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->shop_commodity->where('id', $this->commodity_id)->update($add)) throw new Exception('商品修改失败');

			$info = [
				'carousel' => json_encode($this->carousel),
				'text' => $this->text ? json_encode(explode(',', $this->text)) : json_encode('')
			];
			if(!DI()->notorm->shop_commodity_info->where('commodity_id', $this->commodity_id)->update($info)) throw new Exception('商品修改失败');

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');

			return ['code' => 1008, 'msg' => '商品修改失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['修改成功']];
	}

	//添加商品
	public function add()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$check_post = $this->check_post();
		if($check_post['code']) return $check_post;
		$this->carousel = explode(',', $this->carousel);

		$add = [
			'shop_id' => $check_post['data']['id'],
			'uid' => $this->uid,
			'title' => $this->title,
			'original_price' => round($this->original_price, 2),
			'price' => round($this->price, 2),
			'amount' => intval($this->amount),
			'is_logistic' => $this->is_logistic,
			'img' => $this->carousel[0],
			'description' => $this->description,
			'img_h'=> $this->img_h,
			'img_w'=> $this->img_w,
			'addtime' => time(),
			'assort_id' => $this->assort_id
		];

		if(!(getConfigPri()['commodity_switch'])) $add['state'] = 1;

		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->shop_commodity->insert($add)) throw new Exception('商品添加失败1');

			$info = [
				'commodity_id' => DI()->notorm->shop_commodity->insert_id(),
				'shop_id' => $check_post['data']['id'],
				'carousel' => json_encode($this->carousel)
			];

			if($this->text) $info['text'] = json_encode(explode(',', $this->text));

			if(!DI()->notorm->shop_commodity_info->insert($info)) throw new Exception('商品添加失败2');

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');

			return ['code' => 1008, 'msg' => '商品添加失败3'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['添加成功']];
	}

	private function state($num)
	{
		$arr = ['待审核','正常', '取消'];
		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}

	//判断提交的商品数据
	private function check_post()
	{
		if(mb_strlen($this->title) > 50 || mb_strlen($this->title) <= 0) return ['code' => 1000, 'msg' => '商品名称字数应在1~50个字之间'];
		if($this->original_price > 10000000 || $this->original_price < 0) return ['code' => 1001, 'msg' => '商品原价应在0~10000000之间'];
		if($this->price > 10000000 || $this->price < 0) return ['code' => 1002, 'msg' => '商城价格应在0~10000000之间'];
		if($this->amount > 10000000 || $this->amount < 0) return ['code' => 1003, 'msg' => '库存应在0~10000000之间'];
		//if(mb_strlen($this->img) > 100 || mb_strlen($this->img) <= 0) return ['code' => 1004, 'msg' => '商品图片字符长度应在1~100个字之间'];
		if(mb_strlen($this->description) > 100) return ['code' => 1005, 'msg' => '商品描述的文字长度不能超过100个'];
		if(mb_strlen($this->carousel) > 1000 || mb_strlen($this->carousel) <= 0) return ['code' => 1006, 'msg' => '商品轮播图的字符长度应在1~1000个字之间'];
		if(mb_strlen($this->text) > 1000) return ['code' => 1007, 'msg' => '商品详情的字符长度不能超过1000个'];
		$shop = DI()->notorm->shop->where('uid=? and state = 1', $this->uid)->fetchOne();
		if(!$shop) return ['code' => 1007, 'msg' => '您当前的店铺未审核通过或正在审核'];
		return ['code' => 0, 'data' => $shop];
	}
}