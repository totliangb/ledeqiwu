<?php

/**
* 圈子
*/
class Api_Circle extends PhalApi_Api
{
	public function getRules() {
		return array(
            'addCircle' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'title' => array('name' => 'title', 'type' => 'string', 'desc' => '圈子标题'),
				'flie' => array('name' => 'flie', 'type' => 'string',  'desc' => '文件'),
			),
			'editCircle' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'circle_id' => array('name' => 'circle_id', 'type' => 'int','require' => true, 'desc' => '圈子id'),
				'title' => array('name' => 'title', 'type' => 'string', 'desc' => '圈子标题'),
				'flie' => array('name' => 'flie', 'type' => 'string',  'desc' => '文件'),
			),
			'delCircle' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'circle_id' => array('name' => 'circle_id', 'type' => 'int','require' => true, 'desc' => '圈子id'),
			),
			'MyCircleList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			'addComment' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '评论'),
				'circle_id' => array('name' => 'circle_id', 'type' => 'int','require' => true, 'desc' => '圈子id'),
			),
			'CircleInfo' => array(
				'circle_id' => array('name' => 'circle_id', 'type' => 'int','require' => true, 'desc' => '圈子id'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			'likes' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'circle_id' => array('name' => 'circle_id', 'type' => 'int','require' => true, 'desc' => '圈子id'),
			),
			'forward' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'circle_id' => array('name' => 'circle_id', 'type' => 'int','require' => true, 'desc' => '圈子id'),
			),
			'lists' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'type' => array('name' => 'type', 'type' => 'int','require' => true,'min' => 0, 'default' => 1, 'desc' => '分类'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			'favorites' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'circle_id' => array('name' => 'circle_id', 'type' => 'int','require' => true, 'desc' => '圈子id'),
			),
			'attentionMemberCircle' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			'favoritesList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			'search' => array(
				'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '搜索内容'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
		);
	}

	//轮播
	public function banner()
	{
		$list = DI()->notorm->slide_item->select('id slide_id,title slide_name,image slide_pic,url slide_url')->where('status = 1 and slide_id=4')->order('list_order desc')->limit(6)->fetchAll();

		return ['code' => 0, 'msg' => '', 'info' => $list ? $list : []];
	}

	//搜索
	public function search()
	{
		if($this->p<1){
            $this->p=1;
        }
		$nums=10;
		$start=($this->p-1)*$nums;

		if(mb_strlen($this->content) > 20) $this->content = mb_substr($this->content, 0, 20);

		$where = $this->content ? 'title like  "%'.$this->content.'%"' : '';

		$list = DI()->notorm->circle->select('id,uid,title,flie,likes,forward,comment,addtime')->where($where)->limit($start,$nums)->order('addtime desc')->fetchAll();

		if($list)
		{
			$uid = [];
			foreach ($list as $key => &$value) 
			{
				$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
				if(empty($value['title'])) $value['title'] = '';
				$value['flie'] = empty($value['flie']) ? [] : json_decode($value['flie'], true);
				foreach ($value['flie'] as $k => &$v) 
				{
					$v = get_upload_path($v, 0);
				}

				$uid[$value['uid']] = $value['uid'];
			}

			//用户名
			$user_list = DI()->notorm->user->select('id,user_nicename,avatar')->where('id in ('.implode(',', $uid).')')->limit(count($uid))->fetchAll();
			$user_list = replaceArrayKey($user_list);

			foreach ($list as $key => &$value) 
			{
				$value['name'] = $user_list[$value['uid']]['user_nicename'];
				$value['avatar'] = $user_list[$value['uid']]['avatar'] ? $user_list[$value['uid']]['avatar'] : '';
			}
		}

		return ['code' => 0, 'msg' => '', 'info' => $list ? $list : []];
	}

	//收藏列表
	public function favoritesList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if($this->p<1){
            $this->p=1;
        }
		$nums=10;
		$start=($this->p-1)*$nums;

		$favorites = DI()->notorm->circle_favorites->select('circle_id id')->where('uid=?', $this->uid)->limit($start,$nums)->order('addtime desc')->fetchAll();
		$list = [];
		if($favorites)
		{
			$favorites = replaceArrayKey($favorites);

			$list = DI()->notorm->circle->select('id,uid,title,flie,likes,forward,comment,addtime')->where('id in ('.implode(',', array_keys($favorites)).')')->limit(count($favorites))->fetchAll();

			$circle_id = $uid = [];
			foreach ($list as $key => &$value) 
			{
				$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
				if(empty($value['title'])) $value['title'] = '';
				$value['flie'] = empty($value['flie']) ? [] : json_decode($value['flie'], true);
				foreach ($value['flie'] as $k => &$v) 
				{
					$v = get_upload_path($v, 0);
				}

				$circle_id[] = $value['id'];
				$uid[$value['uid']] = $value['uid'];
			}

			//用户名
			$user_list = DI()->notorm->user->select('id,user_nicename,avatar')->where('id in ('.implode(',', $uid).')')->limit(count($uid))->fetchAll();
			$user_list = replaceArrayKey($user_list);

			//点赞
			$likes = DI()->notorm->circle_likes->select('circle_id id')->where('circle_id in ('.implode(',', $circle_id).') and uid=?',$this->uid)->limit(count($circle_id))->fetchAll();
			$likes = replaceArrayKey($likes);

			//关注
			$users_attention= DI()->notorm->user_attention->select('touid id')->where('touid in ('.implode(',', $uid).') and uid=?',$this->uid)->limit(count($uid))->fetchAll();

			$users_attention = replaceArrayKey($users_attention);

			foreach ($list as $key => &$value) 
			{
				$value['name'] = $user_list[$value['uid']]['user_nicename'];
				$value['avatar'] = $user_list[$value['uid']]['avatar'] ? $user_list[$value['uid']]['avatar'] : '';
				$value['is_attention'] = isset($users_attention[$value['uid']]) ? 1 : 0;
				$value['is_likes'] = isset($likes[$value['id']]) ? 1 : 0;
			}
		}
		
		return ['code' => 0, 'msg' => '', 'info' => $list ? $list : []];
	}

	//关注用户的圈子列表
	public function attentionMemberCircle()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if($this->p<1){
            $this->p=1;
        }
		$nums=10;
		$start=($this->p-1)*$nums;

		//关注
		$users_attention= DI()->notorm->user_attention->select('touid id')->where('uid=?',$this->uid)->fetchAll();

		$list = [];
		if($users_attention)
		{
			$users_attention = replaceArrayKey($users_attention);
			$where = 'uid in ('.implode(',', array_keys($users_attention)).') and state = 1';
			$list = DI()->notorm->circle->select('id,uid,title,flie,likes,forward,comment,addtime')->where($where)->limit($start,$nums)->order('addtime desc')->fetchAll();
			if($list)
			{
				$circle_id = $uid = [];
				foreach ($list as $key => &$value) 
				{
					$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
					if(empty($value['title'])) $value['title'] = '';
					$value['flie'] = empty($value['flie']) ? [] : json_decode($value['flie'], true);
					foreach ($value['flie'] as $k => &$v) 
					{
						$v = get_upload_path($v, 0);
					}
					$circle_id[] = $value['id'];
					$uid[$value['uid']] = $value['uid'];
				}

				//关注
				$users_attention= DI()->notorm->user_attention->select('touid id')->where('touid in ('.implode(',', $uid).') and uid=?',$this->uid)->limit(count($uid))->fetchAll();

				$users_attention = replaceArrayKey($users_attention);

				//用户名
				$user_list = DI()->notorm->user->select('id,user_nicename,avatar')->where('id in ('.implode(',', $uid).')')->limit(count($uid))->fetchAll();
				$user_list = replaceArrayKey($user_list);

				//点赞
				$likes = DI()->notorm->circle_likes->select('circle_id id')->where('circle_id in ('.implode(',', $circle_id).') and uid=?',$this->uid)->limit(count($circle_id))->fetchAll();
				$likes = replaceArrayKey($likes);

				//收藏
				$favorites = DI()->notorm->circle_favorites->select('circle_id id')->where('circle_id in ('.implode(',', $circle_id).') and uid=?',$this->uid)->limit(count($circle_id))->fetchAll();
				$favorites = replaceArrayKey($favorites);

				foreach ($list as $key => &$value) 
				{
					$value['name'] = $user_list[$value['uid']]['user_nicename'];
					$value['avatar'] = $user_list[$value['uid']]['avatar'] ? $user_list[$value['uid']]['avatar'] : '';
					$value['is_attention'] = isset($users_attention[$value['uid']]) ? 1 : 0;
					$value['is_likes'] = isset($likes[$value['id']]) ? 1 : 0;
					$value['is_favorites'] = isset($favorites[$value['id']]) ? 1 : 0;
				}
			}
		}

		return ['code' => 0, 'msg' => '', 'info' => $list ? $list : []];
		
	}

	//收藏
	public function favorites()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!DI()->notorm->circle->select('id')->where('id=? and state=1', $this->circle_id)->fetchOne()) return ['code'=>1000, 'msg' => '收藏的内容不存在'];

		if(DI()->notorm->circle_favorites->select('id')->where('circle_id=? and uid=?',  $this->circle_id, $this->uid)->fetchOne())
		{
			if(!DI()->notorm->circle_favorites->where('circle_id=? and uid=?',  $this->circle_id, $this->uid)->delete()) return ['code'=>1001, 'msg' => '取消收藏失败'];
			return ['code' => 0, 'msg' => '', 'info' => ['取消收藏成功']];
		}
		else
		{
			$add = ['uid' => $this->uid, 'circle_id' => $this->circle_id, 'addtime' => time()];
			if(!DI()->notorm->circle_favorites->insert($add)) return ['code'=>1001, 'msg' => '收藏失败'];
			return ['code' => 0, 'msg' => '', 'info' => ['收藏成功']];
		}
	}

	//最热和最新
	public function lists()
	{
		if(isset($this->uid) && isset($this->token))
		{
			$checkToken = checkToken($this->uid,$this->token);
			if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		}

		if($this->p<1){
            $this->p=1;
        }
		$nums=10;
		$start=($this->p-1)*$nums;

		$order = $this->type ? 'likes desc,forward desc,comment desc,addtime desc' : 'addtime desc';

		$list = DI()->notorm->circle->select('id,uid,title,flie,likes,forward,comment,addtime')->where('state=1')->limit($start,$nums)->order($order)->fetchAll();

		if($list)
		{
			$uid = $circle_id = [];
			foreach ($list as $key => &$value) 
			{
				$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
				if(empty($value['title'])) $value['title'] = '';
				$value['flie'] = empty($value['flie']) ? [] : json_decode($value['flie'], true);
				foreach ($value['flie'] as $k => &$v) 
				{
					$v = get_upload_path($v, 0);
				}
				$uid[$value['uid']] = $value['uid'];
				$circle_id[] = $value['id'];
			}

			//用户名
			$user_list = DI()->notorm->user->select('id,user_nicename,avatar')->where('id in ('.implode(',', $uid).')')->limit(count($uid))->fetchAll();
			$user_list = replaceArrayKey($user_list);
			if(isset($this->uid))
			{
				//关注
				$users_attention= DI()->notorm->user_attention->select('touid id')->where('touid in ('.implode(',', $uid).') and uid=?',$this->uid)->limit(count($uid))->fetchAll();

				$users_attention = replaceArrayKey($users_attention);

				//点赞
				$likes = DI()->notorm->circle_likes->select('circle_id id')->where('circle_id in ('.implode(',', $circle_id).') and uid=?',$this->uid)->limit(count($circle_id))->fetchAll();
				$likes = replaceArrayKey($likes);

				//收藏
				$favorites = DI()->notorm->circle_favorites->select('circle_id id')->where('circle_id in ('.implode(',', $circle_id).') and uid=?',$this->uid)->limit(count($circle_id))->fetchAll();
				$favorites = replaceArrayKey($favorites);
			}

			foreach ($list as $key => &$value) 
			{
			    if (!isset($user_list[$value['uid']])) {
			        unset($list[$key]);
			        continue;
			    }
				$value['name'] = $user_list[$value['uid']]['user_nicename'];
				$value['avatar'] = $user_list[$value['uid']]['avatar'] ? $user_list[$value['uid']]['avatar'] : '';
				$value['is_attention'] = isset($users_attention[$value['uid']]) ? 1 : 0;
				$value['is_likes'] = isset($likes[$value['id']]) ? 1 : 0;
				$value['is_favorites'] = isset($favorites[$value['id']]) ? 1 : 0;
			}

			$list = array_values($list);
		}
		return ['code' => 0, 'msg' => '', 'info' => $list ? $list : []];
	}

	//转发
	public function forward()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$circle = DI()->notorm->circle->where('id=? and state=1',  $this->circle_id)->fetchOne();
		if(!$circle) return ['code'=>1000, 'msg' => '转发的内容不存在'];

		$forward_id = $circle['cid'] ? $circle['cid'] : $this->circle_id;
		$configpri=getConfigPri();
		$add = [
			'uid' => $this->uid,
			'title' => $circle['title'],
			'flie' => $circle['flie'],
			'cid' => $forward_id,
			'addtime' => time(),
			'state' => empty($configpri['circle_switch']) ? 0 : 1
		];
		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->circle->where('id=? and state=1',  $forward_id)->update(['forward' => new NotORM_Literal("forward + 1")])) throw new Exception("转发失败");

			if(!DI()->notorm->circle->insert($add)) throw new Exception("转发失败");
			
			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1005, 'msg' => $e->getMessage()];
			return ['code' => 1001, 'msg' => '转发失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['转发成功']];
	}

	//点赞
	public function likes()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!DI()->notorm->circle->select('id')->where('id=? and state=1',  $this->circle_id)->fetchOne()) return ['code'=>1000, 'msg' => '点赞的内容不存在'];

		if(DI()->notorm->circle_likes->select('id')->where('circle_id=? and uid=?',  $this->circle_id, $this->uid)->fetchOne())
		{
			try {
				DI()->notorm->beginTransaction('db_appapi');

				if(!DI()->notorm->circle_likes->select('id')->where('circle_id=? and uid=?', $this->circle_id, $this->uid)->delete()) throw new Exception("取消点赞失败");

				if(!DI()->notorm->circle->where('id=? and state=1',  $this->circle_id)->update(['likes' => new NotORM_Literal("likes - 1")])) throw new Exception("取消点赞失败");
				
				DI()->notorm->commit('db_appapi');
			} catch (Exception $e) {
				DI()->notorm->rollback('db_appapi');
				//return ['code' => 1005, 'msg' => $e->getMessage()];
				return ['code' => 1001, 'msg' => '取消点赞失败'];
			}

			return ['code' => 0, 'msg' => '', 'info' => ['取消点赞成功']];
		}
		else
		{
			try {
				DI()->notorm->beginTransaction('db_appapi');

				$add = [
					'uid' => $this->uid,
					'circle_id' => $this->circle_id,
					'addtime' => time()
				];

				if(!DI()->notorm->circle_likes->select('id')->insert($add)) throw new Exception("点赞失败");

				if(!DI()->notorm->circle->where('id=? and state=1',  $this->circle_id)->update(['likes' => new NotORM_Literal("likes + 1")])) throw new Exception("点赞失败");
				
				DI()->notorm->commit('db_appapi');
			} catch (Exception $e) {
				DI()->notorm->rollback('db_appapi');
				//return ['code' => 1005, 'msg' => $e->getMessage()];
				return ['code' => 1001, 'msg' => '点赞失败'];
			}

			return ['code' => 0, 'msg' => '', 'info' => ['点赞成功']];
		}
	}

	//圈子详情
	public function CircleInfo()
	{
		if($this->p<1){
            $this->p=1;
        }
		$nums=10;
		$start=($this->p-1)*$nums;

		$info = DI()->notorm->circle->select('id,uid,title,flie,likes,forward,comment,addtime')->where('id=? and state=1',  $this->circle_id)->fetchOne();

		if(!$info) return ['code'=>1000, 'msg' => '数据不存在'];
		$info['addtime'] = date('Y-m-d H:i:s', $info['addtime']);
		if(empty($info['title'])) $info['title'] = '';
		$info['flie'] = empty($info['flie']) ? [] : json_decode($info['flie'], true);

		$userInfo = DI()->notorm->user->select('user_nicename,avatar')->where('id=?',  $info['uid'])->fetchOne();

		$info['name'] = $userInfo['user_nicename'];
		$info['avatar'] = $userInfo['avatar'];

		foreach ($info['flie'] as $k => &$v) 
		{
			$v = get_upload_path($v, 0);
		}

		$list = DI()->notorm->circle_comment->select('uid,content')->where('circle_id=?',$this->circle_id)->limit($start,$nums)->order('id asc')->fetchAll();

		if($list)
		{
			$uid = [];

			foreach ($list as $key => $value) 
			{
				$uid[$value['uid']] = $value['uid'];
			}

			$user_list = DI()->notorm->user->select('id,user_nicename,avatar')->where('id in ('.implode(',', $uid).')')->limit(count($uid))->fetchAll();
			$user_list = replaceArrayKey($user_list);

			foreach ($list as $key => &$value) 
			{
				$value['name'] = $user_list[$value['uid']]['user_nicename'];
				$value['avatar'] = $user_list[$value['uid']]['avatar'] ? $user_list[$value['uid']]['avatar'] : '';
			}
		}
		

		return ['code' => 0, 'msg' => '', 'info' => ['data' => $info, 'list' => $list ? $list : []]];
	}

	//添加评论
	public function addComment()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!DI()->notorm->circle->where('id = ? and state=1',  $this->circle_id)->fetchOne()) return ['code'=>1000, 'msg' => '您要评论的内容不存在'];
		$add = [
			'uid' => $this->uid,
			'circle_id' => $this->circle_id,
			'content' => $this->content,
			'addtime' => time()
		];
		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->circle_comment->insert($add)) throw new Exception("添加失败");
			
			if(!DI()->notorm->circle->where('id = ? and state=1',  $this->circle_id)->update(['comment' => new NotORM_Literal("comment + 1")])) throw new Exception("添加失败");

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1005, 'msg' => $e->getMessage()];
			return ['code' => 1001, 'msg' => '添加失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['添加成功']];
	}

	//我的圈子
	public function MyCircleList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if($this->p<1){
            $this->p=1;
        }
		$nums=10;
		$start=($this->p-1)*$nums;

		$list = DI()->notorm->circle->select('id,title,flie,likes,forward,comment,addtime,state')->where('uid = ?',  $this->uid)->limit($start,$nums)->order('addtime desc')->fetchAll();

		if($list)
		{
			$user_list = DI()->notorm->user->select('id,user_nicename,avatar')->where('id =?', $this->uid)->fetchOne();
			foreach ($list as $key => &$value) 
			{
				$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
				if(empty($value['title'])) $value['title'] = '';
				$value['flie'] = empty($value['flie']) ? [] : json_decode($value['flie'], true);
				foreach ($value['flie'] as $k => &$v) 
				{
					$v = get_upload_path($v, 0);
				}

				$value['name'] = $user_list['user_nicename'];
				$value['avatar'] = $user_list['avatar'] ? $user_list['avatar'] : '';
			}
		}
		
		return ['code' => 0, 'msg' => '', 'info' => $list ? $list : []];
	}

	//删除圈子
	public function delCircle()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->circle->where('id=? and uid = ?', $this->circle_id, $this->uid)->delete()) throw new Exception("删除失败");
			
			if(DI()->notorm->circle_comment->where('circle_id = ?', $this->circle_id)->fetchOne())
			{
				if(!DI()->notorm->circle_comment->where('circle_id = ?', $this->circle_id)->delete()) throw new Exception("删除失败");
			}

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1005, 'msg' => $e->getMessage()];
			return ['code' => 1001, 'msg' => '删除失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['删除成功']];
	}

	//修改圈子
	public function editCircle()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$checkCircle = $this->checkCircle();
		if($checkCircle['code']) return $checkCircle;

		if(!DI()->notorm->circle->select('id')->where('id=? and uid = ?', $this->circle_id, $this->uid)->fetchOne()) return ['code'=>1002, 'msg' => '信息不存在'];
		$configpri=getConfigPri();
		$add = [
			'uid' => $this->uid,
			'title' => $this->title,
			'flie' => $this->flie,
			'state' => empty($configpri['circle_switch']) ? 0 : 1
		];

		if(!DI()->notorm->circle->where('id=? and uid = ?', $this->circle_id, $this->uid)->update($add)) return ['code'=>1003, 'msg' => '修改失败'];
		return ['code' => 0, 'msg' => '', 'info' => ['修改成功']];
	}

	//添加圈子
	public function addCircle()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$checkCircle = $this->checkCircle();
		if($checkCircle['code']) return $checkCircle;

		$configpri=getConfigPri();
		$add = [
			'uid' => $this->uid,
			'title' => $this->title,
			'flie' => $this->flie,
			'addtime' => time(),
			'state' => empty($configpri['circle_switch']) ? 0 : 1
		];

		if(!DI()->notorm->circle->insert($add)) return ['code'=>1002, 'msg' => '添加失败'];
		return ['code' => 0, 'msg' => '', 'info' => ['添加成功']];
	}

	//判断圈子
	private function checkCircle()
	{
		if(!$this->title && !$this->flie) return ['code'=>1000, 'msg' => '标题和文件必须上传任意一个'];

		if($this->title && mb_strlen($this->title) > 500) return ['code'=>1001, 'msg' => '标题字数不能超过500'];

		if($this->flie)
		{
			if(strlen($this->flie) > 1000) return ['code'=>1001, 'msg' => '文件字符长度不能超过1000'];
			$this->flie = json_encode(explode(',', $this->flie));
		}

		return ['code' => 0];
	}
}