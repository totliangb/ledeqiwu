<?php
/**
 * 首页
 */
class Api_Home extends PhalApi_Api {  

	public function getRules() {
		return array(
			'getHot' => array(
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			
			'getFollow' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','min'=>1,'require' => true, 'desc' => '用户ID'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			
			'getNew' => array(
                'lng' => array('name' => 'lng', 'type' => 'string', 'desc' => '经度值'),
                'lat' => array('name' => 'lat', 'type' => 'string','desc' => '纬度值'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
			
			'search' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'min'=>1 ,'desc' => '用户ID'),
				'key' => array('name' => 'key', 'type' => 'string', 'default'=>'' ,'desc' => '用户ID'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			
			'getNearby' => array(
                'lng' => array('name' => 'lng', 'type' => 'string', 'desc' => '经度值'),
                'lat' => array('name' => 'lat', 'type' => 'string','desc' => '纬度值'),
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
			
			'getRecommend' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'min'=>1 ,'desc' => '用户ID'),
			),
			
			'attentRecommend' => array(
				'uid' => array('name' => 'uid', 'type' => 'int' ,'desc' => '用户ID'),
				'touid' => array('name' => 'touid', 'type' => 'string', 'desc' => '关注用户ID，多个用,分隔'),
			),
            'profitList'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','min'=>1,'require' => true, 'desc' => '用户ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
                'type' => array('name' => 'type', 'type' => 'string', 'default'=>'day' ,'desc' => '参数类型，day表示日榜，week表示周榜，month代表月榜，total代表总榜'),
            ),

            
            'consumeList'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','min'=>1,'require' => true, 'desc' => '用户ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
                'type' => array('name' => 'type', 'type' => 'string', 'default'=>'day' ,'desc' => '参数类型，day表示日榜，week表示周榜，month代表月榜，total代表总榜'),
            ),
            
            'getClassLive'=>array(
                'nickname' => array('name' => 'nickname', 'type' => 'string', 'default'=>'' ,'desc' => '用户昵称'),
                'liveclassid' => array('name' => 'liveclassid', 'type' => 'int', 'default'=>'0' ,'desc' => '直播分类ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
            'getBuyerList' => array(
                'type' => array("name" => "type", "type" => "string", "default" => "all", "desc" => "类型")    
            ),
            'getSellerList' => array(
                'type' => array("name" => "type", "type" => "string", "default" => "all", "desc" => "类型")    
            ),
            'getCommodityRank' => array(
                'type' => array("name" => "type", "type" => "string", "default" => "all", "desc" => "类型")    
            ),
            'getLiveInfo' => array(
                'stream' => array("name" => "stream", "type" => "string", 'require' => true, "desc" => "流名")    
            ),
            'searchMulti' => array(
                'type' => array('name' => 'type', 'type' => 'string','require' => true, 'default'=>'' ,'desc' => '类型(goods 商品 live 直播间 shop 店铺)'),
                'key' => array('name' => 'key', 'type' => 'string','require' => true, 'default'=>'' ,'desc' => '关键词'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
		);
	}
	
    /**
     * 配置信息
     * @desc 用于获取配置信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return array info[0] 配置信息
     * @return object info[0].guide 引导页
	 * @return string info[0].guide.switch 开关，0关1开
	 * @return string info[0].guide.type 类型，0图片1视频
	 * @return string info[0].guide.time 图片时间
	 * @return array  info[0].guide.list
	 * @return string info[0].guide.list[].thumb 图片、视频链接
	 * @return string info[0].guide.list[].href 页面链接
     * @return string msg 提示信息
     */
    public function getConfig() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $info = getConfigPub();
        
        unset($info['site_url']);
        unset($info['site_seo_title']);
        unset($info['site_seo_keywords']);
        unset($info['site_seo_description']);
        unset($info['site_icp']);
        unset($info['site_gwa']);
        unset($info['site_admin_email']);
        unset($info['site_analytics']);
        unset($info['copyright']);
        unset($info['qr_url']);
        unset($info['sina_icon']);
        unset($info['sina_title']);
        unset($info['sina_desc']);
        unset($info['sina_url']);
        unset($info['qq_icon']);
        unset($info['qq_title']);
        unset($info['qq_desc']);
        unset($info['qq_url']);
        
        $info_pri = getConfigPri();
        
        $list = getLiveClass();
        $videoclasslist = getVideoClass();
        
        $level= getLevelList();
        
        foreach($level as $k=>$v){
            unset($v['level_up']);
            unset($v['addtime']);
            unset($v['id']);
            unset($v['levelname']);
            $level[$k]=$v;
        }
        
        $levelanchor= getLevelAnchorList();
        
        foreach($levelanchor as $k=>$v){
            unset($v['level_up']);
            unset($v['addtime']);
            unset($v['id']);
            unset($v['levelname']);
            $levelanchor[$k]=$v;
        }
        
        $info['liveclass']=$list;
        $info['videoclass']=$videoclasslist;
        
        $info['level']=$level;
        
        $info['levelanchor']=$levelanchor;
        
        $info['tximgfolder']=$info_pri['tximgfolder'];//腾讯云图片存储目录
        $info['txvideofolder']=$info_pri['txvideofolder'];//腾讯云视频存储目录
        $info['txcloud_appid']=$info_pri['txcloud_appid'];//腾讯云视频APPID
        $info['txcloud_region']=$info_pri['txcloud_region'];//腾讯云视频地区
        $info['txcloud_bucket']=$info_pri['txcloud_bucket'];//腾讯云视频存储桶
        $info['cloudtype']=$info_pri['cloudtype'];//视频云存储类型
		//$info['qiniu_domain']=$info_pri['qiniu_domain_url'];//七牛云存储空间地址（后台配置）
        $info['live_change_price']=$info_pri['workerman']; //直播改价服务器带端口
        
		$info['qiniu_domain']=DI()->config->get('app.Qiniu.space_host').'/';//七牛云存储空间地址（后台配置）
        $info['video_audit_switch']=$info_pri['video_audit_switch']; //视频审核是否开启
        
        /* 私信开关 */
        $info['letter_switch']=$info_pri['letter_switch']; //视频审核是否开启
        
        /* 引导页 */
        $domain = new Domain_Guide();
		$guide_info = $domain->getGuide();
        
        $info['guide']=$guide_info;
        
        /* 小程序开关 */
        $info['applets_switch']='0';
        
		/** 敏感词集合*/
		$dirtyarr=array();
		if($info_pri['sensitive_words']){
            $dirtyarr=explode(',',$info_pri['sensitive_words']);
        }
		$info['sensitive_words']=$dirtyarr;
		//视频水印图片
        $info['video_watermark']=get_upload_path($info_pri['video_watermark']); //视频审核是否开启
		 
        $info['shopexplain_url']=$info['site']."/portal/page/index?id=38";
        $info['stricker_url']=$info['site']."/portal/page/index?id=39";
         
        $rs['info'][0] = $info;

        return $rs;
    }	

    /**
     * 登录方式开关信息
     * @desc 用于获取登录方式开关信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return array info[0].login_type 开启的登录方式
     * @return string info[0].login_type[][0] 登录方式标识

     * @return string msg 提示信息
     */
    public function getLogin() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $info = getConfigPub();
        $rs['info'][0]['login_type'] = $info['login_type'];

        return $rs;
    }		
	
    /**
     * 获取热门主播
     * @desc 用于获取首页热门主播
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return array info[0]['slide'] 
     * @return string info[0]['slide'][].slide_pic 图片
     * @return string info[0]['slide'][].slide_url 链接
     * @return array info[0]['list'] 热门直播列表
     * @return string info[0]['list'][].uid 主播id
     * @return string info[0]['list'][].avatar 主播头像
     * @return string info[0]['list'][].avatar_thumb 头像缩略图
     * @return string info[0]['list'][].user_nicename 直播昵称
     * @return string info[0]['list'][].title 直播标题
     * @return string info[0]['list'][].city 主播位置
     * @return string info[0]['list'][].stream 流名
     * @return string info[0]['list'][].pull 播流地址
     * @return string info[0]['list'][].nums 人数
     * @return string info[0]['list'][].thumb 直播封面
     * @return string info[0]['list'][].level_anchor 主播等级
     * @return string info[0]['list'][].type 直播类型
     * @return string info[0]['list'][].goodnum 靓号
     * @return string msg 提示信息
     */
    public function getHot() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Home();
		$key1='getSlide';
		$slide=getcaches($key1);
		if(!$slide){
			$slide = $domain->getSlide();
			setcaches($key1,$slide);
		}

		$key2="getHot_".$this->p;
		$list=getcaches($key2);
		if(!$list){
		    $userdomain = new Domain_User();
			$list = $domain->getHot($this->p);
		    foreach ($list[0] as &$v){
		        $v['bond_num'] = $userdomain->getBond($v['uid']);
		    }
			setCaches($key2,$list,2); 
		}

        $rs['info'][0]['slide'] = $slide;
        $rs['info'][0]['list'] = $list[0];
        $rs['info'][0]['recommend'] = $list[1];

        return $rs;
    }
    /**
     * 获取关注主播列表
     * @desc 用于获取用户关注的主播的直播列表
     * @return int code 操作码，0表示成功
     * @return string info[0]['title'] 提示标题
     * @return string info[0]['des'] 提示描述
     * @return array info[0]['list'] 直播列表
     * @return string info[0]['list'][].uid 主播id
     * @return string info[0]['list'][].avatar 主播头像
     * @return string info[0]['list'][].avatar_thumb 头像缩略图
     * @return string info[0]['list'][].user_nicename 直播昵称
     * @return string info[0]['list'][].title 直播标题
     * @return string info[0]['list'][].city 主播位置
     * @return string info[0]['list'][].stream 流名
     * @return string info[0]['list'][].pull 播流地址
     * @return string info[0]['list'][].nums 人数
     * @return string info[0]['list'][].thumb 直播封面
     * @return string info[0]['list'][].level_anchor 主播等级
     * @return string info[0]['list'][].type 直播类型
     * @return string info[0]['list'][].goodnum 靓号
     * @return string msg 提示信息
     */
    public function getFollow() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Home();
        $info = $domain->getFollow($this->uid,$this->p);
        $userdomain = new Domain_User();
        foreach ($info as &$v){
            $v['bond_num'] = $userdomain->getBond($v['uid']);
        }

        $rs['info'][0] = $info;

        return $rs;
    }

    /**
     * 获取最新主播
     * @desc 用于获取首页最新开播的主播列表
     * @return int code 操作码，0表示成功
     * @return array info 主播列表
     * @return string info[].uid 主播id
     * @return string info[].avatar 主播头像
     * @return string info[].avatar_thumb 头像缩略图
     * @return string info[].user_nicename 直播昵称
     * @return string info[].title 直播标题
     * @return string info[].city 主播位置
     * @return string info[].stream 流名
     * @return string info[].pull 播流地址
     * @return string info[].nums 人数
     * @return string info[].distance 距离
     * @return string info[].thumb 直播封面
     * @return string info[].level_anchor 主播等级
     * @return string info[].type 直播类型
     * @return string info[].goodnum 靓号
     * @return string msg 提示信息
     */
    public function getNew() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$lng=checkNull($this->lng);
		$lat=checkNull($this->lat);
		$p=checkNull($this->p);
		
		if(!$p){
			$p=1;
		}
		
		$key='getNew_'.$p;
		$info=getcaches($key);
		if(!$info){
			$domain = new Domain_Home();
			$info = $domain->getNew($lng,$lat,$p);
            $userdomain = new Domain_User();
		    foreach ($info as &$v){
		        $v['bond_num'] = $userdomain->getBond($v['uid']);
		    }
			setCaches($key,$info,2);
		}
		
        $rs['info'] = $info;

        return $rs;
    }		
		
	/**
     * 搜索
     * @desc 用于首页搜索会员
     * @return int code 操作码，0表示成功
     * @return array info 会员列表
     * @return string info[].id 用户ID
     * @return string info[].user_nicename 用户昵称
     * @return string info[].avatar 头像
     * @return string info[].sex 性别
     * @return string info[].signature 签名
     * @return string info[].level 等级
     * @return string info[].isattention 是否关注，0未关注，1已关注
     * @return string msg 提示信息
     */
    public function search() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=checkNull($this->uid);
		$key=checkNull($this->key);
		$p=checkNull($this->p);
		if($key==''){
			$rs['code'] = 1001;
			$rs['msg'] = "请填写关键词";
			return $rs;
		}
		
		if(!$p){
			$p=1;
		}
		
        $domain = new Domain_Home();
        $info = $domain->search($uid,$key,$p);
        $userdomain = new Domain_User();
	    foreach ($info as &$v){
	        $v['bond_num'] = $userdomain->getBond($v['uid']);
	    }
        $rs['info'] = $info;

        return $rs;
    }	
	
    /**
     * 获取附近主播
     * @desc 用于获取附近开播的主播列表
     * @return int code 操作码，0表示成功
     * @return array info 主播列表
     * @return string info[].uid 主播id
     * @return string info[].avatar 主播头像
     * @return string info[].avatar_thumb 头像缩略图
     * @return string info[].user_nicename 直播昵称
     * @return string info[].title 直播标题
     * @return string info[].province 省份
     * @return string info[].city 主播位置
     * @return string info[].stream 流名
     * @return string info[].pull 播流地址
     * @return string info[].nums 人数
     * @return string info[].distance 距离
     * @return string info[].thumb 直播封面
     * @return string info[].level_anchor 主播等级
     * @return string info[].type 直播类型
     * @return string info[].goodnum 靓号
     * @return string msg 提示信息
     */
    public function getNearby() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$lng=checkNull($this->lng);
		$lat=checkNull($this->lat);
		$p=checkNull($this->p);
		
		if($lng==''){
			return $rs;
		}
		
		if($lat==''){
			return $rs;
		}
		
		if(!$p){
			$p=1;
		}
		
		$key='getNearby_'.$lng.'_'.$lat.'_'.$p;
		$info=getcaches($key);
		if(!$info){
			$domain = new Domain_Home();
			$info = $domain->getNearby($lng,$lat,$p);
            $userdomain = new Domain_User();
		    foreach ($info as &$v){
		        $v['bond_num'] = $userdomain->getBond($v['uid']);
		    }
			setcaches($key,$info,2);
		}
		
        $rs['info'] = $info;

        return $rs;
    }	
	
	/**
     * 推荐主播
     * @desc 用于显示推荐主播
     * @return int code 操作码，0表示成功
     * @return array info 会员列表
     * @return string info[].id 用户ID
     * @return string info[].user_nicename 用户昵称
     * @return string info[].avatar 头像
     * @return string info[].fans 粉丝数
     * @return string info[].isattention 是否关注，0未关注，1已关注
     * @return string msg 提示信息
     */
    public function getRecommend() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=checkNull($this->uid);
		
		$key='getRecommend';
		$info=getcaches($key);
		if(!$info){
			$domain = new Domain_Home();
			$info = $domain->getRecommend();
            $userdomain = new Domain_User();
		    foreach ($info as &$v){
		        $v['bond_num'] = $userdomain->getBond($v['uid']);
		    }
			setcaches($key,$info,60*10);
		}
		
		foreach($info as $k=>$v){
			$info[$k]['isattention']=(string)isAttention($uid,$v['id']);
		}

        $rs['info'] = $info;

        return $rs;
    }	
	
	/**
     * 关注推荐主播
     * @desc 用于关注推荐主播
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
    public function attentRecommend() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=checkNull($this->uid);
		$touid=checkNull($this->touid);

        if($uid<1){
            $rs['code'] = 1001;
			$rs['msg'] = "参数错误";
			return $rs;
        }
        if($touid==''){
            $rs['code'] = 1001;
			$rs['msg'] = "请选择要关注的主播";
			return $rs;
        }

		$domain = new Domain_Home();
		$info = $domain->attentRecommend($uid,$touid);

        //$rs['info'] = $info;

        return $rs;
    }

    /**
     * 收益榜单
     * @desc 获取收益榜单
     * @return int code 操作码 0表示成功
     * @return string msg 提示信息 
     * @return array info
     * @return string info[0]['user_nicename'] 主播昵称
     * @return string info[0]['avatar_thumb'] 主播头像
     * @return string info[0]['totalcoin'] 主播钻石数
     * @return string info[0]['uid'] 主播id
     * @return string info[0]['levelAnchor'] 主播等级
     * @return string info[0]['isAttention'] 是否关注主播 0 否 1 是
     **/
    
    public function profitList(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $p=checkNull($this->p);
        $type=checkNull($this->type);
        $domain=new Domain_Home();
        $res=$domain->profitList($uid,$type,$p);

        $rs['info']=$res;
        return $rs;
    }

    /**
     * 收益榜单
     * @desc 获取收益榜单
     * @return int code 操作码 0表示成功
     * @return string msg 提示信息 
     * @return array info
     * @return string info[0]['user_nicename'] 用户昵称
     * @return string info[0]['avatar_thumb'] 用户头像
     * @return string info[0]['totalcoin'] 用户钻石数
     * @return string info[0]['uid'] 用户id
     * @return string info[0]['levelAnchor'] 用户等级
     * @return string info[0]['isAttention'] 是否关注用户 0 否 1 是
     **/
    
    public function consumeList(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $p=checkNull($this->p);
        $type=checkNull($this->type);
        $domain=new Domain_Home();
        $res=$domain->consumeList($uid,$type,$p);

        $rs['info']=$res;
        return $rs;
    }
    

    /**
     * 获取分类下的直播(分类可为空)
     * @desc 获取分类下的直播
     * @return int code 操作码 0表示成功
     * @return string msg 提示信息 
     * @return array info
     * @return string info[].uid 主播id
     * @return string info[].avatar 主播头像
     * @return string info[].avatar_thumb 头像缩略图
     * @return string info[].user_nicename 直播昵称
     * @return string info[].title 直播标题
     * @return string info[].city 主播位置
     * @return string info[].stream 流名
     * @return string info[].pull 播流地址
     * @return string info[].nums 人数
     * @return string info[].distance 距离
     * @return string info[].thumb 直播封面
     * @return string info[].level_anchor 主播等级
     * @return string info[].type 直播类型
     * @return string info[].goodnum 靓号
     **/
    
    public function getClassLive(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $nickname=checkNull($this->nickname);
        $liveclassid=checkNull($this->liveclassid);
        $p=checkNull($this->p);
        
        $domain=new Domain_Home();
        $userdomain = new Domain_User();
        $res=$domain->getClassLive($p,$liveclassid,$nickname);
        foreach ($res as &$v){
            $v['bond_num'] = $userdomain->getBond($v['uid']);
        }
        $rs['info']=$res;
        return $rs;
    }
    
	public function getBuyerList(){
	    $rs = array('code' => 0, 'msg' => '', 'info' => array());
	    
	    $type = $this->type;
	    
	    if ($type == "day") {
	        $where = " AND pay_time > ".strtotime(date("Y-m-d"));
	    }elseif ($type == "week"){
	        $where = " AND pay_time > ".(strtotime(date("Y-m-d")) - 86400*6);
	    }elseif ($type == "month"){
	        $where = " AND pay_time > ".(strtotime(date("Y-m-d")) - 86400*29);
	    }else{
	        $where = "";
	    }
	    $list = DI()->notorm->shop_order->select("uid,sum(payment_amount) as amount")->where("deletetime = 0 AND state IN(20,30,50,60)".$where)->order("amount desc")->group("uid")->fetchAll();
	    
	    foreach ($list as &$v){
	        $userinfo=getUserInfo($v['uid']);
	        $v['user_nicename'] = $userinfo['user_nicename'];
	        $v['avatar'] = $userinfo['avatar'];
	        $v['avatar_thumb'] = $userinfo['avatar_thumb'];
	    }
	    $rs['info'] = $list;
	    return $rs;
	}
	
	public function getSellerList(){
	    $rs = array('code' => 0, 'msg' => '', 'info' => array());
	    
	    $type = $this->type;
	    
	    if ($type == "day") {
	        $where = " AND pay_time > ".strtotime(date("Y-m-d"));
	    }elseif ($type == "week"){
	        $where = " AND pay_time > ".(strtotime(date("Y-m-d")) - 86400*6);
	    }elseif ($type == "month"){
	        $where = " AND pay_time > ".(strtotime(date("Y-m-d")) - 86400*29);
	    }else{
	        $where = "";
	    }
	    $list = DI()->notorm->shop_order->select("shop_id,sum(payment_amount) as amount")->where("deletetime = 0 AND state IN(20,30,50,60)".$where)->order("amount desc")->group("shop_id")->fetchAll();
	    
	    foreach ($list as &$v){
	        $shopinfo = DI()->notorm->shop->select("uid,name,shop_img")->where("id = ?",$v['shop_id'])->fetchOne();
	        $userinfo = getUserInfo($shopinfo['uid']);
	        $v['user_nicename'] = $userinfo['user_nicename'];
	        $v['shop_name'] = $shopinfo['name'];
	        $v['shop_img'] = $shopinfo['shop_img'];
	        $v['avatar'] = $userinfo['avatar'];
	        $v['avatar_thumb'] = $userinfo['avatar_thumb'];
	    }
	    $rs['info'] = $list;
	    return $rs;
	}
	
	public function getCommodityRank(){
	    $rs = array('code' => 0, 'msg' => '', 'info' => array());
	    
	    $type = $this->type;
	    
	    if ($type == "day") {
	        $where = " AND b.pay_time > ".strtotime(date("Y-m-d"));
	    }elseif ($type == "week"){
	        $where = " AND b.pay_time > ".(strtotime(date("Y-m-d")) - 86400*6);
	    }elseif ($type == "month"){
	        $where = " AND b.pay_time > ".(strtotime(date("Y-m-d")) - 86400*29);
	    }else{
	        $where = "";
	    }
	    $list = DI()->notorm->shop_order->queryAll("SELECT commodity_id,sum(a.price) as amount,sum(a.num) as num,b.shop_id,c.title FROM cmf_shop_order_info AS a LEFT JOIN cmf_shop_order AS b ON a.order_id = b.id LEFT JOIN cmf_shop_commodity AS c ON c.id = a.commodity_id WHERE b.deletetime = 0 AND b.state IN(20,30,50,60) ".$where." GROUP BY a.commodity_id ORDER BY num DESC",[]);
	   // dump($list);die;
	    foreach ($list as &$v){
	        $shopinfo = DI()->notorm->shop->select("uid,name,shop_img")->where("id = ?",$v['shop_id'])->fetchOne();
	        $v['shop_name'] = $shopinfo['name'];
	        $v['shop_img'] = $shopinfo['shop_img'];
	    }
	    $rs['info'] = $list;
	    return $rs;
	}

    /**
     * 获取首页推荐直播间
     */
    public function getIndexRecommend(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Home();

        $key="getIndexRecommend";
        $indexRecommendData=getcaches($key);
        if(!$indexRecommendData){
            $userdomain = new Domain_User();
            $indexRecommendData = $domain->getIndexRecommend();
            if (!empty($indexRecommendData)) {
                $indexRecommendData['bond_num'] = $userdomain->getBond($indexRecommendData['uid']);
            }
            setCaches($key,$indexRecommendData,2); 
        }
        if (!empty($indexRecommendData)) {
            $rs['info'][] = $indexRecommendData;
        }

        return $rs;
    }

    // 根据流名获取直播间信息
    public function getLiveInfo(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $stream=checkNull($this->stream);
        
        $live_fields='uid,title,city,stream,pull,thumb,isvideo,type,type_val,goodnum,anyway,starttime,isshop,game_action';
        $where = "islive= '1' AND stream = ?";
        $info=DI()->notorm->live
                ->select($live_fields)
                ->where($where,$stream)
                ->fetchOne();
        
        if (!empty($info)) {
            $userdomain = new Domain_User();
            $info=handleLive($info);
            $info['bond_num'] = $userdomain->getBond($info['uid']);
        } 

        $rs['info']=$info;
        return $rs;
    }

    // 新版搜索 按照版块搜索
    public function searchMulti() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $type=checkNull($this->type);
        $key=checkNull($this->key);
        $p=checkNull($this->p);
        if($key==''){
            $rs['code'] = 1001;
            $rs['msg'] = "请填写关键词";
            return $rs;
        }
        
        if(!$p){
            $p=1;
        }
        
        $domain = new Domain_Home();
        $info = $domain->searchMulti($type,$key,$p);

        $rs['info'] = $info;

        return $rs;
    }   

} 
