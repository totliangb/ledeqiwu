<?php

/**
* 收货地址
*/
class Api_Address extends PhalApi_Api
{
	public function getRules() {
		return array(
            'addAddress' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'title' => array('name' => 'title',  'type' => 'string','require' => true, 'desc' => '地址名称'),
				'name' => array('name' => 'name',  'type' => 'string','require' => true, 'desc' => '收货人名称'),
				'phone' => array('name' => 'phone',  'type' => 'string','require' => true, 'desc' => '手机号码'),
				'province' => array('name' => 'province',  'type' => 'int','require' => true, 'desc' => '省'),
				'city' => array('name' => 'city',  'type' => 'int','require' => true, 'desc' => '市'),
				'area' => array('name' => 'area',  'type' => 'int','require' => true, 'desc' => '区'),
				'info' => array('name' => 'info',  'type' => 'string','require' => true, 'desc' => '地址详情'),
				'is_default' => array('name' => 'is_default',  'type' => 'int','require' => true, 'min' => 0, 'default' => 1, 'desc' => '地址详情'),
			),
			'editAddress' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'address_id' => array('name' => 'address_id', 'type' => 'int', 'require' => true, 'desc' => '收货地址id'),
				'title' => array('name' => 'title',  'type' => 'string','require' => true, 'desc' => '地址名称'),
				'name' => array('name' => 'name',  'type' => 'string','require' => true, 'desc' => '收货人名称'),
				'phone' => array('name' => 'phone',  'type' => 'string','require' => true, 'desc' => '手机号码'),
				'province' => array('name' => 'province',  'type' => 'int','require' => true, 'desc' => '省'),
				'city' => array('name' => 'city',  'type' => 'int','require' => true, 'desc' => '市'),
				'area' => array('name' => 'area',  'type' => 'int','require' => true, 'desc' => '区'),
				'info' => array('name' => 'info',  'type' => 'string','require' => true, 'desc' => '地址详情'),
				'is_default' => array('name' => 'is_default',  'type' => 'int','require' => true, 'min' => 0, 'default' => 1, 'desc' => '地址详情'),
			),
			'getAddressList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
			),
			'getAddressInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'address_id' => array('name' => 'address_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '收货地址id'),
			),
			'delAddress' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'address_id' => array('name' => 'address_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '收货地址id'),
			),
			'defaultAddress' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'address_id' => array('name' => 'address_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '收货地址id'),
			),
		);
	}

	//设置默认收货地址
	public function defaultAddress()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		try {
			DI()->notorm->beginTransaction('db_appapi');

			DI()->notorm->shop_address->where('uid = ?', $this->uid)->update(['is_default' => 0]);

			if(!DI()->notorm->shop_address->where('uid = ? and id = ?', $this->uid, $this->address_id)->update(['is_default' => 1])) throw new \Exception('设置默认收货地址失败');
			
			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1008, 'msg' => $e->getMessage()];
			return ['code' => 1008, 'msg' => '设置默认收货地址失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['设置默认收货地址成功']];
	}

	//删除收货地址
	public function delAddress()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!DI()->notorm->shop_address->where('uid = ? and id = ?', $this->uid, $this->address_id)->delete()) return ['code'=>1002, 'msg' => '删除收货地址失败'];
		return ['code' => 0, 'msg' => '', 'info' => ['删除收货地址成功']];
	}

	//收货地址详情
	public function getAddressInfo()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$info = DI()->notorm->shop_address->select('id,title,name,phone,province,city,area,info,is_default')->where('uid = ? and id = ?', $this->uid, $this->address_id)->fetchOne();

		$area = DI()->notorm->shop_area->select('coding,name')->where('coding in ('.implode(',', [$info['province'],$info['city'],$info['area']]).')')->limit(3)->fetchAll();

		$area = replaceArrayKey($area, 'coding');

		if(isset($area[$info['province']])) $info['provincename'] = $area[$info['province']]['name'];
		if(isset($area[$info['city']])) $info['cityname'] = $area[$info['city']]['name'];
		if(isset($area[$info['area']])) $info['areaname'] = $area[$info['area']]['name'];

		return ['code' => 0, 'msg' => '', 'info' => $info];
	}

	//获取收货地址列表
	public function getAddressList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$list = DI()->notorm->shop_address->select('id,title,name,phone,is_default')->where('uid = ?', $this->uid)->order('is_default,id desc')->fetchAll();

		return ['code' => 0, 'msg' => '', 'info' => $list];
	}

	//修改收货地址
	public function editAddress()
	{
		$check_post = $this->check_post();
		if($check_post['code']) return $check_post;

		$edit = [
			'title' => $this->title,
			'name' => $this->name,
			'phone' => $this->phone,
			'province' => $this->province,
			'city' => $this->city,
			'area' => $this->area,
			'info' => $this->info,
			'is_default' => $this->is_default ? 1 : 0
		];

		try {
			DI()->notorm->beginTransaction('db_appapi');

			if($this->is_default)
			{
				if(!DI()->notorm->shop_address->where('uid = ?', $this->uid)->update(['is_default' => 0])) throw new \Exception('修改收货地址失败');
			}

			if(!DI()->notorm->shop_address->where('uid = ? and id = ?', $this->uid, $this->address_id)->update($edit)) throw new \Exception('修改收货地址失败');
			
			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1008, 'msg' => $e->getMessage()];
			return ['code' => 1008, 'msg' => '修改收货地址失败'];
		}
		
		return ['code' => 0, 'msg' => '', 'info' => ['修改收货地址成功']];
	}

	//添加收货地址
	public function addAddress()
	{
		$check_post = $this->check_post();
		if($check_post['code']) return $check_post;

		$add = [
			'uid' => $this->uid,
			'title' => $this->title,
			'name' => $this->name,
			'phone' => $this->phone,
			'province' => $this->province,
			'city' => $this->city,
			'area' => $this->area,
			'info' => $this->info,
			'is_default' => $this->is_default ? 1 : 0
		];


		try {
			DI()->notorm->beginTransaction('db_appapi');

			if($this->is_default && DI()->notorm->shop_address->where('uid = ? and is_default = 1', $this->uid)->sum('id'))
			{
				if(!DI()->notorm->shop_address->where('uid = ?', $this->uid)->update(['is_default' => 0])) throw new \Exception('添加收货地址失败');
			}

			if(!DI()->notorm->shop_address->insert($add)) throw new \Exception('添加收货地址失败');
			
			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1008, 'msg' => $e->getMessage()];
			return ['code' => 1008, 'msg' => '添加收货地址失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['添加收货地址成功']];
	}


	//判断提交
	private function check_post()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		if(mb_strlen(urldecode($this->title)) > 100 || mb_strlen(urldecode($this->title)) <= 0) return ['code'=>1000, 'msg' => '地址名称字数应在1~100位之间'];
		if(mb_strlen(urldecode($this->name)) > 20 || mb_strlen(urldecode($this->name)) <= 0) return ['code'=>1001, 'msg' => '收货人名称字数应在1~20位之间'];
		if(strlen($this->phone) != 11 || preg_match('/\D/', $this->phone)) return ['code'=>1002, 'msg' => '收货人手机号码错误'];
		if($this->province <= 0) return ['code'=>1003, 'msg' => '省行政编码错误'];
		if($this->city <= 0) return ['code'=>1004, 'msg' => '市行政编码错误'];
		if($this->area <= 0) return ['code'=>1005, 'msg' => '县行政编码错误'];
		if(mb_strlen(urldecode($this->info)) > 50 || mb_strlen(urldecode($this->info)) <= 0) return ['code'=>1006, 'msg' => '地址详情字数应在1~50位之间'];

		$area = DI()->notorm->shop_area->select('coding,parent_coding,level')->where('coding in ('.implode(',', [$this->province, $this->city, $this->area]).')')->limit(3) ->fetchAll();
		if(!$area || count($area) != 3) return ['code'=>1007, 'msg' => '收货地址错误'];
		$area = replaceArrayKey($area, 'coding');

		if(!isset($area[$this->province]) || $area[$this->province]['level'] != 1) return ['code'=>1008, 'msg' => '省行政编码错误'];
		if(!isset($area[$this->city]) || $area[$this->city]['level'] != 2 || $area[$this->city]['parent_coding'] != $this->province) return ['code'=>1009, 'msg' => '市行政编码错误'];
		if(!isset($area[$this->area]) || $area[$this->area]['level'] != 3 || $area[$this->area]['parent_coding'] != $this->city) return ['code'=>1010, 'msg' => '县、区行政编码错误'];

		return ['code' => 0];
	}
}