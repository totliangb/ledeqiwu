<?php
/**
 * 鉴宝
 */
if (!session_id()) session_start();
class Api_Appraise extends PhalApi_Api {

	public function getRules() {
        return array(
            'getAppraiseList' => array(
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
            'getAppraiseInfo' => array(
                'id' => array('name' => 'id', 'type' => 'int','require' => true, 'default'=>'1' ,'desc' => '鉴定ID'),
                'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
            ),
            'getViewAppraiseAnswerList' => array(
                'id' => array('name' => 'id', 'type' => 'int','require' => true, 'default'=>'1' ,'desc' => '鉴定ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
            'applyNormalAppraise' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'assort_id' => array('name' => 'assort_id', 'type' => 'string', 'require' => true, 'desc' => '分类'),
                'touid' => array('name' => 'touid', 'type' => 'int','require' => true, 'desc' => '专家用户ID'),
                'pics' => array('name' => 'pics', 'require' => true, 'type' => 'string', 'desc' => '图片'),
                'description' => array('name' => 'description', 'type' => 'string', 'require' => true, 'desc' => '描述'),
                // 'anonymous' => array('name' => 'anonymous', 'type' => 'int','require' => true, 'desc' => '是否匿名'),
            ),
            'appraisePay' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'desc' => '订单ID'),
                'order_sn' => array('name' => 'order_sn', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
                'pay_code' => array('name' => 'pay_code', 'type' => 'string', 'require' => true, 'desc' => '支付方式CODE'),
            ),
            'myAppraiseStatusOptions' => array(
                'type' => array('name' => 'type', 'type' => 'int', 'desc' => '用户类型 0 普通用户 1 鉴定专家'),
            ),
            'myAppraiseList' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'type' => array('name' => 'type', 'type' => 'int', 'desc' => '用户类型 0 普通用户 1 鉴定专家'),
                'pay_status' => array('name' => 'pay_status', 'type' => 'string', 'desc' => '支付状态'),
                'status' => array('name' => 'status', 'type' => 'string', 'desc' => '鉴定状态'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
            'applyAppraiseAnswerView' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'apply_id' => array('name' => 'apply_id', 'type' => 'int', 'desc' => '鉴定ID'),
            ),
            'appraiseAnswerViewPay' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'desc' => '订单ID'),
                'order_sn' => array('name' => 'order_sn', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
                'pay_code' => array('name' => 'pay_code', 'type' => 'string', 'require' => true, 'desc' => '支付方式CODE'),
            ),
            'expertAppraiseList' => array(
                'expert_uid' => array('name' => 'expert_uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
            'appraiseOrderChangeStatus' => array(
                'order_sn' => array('name' => 'order_sn', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
                'status' => array('name' => 'status', 'type' => 'string', 'require' => true, 'desc' => '订单状态（0 鉴定中 1 鉴定完成 2 鉴定超时）'),
                'pay_status' => array('name' => 'pay_status', 'type' => 'string','require' => true, 'desc' => '订单支付状态（0 待付款 1 已付款）'),
            ),
            'appraiseAnswerViewOrderChangeStatus' => array(
                'order_sn' => array('name' => 'order_sn', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
                'pay_status' => array('name' => 'pay_status', 'type' => 'string','require' => true, 'desc' => '订单支付状态（0 待付款 1 已付款）'),
            ),
        );
    }

    /**
     * 鉴宝 列表页展示
     */
    public function getAppraiseList()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $p=checkNull($this->p);
        
        if(!$p){
            $p=1;
        }
        
        $domain = new Domain_Appraise();
        $info = $domain->getAppraiseList($p);

        $rs['info'] = $info;

        return $rs;
    }

    /**
     * 鉴宝详细信息
     */
    public function getAppraiseInfo()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $uid = checkNull($this->uid);
        $id=checkNull($this->id);
        
        if(!$p){
            $p=1;
        }
        
        $domain = new Domain_Appraise();
        $info = $domain->getAppraiseInfo($id,$uid);

        // 进行浏览数+1操作
        // if (!empty($info) && $uid) {
        //     $new_view_num = $domain->updateAppraiseViewNum($info['id'],$uid,$info['view_num']);
        //     $info['view_num'] = $new_view_num;
        // }

        $rs['info'] = $info;

        return $rs;
    }

    /**
     * 查看申请查看鉴宝信息回复记录列表
     */
    public function getViewAppraiseAnswerList()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $id=checkNull($this->id);
        $p=checkNull($this->p);
        
        if(!$p){
            $p=1;
        }
        
        $domain = new Domain_Appraise();
        $info = $domain->getViewAppraiseAnswerList($id,$p);

        $rs['info'] = $info;

        return $rs;
    }

    /**
     * 申请普通鉴宝 
     */
    public function applyNormalAppraise()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];


        $data = [];

        if(isset($this->description))
        {
            if(mb_strlen($this->description) > 200) return ['code'=>1001, 'msg' => '描述字数不能大于200个字符'];
            $data['description'] = $this->description;
        }
        if (empty($this->pics)) {
            return ['code'=>1002, 'msg' => '信息不全，请补充图片'];
        }
        if (empty($this->assort_id)) {
            return ['code'=>1003, 'msg' => '信息不全，请置顶鉴宝分类'];
        }
        if (empty($this->touid)) {
            return ['code'=>1004, 'msg' => '信息不全，请指定鉴宝专家'];
        }

        $data['uid'] = checkNull($this->uid);
        $data['assort_id'] = json_decode($this->assort_id);
        $data['pics'] = $this->pics;
        $data['touid'] = checkNull($this->touid);
        // $data['anonymous'] = checkNull($this->anonymous);

        // 普通鉴定
        $data['type'] = 1;
        try {
            $domain = new Domain_Appraise();
            $result = $domain->applyAppraise($data);
            
            if ($result) {
                $rs['info'] = $result;
            }else{
                $rs['info'] = array('申请失败');
            }
        } catch (Exception $e) {
            $rs['code'] = 1005;
            $rs['msg'] = $e->getMessage();
        }

        return $rs;

    }

    /**
     * 鉴宝 支付
     */
    public function appraisePay()
    {
        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        // 校验订单ID 订单SN
        if(empty($this->order_id)){
            return ['code'=>1011, 'msg' => '传入参数：order_id'];
        }
        if(empty($this->order_sn)){
            return ['code'=>1011, 'msg' => '传入参数：order_sn'];
        }
        if(empty($this->pay_code)){
            return ['code'=>1011, 'msg' => '传入参数：pay_code'];
        }

        $order = DI()->notorm->appraise_apply->where("id=? and uid = ? and apply_order_sn = ? and pay_status = '0'", $this->order_id, $this->uid,$this->order_sn)->fetchOne();
        if(!$order) return ['code'=>1001, 'msg' => '订单已支付或已取消'];

        // 判断专家是否存在
        $expert = DI()->notorm->user_expert->select('uid')->where("uid = ? and apply_status = '1'",$order['touid'])->fetchOne();
        if(empty($expert)) return ['code'=>1002, 'msg' => '专家信息未找到'];

        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        try {
            // 调整order数据
            if(!empty($order)){
                $order['order_sn'] = $order['apply_order_sn'];
                // $order['pay_sn'] = $order['pay_sn'];
                $order['amount'] = $order['appraise_fee'];
            }

            $params = array(
                'code' => $this->pay_code,
                'notify_url' => '/Appapi/pay/appraise_notify_'.$this->pay_code,
            );

            // 第三方支付
            $domainPay = new Domain_Pay();
            $result = $domainPay->_api_pay($order,$params);

            $rs['info'][0]=$result;
        } catch (Exception $e) {
            $rs['code'] = 1002;
            $rs['msg'] = $e->getMessage();
        }

        return $rs; 
    }

    /**
     * 我的鉴定-状态选项及对应参数
     */
    public function myAppraiseStatusOptions()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $options = array();

        // 用户类型 0为普通用户 1为专家
        $type=checkNull($this->type);

        if($type == 1){
            // 专家
            // 待鉴定 已鉴定 鉴定失败
            $options[] = array('name'=>'待鉴定','params'=>array('type'=>'1','pay_status'=>'1','status'=>'0'));
            $options[] = array('name'=>'已鉴定','params'=>array('type'=>'1','pay_status'=>'1','status'=>'1'));
            $options[] = array('name'=>'鉴定失败','params'=>array('type'=>'1','pay_status'=>'1','status'=>'2'));
        }else if($type == 0){
            // 普通用户
            // 待付款 鉴定中 已鉴定 鉴定失败
            $options[] = array('name'=>'待付款','params'=>array('type'=>'0','pay_status'=>'0','status'=>'0'));
            $options[] = array('name'=>'鉴定中','params'=>array('type'=>'0','pay_status'=>'1','status'=>'0'));
            $options[] = array('name'=>'已鉴定','params'=>array('type'=>'0','pay_status'=>'1','status'=>'1'));
            $options[] = array('name'=>'鉴定失败','params'=>array('type'=>'0','pay_status'=>'1','status'=>'2'));
        }

        $rs['info'] = $options;

        return $rs;
    }

    /**
     * 我的鉴定
     */
    public function myAppraiseList()
    {   
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        // 用户类型 0为普通用户 1为专家
        $type=checkNull($this->type);
        // 支付状态 
        $pay_status = checkNull($this->pay_status);
        // 鉴定状态
        $status = checkNull($this->status);
        
        $uid=checkNull($this->uid);
        $p=checkNull($this->p);

        $other = array();
        $other['type'] = $type;
        $other['pay_status'] = $pay_status;
        $other['status'] = $status;
        if(!$p){
            $p=1;
        }
        
        $domain = new Domain_Appraise();
        $info = $domain->myAppraiseList($uid,$p,$other);

        $rs['info'] = $info;

        return $rs;
    }

    /**
     * 申请查看鉴宝结果
     */
    public function applyAppraiseAnswerView()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        $data = array();
        $data['apply_id'] = $this->apply_id;
        $data['uid'] = $this->uid;

        try {
            $domain = new Domain_Appraise();
            $info = $domain->applyAppraiseAnswerView($data);

            $rs['info'] = $info;
        } catch (Exception $e) {
            $rs['code']=1005;
            $rs['msg']=$e->getMessage();
        }

        return $rs;

    }

    /**
     * 查看鉴宝结果 支付
     */
    public function appraiseAnswerViewPay()
    {
        $checkToken = checkToken($this->uid,$this->token);
        if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        // 校验订单ID 订单SN
        if(empty($this->order_id)){
            return ['code'=>1011, 'msg' => '传入参数：order_id'];
        }
        if(empty($this->order_sn)){
            return ['code'=>1011, 'msg' => '传入参数：order_sn'];
        }
        if(empty($this->pay_code)){
            return ['code'=>1011, 'msg' => '传入参数：pay_code'];
        }

        $order = DI()->notorm->appraise_answer_view->where("id=? and uid = ? and view_order_sn = ? and pay_status = '0'", $this->order_id, $this->uid,$this->order_sn)->fetchOne();
        if(!$order) return ['code'=>1001, 'msg' => '订单已支付或已取消'];

        
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        try {
            // 调整order数据
            if(!empty($order)){
                $order['order_sn'] = $order['view_order_sn'];
                // $order['pay_sn'] = $order['pay_sn'];
                $order['amount'] = $order['view_fee'];
            }

            $params = array(
                'code' => $this->pay_code,
                'notify_url' => '/Appapi/pay/appraise_answer_notify_'.$this->pay_code,
            );

            // 第三方支付
            $domainPay = new Domain_Pay();
            $result = $domainPay->_api_pay($order,$params);

            $rs['info'][0]=$result;
        } catch (Exception $e) {
            $rs['code'] = 1002;
            $rs['msg'] = $e->getMessage();
        }

        return $rs;
    }

    // 专家鉴定记录 鉴定完成的
    public function expertAppraiseList()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        // 用户类型 0为普通用户 1为专家
        $type=1;
        // 支付状态 
        $pay_status = '1';
        // 鉴定状态
        $status = '1';
        
        $expert_uid=checkNull($this->expert_uid);
        $p=checkNull($this->p);

        $other = array();
        $other['type'] = $type;
        $other['pay_status'] = $pay_status;
        $other['status'] = $status;
        if(!$p){
            $p=1;
        }
        
        $domain = new Domain_Appraise();
        $info = $domain->myAppraiseList($expert_uid,$p,$other);

        $rs['info'] = $info;

        return $rs;
    }

    // 鉴宝订单更改订单状态-测试用接口
    public function appraiseOrderChangeStatus()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        if(empty($this->order_sn)){
            return ['code'=>1011, 'msg' => '传入参数：order_sn'];
        }

        $upData = array();
        $upData['status'] = $this->status;
        $upData['pay_status'] = $this->pay_status;

        $result = DI()->notorm->appraise_apply->where("apply_order_sn = ?", $this->order_sn)->update($upData);
        if(!$result){
            $rs['code'] = 1002;
            $rs['msg'] = '更新失败';
        }else{
            $rs['info'][0] = '更新成功';
        }

        return $rs;
    }

    // 查看鉴宝结果订单更改订单状态-测试用接口
    public function appraiseAnswerViewOrderChangeStatus()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        if(empty($this->order_sn)){
            return ['code'=>1011, 'msg' => '传入参数：order_sn'];
        }

        $upData = array();
        $upData['pay_status'] = $this->pay_status;

        $result = DI()->notorm->appraise_answer_view->where("view_order_sn = ?", $this->order_sn)->update($upData);
        if(!$result){
            $rs['code'] = 1002;
            $rs['msg'] = '更新失败';
        }else{
            $rs['info'][0] = '更新成功';
        }

        return $rs;
    }


}