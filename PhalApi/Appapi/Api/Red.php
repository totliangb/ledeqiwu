<?php
/**
 * 红包
 */
class Api_Red extends PhalApi_Api {

	public function getRules() {
		return array(
			'sendRed' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
                'stream' => array('name' => 'stream', 'type' => 'string',  'require' => true, 'desc' => '流名'),
				// 'type' => array('name' => 'type', 'type' => 'int',  'require' => true, 'desc' => '红包类型，0普通，1手气'),
				// 'type_grant' => array('name' => 'type_grant', 'type' => 'int',  'require' => true, 'desc' => '发放类型，0立即 1延迟'),
				'amount' => array('name' => 'amount', 'type' => 'int',  'require' => true, 'desc' => '红包金额'),
				'nums' => array('name' => 'nums', 'type' => 'int', 'require' => true, 'desc' => '数量'),
                'mins' => array('name' => 'mins', 'type' => 'int', 'require' => true, 'desc' => '延迟抢红包时间'),
                'des' => array('name' => 'des', 'type' => 'string', 'default'=>'恭喜发财，大吉大利', 'desc' => '描述'),
			),
            
            'getRedList' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'stream' => array('name' => 'stream', 'type' => 'string',  'require' => true, 'desc' => '流名'),
                'sign' => array('name' => 'sign', 'type' => 'string',  'require' => true, 'desc' => '签名'),
			),
            'robRed' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
                'stream' => array('name' => 'stream', 'type' => 'string',  'require' => true, 'desc' => '流名'),
                'redid' => array('name' => 'redid', 'type' => 'int',  'require' => true, 'desc' => '红包ID'),
                'sign' => array('name' => 'sign', 'type' => 'string',  'require' => true, 'desc' => '签名'),
			),
            'getRedRobList' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'stream' => array('name' => 'stream', 'type' => 'string',  'require' => true, 'desc' => '流名'),
                'redid' => array('name' => 'redid', 'type' => 'int',  'require' => true, 'desc' => '红包ID'),
                'sign' => array('name' => 'sign', 'type' => 'string',  'require' => true, 'desc' => '签名'),
			),
            'getRedNow' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'stream' => array('name' => 'stream', 'type' => 'string',  'require' => true, 'desc' => '流名'),
                'sign' => array('name' => 'sign', 'type' => 'string',  'require' => true, 'desc' => '签名'),
            ),
            'sharePageEnterUserHandle' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
                'code' => array('name' => 'code', 'type' => 'string', 'require' => true, 'desc' => '分享码'),
            ),
		);
	}

    //生成唯一订单号
    private function makeRedSn()
    {
        $time = explode(' ', microtime());
        return $time[1].intval($time[0] * 1000000).mt_rand(10000, 99999);
    }

	/**
	 * 发送红包
	 * @desc 用于 发送红包
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].redid 红包ID
	 * @return string msg 提示信息
	 */
	public function sendRed() {
		$rs = array('code' => 0, 'msg' => '发送成功', 'info' => array());
		
		$uid=$this->uid;
        $token=checkNull($this->token);
        $stream=checkNull($this->stream);
		$type=1;
		$type_grant=1;
		$amount=$this->amount;
		$nums=$this->nums;
        $mins=$this->mins;
		$des=checkNull($this->des);

		$red_sn = $this->makeRedSn();
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		
		if((int)$amount==0){
			$rs['code']=1002;
			$rs['msg']='请输入正确的金额';		
			return $rs;						
		}		
        
        if((int)$nums==0){
			$rs['code']=1003;
			$rs['msg']='请输入正确的个数';		
			return $rs;						
		}
        
        if($type==0){
            /* 平均 */
            // $avg=$amount ;
            // $amount=$avg*$nums;
        }else{
            if($nums > $amount){
                $rs['code']=1004;
                $rs['msg']='红包数量不能超过红包金额';		
                return $rs;
            }
        }        
        
        if(mb_strlen($des)>20){
            $rs['code']=1004;
			$rs['msg']='红包名称最多20个字';		
			return $rs;
        }

		
        $stream_a=explode("_",$stream);
        $liveuid=$stream_a[0];
        $showid=$stream_a[1];
        if((int)$liveuid==0 || (int)$showid==0){
            $rs['code']=1007;
            $rs['msg']='信息错误';		
            return $rs;
        }
        
        $nowtime=time();
        $addtime=$nowtime;
        $effecttime=$nowtime;
        if($type_grant==1){
            $effecttime=$nowtime + $mins * 60;
        }

        try {
            DI()->notorm->beginTransaction('db_appapi');

            // 校验当前直播间是否存在未结束红包
            $hasRedFlag = DI()->notorm->red
                    ->where('liveuid = ? AND showid = ? AND endtime = 0 AND status = 0',$liveuid,$showid)
                    ->fetchOne();
            if(!empty($hasRedFlag)){
                throw new Exception('当前直播间存在未结束的红包');
            }

            $data=array(
                "uid"=>$uid,
                "red_sn"=>$red_sn,
                "liveuid"=>$liveuid,
                "showid"=>$showid,
                "type"=>$type,
                "type_grant"=>$type_grant,
                "amount"=>$amount,
                "nums"=>$nums,
                "mins"=>$mins,
                "des"=>$des,
                "effecttime"=>$effecttime,
                "status"=>0,
                "addtime"=>$addtime,
            );
            $domain = new Domain_Red();
            $result = $domain->sendRed($data);
            if($result['code']!=0){ 
                throw new Exception($result['msg']);
            }

            // 扣除相应余额
            $now_user_info = DI()->notorm->user
                            ->select("user_nicename")
                            ->where('id=?',$uid)
                            ->fetchOne();
            $data_log = array();
            $data_log['uid'] = $uid;
            $data_log['nickname'] = $now_user_info['user_nicename'];
            $data_log['amount'] = $amount;
            $data_log['order_sn'] = $red_sn;

            $pdModel = new Model_Pd();
            $pdFlag = $pdModel->changePd('red_pay',$data_log);
            if(!$pdFlag) throw new Exception('扣除金额失败');

            // redis 处理
            $redinfo=$result['info'];
            
            $redid=$redinfo['id'];
            
            $key='red_list_'.$stream;
            DI()->redis->rPush($key,$redid);
            
            $key2='red_list_'.$stream.'_'.$redid;
            $red_list=$this->redlist($amount,$nums,$type);
            foreach($red_list as $k=>$v){
                DI()->redis->rPush($key2,$v);
            }

            // 添加倒计时
            $configpri=getConfigPri();
            $red_end_mins = isset($configpri['red_end_mins']) ? $configpri['red_end_mins'] : 1;
            $key3='red_list_'.$stream.'_'.$redid.'_overtime';
            // 毫秒单位
            $overtime = ($effecttime + ($red_end_mins * 60) )*1000;
            DI()->redis->set($key3,$overtime);

            $rs['info'][0]['redid']=(string)$redid;

            DI()->notorm->commit('db_appapi');
        } catch (Exception $e) {
            DI()->notorm->rollback('db_appapi');
            $rs['code']=1007;
            $rs['msg']=$e->getMessage();
        }

		return $rs;			
	}		

    /**
     * 获取当前直播间红包
     */
    public function getRedNow(){
        // 生成测试签名
        // "stream=" + stream + "&" + SALT
        // 76576076c1f5f657b634e966c8836a06
        // echo md5("stream=37405_444444&76576076c1f5f657b634e966c8836a06");exit;
        // 25ade3499ec46e6949cd3d960d204a17

        $configpri=getConfigPri();
        $configpub = getConfigPub();
        $red_end_mins = isset($configpri['red_end_mins']) ? $configpri['red_end_mins'] : 1;

        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $sign=checkNull($this->sign);
        $stream=checkNull($this->stream);
        
        $checkdata=array(
            'stream'=>$stream
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
            $rs['msg']='签名错误';
            return $rs; 
        }
        
        $stream_a=explode("_",$stream);
        $liveuid=$stream_a[0];
        $showid=$stream_a[1];
        if((int)$liveuid==0 || (int)$showid==0){
            $rs['code']=1007;
            $rs['msg']='信息错误';      
            return $rs;
        }

        $domain = new Domain_Red();
        $result = $domain->getRedNow($liveuid,$showid);

        if(!empty($result)){

            $userinfo=getUserInfo($result['uid']);
            
            $result['user_nicename']=$userinfo['user_nicename'];
            $result['avatar']=get_upload_path($userinfo['avatar']);
            $result['avatar_thumb']=get_upload_path($userinfo['avatar_thumb']);

            // 是否允许进行抢红包操作
            $isrob='0';

            // 分享进来的人数
            $ifshared = 0;

            // 已抢到金额
            $win_amount = 0;

            // 开抢时间
            // $result['effecttime']
            if($value['status'] == 0){
                // 预计结束时间 = 开抢时间+设置的自动结束时间
                $result['overtime'] = $result['effecttime'] + ($red_end_mins * 60);

                if($uid){
                    // 已登录用户
                    // 判断是否允许进行抢红包操作
                    $key='red_user_winning_'.$stream.'_'.$result['id'];
                    $key2='red_list_'.$stream.'_'.$result['id'];
                    $key3='red_list_shared_'.$stream.'_'.$result['id'];
                    $ifwin=DI()->redis->zScore($key,$uid);
                    if($ifwin==false){
                        $ifexist=DI()->redis->exists($key2);
                        // 验证是否为分享成功的用户
                        $ifshared=DI()->redis->zScore($key3,$uid);
                        if($ifexist && $ifshared){
                            $isrob='1';
                        }
                    }else{
                        $win_amount = $ifwin;
                    }
                }
            }else{
                // 结束时间 红包抢完或自动结束时间到
                $result['overtime'] = $result['endtime'];
            }

            $result['shared_num'] = $ifshared ? $ifshared : 0;

            // 是否已经抢过了
            $result['is_robed'] = $ifwin ? 1 : 0;
            $result['win_amount'] = $win_amount ? $win_amount : 0;

            // 分享链接地址
            $share_url = '';
            if($uid){
                // 分享码加密
                $share_code =  $uid."_".$stream."_".$result['id'];
                $share_encode_str = LRSCEncode($share_code);
                $share_url = $configpub['site'].'/appapi/agent/redSharePage?code='.$share_encode_str;
            }
            $result['share_url'] = $share_url;

            // 关闭界面倒计时时长
            $result['close_red_view_mins'] = 5;

            $result['isrob']=$isrob;
        }

        return $result;
    }

    /**
     * 分享页面进入直播间后的分享码处理
     */
    public function sharePageEnterUserHandle()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $checkToken=checkToken($this->uid,$this->token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $code=checkNull($this->code);

        // 解密分享码
        $codeData = LRSCDecode($code);
        $codeData = explode("_",$codeData);

        $run_flag = false;

        // 发起分享的用户ID
        $uid = $codeData['0'];
        $liveuid = $codeData['1'];
        $show_id = $codeData['2'];
        $red_id = $codeData['3'];
        $stream = $liveuid."_".$show_id;

        // 校验当前用户和进入直播间的用户ID是否相同（分享者自己点击分享链接进入直播间）
        if($uid != $this->uid){
            $run_flag = true;
        }

        // 校验用户
        if ($run_flag) {
            $run_flag = false;
            $item_user_info = DI()->notorm->user->select('*')->where('id=?',$uid)->fetchOne();
            if(!empty($item_user_info)){
                $run_flag = true;
            }
        }

        if ($run_flag) {
            $run_flag = false;
            // 校验直播间是否正在进行中
            $live_model = new Model_Live();
            $live_status = $live_model->checkLiveing($liveuid,$stream);
            $live_status = '1';
            if($live_status == '1'){
                // 直播中
                $run_flag = true;
            }
        }

        if ($run_flag) {
            $run_flag = false;
            // 校验红包是否为当前直播间红包 且 还未抢完
            $red_model = new Model_Red();
            $redInfo = $red_model->getRedNow($liveuid,$show_id);
            if(!empty($redInfo)){
                if($redInfo['id'] == $red_id && $redInfo['status']=='0'){
                    // 当前红包不是分享的红包
                    $run_flag = true;
                }
            }
        }

        if($run_flag){
            // 更新Redis 记录可以抢红包的用户
            $key = "red_list_shared_".$stream."_".$red_id;
            $key2 = "red_list_shared_".$stream."_".$red_id."_".$uid;

            // 校验进来的用户ID 是否已有记录
            $hasFlag = DI()->redis->zScore($key2,$this->uid);
            if (!$hasFlag) {
                // 记录分享页进来的用户ID
                DI()->redis->zAdd($key2,1,$this->uid);

                $old_score = DI()->redis->zScore($key,$uid);
                $old_score = $old_score ? $old_score : 0;
                $score = $old_score + 1;
                DI()->redis->zAdd($key,$score,$uid);
            }
        }

        return $rs;
    }

	/**
	 * 获取红包列表
	 * @desc 用于 获取红包列表
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[].id 红包ID 
	 * @return string info[].uid 发布者ID 
	 * @return string info[].type 红包类型 
	 * @return string info[].type_grant 发放类型 
	 * @return string info[].second 剩余时间(秒) 
	 * @return string info[].isrob 是否能抢 
	 * @return string msg 提示信息
	 */
	public function getRedList() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $sign=checkNull($this->sign);
        $stream=checkNull($this->stream);
        
        $checkdata=array(
            'stream'=>$stream
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
        $stream_a=explode("_",$stream);
        $liveuid=$stream_a[0];
        $showid=$stream_a[1];
        if((int)$liveuid==0 || (int)$showid==0){
            $rs['code']=1007;
            $rs['msg']='信息错误';		
            return $rs;
        }
        
        $domain = new Domain_Red();
		$result = $domain->getRedList($liveuid,$showid);
        
        $nowtime=time();
        foreach($result as $k=>$v){
            $userinfo=getUserInfo($v['uid']);
            
            $v['user_nicename']=$userinfo['user_nicename'];
            $v['avatar']=$userinfo['avatar'];
            $v['avatar_thumb']=$userinfo['avatar_thumb'];
            $v['second']='0';
            if($v['type_grant']==1){
                if($v['effecttime']>$nowtime){
                    $v['second']=(string)($v['effecttime']-$nowtime);
                }
            }
            $isrob='0';
            
            $key='red_user_winning_'.$stream.'_'.$v['id'];
            $key2='red_list_'.$stream.'_'.$v['id'];
            $ifwin=DI()->redis->zScore($key,$uid);
            if($ifwin==false){
                $ifexist=DI()->redis->exists($key2);
                if($ifexist){
                    $isrob='1';
                }
            }
            $v['isrob']=$isrob;
            $result[$k]=$v;
            
        }
		
        
        $rs['info']=$result;
        
        return $rs;
        
    }
    
	/**
	 * 抢红包
	 * @desc 用于 用户抢红包
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0] 
	 * @return string info[0].win 抢到的红包金额，0表示没抢到
	 * @return string info[0].msg 提示信息
	 * @return string msg 提示信息
	 */
	public function robRed() {
        // 生成测试签名
        // "redid=" + redPackId + "&stream=" + stream + "&uid=" + uid + "&" + SALT
        // 76576076c1f5f657b634e966c8836a06
        // echo md5("redid=32&stream=37405_444444&uid=37405&76576076c1f5f657b634e966c8836a06");exit;
        // 00ef582709a256271a86431d58aa5dae

        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $stream=checkNull($this->stream);
        $redid=checkNull($this->redid);
        $sign=checkNull($this->sign);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
        $checkdata=array(
            'uid'=>$uid,
            'redid'=>$redid,
            'stream'=>$stream,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        }
        
        $nowtime=time();
        $key='red_user_winning_'.$stream.'_'.$redid;
        $key2='red_list_'.$stream.'_'.$redid;
        $key3='red_list_shared_'.$stream.'_'.$redid;
        
        $result=array(
            'win'=>'0',
            'msg'=>'手慢了，红包派完了',
        );
        
        $ifwin=DI()->redis->zScore($key,$uid);
        if($ifwin==false){
            $ifexist=DI()->redis->exists($key2);
            $ifshared=DI()->redis->zScore($key3,$uid);

            if(!$ifshared){
                $result['win']='0';
                $result['msg'] = '还未完成分享';
            }
            if(!$ifexist){
                $result['win']='0';
                $result['msg'] = '未找到红包信息';
            }

            if($ifexist && $ifshared){
                $amount=DI()->redis->lPop($key2);
                if($amount>0){
                    
                    $stream_a=explode("_",$stream);
                    $liveuid=$stream_a[0];
                    $showid=$stream_a[1];
        
                    try {
                        DI()->notorm->beginTransaction('db_appapi');


                        $data=array(
                            'uid'=>$uid,
                            'record_sn'=>$this->makeRedSn(),
                            'redid'=>$redid,
                            'amount'=>$amount,
                            'showid'=>$showid,
                            'addtime'=>$nowtime
                        );
                        
                        $domain = new Domain_Red();
                        $result2 = $domain->robRed($data);
                        $score=$amount;
                        DI()->redis->zAdd($key,$score,$uid);
                        
                        $result['win']=(string)$amount;
                        $result['msg'] = '';
                        $rs['info'][0]=$result;

                        DI()->notorm->commit('db_appapi');
                    } catch (Exception $e) {
                        DI()->notorm->rollback('db_appapi');
                        // 将金额退回
                        DI()->redis->lPush($key2,$amount);
                        $rs['code']=1007;
                        $rs['msg']=$e->getMessage();
                    }
                }
            }else{
                $rs['info'][0]=$result;
            }
            
        }else{
            $amount = $ifwin;
            $result['win']=(string)$amount;
            $result['msg'] = '';
            $rs['info'][0]=$result;
        }
        
        return $rs;
        
    }
    	
    /**
	 * 红包领取列表
	 * @desc 用于 获取红包领取列表
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return object info[0].redinfo 红包信息
	 * @return string info[0].redinfo.coin 总金额
	 * @return string info[0].redinfo.nums 总数量
	 * @return string info[0].redinfo.coin_rob 已抢金额
	 * @return string info[0].redinfo.nums_rob 已抢数量
     * @return array info[0].list 领取列表
     * @return string info[0].win 抢到金额，0表示未抢到
	 * @return string msg 提示信息
	 */
	public function getRedRobList() {
        // 生成测试签名
        // "redid=" + redPackId + "&stream=" + stream + "&" + SALT
        // 76576076c1f5f657b634e966c8836a06
        // echo md5("redid=32&stream=37405_444444&76576076c1f5f657b634e966c8836a06");exit;
        // 00ef582709a256271a86431d58aa5dae

        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $sign=checkNull($this->sign);
        $stream=checkNull($this->stream);
        $redid=checkNull($this->redid);
        
        $checkdata=array(
            'redid'=>$redid,
            'stream'=>$stream,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
			$rs['msg']='签名错误';
			return $rs;	
        } 
        
        $stream_a=explode("_",$stream);
        $liveuid=$stream_a[0];
        $showid=$stream_a[1];
        
        $domain = new Domain_Red();
		$redinfo = $domain->getRedInfo($redid);
        if(!$redinfo){
            $rs['code']=1002;
			$rs['msg']='红包不存在';
			return $rs;	
        }
        
        // 获取直播头像
        $live_model = new Model_Live();
        $liveinfo = $live_model->getLiveInfo($redinfo['uid']);

        $senduserinfo=getUserInfo($redinfo['uid']);

        $redinfo['user_nicename']=$senduserinfo['user_nicename'];
        $redinfo['avatar']=get_upload_path($senduserinfo['avatar']);
        $redinfo['avatar_thumb']=get_upload_path($senduserinfo['avatar_thumb']);
        
        $list=array();
        $win=0;

        $win_list=$domain->getRedRobList($redid);
        foreach($win_list as $k=>$v){
            $userinfo=getUserInfo($v['uid']);

            $amount=$v['amount'];

            
            if($v['uid']==$uid){
                $win=$amount;
            }
            
            $data=array(
                'uid'=>$userinfo['id'],
                'user_nicename'=>$userinfo['user_nicename'],
                'avatar'=>get_upload_path($userinfo['avatar']),
                'avatar_thumb'=>get_upload_path($userinfo['avatar_thumb']),
                'win'=>$amount,
                'time'=>date('H:i:s',$v['addtime']),
            );
            $list[]=$data;
            
        }
        
        $rs['info'][0]['redinfo']=$redinfo;
        $rs['info'][0]['list']=$list;
        $rs['info'][0]['win']=(string)$win;

        // 直播间头像
        $rs['info'][0]['thumb'] = $liveinfo['thumb'];
        
        return $rs;
    }
    
  
    /**
     * 分配红包个数
     * @param int $total
     */
    protected function redlist($total,$nums,$type){
        if($type==1){
            /* 手气红包 */
            $list=$this->makeRedPacket($total,$nums);
        }else{
            /* 平均红包 */
            $list=$this->red_average($total,$nums);
        }
        
        return $list;
    }

    /**
     * 平分红包
     * @param int $total
     */
    protected function red_average($total,$nums){
        $coin=floor($total/$nums);
        $list = array();
        for($i=0;$i<$nums;$i++){
            $list[]=$coin;
        }

        return $list;
    }

    /**
     * 预生成好，红包随机队列
     * @param int $total
     */
    protected function red_rand_list($total){
        $list = array();
        while ($total > 0) {
            $diamonds = mt_rand(1, 20);//随机取：1至20中的一个数字
            if ($total >= $diamonds) {
                $total = $total - $diamonds;
                $list[] = $diamonds;
            } else {
                if ($total >= 1) {
                    $diamonds = 1;
                    $total = $total - $diamonds;
                    $list[] = $diamonds;
                }
            }
        }

        return $list;
    }

    /**
     * 把$total 生成指定数量$num的，随机列表数
     * @param int $total
     * @param int $num
     * @return multitype:number
     */
    protected function red_rand_list2($total, $num){
        $list = array();
        if ($num > $total) {
            $num = $total;
        }
        //先生成一批为：1 的
        for ($x = 0; $x < $num; $x++) {
            $list[] = 1;
            $total = $total - 1;
        }

        while ($total > 0) {
            foreach ($list as $k => $v) {
                $diamonds = mt_rand(1, 19);//随机取：1至20中的一个数字
                if ($total >= $diamonds) {
                    $total = $total - $diamonds;
                } else {
                    if ($total >= 1) {
                        $diamonds = 1;
                        $total = $total - $diamonds;
                    }
                }

                $list[$k] = $v + $diamonds;
                if ($total == 0) {
                    break;
                }
            }
        };

        return $list;
    }

    function makeRedPacket($money, $n, $rate = 0.5)
    {
        $money = $money * 100;

        //每个红包先保留1分钱
        $hold = $n;
        //分剩下的钱
        $remainder = $money - $hold;
        $result = [];
        for($i = 1; $i <= $n; $i++){
            //如果剩余的钱没有了就给1分钱
            if($remainder <= 0){
                $result[] = 1;
            }
            else{
                $max = floor($remainder * $rate);
                $rand = mt_rand(1, $max);
                //把保留的1分钱加进去
                $result[] = $rand + 1;
                //剩余的钱需要减去刚才发出去的
                $remainder -= $rand;
            }
        }
        //如果剩余的钱没有分配完直接给到第一个元素
        if($remainder > 0){
            $result[0] += $remainder;
        }
        //把数组随机打乱
        shuffle($result);

        // 转化为 元单位
        foreach ($result as $key => &$value) {
            $value = sprintf("%.2f",$value/100);
        }
        return $result;
    }

}
