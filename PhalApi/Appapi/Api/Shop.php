<?php
/**
 * 店铺管理
 */
class Api_Shop extends PhalApi_Api {
	public function getRules() {
		return array(
            'application' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'name' => array('name' => 'name', 'type' => 'string', 'require' => true, 'desc' => '店铺名称'),
				'shop_img' => array('name' => 'shop_img', 'type' => 'string', 'desc' => '店铺头像'),
				'assort_id' => array('name' => 'assort_id', 'type' => 'string', 'require' => true, 'desc' => '店铺分类'),
				'description' => array('name' => 'description', 'type' => 'string', 'desc' => '店铺描述'),
				'certificate_img' => array('name' => 'certificate_img', 'type' => 'string', 'desc' => '营业执照照片'),
			),
			'getShop' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token')
			),
			'isShop' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token')
			),
			'edit'=> array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'shop_id' => array('name' => 'shop_id', 'type' => 'int', 'require' => true, 'desc' => '店铺id'),
				'shop_img' => array('name' => 'shop_img', 'type' => 'string', 'desc' => '店铺头像'),
				'description' => array('name' => 'description', 'type' => 'string', 'desc' => '店铺描述'),
				'certificate_img' => array('name' => 'certificate_img', 'type' => 'string', 'desc' => '营业执照照片'),
			),
			'getList' => array(
				'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1,'desc' => '页数')
			),
			'addAccount' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 1,'default'=>1, 'require' => true, 'desc' => '分类'),
				'account_bank' => array('name' => 'account_bank', 'type' => 'string', 'desc' => '银行名称'),
				'name' => array('name' => 'name', 'type' => 'string','require' => true,  'desc' => '姓名'),
				'account' => array('name' => 'account', 'type' => 'string','require' => true,  'desc' => '账号'),
			),
			'editAccount' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'account_id' => array('name' => 'account_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '账户id'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 1,'default'=>1, 'require' => true, 'desc' => '分类'),
				'account_bank' => array('name' => 'account_bank', 'type' => 'string', 'desc' => '银行名称'),
				'name' => array('name' => 'name', 'type' => 'string','require' => true,  'desc' => '姓名'),
				'account' => array('name' => 'account', 'type' => 'string','require' => true,  'desc' => '账号'),
			),
			'accountList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
			),
			'shopBalance' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
			),
			'delAccount' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'account_id' => array('name' => 'account_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '账户id'),
			),
			'shopWithdraw' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'account_id' => array('name' => 'account_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '账户id'),
				'amount' => array('name' => 'amount', 'type' => 'float', 'min' => 1, 'require' => true, 'desc' => '提现金额'),
			),
			'shopStatistics' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
			),
			'shopSalesList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'StartTime' => array('name' => 'StartTime', 'type' => 'string', 'require' => true, 'desc' => '开始时间'),
				'EndTime' => array('name' => 'EndTime', 'type' => 'string', 'require' => true, 'desc' => '结束时间'),
			),
			'orderList' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'startTime' => array('name' => 'StartTime', 'type' => 'string', 'desc' => '开始时间'),
				'endTime' => array('name' => 'EndTime', 'type' => 'string', 'desc' => '结束时间'),
				'type' => array('name' => 'type' ,'type' => 'int', 'default' => "-1", 'desc' => '类型'),
				'p' => array('name' => 'p', 'type' => 'int', 'default' => 1, 'desc' => '分页')
			),
			'orderInfo' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'desc' => '订单ID')
			), 
			'add_logistic' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'desc' => '订单ID'),
				'numbering' => array('name' => 'numbering', 'type' => 'string', 'require' => true, 'desc' => '订单号'),
				'address_id' => array('name' => 'address_id', 'type' => 'int', 'default' => "", 'desc' => '地址ID'),
				'address_info' => array('name' => 'address_info', 'type' => 'string', 'default' => "", 'desc' => '详细地址'),
				'logistic_id' => array('name' => 'logistic_id', 'type' => 'int', 'require' => true, 'desc' => '快递ID'),
				'logistic_num' => array('name' => 'logistic_num', 'type' => 'string', 'require' => true, 'desc' => '快递单号'),
			),
			'reply_list' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'type' => array('name' => 'type', 'type' => "int", 'default' => 0, 'desc' => '显示方式 0全部 1未评论 2已评论'),
				'p' => array('name' => 'p','type' => 'int', 'default' => 1, 'desc' => "页码")
			),
			'add_reply' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'desc' => '订单号'),
				'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '回复内容')
			),
			'disputeList' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'p' => array('name' => 'p','type' => 'int', 'default' => 1, 'desc' => "页码")
			),
			'disputeInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'dispute_id' => array('name' => 'dispute_id', 'type' => 'int','require' => true, 'desc' => '纠纷id'),
			),
			'redList' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'p' => array('name' => 'p','type' => 'int', 'default' => 1, 'desc' => "页码")
			),
            'completion' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'assort_id' => array('name' => 'assort_id', 'type' => 'string', 'require' => true, 'desc' => '店铺分类'),
			),
		);
	}

	//店铺指定时间销售列表
	public function shopSalesList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$StartTime = strtotime(date('Y-m-d', strtotime($this->StartTime)).' 00:00:00');
		$EndTime = strtotime(date('Y-m-d', strtotime($this->EndTime)).' 23:59:59');

		if(!$StartTime || !$EndTime)  return ['code'=>700, 'msg' => '时间格式错误'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1000, 'msg' => '店铺不存在或已关闭'];

		$list = DI()->notorm->shop_order->select('id,numbering,shop_id,state,payment_amount,pay_time')->where('shop_id = ? and state >= 20 and pay_time between ? and ?', $shop['id'],$StartTime, $EndTime)->fetchAll();

		$amount = 0;
		foreach ($list as $key => &$value) 
		{
			$value['pay_time'] = date('Y-m-d H:i:s', $value['pay_time']);
			$amount += $value['payment_amount'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['amount' => $amount, 'list' => $list ? [$list] : []]];
	}

	//店铺数据统计
	public function shopStatistics()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1000, 'msg' => '店铺不存在或已关闭'];

		$time = [strtotime(date('Y-m-d').' 00:00:00'), strtotime(date('Y-m-d').' 23:59:59')];

		$list = DI()->notorm->shop_commodity->select('state')->where('shop_id = ?', $shop['id'])->fetchAll();

		$commodityNum = $UnlistedGoods = $PendingProducts = 0;

		foreach ($list as $key => $value) 
		{
			switch ($value['state']) {
				case 0:
					$PendingProducts++;
					break;
				case 1:
					$commodityNum++;
					break;
				case 2:
					$UnlistedGoods++;
					break;
			}
		}

		$SalesToday = DI()->notorm->shop_order->where('shop_id = ? and state >= 20 and pay_time between ? and ?', $shop['id'],$time[0], $time[1])->sum('payment_amount');

		$info = [
			//今日销售额
			'SalesToday' => $SalesToday ? $SalesToday : '0',
			//今日订单数
			'TodayOrder' => DI()->notorm->shop_order->where('shop_id = ? and state >= 20 and pay_time between ? and ?', $shop['id'],$time[0], $time[1])->count('id'),
			//退款申请
			'RefundApplication' => DI()->notorm->shop_refund->where('shop_id = ? and state = 0', $shop['id'])->count('id'),
			//待发货订单
			'PendingOrders' => DI()->notorm->shop_order->where('shop_id = ? and state = 20', $shop['id'])->count('id'),
			//入驻商品数
			'commodityNum' => $commodityNum,
			//待审核商品
			'PendingProducts' => $PendingProducts,
			//已下架商品
			'UnlistedGoods' => $UnlistedGoods
		];

		return ['code' => 0, 'msg' => '', 'info' => $info];
	}

	//查看店铺状态
	public function isShop()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();

		return ['code' => 0, 'msg' => '', 'info' => (empty($shop)) ? ['is_shop' => 0, 'shop_id' => 0] : ['is_shop' => 1, 'shop_id' => intval($shop['id'])]];
	}

	//店铺提现
	public function shopWithdraw()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop = DI()->notorm->shop->select('id,name,balance,balance_freeze')->where('state=1 and uid=?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1000, 'msg' => '店铺不存在'];

		$config=getConfigPri();

		if($this->amount < $config['cash_min']) return ['code'=>1002, 'msg' => '提现金额不得低于'.$config['cash_min']."元"];

		if(!DI()->notorm->cash_account->select('id')->where('uid=?',$this->uid)->fetchOne()) return ['code'=>1001, 'msg' => '账户不存在'];

		if($shop['balance'] < $this->amount)  return ['code'=>1002, 'msg' => '提现金额不足'];

		try {
			DI()->notorm->beginTransaction('db_appapi');

			if(!DI()->notorm->shop_withdraw->insert([
				'shop_id' => $shop['id'],
				'account_id' => $this->account_id,
				'amount' => $this->amount,
				'orderno' => $uid.'_'.time().rand(100,999),
				'addtime' => time()
			])) throw new Exception("申请失败");
			
			if(!DI()->notorm->shop->where('id=?',$shop['id'])->update([
				'balance' => new NotORM_Literal("balance - ".$this->amount),
				'balance_freeze' => new NotORM_Literal("balance_freeze + ".$this->amount),
			])) throw new Exception("申请失败");

			if(!DI()->notorm->shop_amount_log->insert([
				'shop_id' => $shop['id'],
				'order_id' => 0,
				'type' => 1,
				'amount' => $this->amount,
				'type2' => 1,
				'addtime' => time(),
				'text' => '用户申请提现'
			])) throw new Exception("申请失败");

			if(!DI()->notorm->shop_amount_log->insert([
				'shop_id' => $shop['id'],
				'order_id' => 0,
				'type' => 0,
				'amount' => $this->amount,
				'type2' => 0,
				'addtime' => time(),
				'text' => '用户申请提现'
			])) throw new Exception("申请失败");

			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');

			return ['code' => 1008, 'msg' => '申请提现失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['申请提现成功']];
	}

	//商家余额
	public function shopBalance()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop = DI()->notorm->shop->select('id,name,balance,balance_freeze')->where('state=1 and uid=?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1004, 'msg' => '店铺不存在'];

		if($shop)
		{
			$shop['balance'] = $shop['balance'] ? $shop['balance'] : '0';
			$shop['balance_freeze'] = $shop['balance_freeze'] ? $shop['balance_freeze'] : '0';
		}

		return ['code' => 0, 'msg' => '', 'info' => $shop ? $shop : []];
	}


	//账户列表
	public function accountList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1004, 'msg' => '店铺不存在'];

		$list = DI()->notorm->cash_account->select('id,type,account_bank,name,account')->where('shop_id=? and type2 = 1',$shop['id'])->fetchAll();

		if($list)
		{
			foreach ($list as $key => &$value) 
			{
				$value['account_bank'] = empty($value['account_bank']) ? '':$value['account_bank'];
			}
		}

		return ['code' => 0, 'msg' => '', 'info' => $list ? $list : []];
	}

	//删除账户
	public function delAccount()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1004, 'msg' => '店铺不存在'];

		if(!DI()->notorm->cash_account->where('id=? and shop_id=?',$this->account_id,$shop['id'])->delete()) return ['code'=>1004, 'msg' => '删除失败'];

		return ['code' => 0, 'msg' => '', 'info' => ['删除成功']];
	}

	//修改账号
	public function editAccount()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!in_array($this->type, [1,2,3])) return ['code'=>1000, 'msg' => '分类参数错误'];

		$data = [];
		if(!empty($this->account_bank))
		{
			if(mb_strlen($this->account_bank) > 100) return ['code'=>1001, 'msg' => '银行名称字数不能大于100位'];
			$data['account_bank'] = $this->account_bank;
		}

		if(mb_strlen($this->name) > 100 || mb_strlen($this->name) <= 0) return ['code'=>1002, 'msg' => '姓名字数应在1~100位之间'];

		if(mb_strlen($this->account) > 100 || mb_strlen($this->account) <= 0) return ['code'=>1003, 'msg' => '账号字数应在1~100位之间'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1004, 'msg' => '店铺不存在'];

		if(!DI()->notorm->cash_account->select('id')->where('id=? and shop_id=?',$this->account_id,$shop['id'])->fetchOne()) return ['code'=>1005, 'msg' => '没有账户信息'];

		$data['name'] = $this->name;
		$data['account'] = $this->account;
		$data['type'] = $this->type;

		if(!DI()->notorm->cash_account->where('id=? and shop_id=?',$this->account_id,$shop['id'])->update($data)) return ['code'=>1006, 'msg' => '修改账户失败'];

		return ['code' => 0, 'msg' => '', 'info' => ['修改账户成功']];
	}

	//添加账户
	public function addAccount()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		if(!in_array($this->type, [1,2,3])) return ['code'=>1000, 'msg' => '分类参数错误'];

		$data = [];
		if(!empty($this->account_bank))
		{
			if(mb_strlen($this->account_bank) > 100) return ['code'=>1001, 'msg' => '银行名称字数不能大于100位'];
			$data['account_bank'] = $this->account_bank;
		}

		if(mb_strlen($this->name) > 100 || mb_strlen($this->name) <= 0) return ['code'=>1002, 'msg' => '姓名字数应在1~100位之间'];

		if(mb_strlen($this->account) > 100 || mb_strlen($this->account) <= 0) return ['code'=>1003, 'msg' => '账号字数应在1~100位之间'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1004, 'msg' => '店铺不存在'];

		$data['shop_id'] = $shop['id'];
		$data['name'] = $this->name;
		$data['account'] = $this->account;
		$data['type'] = $this->type;
		$data['type2'] = 1;
		$data['addtime'] = time();

		if(!DI()->notorm->cash_account->insert($data)) return ['code'=>1005, 'msg' => '添加账户失败'];

		return ['code' => 0, 'msg' => '', 'info' => ['添加账户成功']];
	}

	//获取店铺列表
	public function getList()
	{
		if($this->p<1){
            $this->p=1;
        }
		$nums=10;
		$start=($this->p-1)*$nums;

		$list = DI()->notorm->shop->select('id,name,shop_img,description')->where('state=1')->order("sort desc")->limit($start,$nums)->fetchAll();

		foreach ($list as $key => &$value) 
		{
			$value['shop_img'] = get_upload_path($value['shop_img'], 0);
		}

		return ['code' => 0, 'msg' => '', 'info' => $list];
	}

	//获取店铺信息
	public function getShop()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$data = DI()->notorm->shop->select('id,name,shop_img,description,certificate_img,state,text,balance,balance_freeze,assort_id')->where('uid=?', $this->uid)->fetchOne();
		if(!$data) return ['code'=>1000, 'msg' => '该用户未申请店铺!'];
		$data['statename'] = $this->state($data['state']);
		$data['description'] = $data['description'] ? $data['description'] : '';
		$data['certificate_img'] = $data['certificate_img'] ? get_upload_path($data['certificate_img']) : '';
		$data['shop_img'] = $data['shop_img'] ? get_upload_path($data['shop_img']) : '';
		$data['assort_id'] = $data['assort_id'];
		$assort_name = '';
		if($data['assort_id']){
			$item_assort_class_data = DI()->notorm->shop_assort->select('id,name')->where('state=1 and id = ?',$data['assort_id'])->fetchOne();
			if(!empty($item_assort_class_data)){
				$assort_name = $item_assort_class_data['name'];
			}
		}
		$data['assort_name'] = $assort_name;

		if($data['state'] != 2)
		{
			unset($data['text']);
		}
		else
		{
			$data['text'] = $data['text'] ? $data['text'] : '';
		}
		
		$data['shopurl'] = getConfigPub()['site'].'/merchant';

		return ['code' => 0, 'msg' => '', 'info' => [$data]];
	}



	//申请店铺
	public function application()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$data = [];

		if(mb_strlen($this->name) > 20 || mb_strlen($this->name) <= 0) return ['code'=>1000, 'msg' => '店铺名称字数应在0~20位之间'];

		if(isset($this->description))
		{
			if(mb_strlen($this->description) > 100) return ['code'=>1001, 'msg' => '店铺描述字数不能大于100个字符'];
			$data['description'] = $this->description;
		}

        if(isset($this->shop_img))
        {
        	if(mb_strlen($this->shop_img) > 100) return ['code'=>1002, 'msg' => '头像/封面字数不能大于100个字符'];
        	$data['shop_img'] = $this->shop_img;
        }

        if(isset($this->certificate_img))
        {
        	if(mb_strlen($this->certificate_img) > 100) return ['code'=>1003, 'msg' => '营业执照字数不能大于100个字符'];
        	$data['certificate_img'] = $this->certificate_img;
        }
        if(!$this->assort_id)
        {
        	return ['code'=>1002, 'msg' => '请选择一个类目'];
        }

        $data['uid'] = $this->uid;
        $data['name'] = $this->name;
        $data['application_time'] = time();
    	$data['assort_id'] = $this->assort_id;
        
        if(!(DI()->notorm->user_auth->where('uid=? && status = 1', $this->uid)->fetchOne())) return ['code'=>1004, 'msg' => '该主播不存在'];
        if(DI()->notorm->shop->where('name=?', $this->name)->fetchOne()) return ['code'=>1005, 'msg' => '店铺名称重复'];
        if(DI()->notorm->shop->where('uid=?', $this->uid)->fetchOne()) return ['code'=>1006, 'msg' => '该用户已申请店铺'];

        
        if(DI()->notorm->shop->insert($data))
        {
        	return ['code' => 0, 'msg' => '', 'info' => ['申请成功']];
        }

        return ['code' => 1007, 'msg' => '申请失败'];
	}

	//修改接口
	public function edit()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$data = [];

		if(isset($this->description))
		{
			if(mb_strlen($this->description) > 100) return ['code'=>1001, 'msg' => '店铺描述字数不能大于100个字符'];
			$data['description'] = $this->description;
		}

        if(isset($this->shop_img))
        {
        	if(mb_strlen($this->shop_img) > 100) return ['code'=>1002, 'msg' => '头像/封面字数不能大于100个字符'];
        	$data['shop_img'] = $this->shop_img;
        }

        if(isset($this->certificate_img))
        {
        	if(mb_strlen($this->certificate_img) > 100) return ['code'=>1003, 'msg' => '营业执照字数不能大于100个字符'];
        	$data['certificate_img'] = $this->certificate_img;
        }
        
        if(!(DI()->notorm->user_auth->where('uid=? && status = 1', $this->uid)->fetchOne())) return ['code'=>1004, 'msg' => '该主播不存在'];
        $shop_info = DI()->notorm->shop->where('uid=? and id = ?', $this->uid, $this->shop_id)->fetchOne();

        if(!$shop_info) return ['code'=>1006, 'msg' => '店铺不存在'];

        if(2 == $shop_info['state']) $shop_info['state'] = 0;
        if(DI()->notorm->shop->where('uid=? and id = ?', $this->uid, $this->shop_id)->update($data))
        {
        	return ['code' => 0, 'msg' => '', 'info' => ['修改成功']];
        }

        return ['code' => 1007, 'msg' => '修改失败'];
	}


	private function state($num)
	{
		$arr = ['申请中', '正常', '未通过'];
		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}
	
	//分割线 ---- 2021-05-24 ----
	
	//订单列表
	public function orderList(){
	    $checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
        
		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();

		if(!$shop) return ['code'=>1000, 'msg' => '您还未开通店铺哦'];
		
        $StartTime = $this->startTime?strtotime(date('Y-m-d', strtotime($this->startTime)).' 00:00:00'):"";
		$EndTime = $this->endTime?strtotime(date('Y-m-d', strtotime($this->endTime)).' 23:59:59'):"";

		if (!$StartTime && !$EndTime){
		    $timewhere = "";
		}
		    
		if ($StartTime && !$EndTime){
		    $timewhere = " AND addtime > ".$StartTime;
		}
		if (!$StartTime && $EndTime){
		    $timewhere = " AND addtime < ".$EndTime;
		}
		if ($StartTime && $EndTime){
		    $timewhere = " AND addtime BETWEEN ".$StartTime." AND ".$EndTime;
		}
		
		$p = $this->p;
		$start = ($p - 1) * 10;
		$limit = 10;
        $status = $this->type;

        if ($status == -1){
            $list = DI()->notorm->shop_order->select("id,numbering,shop_id,state,payment_amount,pay_time,address,address_info")->where('shop_id = ? AND deletetime = 0'.$timewhere, $shop['id'])->limit($start,$limit)->order("addtime desc")->fetchAll();
        }else{
            if ($status == 40){
                $list = DI()->notorm->shop_order->select("id,numbering,shop_id,state,payment_amount,pay_time,address,address_info")->where('shop_id = ? AND state IN (40,41,42,43) AND deletetime = 0'.$timewhere, $shop['id'])->limit($start,$limit)->order("addtime desc")->fetchAll();
            }else{
                $list = DI()->notorm->shop_order->select("id,numbering,shop_id,state,payment_amount,pay_time,address,address_info")->where('shop_id = ? AND state = ? AND deletetime = 0'.$timewhere, $shop['id'],$status)->limit($start,$limit)->order("addtime desc")->fetchAll();
            }
            
        }
        
        if($list)
		{
			$list = replaceArrayKey($list);
			$order_id = array_keys($list);

			$order_info = DI()->notorm->shop_order_info->select('price,order_id,title,img,num,commodity_id')->where('order_id in ('.implode(',', $order_id).') and shop_id=?', $shop['id'])->fetchAll();

			$shop_refund = DI()->notorm->shop_refund->select('id,order_id')->where('order_id in ('.implode(',', $order_id).') and shop_id=? and state = 1', $shop['id'])->fetchAll();
            
			$shop_refund_count = count($shop_refund);

			if($shop_refund_count) $shop_refund = replaceArrayKey($shop_refund,'order_id');

			foreach ($order_info as $key => $value)
			{
				if(isset($list[$value['order_id']]))
				{
					$value['img'] = get_upload_path($value['img'], 0);
					$list[$value['order_id']]['shoplist'][] = $value;
				}
			}

			foreach ($list as $key => &$value) 
			{
				if(!isset($value['is_refund'])) $value['is_refund'] = 0;
				$dispute = DI()->notorm->shop_dispute->where("order_id = ?",$value['id'])->fetchOne();
				if ($dispute){
				    $list[$key]['is_dispute'] = 1;
				}else{
				    $list[$key]['is_dispute'] = 0;
				}
				$list[$key]['pay_time'] = $value['pay_time']?date('Y-m-d H:i:s', $value['pay_time']):'';
				if ($value['address']){
				    $list[$key]['address_info'] = DI()->notorm->shop_address->select("uid,title,name,phone")->where("id = ?",$value['address'])->fetchOne();
				    $list[$key]['address_info'] = $list[$key]['address_info'] ? $list[$key]['address_info'] : [];
				}else{
				    $list[$key]['address_info'] = [];
				}
				
			}
		}

		return ['code' => 0, 'msg' => '', 'info' => $list ? array_values($list) : []];
	}
	
	//读取快递信息
	public function logistics(){
		$logistics = getLogistics();
		return ['code' => 0, 'msg' => '', 'info' => $logistics];
	}
	
	public function orderInfo(){
	    $checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		
		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		
		$id = $this->order_id;
		$info = DI()->notorm->shop_order->where('id = ? AND shop_id = ? AND deletetime = 0', $id,$shop['id'])->fetchOne();
		if ($info['logistic_id']){
		    $info['logistic_name'] = DI()->notorm->shop_logistics->where("id = ?",$info['logistic_id'])->fetchOne()["name"];
		}
		$info['order_info'] = DI()->notorm->shop_order_info->select("commodity_id,title,img,num,price")->where("order_id = ?",$id)->fetchAll();
		if (!$info){
		    return ['code' => 1001, 'msg' => '订单信息不存在'];
		}
		$reply = DI()->notorm->shop_evaluation->select("rating,content,img,reply_time,reply_content,addtime")->where("order_id = ?",$id)->fetchOne();
		$info['reply_info'] = $reply?$reply:[];
		
		
		$address = DI()->notorm->shop_address->select("title,name,phone")->where("id = ?",$info['address'])->fetchOne();

		$info['address_info'] = $address;
		return ['code' => 0, 'msg' => '', 'info' => $info];
	}
	
	//发货
	public function add_logistic(){
	    $checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		
		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		
		$id = $this->order_id;
		$info = DI()->notorm->shop_order->where('id = ? AND shop_id = ? AND deletetime = 0', $id,$shop['id'])->fetchOne();
		
		$numbering = $this->numbering;

		if ($numbering != $info['numbering']){
		    return ['code'=>1003, 'msg' => '订单号填写有误，请检查'];
		}
		
		$logistic_id = $this->logistic_id;
		$logistic_num = $this->logistic_num;
		
		if (!$logistic_id){
		    return ['code'=>1001, 'msg' => '请选择快递公司'];
		}
		if (!$logistic_num){
		    return ['code'=>1002, 'msg' => '请填写快递单号'];
		}
		$update = [
		    "state" => 30,
		    "single_number" => $logistic_num,
		    "logistics_id"  => $logistic_id,
		    "deliverytime" => time()
		];
		
		$address_id = $this->address_id?$this->address_id:$info['address'];
		$address_info = $this->address_info?$this->address_info:$info['address_info'];
		
		$address = DI()->notorm->shop_address->where("id = ?",$address_id)->fetchOne();
		
		if ($address_info == ""){
		    $address_info = $address['title'];
		}
		if (!$address && !$address_info){
		    return ['code'=>1004, 'msg' => '地址信息有误'];
		}
		
		$update['address_info'] = $address_info;

		$res = DI()->notorm->shop_order->where("state in (20,30) AND id = ?",$id)->update($update);
		if ($res){
		    return ['code'=>0, 'msg' => '发货成功', 'info' => ["logistic_num" => $logistic_num]];
		}else{
		    return ['code'=>1004, 'msg' => '发货失败'];
		}
	}
	
	public function reply_list(){
	    $checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		
		$type = $this->type;
		$p = $this->p;
		$start = ($p-1)*15;
		$limit = 15;
		if ($type == 1){
		    $info = DI()->notorm->shop_evaluation->where("reply_content is null AND shop_id = ?".$shop['id'])->limit($start,$limit)->fetchAll();
		}elseif ($type == 2){
		    $info = DI()->notorm->shop_evaluation->where("reply_content is not null AND shop_id = ?".$shop['id'])->limit($start,$limit)->fetchAll();
		}else{
		    $info = DI()->notorm->shop_evaluation->where("shop_id = ?",$shop['id'])->limit($start,$limit)->fetchAll();
		}
		
		if (!$info){
		    return ['code'=>1001, 'msg' => '暂无评论', 'info' => []];
		}else{
		    return ['code' => 0, 'msg' => '成功', 'info' => $info];
		}
	}
	
	public function add_reply(){
	    $checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		
		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		$order_id = $this->order_id;
		$order = DI()->notorm->shop_order->where("id = ? AND shop_id = ? AND state = 60",$order_id,$shop['id'])->fetchOne();
		if (!$order){
		    return ["code" => 800, "msg" => "买家未评价该订单，无法回复评价"];
		}
		
		$evalution = DI()->notorm->where("order_id = ?",$order_id)->fetchOne();
		$content = $this->content;
		if (strlen($content) <= 0 || strlen($content) > 100){
		    return ["code" => 1002, "msg" => "回复内容的字数应在1~100位之间"];
		}
		
		$update = [
		    "reply_content" => $content,
		    "reply_time" => time()
		];
		$res = DI()->notorm->shop_evalution->where("order_id = ?",$order_id)->update($update);
		$res2 = DI()->notorm->shop_order->where("id = ?",$order_id)->update(array("is_reply" => 1));
		
		if ($res && $res2){
		    return ["code" => 0, "msg" => "回复成功"];
		}else{
		    return ["code" => 1001, "msg" => "回复失败"];
		}
	}
	
	public function disputeList(){
	    $checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		
		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();
		
		$p = $this->p;
		$start = ($p - 1) * 10;
		$limit = 10;
		
		$list = DI()->notorm->shop_dispute->where("shop_id = ?",$shop['id'])->order("addtime desc")->limit($start,$limit)->fetchAll();
        
        if (empty($list)){
            return ['code' => 0, 'msg' => '', 'info' => $list];
        }
		$order_id = [];
		foreach ($list as $key => &$value) 
		{
			$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
			$order_id[] = $value['order_id'];
		}

		$order = DI()->notorm->shop_order->select('id,numbering,payment_amount')->where('id in ('.implode(',', $order_id).')')->limit(count($order_id))->fetchAll();
		$order = replaceArrayKey($order);
		
		foreach ($list as &$value){
		    $value['img'] = json_decode($value['img'],true);
		    foreach ($value['img'] as &$v){
		        $v = get_upload_path($v,0);
		    }
		  //  $value['addtime'] = $value['addtime']?date("Y-m-d H:i:s",$value['addtime']):"";
		    $value['checktime'] = $value['checktime']?date("Y-m-d H:i:s",$value['checktime']):"";
		    if(isset($order[$value['order_id']]))
			{
				$value['numbering'] = $order[$value['order_id']]['numbering'];
				$value['payment_amount'] = $order[$value['order_id']]['payment_amount'];
			}
		}
		return ['code' => 0, 'msg' => '', 'info' => $list];
	}
	
	public function disputeInfo()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		
		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid=?', $this->uid)->fetchOne();

		$dispute = DI()->notorm->shop_dispute->where('id = ? AND shop_id = ?', $this->dispute_id,$shop['id'])->fetchOne();
        
		if(!$dispute)  return ['code'=>1000, 'msg' => '记录不存在'];

		$order = DI()->notorm->shop_order->select('id,numbering,payment_amount')->where('id=?', $dispute['order_id'])->fetchOne();
        
		$dispute['numbering'] = $order['numbering'];
		$dispute['payment_amount'] = $order['payment_amount'];
		$dispute['addtime'] = date('Y-m-d H:i:s', $dispute['addtime']);
		$dispute['img'] = json_decode($dispute['img'], true);
		if(empty($dispute['reply'])) $dispute['reply'] = '';
		$dispute['checktime'] = empty($dispute['checktime']) ? '' : date('Y-m-d H:i:s', $dispute['checktime']);

		return ['code' => 0, 'msg' => '', 'info' => $dispute];
	}

	// 红包发放记录
	public function redList()
	{
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $p=checkNull($this->p);

	    $checkToken = checkToken($uid,$token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

        if($p<1){
            $p=1;
        }
        
        $nums=50;
        $start=($p-1)*$nums;

		$lists = DI()->notorm->red
                ->select("*")
                ->where('liveuid = ?',$uid)
                ->order('id desc')
                ->limit($start,$nums)
                ->fetchAll();

        foreach ($lists as $key => &$value) {
        	$item_user_info=getUserInfo($value['liveuid']);
        	$value['user_nicename'] = $item_user_info['user_nicename'];
			$value['avatar'] = $item_user_info['avatar'];
			$value['avatar_thumb'] = $item_user_info['avatar_thumb'];
        }

        $rs['info']=$lists;
        
		return $rs;
	}

	// 补全信息-选择分类信息
	public function completion()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$data = [];

        if(!$this->assort_id)
        {
        	return ['code'=>1002, 'msg' => '请选择一个类目'];
        }

        $this->uid;
    	$data['assort_id'] = $this->assort_id;

        if(!(DI()->notorm->user_auth->where('uid=? && status = 1', $this->uid)->fetchOne())) return ['code'=>1004, 'msg' => '该主播不存在'];

    	$shop_info = DI()->notorm->shop->where('uid=? && state = 1', $this->uid)->fetchOne();
    	if (!empty($shop_info)) {
    		// 判断是否已存在分类信息
    		if($shop_info['assort_id']){
    			return ['code'=>1004, 'msg' => '已存在分类'];
    		}
    	}else{
    		return ['code'=>1004, 'msg' => '店铺不存在或申请未通过'];
    	}
        
        if(DI()->notorm->shop->where('uid=? && state = 1', $this->uid)->update($data))
        {
        	return ['code' => 0, 'msg' => '', 'info' => ['补全成功']];
        }

        return ['code' => 1007, 'msg' => '补全失败'];
	}
}