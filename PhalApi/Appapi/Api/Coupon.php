<?php

/**
* 优惠券
*/
class Api_Coupon extends PhalApi_Api
{
	public function getRules() {
		return array(
            'addCoupon' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'discounted_price' => array('name' => 'discounted_price',  'type' => 'float','require' => true, 'desc' => '优惠金额'),
				'use_amount' => array('name' => 'use_amount',  'type' => 'float','require' => true, 'desc' => '使用金额'),
				'start_time' => array('name' => 'start_time',  'type' => 'string','require' => true, 'desc' => '使用开始时间'),
				'end_time' => array('name' => 'end_time',  'type' => 'string','require' => true, 'desc' => '使用结束时间'),
				'num' => array('name' => 'num',  'type' => 'int','require' => true, 'desc' => '发放数量'),
				'receive_start_time' => array('name' => 'receive_start_time',  'type' => 'string','require' => true, 'desc' => '开始领取时间'),
				'receive_end_time' => array('name' => 'receive_end_time',  'type' => 'string','require' => true, 'desc' => '领取结束时间'),
			),
			'editCoupon' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'coupon_id' => array('name' => 'coupon_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '优惠券id'),
				'discounted_price' => array('name' => 'discounted_price',  'type' => 'float','require' => true, 'desc' => '优惠金额'),
				'use_amount' => array('name' => 'use_amount',  'type' => 'float','require' => true, 'desc' => '使用金额'),
				'start_time' => array('name' => 'start_time',  'type' => 'string','require' => true, 'desc' => '使用开始时间'),
				'end_time' => array('name' => 'end_time',  'type' => 'string','require' => true, 'desc' => '使用结束时间'),
				'num' => array('name' => 'num',  'type' => 'int','require' => true, 'desc' => '发放数量'),
				'receive_start_time' => array('name' => 'receive_start_time',  'type' => 'string','require' => true, 'desc' => '开始领取时间'),
				'receive_end_time' => array('name' => 'receive_end_time',  'type' => 'string','require' => true, 'desc' => '领取结束时间'),
			),
			'delCoupon' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'coupon_id' => array('name' => 'coupon_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '优惠券id'),
			),
			'couponList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'shop_id' => array('name' => 'shop_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '店铺id'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 0, 'default' => 1, 'require' => true, 'desc' => '分类'),
			),
			'couponInfo' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'coupon_id' => array('name' => 'coupon_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '优惠券id'),
				'type' => array('name' => 'type', 'type' => 'int', 'min' => 0, 'default' => 1, 'require' => true, 'desc' => '分类'),
			),
			'receiveCoupon' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'coupon_id' => array('name' => 'coupon_id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '优惠券id')
			),
			'memberCouponList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token')
			),
			'orderCoupon' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'cart_id' => array('name' => 'cart_id', 'type' => 'string', 'require' => true, 'desc' => '购物车id'),
			),
			'orderListCoupon' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'order_id' => array('name' => 'order_id', 'type' => 'int', 'require' => true, 'min'=>0, 'desc' => '订单id'),
			),
			'immediatelyPayCoupon' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'commodity_id' => array('name' => 'commodity_id', 'type' => 'int', 'require' => true, 'desc' => '商品id'),
				'num' => array('name' => 'num', 'type' => 'int', 'require' => true, 'min'=>1,'default' => 1, 'desc' => '商品数量'),
			),
		);
	}

	//立即支付优惠券列表
	public function immediatelyPayCoupon()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$commodity = DI()->notorm->shop_commodity->select('price,shop_id')->where('id=? and state=1', $this->commodity_id)->fetchOne();

		if(!$commodity) return ['code'=>1000, 'msg' => '商品信息错误'];

		$price = $commodity['price']*$this->num;

		$coupon_list = DI()->notorm->shop_coupon
					->select('id,discounted_price,use_amount,start_time,end_time,receive_start_time,receive_end_time')
					->where('shop_id =? and state = 1 and use_amount <=? and end_time >? and start_time < ?', $commodity['shop_id'], $price, time(), time())
					->fetchAll();
		$coupon_list = replaceArrayKey($coupon_list);

		$coupon = DI()->notorm->shop_coupon_receive
				->select('id,shop_id,coupon_id,discounted_price,use_amount,start_time,end_time')
				->where('uid=? and state=0 and shop_id=? and use_amount <=? and end_time >? and start_time < ?', $this->uid, $commodity['shop_id'], $price,time(),time())
				->fetchAll();

		//已领取优惠券
		if($coupon)
		{
			foreach ($coupon as $key => &$value) 
			{
				$value['type'] = 0;
				$value['start_time'] = date('Y-m-d H:i', $value['start_time']);
				$value['end_time'] = date('Y-m-d H:i', $value['end_time']);
				if(isset($coupon_list[$value['coupon_id']])) unset($coupon_list[$value['coupon_id']]);
			}
		}

		if($coupon_list)
		{
			foreach ($coupon_list as $k => &$v) 
			{
				$v['type'] = 0;
				$v['start_time'] = date('Y-m-d H:i', $v['start_time']);
				$v['end_time'] = date('Y-m-d H:i', $v['end_time']);
			}
			$coupon_list = array_values($coupon_list);
		}


		return ['code' => 0, 'msg' => '', 'info' => ['received' => $coupon ? $coupon :[], 'coupon_list' => $coupon_list ? $coupon_list : []]];
	}

	//列表订单可以使用的优惠券
	public function orderListCoupon()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$order = DI()->notorm->shop_order->where('id=? and state = 0 and deletetime = 0', $this->order_id)->fetchOne();

		if(!$order) return ['code'=>1000, 'msg' => '订单状态不正确或订单不存在'];

		$coupon_list = DI()->notorm->shop_coupon
					->select('id,discounted_price,use_amount,start_time,end_time,receive_start_time,receive_end_time')
					->where('shop_id =? and state = 1 and use_amount <=? and end_time >? and start_time < ?', $order['shop_id'], $order['payment_amount'], time(), time())
					->fetchAll();
		$coupon_list = replaceArrayKey($coupon_list);

		$coupon = DI()->notorm->shop_coupon_receive
				->select('id,shop_id,coupon_id,discounted_price,use_amount,start_time,end_time')
				->where('uid=? and state=0 and shop_id=? and use_amount <=? and end_time >? and start_time < ?', $this->uid, $order['shop_id'], $order['payment_amount'],time(),time())
				->fetchAll();

		//已领取优惠券
		if($coupon)
		{
			foreach ($coupon as $key => &$value) 
			{
				$value['type'] = 0;
				$value['start_time'] = date('Y-m-d H:i', $value['start_time']);
				$value['end_time'] = date('Y-m-d H:i', $value['end_time']);
				if(isset($coupon_list[$value['coupon_id']])) unset($coupon_list[$value['coupon_id']]);
			}
		}

		if($coupon_list)
		{
			foreach ($coupon_list as $k => &$v) 
			{
				$v['type'] = 0;
				$v['start_time'] = date('Y-m-d H:i', $v['start_time']);
				$v['end_time'] = date('Y-m-d H:i', $v['end_time']);
			}
			$coupon_list = array_values($coupon_list);
		}


		return ['code' => 0, 'msg' => '', 'info' => ['received' => $coupon ? $coupon :[], 'coupon_list' => $coupon_list ? $coupon_list : []]];
	}

	//订单可以使用的优惠券
	public function orderCoupon()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		$coupon = DI()->notorm->shop_coupon_receive->select('id,shop_id,coupon_id')->where('uid=? and state=0', $this->uid)->fetchAll();

		$this->cart_id = explode(',', $this->cart_id);
		$cart_id = [];
		foreach ($this->cart_id as $key => &$value) 
		{
			if(!intval($value)) return ['code'=>1000, 'msg' => '参数错误'];
			$cart_id[] = intval($value);
		}

		if(!$cart_id) return ['code'=>1001, 'msg' => '参数错误'];

		$cart = DI()->notorm->shop_car->select('commodity_id,num,shop_id')->where('id in ('.implode(',', $cart_id).') and uid = ?', $this->uid)->fetchAll();

		if(!$cart) return ['code'=>1001, 'msg' => '购物车中没有您选中的商品'];

		$commodity_id = $shop_id = [];
		foreach ($cart as $key => $value) 
		{
			$commodity_id[] = $value['commodity_id'];
			$shop_id[$value['shop_id']] = $value['shop_id'];
		}

		$coupon_list = DI()->notorm->shop_coupon->select('id,discounted_price,use_amount,start_time,end_time,receive_start_time,receive_end_time')->where('shop_id in ('.implode(',', $shop_id).') and state = 1')->fetchAll();
		$coupon_list = replaceArrayKey($coupon_list);

		//已有的优惠券
		if($coupon)
		{
			$coupon_id = [];
			foreach ($coupon as $key => $value) 
			{
				$coupon_id[$value['coupon_id']] = $value['coupon_id'];
			}

			$coupon_data = DI()->notorm->shop_coupon->where('id in ('.implode(',', $coupon_id).') and state = 1')->fetchAll();
			$coupon_data = replaceArrayKey($coupon_data, 'id');

			//获取商品信息
			$commodity_info = DI()->notorm->shop_commodity->select('id,img,shop_id,price,amount,title')->where('id in ('.implode(',', $commodity_id).') and state = 1')->fetchAll();
			if(count($commodity_info) != count($commodity_id)) return ['code'=>1002, 'msg' => '您选择的商品当中有已下架商品'];
			$commodity_info = replaceArrayKey($commodity_info);

			$shop_amount = [];
			foreach ($cart as $key => $value) 
			{
				$shop_amount[$value['shop_id']] += $commodity_info[$value['commodity_id']]['price']*$value['num'];
			}

			$list = [];
			foreach ($coupon as $key => $value) 
			{
				if(isset($shop_amount[$value['shop_id']]) && isset($coupon_data[$value['coupon_id']])) //已经领取店铺的券
				{
					if($shop_amount[$value['shop_id']] >= $coupon_data[$value['coupon_id']]['use_amount']) //大于最低使用金额
					{

						if($coupon_data[$value['coupon_id']]['start_time'] < time() && $coupon_data[$value['coupon_id']]['end_time'] > time())//在使用期
						{
							$type = 0;
						}
						else if($coupon_data[$value['coupon_id']]['start_time'] > time())//未到使用期
						{
							$type = 1;
						}
						else //过期
						{
							$type = 2;
						}
					}
					else
					{
						$type = 3;
					}

					$list[] = [
						'id' => $value['id'],
						'discounted_price' => $coupon_data[$value['coupon_id']]['discounted_price'],
						'use_amount' => $coupon_data[$value['coupon_id']]['use_amount'],
						'start_time' => date('Y-m-d H:i', $coupon_data[$value['coupon_id']]['start_time']),
						'end_time' => date('Y-m-d H:i', $coupon_data[$value['coupon_id']]['end_time']),
						'type' => $type
					];

					if(isset($coupon_list[$value['coupon_id']])) unset($coupon_list[$value['coupon_id']]);
				}
			}
		}

		if($coupon_list)
		{
			foreach ($coupon_list as $key => &$value) 
			{
				if($value['receive_end_time'] > time() && $value['receive_start_time'] < time())
				{
					$value['start_time'] = date('Y-m-d H:i', $value['start_time']);
					$value['end_time'] = date('Y-m-d H:i', $value['end_time']);
					$value['receive_start_time'] = date('Y-m-d H:i', $value['receive_start_time']);
					$value['receive_end_time'] = date('Y-m-d H:i', $value['receive_end_time']);
				}
				else
				{
					unset($coupon_list[$key]);
				}
			}

			$coupon_list = array_values($coupon_list);
		}


		return ['code' => 0, 'msg' => '', 'info' => ['received' => $list ? $list :[], 'coupon_list' => $coupon_list ? $coupon_list : []]];
	}


	//用户优惠券列表
	public function memberCouponList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		$list = DI()->notorm->shop_coupon_receive->select('id,shop_id,discounted_price,use_amount,start_time,end_time,state,addtime')->where('uid = ?', $this->uid)->fetchAll();

		if($list)
		{
			$state = ['未使用', '已使用', '过期'];
			$shop_id = [];
			foreach ($list as $key => &$value) 
			{
				$value['start_time'] = date('Y-m-d H:i', $value['start_time']);
				$value['end_time'] = date('Y-m-d H:i', $value['end_time']);
				$value['addtime'] = date('Y-m-d H:i', $value['addtime']);
				$value['statename'] = isset($state[$value['state']]) ? $state[$value['state']] : '未定义状态';
				$shop_id[$value['shop_id']] = $value['shop_id'];
			}

			$shop_list = DI()->notorm->shop->select('id,name')->where('id in ('.implode(',', $shop_id).')')->fetchAll();
			$shop_list = replaceArrayKey($shop_list);

			foreach ($list as $key => &$value) 
			{
				$value['shop_name'] = $shop_list[$value['shop_id']]['name'];
			}
		}
		return ['code' => 0, 'msg' => '', 'info' => $list];
	}


	//领取优惠券
	public function receiveCoupon()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];


		$redis = connectionRedis();
		$ret = $redis->decr('shopCoupon'.$this->coupon_id);
		if($ret < 0)
		{
			$redis->del('shopCoupon'.$this->coupon_id);
			if(0 > $ret) return ['code'=>1001, 'msg' => '优惠券已抢完'];
		}

		$info = DI()->notorm->shop_coupon->select('id,shop_id,discounted_price,use_amount,num,start_time,end_time,receive_start_time,receive_end_time')->where('id = ? and state=1', $this->coupon_id)->fetchOne();
		if(!$info)
		{
			$redis->incr('shopCoupon'.$this->coupon_id);
			return ['code'=>1000, 'msg' => '优惠券不存在'];
		}

		if($info['receive_start_time'] > time() || $info['receive_end_time'] < time())
		{
			$redis->incr('shopCoupon'.$this->coupon_id);
			return ['code'=>1000, 'msg' => '不在领取时间内'];
		}

		$add = [
			'uid' => $this->uid,
			'shop_id' => $info['shop_id'],
			'coupon_id' => $info['id'],
			'discounted_price' => $info['discounted_price'],
			'use_amount' => $info['use_amount'],
			'start_time' => $info['start_time'],
			'end_time' => $info['end_time'],
			'addtime' =>time()
		];

		try {
			DI()->notorm->beginTransaction('db_appapi');
			if(!DI()->notorm->shop_coupon_receive->insert($add)) throw new \Exception('领取优惠券失败');

			if(!DI()->notorm->shop_coupon->where('id=?', $this->coupon_id)->update(['num' => new NotORM_Literal("num - 1")])) throw new \Exception('领取优惠券失败');
			
			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1008, 'msg' => $e->getMessage()];
			return ['code' => 1008, 'msg' => '领取优惠券失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['领取优惠券成功']];
	}

	//获取优惠券详情
	public function couponInfo()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$where = $this->type ? ' and state=1' : '';

		$info = DI()->notorm->shop_coupon->select('id,shop_id,discounted_price,use_amount,start_time,end_time,state,num,receive_start_time,receive_end_time')->where('id = ?'.$where, $this->coupon_id)->fetchOne();

		if(!$info) return ['code'=>1001, 'msg' => '优惠券不存在'];

		$info['start_time'] = date('Y-m-d H:i', $info['start_time']);
		$info['end_time'] = date('Y-m-d H:i', $info['end_time']);
		$info['receive_start_time'] = date('Y-m-d H:i', $info['receive_start_time']);
		$info['receive_end_time'] = date('Y-m-d H:i', $info['receive_end_time']);
		$info['statename'] = $this->state($info['state']);

		return ['code' => 0, 'msg' => '', 'info' => $info];
	}

	//店铺优惠券列表
	public function couponList()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and id = ?', $this->shop_id)->fetchOne();
		if(!$shop) return ['code'=>1007, 'msg' => '店铺不存在或已关闭'];

		$where = $this->type ? ' and state=1' : '';

		$list = DI()->notorm->shop_coupon->select('id,shop_id,discounted_price,use_amount,start_time,end_time,state,num,receive_start_time,receive_end_time')->where('shop_id = ?'.$where, $this->shop_id)->fetchAll();

		if($list)
		{
			foreach ($list as $key => &$value) 
			{
				$value['start_time'] = date('Y-m-d H:i', $value['start_time']);
				$value['end_time'] = date('Y-m-d H:i', $value['end_time']);
				$value['receive_start_time'] = date('Y-m-d H:i', $value['receive_start_time']);
				$value['receive_end_time'] = date('Y-m-d H:i', $value['receive_end_time']);
				$value['statename'] = $this->state($value['state']);
			}
		}

		return ['code' => 0, 'msg' => '', 'info' => $list];
	}


	//删除优惠券
	public function delCoupon()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid = ?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1007, 'msg' => '店铺不存在或已关闭'];

		$redis = connectionRedis();
		try {
			DI()->notorm->beginTransaction('db_appapi');
			if(!DI()->notorm->shop_coupon->where('shop_id = ? and id = ?', $shop['id'], $this->coupon_id)->delete()) throw new \Exception('修改优惠券失败');

			if(!$redis->del('shopCoupon'.$this->coupon_id)) throw new \Exception('修改优惠券失败');
			
			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1008, 'msg' => $e->getMessage()];
			return ['code' => 1008, 'msg' => '删除优惠券失败'];
		}

		return ['code' => 0, 'msg' => '', 'info' => ['删除优惠券成功']];
	}

	//修改优惠券
	public function editCoupon()
	{
		$check_post = $this->check_post();
		if($check_post['code']) return $check_post;

		$edit = [
			'discounted_price' => $this->discounted_price,
			'use_amount' => $this->use_amount,
			'start_time' => $this->start_time,
			'end_time' => $this->end_time,
			'num' => $this->num,
			'receive_start_time' => $this->receive_start_time,
			'receive_end_time' => $this->receive_end_time
		];

		$redis = connectionRedis();
		try {
			DI()->notorm->beginTransaction('db_appapi');
			if(!DI()->notorm->shop_coupon->where('id=? and shop_id =?', $this->coupon_id, $check_post['data']['id'])->update($edit)) throw new \Exception('修改优惠券失败');

			if(!$redis->del('shopCoupon'.$this->coupon_id)) throw new \Exception('修改优惠券失败');
			if(!$redis->incrBy('shopCoupon'.$this->coupon_id, $this->num)) throw new \Exception('修改优惠券失败');
			
			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');
			//return ['code' => 1008, 'msg' => $e->getMessage()];
			return ['code' => 1008, 'msg' => '修改优惠券失败'];
		}
		return ['code' => 0, 'msg' => '', 'info' => ['order_id' => ['修改优惠券成功']]];
	}

	//添加优惠券
	public function addCoupon()
	{
		$check_post = $this->check_post();
		if($check_post['code']) return $check_post;

		$add = [
			'shop_id' => $check_post['data']['id'],
			'discounted_price' => $this->discounted_price,
			'use_amount' => $this->use_amount,
			'start_time' => $this->start_time,
			'end_time' => $this->end_time,
			'num' => $this->num,
			'receive_start_time' => $this->receive_start_time,
			'receive_end_time' => $this->receive_end_time,
			'addtime' => time()
		];

		$redis = connectionRedis();
		try {
			DI()->notorm->beginTransaction('db_appapi');
			if(!DI()->notorm->shop_coupon->insert($add)) throw new \Exception('添加优惠券失败');

			if(!$redis->incrBy('shopCoupon'.DI()->notorm->shop_coupon->insert_id(), $this->num)) throw new \Exception('添加优惠券失败');
			
			DI()->notorm->commit('db_appapi');
		} catch (Exception $e) {
			DI()->notorm->rollback('db_appapi');

			return ['code' => 1008, 'msg' => '添加优惠券失败'];
		}
		return ['code' => 0, 'msg' => '', 'info' => ['order_id' => ['添加优惠券成功']]];
	}

	//判断提交的数据
	private function check_post()
	{
		$checkToken = checkToken($this->uid,$this->token);
		if($checkToken==700) return ['code'=>700, 'msg' => 'token已过期，请重新登陆'];
		if($this->discounted_price > 100000000 || $this->discounted_price < 0) return ['code'=>1000, 'msg' => '优惠金额面额应在0~100000000之间'];
		if($this->use_amount > 100000000 || $this->use_amount < 0) return ['code'=>1001, 'msg' => '使用金额面额应在0~100000000之间'];
		$this->start_time = strtotime($this->start_time);
		if(!$this->start_time) return ['code'=>1002, 'msg' => '使用开始时间格式错误'];
		$this->end_time = strtotime($this->end_time);
		if(!$this->end_time) return ['code'=>1003, 'msg' => '使用结束时间格式错误'];
		$this->receive_start_time = strtotime($this->receive_start_time);
		if(!$this->receive_start_time) return ['code'=>1004, 'msg' => '开始领取时间格式错误'];
		$this->receive_end_time = strtotime($this->receive_end_time);
		if(!$this->receive_end_time) return ['code'=>1005, 'msg' => '领取结束时间格式错误'];
		if($this->num > 100000000 || $this->num < 0) return ['code'=>1006, 'msg' => '发放数量应在0~100000000之间'];

		$shop = DI()->notorm->shop->select('id')->where('state=1 and uid = ?', $this->uid)->fetchOne();
		if(!$shop) return ['code'=>1007, 'msg' => '店铺不存在或已关闭'];

		return ['code' => 0, 'data' => $shop];
	}

	//状态
	private function state($num)
	{
		$arr = ['关闭', '正常'];
		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}
}