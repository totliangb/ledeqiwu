<?php
/**
 * ${名称}
 * @author 姚翔（512282389@qq.com iaage2010@gmail.com）深圳市将才网络科技有限公司
 * @version 1.0.0
 * @desc ${描述}
 * @createTime 2020/5/2 11:41
 */

class Domain_Userauth
{

    public function addAuth($post){

        $rs = array('code' => 0, 'msg' => '成功', 'info' => array());
        if(!$post['uid']){
            $rs['code'] = '1002';
            $rs['msg'] = '用户信息不存在';
            return $rs;
        }
        if(!$post['real_name'] || !$post['mobile'] || !$post['cer_no'] || !$post['front_view'] || !$post['back_view'] || !$post['handset_view']){
            $rs['code'] = '1002';
            $rs['msg'] = '请填写完整信息';
            return $rs;
        }

        $post['status'] = 0;
        $time = time();
        $model = new Model_Userauth();
        $rst = $model->addAuth($post);
        if($rst){
            return $rs;
        }else{
            $rs['code'] = '1003';
            $rs['msg'] = '提交失败或重复认证';
            return $rs;
        }

    }

    public function getAuthInfo($uid){
        $model = new Model_Userauth();
        //验证是否已支付
        // $is_bond = $model->checkPayment($uid,"bond");
        // $is_cert = $model->checkPayment($uid,"cert");
        //再返回用户信息
        $rst = $model->getAuthInfo($uid);
        return $rst;

    }
    
    public function getOrder($uid){
        $model = new Model_Userauth();
        $rst = $model->getOrder($uid);
        return $rst;
    }

    public function addAuthPay($uid,$type,$order="",$reason="",$pay_code=""){
        $rs = array('code' => 0, 'msg' => '成功', 'info' => array());
        $config = getConfigPri();
        $reorder = $reason = "";
        if (!in_array($type,["bond","cert","rebond"])){
            $rs['code'] = '1001';
            $rs['msg'] = 'type参数错误';
            return $rs;
        }
        
        if ($type == "rebond"){
            //查询相关支付订单
            $select = DI()->notorm->user_auth_payment->where("type = 'bond' AND uid = ? AND payment = ? AND status = 'SUCCESS'",$uid,$order)->order("addtime desc")->fetchOne();
            if (!$select){
                $rs['code'] = 1001;
                $rs['msg'] = "未查询到保证金支付订单";
                return $rs;
            }
            
            $time = time();
            $shop = DI()->notorm->shop->select("id,uid")->where("uid = ?",$uid)->fetchOne();
            //未完成的订单
            $nofinishorder = DI()->notorm->shop_order->where("shop_id = ? AND state < 50")->order("addtime desc")->fetchAll();
            if (count($nofinishorder) > 0){
                $rs['code'] = 1002;
                $rs['msg'] = "存在未完成的订单，无法退款";
                return $rs;
            }
            //订单完成后多少天才可以退保证金
            $newestorder = DI()->notorm->shop_order->where("shop_id = ?",$shop['id'])->order("addtime desc")->fetchOne();
            if (strtotime("+{$config['shop_refunddate']} days",$newestorder['addtime']) > $time){
                $rs['code'] = 1003;
                $rs['msg'] = "存在{$config['shop_refunddate']}天内的订单，无法退款";
                return $rs;
            }
            
            //存在举报记录且未处理
            $report = DI()->notorm->report->where("touid = ? AND status = 0",$uid)->fetchAll();
            if (count($report) > 0){
                $rs['code'] = 1004;
                $rs['msg'] = "存在举报记录，无法退款";
                return $rs;
            }
            
            //存在举报记录但已处理未超过30天的
            $newestreport = DI()->notorm->report->where("touid = ? AND status = 1",$uid)->order("uptime desc")->fetchOne();
            if (strtotime("+{$config['shopreport_refunddate']} days",$newestorder['uptime']) > $time){
                $rs['code'] = 1005;
                $rs['msg'] = "举报处理完成未过{$config['shopreport_refunddate']}个自然日，无法退款";
                return $rs;
            }
        }
        
        if ($type == "bond"){
            $price = $config['bond_price'];
        }
        
        if ($type == "cert"){
            $price = $config['cert_price'];
        }
        
        if ($type == "rebond"){
            $reorder = $this->trade_no();
            $ordernum = $order;
        }else{
            $ordernum = $this->trade_no();
        }
        
        $model = new Model_Userauth();
        $rst = $model->addAuthPay($uid,$price,$ordernum,$type,$reorder,$reason,$pay_code);
        if ($rst){
            $rs['info'][0] = $rst;
            return $rs;
        }else{
            $rs['code'] = '1006';
            $rs['msg'] = '添加失败';
            return $rs;
        }
    }
    
    public function addAuthWxPay($uid,$type,$order="",$reason=""){
        $rs = array('code' => 0, 'msg' => '成功', 'info' => array());
        $config = getConfigPri();
        $reorder = $reason = "";
        if (!in_array($type,["bond","cert","rebond"])){
            $rs['code'] = '1001';
            $rs['msg'] = 'type参数错误';
            return $rs;
        }
        
        if ($type == "rebond"){
            //查询相关支付订单
            $select = DI()->notorm->user_auth_payment->where("type = 'bond' AND uid = ? AND payment = ? AND status = 'SUCCESS'",$uid,$order)->order("addtime desc")->fetchOne();
            if (!$select){
                $rs['code'] = 1001;
                $rs['msg'] = "未查询到保证金支付订单";
                return $rs;
            }
            
            $time = time();
            $shop = DI()->notorm->shop->select("id,uid")->where("uid = ?",$uid)->fetchOne();
            //未完成的订单
            $nofinishorder = DI()->notorm->shop_order->where("shop_id = ? AND state < 50")->order("addtime desc")->fetchAll();
            if (count($nofinishorder) > 0){
                $rs['code'] = 1002;
                $rs['msg'] = "存在未完成的订单，无法退款";
                return $rs;
            }
            //订单完成后多少天才可以退保证金
            $newestorder = DI()->notorm->shop_order->where("shop_id = ?",$shop['id'])->order("addtime desc")->fetchOne();
            if (strtotime("+{$config['shop_refunddate']} days",$newestorder['addtime']) > $time){
                $rs['code'] = 1003;
                $rs['msg'] = "存在{$config['shop_refunddate']}天内的订单，无法退款";
                return $rs;
            }
            
            //存在举报记录且未处理
            $report = DI()->notorm->report->where("touid = ? AND status = 0",$uid)->fetchAll();
            if (count($report) > 0){
                $rs['code'] = 1004;
                $rs['msg'] = "存在举报记录，无法退款";
                return $rs;
            }
            
            //存在举报记录但已处理未超过30天的
            $newestreport = DI()->notorm->report->where("touid = ? AND status = 1",$uid)->order("uptime desc")->fetchOne();
            if (strtotime("+{$config['shopreport_refunddate']} days",$newestorder['uptime']) > $time){
                $rs['code'] = 1005;
                $rs['msg'] = "举报处理完成未过{$config['shopreport_refunddate']}个自然日，无法退款";
                return $rs;
            }
        }
        
        if ($type == "bond"){
            $price = $config['bond_price'];
        }
        
        if ($type == "cert"){
            $price = $config['cert_price'];
        }
        
        if ($type == "rebond"){
            $reorder = $this->trade_no();
            $ordernum = $order;
        }else{
            $ordernum = $this->trade_no();
        }
        
        $model = new Model_Userauth();
        $rst = $model->addAuthWxPay($uid,$price,$ordernum,$type,$reorder,$reason);
        if ($rst){
            $rs['info'][0] = $rst;
            return $rs;
        }else{
            $rs['code'] = '1006';
            $rs['msg'] = '添加失败';
            return $rs;
        }
    }
    
    //手动退款保证金
    public function addRefundBond($uid,$accountid,$reason=""){
        $rs = array('code' => 0, 'msg' => '成功', 'info' => array());
        $config = getConfigPri();
        $reorder = $reason = "";
        
        //查询相关支付订单
        $select = DI()->notorm->user_auth_payment->where("type = 'bond' AND uid = ? AND status = 'SUCCESS'",$uid)->order("addtime desc")->fetchOne();
        if (!$select){
            $rs['code'] = 1001;
            $rs['msg'] = "未查询到保证金支付订单";
            return $rs;
        }
        
        //查找账户
        $account = DI()->notorm->cash_account->where("uid = ? AND id = ?",$uid,$accountid)->fetchOne();
        if (!$account){
            $rs['code'] = 1007;
            $rs['msg'] = "未查询到该账户";
            return $rs;
        }
        
        $time = time();
        $shop = DI()->notorm->shop->select("id,uid")->where("uid = ?",$uid)->fetchOne();
        //未完成的订单
        $nofinishorder = DI()->notorm->shop_order->where("shop_id = ? AND state < 50",$shop['id'])->order("addtime desc")->fetchAll();
        if (count($nofinishorder) > 0){
            $rs['code'] = 1002;
            $rs['msg'] = "存在未完成的订单，无法退款";
            return $rs;
        }
        //订单完成后多少天才可以退保证金
        $newestorder = DI()->notorm->shop_order->where("shop_id = ?",$shop['id'])->order("addtime desc")->fetchOne();
        if (strtotime("+{$config['shop_refunddate']} days",$newestorder['addtime']) > $time){
            $rs['code'] = 1003;
            $rs['msg'] = "存在{$config['shop_refunddate']}天内的订单，无法退款";
            return $rs;
        }
        
        //存在举报记录且未处理
        $report = DI()->notorm->report->where("touid = ? AND status = 0",$uid)->fetchAll();
        if (count($report) > 0){
            $rs['code'] = 1004;
            $rs['msg'] = "存在举报记录，无法退款";
            return $rs;
        }
        
        //存在举报记录但已处理未超过30天的
        $newestreport = DI()->notorm->report->where("touid = ? AND status = 1",$uid)->order("uptime desc")->fetchOne();
        if (strtotime("+{$config['shopreport_refunddate']} days",$newestorder['uptime']) > $time){
            $rs['code'] = 1005;
            $rs['msg'] = "举报处理完成未过{$config['shopreport_refunddate']}个自然日，无法退款";
            return $rs;
        }
        
        $model = new Model_Userauth();
        $rst = $model->addRefundBond($uid,$accountid,$reason);
        if ($rst){
            $rs['info'][0] = $rst;
            return $rs;
        }else{
            $rs['code'] = '1006';
            $rs['msg'] = '添加失败';
            return $rs;
        }
    }
    
    //生成唯一订单号
	private function trade_no()
	{
		$time = explode(' ', microtime());
		return $time[1].intval($time[0] * 1000000).mt_rand(10000, 99999);
	}

    //保证金是否申请提现
    public function getBondRefundStatus($uid)
    {
        $has_refund_log = false;
        $refund_data = DI()->notorm->user_auth_refund->where("uid = ? AND type = 'rebond' AND status = ''",$uid)->fetchOne();

        if (!empty($refund_data)) {
            $has_refund_log = true;
        }
        return $has_refund_log;
    }

}