<?php

class Domain_Pd {

	public function getPdList($uid,$p) {
			$rs = array();

			$model = new Model_Pd();
			$rs = $model->getPdList($uid,$p);

			return $rs;
	}

	public function addPdCash($data) {
		$rs = array();

		try {
			$model = new Model_Pd();
			$rs = $model->addPdCash($data);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

		return $rs;
	}

	public function getPdData($uid) {
			$rs = array();

			$model = new Model_Pd();
			$rs = $model->getPdData($uid);

			return $rs;
	}

}