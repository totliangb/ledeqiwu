<?php

class Domain_Pay {

    public $payment_types = array(
        'ali'=>array(
            'id'=>'ali',
            'name'=>'支付宝支付',
        ),
        'wx'=>array(
            'id'=>'wx',
            'name'=>'微信支付',
        ),
        'aliscan'=>array(
            'id'=>'aliscan',
            'name'=>'当面付',
        ),
    );

    // 第三方支付
    public function _api_pay($order,$params)
    {
        $pay_code = $params['code'] ? $params['code'] : '';
        $notify_url = $params['notify_url'] ? $params['notify_url'] : '';

        $configpri = getConfigPri(); 
        $configpub = getConfigPub();

        if(empty($pay_code) && !isset($this->payment_types[$pay_code])){
            throw new Exception("未知的支付方式");
        }
        $nowPayCode = $this->payment_types[$pay_code]['id'];

        switch ($nowPayCode) {
            case 'ali':
                // 校验 当前支付方式是否开启
                if(!$configpri['aliapp_switch']){
                    throw new Exception("当前支付方式未开启"); 
                }
                // 定义支付宝需要的参数

                $paramsData = array();
                $paramsData['out_trade_no'] = $order['pay_sn'];
                $paramsData['subject'] = $order['order_sn'];
                $paramsData['body'] = $order['order_sn'];
                $paramsData['notify_url'] = $configpub['site'].$notify_url;
                $paramsData['total_amount'] = $order['amount'];
                $paramsData['extend_params'] = '';

                $ali = new \Alipay_Lite();
                $paramarr2 = $ali->setCofig()->apppay($paramsData);

                return $paramarr2;
                break;

            case 'wx':
                if(!$configpri['wx_switch']){
                    throw new Exception("当前支付方式未开启"); 
                }
                if($configpri['wx_appid']== "" || $configpri['wx_mchid']== "" || $configpri['wx_key']== ""){
                    throw new Exception("未配置支付相关参数");               
                }

                $noceStr = md5(rand(100,1000).time());//获取随机字符串
                $time = time();

                $paramarr = array(
                    "appid"       =>   $configpri['wx_appid'],
                    "body"        =>    $order['order_sn'],
                    "mch_id"      =>    $configpri['wx_mchid'],
                    "nonce_str"   =>    $noceStr,
                    "notify_url"  =>    $configpub['site'].$notify_url,
                    "out_trade_no"=>    $order['pay_sn'],
                    "total_fee"   =>    $order['amount']*100, 
                    "trade_type"  =>    "APP"
                );
                $sign = sign($paramarr,$configpri['wx_key']);//生成签名
                $paramarr['sign'] = $sign;
                $paramXml = "<xml>";
                foreach($paramarr as $k => $v){
                    $paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
                }
                $paramXml .= "</xml>";
                     
                $ch = curl_init ();
                @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
                @curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
                @curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/pay/unifiedorder");
                @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                @curl_setopt($ch, CURLOPT_POST, 1);
                @curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
                @$resultXmlStr = curl_exec($ch);
                if(curl_errno($ch)){
                    //print curl_error($ch);
                    file_put_contents('./appraisewxpay.txt',date('y-m-d H:i:s').' 提交参数信息 ch:'.json_encode(curl_error($ch))."\r\n",FILE_APPEND);
                }
                curl_close($ch);

                $result2 = xmlToArray($resultXmlStr);
                
                if($result2['return_code']=='FAIL'){
                    throw new Exception($result2['return_msg']);
                }
                $time2 = time();
                $prepayid = $result2['prepay_id'];
                $sign = "";
                $noceStr = md5(rand(100,1000).time());//获取随机字符串
                $paramarr2 = array(
                    "appid"     =>  $configpri['wx_appid'],
                    "noncestr"  =>  $noceStr,
                    "package"   =>  "Sign=WXPay",
                    "partnerid" =>  $configpri['wx_mchid'],
                    "prepayid"  =>  $prepayid,
                    "timestamp" =>  $time2
                );
                $paramarr2["sign"] = sign($paramarr2,$configpri['wx_key']);//生成签名
                
                return $paramarr2;
                break;
                
            case 'aliscan':
                if(!$configpri['aliscan_switch']){
                    throw new Exception("当前支付方式未开启"); 
                }
                break;
        }



    }

    // 第三方退款
    public function _api_refund($order,$params)
    {
        $pay_code = $params['code'] ? $params['code'] : '';

        $configpri = getConfigPri(); 
        $configpub = getConfigPub();

        if(empty($pay_code) && !isset($this->payment_types[$pay_code])){
            throw new Exception("未知的支付方式");
        }
        $nowPayCode = $this->payment_types[$pay_code]['id'];

        $result = false;
        switch ($nowPayCode) {
            case 'ali':
                // 校验 当前支付方式是否开启
                if(!$configpri['aliapp_switch']){
                    throw new Exception("当前支付方式未开启"); 
                }
                $ali = new \Alipay_Lite();
                $result = $ali->setCofig()->TradeRefundRequest($order);
                break;

            case 'wx':
                if(!$configpri['wx_switch']){
                    throw new Exception("当前支付方式未开启"); 
                }
                if($configpri['wx_appid']== "" || $configpri['wx_mchid']== "" || $configpri['wx_key']== ""){
                    throw new Exception("未配置支付相关参数");               
                }

                $wxpay = new \Wxpay_Lite();
                $result = $wxpay->RefundRequest($order);
                break;
        }
        if(!$result) throw new Exception('退款失败');
        return $result;
    }
}