<?php

class Domain_Expert {
	public function application($data) {
		$rs = array();

		$model = new Model_Expert();
		$rs = $model->application($data);

		return $rs;
	}

	public function application_update($data) {
		$rs = array();

		$model = new Model_Expert();
		$rs = $model->application_update($data);

		return $rs;
	}

	public function getApplyData($uid) {
		$rs = array();

		$model = new Model_Expert();
		$rs = $model->getApplyData($uid);

		return $rs;
	}

	public function getExpertUserData($assort_id) {
		$rs = array();

		$model = new Model_Expert();
		$rs = $model->getExpertUserData($assort_id);

		return $rs;
	}

	public function appraise($order,$data) {
		$rs = array();
		try {
			$model = new Model_Expert();
			$rs = $model->appraise($order,$data);	
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

		return $rs;
	}

	public function getExpertUserInfo($uid) {
		$rs = array();

		$model = new Model_Expert();
		$rs = $model->getExpertUserInfo($uid);

		return $rs;
	}

	public function isExpert($uid) {
		$rs = array();

		$model = new Model_Expert();
		$rs = $model->isExpert($uid);

		return $rs;
	}
}
