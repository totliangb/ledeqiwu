<?php

class Domain_FuliMarket {

    public function getLevel() {
        $rs = array();
        $model = new Model_FuliMarket();
        $rs = $model->getLevel();
        return $rs;
    }

    public function getFuliGoodsDataByLevel($level) {
        $rs = array();
        $model = new Model_FuliMarket();
        $rs = $model->getFuliGoodsDataByLevel($level);
        return $rs;
    }

    public function getFuliGoodsIndexData() {
        $rs = array();
        $model = new Model_FuliMarket();
        $rs = $model->getFuliGoodsIndexData();
        return $rs;
    }
    
    public function getFuliGoodsCount() {
        $rs = array();
        $model = new Model_FuliMarket();
        $rs = $model->getFuliGoodsCount();
        return $rs;
    }

    public function getFuliGoodsSharedData($uid,$goods_id) {
        $rs = array();
        $model = new Model_FuliMarket();
        $rs = $model->getFuliGoodsSharedData($uid,$goods_id);
        return $rs;
    }

    public function orderCreate($commodity_info,$uid,$address_info) {
        $rs = array();
        $model = new Model_FuliMarket();
        $rs = $model->orderCreate($commodity_info,$uid,$address_info);
        return $rs;
    }

}