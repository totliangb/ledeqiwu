<?php

class Domain_Recommendlive {

    public function getRecommendSlide() {
        $rs = array();
        $model = new Model_Recommendlive();
        $rs = $model->getRecommendSlide();
        return $rs;
    }

	public function getLiveRoomList($p) {
        $rs = array();

        $model = new Model_Recommendlive();
        $rs = $model->getLiveRoomList($p);
				
        return $rs;
    }
}