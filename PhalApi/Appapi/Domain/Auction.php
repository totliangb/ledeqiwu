<?php

class Domain_Auction {

    public function getNowAuctionGoods($liveuid,$showid) {
        $rs = array();

        $model = new Model_Auction();
        $rs = $model->getNowAuctionGoods($liveuid,$showid);
                
        return $rs;
    }

    public function createAuctionOrder($liveuid,$showid,$win_data){
        $rs = array();

        $model = new Model_Auction();
        $rs = $model->createAuctionOrder($liveuid,$showid,$win_data);
                
        return $rs;
    }

    public function completingAuctionOrderAddress($order_id,$address_info){
        $rs = array();

        $model = new Model_Auction();
        $rs = $model->completingAuctionOrderAddress($order_id,$address_info);
                
        return $rs;
    }
    
    public function withoutAuction($liveuid,$showid){
        $rs = array();

        $model = new Model_Auction();
        $rs = $model->withoutAuction($liveuid,$showid);
                
        return $rs;
    }

}