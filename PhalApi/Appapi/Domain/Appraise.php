<?php

class Domain_Appraise {

    public function getAppraiseList($p) {
        $rs = array();

        $model = new Model_Appraise();
        $rs = $model->getAppraiseList($p);
                
        return $rs;
    }

    public function getAppraiseInfo($id,$uid) {
        $rs = array();

        $model = new Model_Appraise();
        $rs = $model->getAppraiseInfo($id,$uid);
                
        return $rs;
    }

    public function getViewAppraiseAnswerList($id,$p) {
        $rs = array();

        $model = new Model_Appraise();
        $rs = $model->getViewAppraiseAnswerList($id,$p);
                
        return $rs;
    }

    //生成唯一订单号
    private function makeOrderSn()
    {
        $time = explode(' ', microtime());
        return $time[1].intval($time[0] * 1000000).mt_rand(10000, 99999);
    }
    
    /**
     * 生成支付单编号(两位随机 + 从2000-01-01 00:00:00 到现在的秒数+微秒+会员ID%1000)，该值会传给第三方支付接口
     * 长度 =2位 + 10位 + 3位 + 3位  = 18位
     * 1000个会员同一微秒提订单，重复机率为1/100
     * @return string
     */
    private function makePaySn($member_id) {
        return mt_rand(10,99)
              . sprintf('%010d',time() - 946656000)
              . sprintf('%03d', (float) microtime() * 1000)
              . sprintf('%03d', (int) $member_id % 1000);
    }

    public function applyAppraise($data) {
        $rs = array();

        if (!empty($data)) {
            // 生成 订单号 支付号
            $data['apply_order_sn'] = $this->makeOrderSn();
            $data['pay_sn'] = $this->makePaySn($data['uid']);
            // 
            $data['add_time'] = time();
        }
        try {
            $model = new Model_Appraise();
            $rs = $model->applyAppraise($data);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $rs;
    }

    public function myAppraiseList($uid,$p,$other)
    {
        $rs = array();

        $model = new Model_Appraise();
        $rs = $model->myAppraiseList($uid,$p,$other);
                
        return $rs;
    }

    public function updateAppraiseViewNum($id,$uid,$old_view_num)
    {
        $rs = array();

        $model = new Model_Appraise();
        $rs = $model->updateAppraiseViewNum($id,$uid,$old_view_num);
                
        return $rs;
    }

    public function applyAppraiseAnswerView($data) {
        $rs = array();

        if (!empty($data)) {
            // 获取查看答案所需支付的金额
            $configpri=getConfigPri();
            $view_fee = isset($configpri['appraise_answer_view_fee']) ? $configpri['appraise_answer_view_fee'] : 0;
            $data['view_fee'] = $view_fee;

            // 生成 订单号 支付号
            $data['view_order_sn'] = $this->makeOrderSn();
            $data['pay_sn'] = $this->makePaySn($data['uid']);
            // 
            $data['add_time'] = time();
        }

        try {
            $model = new Model_Appraise();
            $rs = $model->applyAppraiseAnswerView($data);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $rs;
    }

    // 微信退款（鉴定订单）
    public function wxRefund($order){
        $configpri = getConfigPri(); 
        $configpub = getConfigPub(); 

         //配置参数检测
                    
        if($configpri['wx_appid']== "" || $configpri['wx_mchid']== "" || $configpri['wx_key']== ""){
            $rs['code'] = 1002;
            $rs['msg'] = '微信未配置';
            return $rs;                  
        }
        require_once(API_ROOT."/Library/WxPay/Lite.php");
        $wxpay = new Wxpay_Lite();
        $result = $wxpay->RefundRequest($order);
        return $result;
    }

}
