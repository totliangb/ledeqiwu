<?php
require_once 'lib/WxPay.Api.php';

/**
* 微信支付
*/
class Wxpay_Lite
{
    protected $config;

    //设置
    public function __construct()
    {
        $configpri = getConfigPri();
        $this->config = [
            'wx_appid' => $configpri['wx_appid'],
            'wx_mchid' => $configpri['wx_mchid'],
            'wx_key' => $configpri['wx_key']
        ];
        return $this;
    }

    public function RefundRequest($order)
    {
        $input = new WxPayRefund();

        $input->SetOut_trade_no($order['numbering']);
        $input->SetOut_refund_no($this->trade_no());
        $input->SetTotal_fee($order['payment_amount']*100);
        $input->SetRefund_fee($order['payment_amount']*100);
        $input->SetOp_user_id($configpri['wx_mchid']);
        
        $result = WxPayApi::refund($input);

        if($result['result_code'] == "SUCCESS"){
            return 1;
        } else {
            return 0;
        }
    }
    
    //生成唯一订单号
	private function trade_no()
	{
		$time = explode(' ', microtime());
		return $time[1].intval($time[0] * 1000000).mt_rand(10000, 99999);
	}
}