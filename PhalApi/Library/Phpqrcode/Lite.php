<?php
/**
 * 生成二维码
 */

include dirname(__FILE__) . '/phpqrcode/phpqrcode.php';

class Phpqrcode_Lite {

	public function flies($url)
	{
		$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR; 
		//$data = 'https://www.baidu.com';
		$ecc = 'H'; // L-smallest, M, Q, H-best 
		$size = 4; // 1-50 
		$filename = $PNG_TEMP_DIR.'qrcode_'.time().'.png'; 
		QRcode::png($url, $filename, $ecc, $size); 
		chmod($filename, 0777); 
		$data = DI()->qiniu->uploadFile2($filename);
        
		if($data)
		{
			unlink($filename);
		}
		return $data;
	}
}