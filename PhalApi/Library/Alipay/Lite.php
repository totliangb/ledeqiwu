<?php
require_once 'AopClient.php';
require_once 'AopCertification.php';
require_once 'request/AlipayTradeQueryRequest.php';
require_once 'request/AlipayTradeWapPayRequest.php';
require_once 'request/AlipayTradeAppPayRequest.php';
require_once 'request/AlipayTradeRefundRequest.php';

/**
* 阿里云支付
*/
class Alipay_Lite
{
    protected $config;

    //设置
    public function setCofig()
    {
        $configpri = getConfigPri();
        $this->config = [
            'aliscan_rsakey' => $configpri['aliapp_key_android'],
            'aliscan_pubkey' => $configpri['aliapp_alipayrsaPublicKey'],
            'appId' => $configpri['aliapp_partner']
        ];
        return $this;
    }

    public function TradeRefundRequest($data)
    {
        $aop = new AopClient();
        $aop->appId = $this->config['appId'];
        $aop->rsaPrivateKey = $this->config['aliscan_rsakey'];
        $aop->alipayrsaPublicKey=$this->config['aliscan_pubkey'];
        $aop->signType = 'RSA2';
        $request = new AlipayTradeRefundRequest();
        $request->setBizContent(json_encode([
            'out_trade_no' => $data['numbering'],
            'refund_amount' => $data['payment_amount'],
        ]));
        $result = $aop->execute ( $request); 

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        file_put_contents('ali'.date('Ymd').'.txt',date('Y-m-d H:i:s').'  msg:'.json_encode($result->$responseNode)."\r\n",FILE_APPEND);
        if(!empty($resultCode)&&$resultCode == 10000){
            return 1;
        } else {
            return 0;
        }
    }

    public function apppay($data)
    {
        $aop = new AopClient;

        $aop->appId = $this->config['appId'];
        $aop->rsaPrivateKey = $this->config['aliscan_rsakey'];
        $aop->alipayrsaPublicKey=$this->config['aliscan_pubkey'];
        $aop->signType = 'RSA2';

        $request = new AlipayTradeAppPayRequest();

        //商户订单号，商户网站订单系统中唯一订单号，必填
        $out_trade_no = trim($data['out_trade_no']);

        //订单名称，必填
        $subject = trim($data['subject']);

        //付款金额，必填
        $total_amount = floatval($data['total_amount']);

        //商品描述，可空
        $body = trim($data['body']);

        //业务扩展参数，可空
        $passbackParams = trim($data['extend_params']);

        //超时时间
        $timeout_express="30m";

        $bizContent = array(
            'body' => $body,
            'subject' => $subject,
            'out_trade_no' => $out_trade_no,
            'timeout_express' => $timeout_express,
            'total_amount' => $total_amount,
            'product_code' => 'QUICK_MSECURITY_PAY',
        );
        $bizContent = json_encode($bizContent);

        $request->setNotifyUrl($data['notify_url']);

        $request->setBizContent($bizContent);

        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $aop->sdkExecute($request);
        //htmlspecialchars是为了输出到页面时防止被浏览器将关键参数html转义，实际打印到日志以及http传输不会有这个问题
        return ($response);//就是orderString 可以直接给客户端请求，无需再做处理。
    }
}