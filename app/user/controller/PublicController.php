<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------
namespace app\user\controller;

use cmf\controller\HomeBaseController;
use app\user\model\UserModel;
use think\Validate;
// 引入鉴权类  
use Qiniu\Auth;
// 引入上传类  
use Qiniu\Storage\UploadManager;  
use think\Db;
use think\facade\View;
use cmf\lib\Upload;

class PublicController extends HomeBaseController
{

    // 用户头像api
    public function avatar()
    {
        $id   = $this->request->param("id", 0, "intval");
        $user = UserModel::get($id);

        $avatar = '';
        if (!empty($user)) {
            $avatar = cmf_get_user_avatar_url($user['avatar']);
            if (strpos($avatar, "/") === 0) {
                $avatar = $this->request->domain() . $avatar;
            }
        }

        if (empty($avatar)) {
            $cdnSettings = cmf_get_option('cdn_settings');
            if (empty($cdnSettings['cdn_static_root'])) {
                $avatar = $this->request->domain() . "/static/images/headicon.png";
            } else {
                $cdnStaticRoot = rtrim($cdnSettings['cdn_static_root'], '/');
                $avatar        = $cdnStaticRoot . "/static/images/headicon.png";
            }

        }

        return redirect($avatar);
    }
    
    /**
     * webuploader 上传
     */
    public function webuploader()
    {
        $file = request()->file('img');
        if(!$file)
        {
            echo json_encode(['code'=>1, 'info'=>'上传文件不存在']);exit;
        }
        
        $file_url = '../upload/user/';
        
        
        $config = Db::name('plugin')->where('name','Qiniu')->value('config');
        $config = json_decode($config, true);
        
        // 构建一个鉴权对象  
        $qiniu = new Auth($config['accessKey'],$config['secretKey']);  
    
        // 生成上传的token  
        $token = $qiniu->uploadToken($config['bucket']);
        $uploadMgr = new UploadManager();  
        
        $file_name = [];
        foreach ($file as $k =>$v)
        {
            $info = $v->move($file_url);
            $data = $uploadMgr->putFile($token,$info->getFilename(),WEB_ROOT.'/../upload/user/'.$info->getSaveName());
            $file_name[] = '/'.$info->getFilename();
        }
        
        echo json_encode(['code'=>0, 'data'=>$file_name]);exit;
    }
    
    /**
     * webuploader 上传
     */
    public function webuploader2()
    {
        if ($this->request->isPost()) {

            $file = request()->file('file');
            if(!$file)
            {
                echo json_encode(['code'=>1, 'info'=>'上传文件不存在']);exit;
            }
            
            $file_url = '../upload/user/';
            
            
            $config = Db::name('plugin')->where('name','Qiniu')->value('config');
            $config = json_decode($config, true);
            
            // 构建一个鉴权对象  
            $qiniu = new Auth($config['accessKey'],$config['secretKey']);  
        
            // 生成上传的token  
            $token = $qiniu->uploadToken($config['bucket']);
            $uploadMgr = new UploadManager();  
            
            $file_name = [];
            $info = $file->move($file_url);
            $data = $uploadMgr->putFile($token,$info->getFilename(),WEB_ROOT.'/../upload/user/'.$info->getSaveName());
            $file_name = $config['protocol'].'://'.$config['domain'].'/'.$info->getFilename();
            
            $this->success("上传成功!", '', ['filepath' => '/'.$info->getFilename(), 'name' => $_POST['name'], 'id' => $_POST['id'], 'preview_url' => $file_name, 'url' => $file_name]);


        } else {
            $uploadSetting = cmf_get_upload_setting();

            $arrFileTypes = [
                'image' => ['title' => 'Image files', 'extensions' => $uploadSetting['file_types']['image']['extensions']],
                'video' => ['title' => 'Video files', 'extensions' => $uploadSetting['file_types']['video']['extensions']],
                'audio' => ['title' => 'Audio files', 'extensions' => $uploadSetting['file_types']['audio']['extensions']],
                'file'  => ['title' => 'Custom files', 'extensions' => $uploadSetting['file_types']['file']['extensions']]
            ];

            $arrData = $this->request->param();
            if (empty($arrData["filetype"])) {
                $arrData["filetype"] = "image";
            }

            $fileType = $arrData["filetype"];

            if (array_key_exists($arrData["filetype"], $arrFileTypes)) {
                $extensions                = $uploadSetting['file_types'][$arrData["filetype"]]['extensions'];
                $fileTypeUploadMaxFileSize = $uploadSetting['file_types'][$fileType]['upload_max_filesize'];
            } else {
                $this->error('上传文件类型配置错误！');
            }


            View::share('filetype', $arrData["filetype"]);
            View::share('extensions', $extensions);
            View::share('upload_max_filesize', $fileTypeUploadMaxFileSize * 1024);
            View::share('upload_max_filesize_mb', intval($fileTypeUploadMaxFileSize / 1024));
            $maxFiles  = intval($uploadSetting['max_files']);
            $maxFiles  = empty($maxFiles) ? 20 : $maxFiles;
            $chunkSize = intval($uploadSetting['chunk_size']);
            $chunkSize = empty($chunkSize) ? 512 : $chunkSize;
            View::share('max_files', $arrData["multi"] ? $maxFiles : 1);
            View::share('chunk_size', $chunkSize); //// 单位KB
            View::share('multi', $arrData["multi"]);
            View::share('app', $arrData["app"]);

            $content = hook_one('fetch_upload_view');

            $tabs = ['local', 'url', 'cloud'];

            $tab = !empty($arrData['tab']) && in_array($arrData['tab'], $tabs) ? $arrData['tab'] : 'local';

            if (!empty($content)) {
                $this->assign('has_cloud_storage', true);
            }

            if (!empty($content) && $tab == 'cloud') {
                return $content;
            }

            $tab = $tab == 'cloud' ? 'local' : $tab;

            $this->assign('tab', $tab);
            return $this->fetch(":webuploader");

        }
    }

}
