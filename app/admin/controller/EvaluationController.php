<?php
namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;
/**
* 订单评价
*/
class EvaluationController extends AdminbaseController
{
	protected $table = 'shop_evaluation';

	public function lists()
	{

		$list = Db::name($this->table)->alias('a')
				->join('__SHOP_ORDER__ b ',' a.order_id = b.id')
				->join('__USER__ c ',' a.uid = c.id')
				->field('a.id,a.rating,a.content,a.addtime,b.numbering,c.user_nicename')
				->order('a.addtime desc')
				->paginate(20);

		if($list)
		{
			$lists = $list->items();
			foreach ($lists as $key => &$value) 
			{
				$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
			}
		}

		$this->assign("page", $list->render());
        $this->assign('list', $lists ? $lists : []);
        return $this->fetch();
	}

	public function views()
	{
		$id = request()->param('id', 'intval');

		if(!$id) $this->error('参数错误');

		$info = Db::name($this->table)->alias('a')
						->join('__SHOP_ORDER__ b ',' a.order_id = b.id')
						->join('__USER__ c ',' a.uid = c.id')
						->field('a.id,a.rating,a.content,a.addtime,a.img,a.reply_content,a.reply_time,b.numbering,c.user_nicename')
						->where('a.id', $id)
						->find();
		$info['addtime'] = date('Y-m-d H:i:s', $info['addtime']);
		if(!empty($info['reply_time']))$info['reply_time'] = date('Y-m-d H:i:s', $info['reply_time']);
		if(!empty($info['img'])) $info['img'] = json_decode($info['img'], true);
		$this->assign('info', $info);
		return $this->fetch();
	}
}