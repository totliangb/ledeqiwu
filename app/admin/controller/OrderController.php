<?php
namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;
//订单
class OrderController extends AdminbaseController{
	protected $table = 'shop_order';

	protected function state($num)
	{
		$arr = ['待支付',10 => '取消',20 => '已支付',30=>'已发货',40=>'退货/退款申请',41=>'退货成功',42=>'卖家同意退货',43=>'正在退货',50=>'已收货',60=>'已评价'];

		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}

	public function views()
	{
		$id = request()->param('id', 'intval');
		//获取订单信息
		$order = Db::name('shop_order')->where('id', $id)->find();
        
        $address = Db::name("shop_address")->where("id",$order['address'])->find();
		if(!$order) exit('信息不存在');
		//获取商品详情
		$order_list = Db::name('shop_order_info')->where('order_id='.$order['id'])->select();

		//店铺信息
		$shop = Db::name('shop')->where('id',$order['shop_id'])->value('name');

		//购买人信息
		$user = Db::name('user')->where('id',$order['uid'])->value('user_nicename');
        
		$this->assign('id', $id);
		$this->assign('shop_name', $shop);
		$this->assign('user_name', $user);
		$this->assign('order_list', $order_list);
		$this->assign('order', $order);
		$this->assign("address",$address);
        return $this->fetch();
	}
}