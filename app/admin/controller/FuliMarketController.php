<?php
/**
 * 福利商城管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
    
class FuliMarketController extends AdminbaseController {

    /**
     * 邀请等级管理
     */
    public function level()
    {
        $lists = Db::name("fuli_level")
            ->order("level_id asc")
            ->paginate(20);
        
        $page = $lists->render();

        $this->assign('lists', $lists);

        $this->assign("page", $page);
        
        return $this->fetch();
    }

    
    public function level_del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('fuli_level')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $action="删除邀请等级：{$id}";
        setAdminLog($action);
                    
        $this->levelResetCache();
        $this->success("删除成功！",url("FuliMarket/level"));
            
    }       

    public function level_add(){
        return $this->fetch();
    }
    
    public function levelAddPost(){
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();

            $level_id=$data['level_id'];

            if($level_id==""){
                $this->error("等级不能为空");
            }
            
            $check = Db::name('fuli_level')->where(["level_id"=>$level_id])->find();
            if($check){
                $this->error('等级不能重复');
            }
                
            $goods_shared_num=$data['goods_shared_num'];
            if($goods_shared_num==""){
                $this->error("请填写邀请人数");
            }
            
            $data['addtime']=time();
            
            $id = DB::name('fuli_level')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            
            $action="添加邀请等级：{$id}";
            setAdminLog($action);
            
            $this->levelResetCache();
            $this->success("添加成功！");
            
        }           
    }
        
    public function level_edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('fuli_level')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('data', $data);
        return $this->fetch();
    }
    
    public function levelEditPost(){
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
            $id=$data['id'];
            $level_id=$data['level_id'];

            if($level_id==""){
                $this->error("等级不能为空");
            }
            
            $check = Db::name('fuli_level')->where([['level_id','=',$level_id],['id','<>',$id]])->find();
            if($check){
                $this->error('等级不能重复');
            }
                
            $goods_shared_num=$data['goods_shared_num'];
            if($goods_shared_num==""){
                $this->error("请填写邀请人数");
            }
            $rs = DB::name('fuli_level')->update($data);
            if($rs===false){
                $this->error("修改失败！");
            }
            
            $action="编辑邀请等级：{$data['id']}";
            setAdminLog($action);
            
            $this->levelResetCache();
            $this->success("修改成功！");
        }
    }
    
    public function levelResetCache(){
        $key='fulilevel';
        
        $level= Db::name("fuli_level")->field('level_id,level_name,goods_shared_num')->where('goods_shared_num > 0')->order("goods_shared_num desc")->select();
        if($level){
            setcaches($key,$level);
        }
        
        return 1;
    }

    /**
     * 福利商城商品管理
     */
    public function goods_list()
    {
        $data = $this->request->param();

        $map=[];
        $lists = Db::name("fuli_goods")
                ->where($map)
                ->order("goods_add_time DESC")
                ->paginate(20);

        $lists->each(function($v,$k){
            // $v['status_name'] = $this->getStatusName($v['status'],$v['pay_status']);
            // $v['pay_type_name'] = $v['pay_type'] ? $this->getPayType($v['pay_type']) : '';

            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

        $this->assign('lists', $lists);

        $this->assign("page", $page);
        
        return $this->fetch();
    }

    /**
     * 福利商品添加
     */
    public function goods_add()
    {
        $fuli_level = getcaches('fulilevel');

        if(!empty($fuli_level)){
            // 升序
            usort($fuli_level, function($a,$b){
                if ($a['level_id']==$b['level_id']) return 0;
                return ($a['level_id']<$b['level_id'])?-1:1;
            });
        }

        $this->assign("fuli_level", $fuli_level);

        return $this->fetch();
    }

    /**
     * 福利商品添加处理
     */
    function addPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();

            if (!$data['level_id']) {
                $this->error("请选择邀请等级");
            }else{
                // 获取邀请人数
                $fuli_level = getcaches('fulilevel');
                if($fuli_level){
                    $fuli_level = array_column($fuli_level, NULL, 'level_id');
                    if(isset($fuli_level[$data['level_id']]) && $fuli_level[$data['level_id']]){
                        // 更新 邀请人数
                        $data['goods_shared_num'] = $fuli_level[$data['level_id']]['goods_shared_num'];
                    }
                }else{
                    $this->error("请添加邀请等级后添加商品");
                }
            }

            $insert_data = array(
                'goods_name' => $data['goods_name'],
                'goods_price' => $data['goods_price'],
                'level_id' => $data['level_id'],
                'goods_shared_num' => $data['goods_shared_num'],
                'goods_image' => $data['goods_image'],
                'goods_storage' => $data['goods_storage'],
                'goods_add_time' => time(),
                'goods_body' => isset($data['goods_body']) ? $data['goods_body'] : '',
                'vendor_person' => $data['vendor_person'],
                'vendor_tel' => $data['vendor_tel'],
                'carousel_url' => $data['carousel_url'] ? json_encode($data['carousel_url']) : '',
                'carousel_names' => $data['carousel_names'] ? json_encode($data['carousel_names']) : '',
            );
            $rs = DB::name('fuli_goods')->insert($insert_data);
            if($rs===false){
                $this->error("添加失败！");
            }
            
            $action="添加福利商品信息：{$rs['id']}";
            setAdminLog($action);
            
            $this->success("添加成功！");
                             
        }       
    }

    /**
     * 福利商品编辑
     */
    public function goods_edit()
    {
        $id = 0;
        if(request()->param('id'))
        {
            $id = request()->param('id', 'intval');
            $info = Db::name('fuli_goods')
                    ->where('id', $id)
                    ->field('*')
                    ->find();
            if(!empty($info)){
                $info['carousel_url'] = json_decode($info['carousel_url'],true);
                if($info['carousel_url']) {
                    foreach ($info['carousel_url'] as $key => $value) {
                        $item = array();
                        $item['url'] = get_upload_path($value, 0);
                        $item['val'] = $value;
                        $info['carousel_url'][$key] = $item;
                    }
                }

                $info['carousel_names'] = json_decode($info['carousel_names'],true);

            }

            $this->assign('info', $info);
        }

        $fuli_level = getcaches('fulilevel');

        if(!empty($fuli_level)){
            // 升序
            usort($fuli_level, function($a,$b){
                if ($a['level_id']==$b['level_id']) return 0;
                return ($a['level_id']<$b['level_id'])?-1:1;
            });
        }

        $this->assign("fuli_level", $fuli_level);

        $this->assign('id', $id);

        return $this->fetch();
    }

    /**
     * 福利商品编辑处理
     */
    function editPost()
    {
        if ($this->request->isPost()) {
            $data      = $this->request->param();

            if (!$data['level_id']) {
                $this->error("请选择邀请等级");
            }else{
                // 获取邀请人数
                $fuli_level = getcaches('fulilevel');
                if($fuli_level){
                    $fuli_level = array_column($fuli_level, NULL, 'level_id');
                    if(isset($fuli_level[$data['level_id']]) && $fuli_level[$data['level_id']]){
                        // 更新 邀请人数
                        $data['goods_shared_num'] = $fuli_level[$data['level_id']]['goods_shared_num'];
                    }
                }else{
                    $this->error("请添加邀请等级后添加商品");
                }
            }

            $update_data = array(
                'goods_name' => $data['goods_name'],
                'goods_price' => $data['goods_price'],
                'level_id' => $data['level_id'],
                'goods_shared_num' => $data['goods_shared_num'],
                'goods_image' => $data['goods_image'],
                'goods_storage' => $data['goods_storage'],
                'goods_body' => isset($data['goods_body']) ? $data['goods_body'] : '',
                'vendor_person' => $data['vendor_person'],
                'vendor_tel' => $data['vendor_tel'],
                'carousel_url' => $data['carousel_url'] ? json_encode($data['carousel_url']) : '',
                'carousel_names' => $data['carousel_names'] ? json_encode($data['carousel_names']) : '',
            );
            $where = array(
                'id'=>$data['id']
            );
            $rs = DB::name('fuli_goods')->where($where)->update($update_data);
            if($rs===false){
                $this->error("编辑失败！");
            }
            
            $action="编辑福利商品信息：{$rs['id']}";
            setAdminLog($action);
            
            $this->success("编辑成功！");
                             
        }       
    }

    /**
     * 福利商品删除
     */
    public function goods_delete()
    {
        $id = $this->request->param('id', 0, 'intval');

        $rs = DB::name('fuli_goods')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }

        $action="删除积分商品：{$id}";
        setAdminLog($action);

        $this->success("删除成功！",url("FuliMarket/goods_list"));

    }

    protected function getStatus($k=''){
        $status=array(
            '10'=>'待发货',
            '30'=>'已发货',
            '40'=>'已收货',
            '50'=>'已完成',
            '2'=>'已取消',
        );
        if($k===''){
            return $status;
        }
        
        return isset($status[$k]) ? $status[$k]: '';
    }

    /**
     * 福利商城商品管理
     */
    public function order_list()
    {
        /**搜索条件**/
        $data = $this->request->param();
        $map=[];

        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['add_time','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['add_time','<=',strtotime($end_time) + 60*60*24];
        }
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $statusItem = $this->getStatus($status);
            if(!empty($statusItem)){
                $map = array_merge($map,array('status'=>$status));
            }
        }else{
            $map[] = ['status','neq',35];
        }
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['uid','=',$uid];
            }
        }

        $order_sn=isset($data['order_sn']) ? $data['order_sn']: '';
        if($order_sn!=''){
            $map[]=['order_sn','like','%'.$order_sn.'%'];
        }

        $lists = Db::name("fuli_order")
                ->where($map)
                ->order("add_time DESC")
                ->paginate(20);

        $lists->each(function($v,$k){
            $v['status_name'] = $this->getStatus($v['status']);

            $itemUserinfo = getUserInfo($v['uid'],0);
            $v['nickname'] = $itemUserinfo['user_nicename'];
            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

        $this->assign('lists', $lists);

        $this->assign("page", $page);

        $this->assign('status', $this->getStatus());
        
        return $this->fetch();
    }

    /**
     * 福利订单 详情处理
     */
    public function edit_order()
    {
        $id = request()->param('id', 'intval');
        //获取订单信息
        $order = Db::name('fuli_order')->where('id', $id)->find();
        
        $address = Db::name("shop_address")->where("id",$order['address'])->find();
        if(!$order) exit('信息不存在');
        //获取商品详情
        $order_list = Db::name('fuli_ordergoods')->where('orderid='.$order['id'])->select();

        $logistics = Db::name('shop_logistics')->field('id,name')->order('sort desc')->where('state=1')->select()->toArray();

        foreach ($logistics as $key => $value) 
        {
            if($value['id'] == $order['logistics_id'])
            {
                $order['logistics_name'] = $value['name'];
            }
        }

        foreach ($order_list as $key => $value) {
            // 获取当前商品发货联系人及联系方式
            $goodsInfo = Db::name('fuli_goods')->where('id='.$value['goodsid'])->find();
            $value['vendor_person'] = $goodsInfo['vendor_person'] ? $goodsInfo['vendor_person'] : '-';
            $value['vendor_tel'] = $goodsInfo['vendor_tel'] ? $goodsInfo['vendor_tel'] : '-';
            $order_list[$key] = $value;
        }
        
        $this->assign('id', $id);
        $this->assign('order_list', $order_list);
        $this->assign('order', $order);
        $this->assign("address",$address);
        $this->assign('shop_logistics', json_encode($logistics));
        return $this->fetch();
    }

    /**
     * 福利订单 发货处理
     */
    public function shipPost()
    {
        $id = request()->param('id', 'intval');
        $data['logistics_id'] = request()->param('logistics_id', 'intval');
        $data['shippingcode'] = request()->param('shippingcode');
        if(!$id || !$data['logistics_id'] || !$data['shippingcode']) $this->error('参数错误');

        $data['status'] = 30;
        $data['shippingtime'] = time();
        if(!Db::name('fuli_order')->where('status in (10) and id='.$id)->update($data)) $this->error('发货失败');

        $action="福利订单发货：{$id}";
        setAdminLog($action);

        $this->success('发货成功');
    }

}
