<?php
/**
 * 鉴宝管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
    
class ExpertController extends AdminbaseController {

    /**
     * 申请状态 
     */
    protected function getStatus($k=''){
        $status=array(
            '0'=>'待审核',
            '1'=>'审核通过',
            '2'=>'审核未通过',
        );
        if($k===''){
            return $status;
        }
        
        return isset($status[$k]) ? $status[$k]: '';
    }

    /**
     * 获取分类数据
     */
    protected function getCategory($k=''){
        $data=Db::name('appraise_assort')
            ->field("id,name,img")
            ->where("state = 1")
            ->order("sort desc")
            ->select()->toArray();

        if($k===''){
            return $data;
        }else{
            $nameData = array_column($data,'name','id');
            return isset($nameData[$k]) ? $nameData[$k]: '';
        }
    }

    /**
     * 鉴宝专家列表 
     */
    public function expert_user()
    {
        // 专家申请记录
        /**搜索条件**/
        $data = $this->request->param();
        $map=[];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['apply_time','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['apply_time','<=',strtotime($end_time) + 60*60*24];
        }
        
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $map[]=['apply_status','=',$status];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['uid','=',$uid];
            }
        }

        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['realname|phone|card_no','like','%'.$keyword.'%'];
        }

        $lists = Db::name("user_expert")
                ->where($map)
                ->order("apply_time DESC")
                ->paginate(20);
        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
            $v['phone']=m_s($v['phone']);
            $v['card_no']=m_s($v['card_no']);

            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

        $this->assign('lists', $lists);

        $this->assign("page", $page);

        $this->assign('status', $this->getStatus());
        
        return $this->fetch();
    }

    /**
     * 专家申请信息详情
     */
    public function info()
    {
        $uid   = $this->request->param('uid', 0, 'intval');
        
        $data=Db::name('user_expert')
            ->where("uid={$uid}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        $classData=Db::name('user_expert_class')
            ->where("uid={$uid}")
            ->select();

        foreach ($classData as $key => $value) {
            $value['name'] = $this->getCategory($value['assort_id']);
            $classData[$key] = $value;
        }
        
        $data['userinfo']=getUserInfo($data['uid']);
        $data['phone']=m_s($data['phone']);
        $data['card_no']=m_s($data['card_no']);
        if(!empty($data['front_card_view'])){
            $data['front_card_view'] = str_replace("[", "", $data['front_card_view']);
            $data['front_card_view'] = str_replace("]", "", $data['front_card_view']);
            $data['front_card_view'] = str_replace("\\", "", $data['front_card_view']);
            $data['front_card_view'] = str_replace("\"", "", $data['front_card_view']);

            $img_urls = explode(",", $data['front_card_view']);
            $data['front_card_view'] = get_upload_path($img_urls[0],0);
        }
        if(!empty($data['back_card_view'])){
            $data['back_card_view'] = str_replace("[", "", $data['back_card_view']);
            $data['back_card_view'] = str_replace("]", "", $data['back_card_view']);
            $data['back_card_view'] = str_replace("\\", "", $data['back_card_view']);
            $data['back_card_view'] = str_replace("\"", "", $data['back_card_view']);

            $img_urls = explode(",", $data['back_card_view']);
            $data['back_card_view'] = get_upload_path($img_urls[0],0);
        }
        if(!empty($data['hand_card_view'])){
            $data['hand_card_view'] = str_replace("[", "", $data['hand_card_view']);
            $data['hand_card_view'] = str_replace("]", "", $data['hand_card_view']);
            $data['hand_card_view'] = str_replace("\\", "", $data['hand_card_view']);
            $data['hand_card_view'] = str_replace("\"", "", $data['hand_card_view']);

            $img_urls = explode(",", $data['hand_card_view']);
            $data['hand_card_view'] = get_upload_path($img_urls[0],0);
        }
        if(!empty($data['cert_view'])){
            $cert_view = json_decode($data['cert_view'],true);
            foreach ($cert_view as $key => &$value) {
                $value = get_upload_path($value, 0);
            }
            $data['cert_view'] = $cert_view;
        }
        
        $this->assign('category', $this->getCategory());
        $this->assign('classData', $classData);

        $this->assign('status', $this->getStatus());
        
        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * 专家申请审核操作(通过/不通过)
     */
    public function passPost()
    {
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            $uid=$data['uid'];

            if(isset($data['change_fee']) && $data['change_fee']){
                unset($data['change_fee']);
                
                // 更改鉴定费操作
                $rs = DB::name('user_expert')->update($data);
                if($rs===false){
                    $this->error("更新失败！");
                }else{
                    $this->success("更新成功！");
                }

                $action="更新鉴宝专家费用：{$uid}";
                
                setAdminLog($action);

                $this->success("更新成功！");
            }else{
                $apply_status=$data['apply_status'];

                if($apply_status=='0'){
                    
                }else if($apply_status == '1'){
                    $data['pass_time']=time();
                }else if($apply_status == '2'){
                    if(empty($data['unpass_reason'])){
                        // $this->error("请填写未通过原因");
                    }
                }
                
                $rs = DB::name('user_expert')->update($data);
                if($rs===false){
                    $this->error("审核失败！");
                }else{
                    if($apply_status=='0'){
                        $this->success("修改成功！");
                    }else if($apply_status == '1'){
                        // 更新绑定分类状态
                        DB::name('user_expert_class')->where("uid = ".$uid)->update(array('status'=>'1'));
                    }
                }
                
                $action="";
                if($apply_status=='2'){
                    $action="审核鉴宝专家申请：{$uid} - 拒绝";
                }else if($apply_status=='1'){
                    $action="审核鉴宝专家申请：{$uid} - 同意";
                }
                
                setAdminLog($action);
                

                if($apply_status=='0'){
                    $this->success("修改成功！");
                }else{
                    $this->success("审核成功！");   
                }   
            }
        }
    }


    /**
     * 专家申请删除
     */
    public function delete()
    {
        $uid = $this->request->param('uid', 0, 'intval');
        
        $rs = DB::name('user_expert')->where("uid={$uid}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $this->success("删除成功！");
    }
}