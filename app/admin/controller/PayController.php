<?php

namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;
/**
* 支付相关（微信付款至零钱）
*/
class PayController extends AdminbaseController
{
	private function writeLogData($data,$unEncode=false) {
		$writeData = '';
		$writeData .= date('Y-m-d H:i:s').":";
        if($unEncode){
            $writeData .= $data;
        }else{ 
            $writeData .= json_encode($data);   
        }
		$writeData .= "\n\r";
		file_put_contents(CMF_ROOT.'/data/runtime/log/'.date('Ym').'/pay_'.date('Ymd').'.txt', $writeData, FILE_APPEND);
	}

	public function payToWexin($id,$orderno,$money,$uid,$action) {
	
        $this->writeLogData("Start payToWexin");
        $configpri=getConfigPri();

        $this->writeLogData($orderno);
        $this->writeLogData($money);
        $this->writeLogData($action);

        $order_sn = str_replace("_", 'X', $orderno);
        $amount = $money;

        if(!$configpri['wx_switch']){
            // throw new Exception("当前支付方式未开启"); 
            $this->error("当前支付方式未开启");
        }
        if($configpri['wx_appid']== "" || $configpri['wx_mchid']== "" || $configpri['wx_key']== ""){
            // throw new Exception("未配置支付相关参数");     
            $this->error("未配置支付相关参数");
        }

        $noceStr = md5(rand(100,1000).time());//获取随机字符串
        $time = time();

        if($action == 'withdraw'){
            // 店铺提现 shop_id 转 uid
            $shop_info = Db::name('shop')->field("uid")->where('id',$uid)->find();
            if(!empty($shop_info)){
                $uid = $shop_info['uid'];
            }else{
                // throw new Exception("未找到店铺信息");
                $this->error("未找到店铺信息");
            }
        }
        // 用户信息
        $now_user_info = Db::name('user')->where(array('id'=>$uid))->field('user_nicename,openid')->find();   
        if(empty($now_user_info)){
            // throw new Exception("未找到用户信息");
            $this->error("未找到用户信息");
        }else if(!$now_user_info['openid']){
            // throw new Exception("用户未绑定微信账号");
            $this->error("用户未绑定微信账号");
        }

        $paramarr = array(
            "mch_appid"         =>  $configpri['wx_appid'],
            "mchid"             =>  $configpri['wx_mchid'],
            "nonce_str"         =>  $noceStr,
            "partner_trade_no"  =>  $order_sn,
            "openid"            => $now_user_info['openid'],//'o_Vu96hHjDgLr-LWMdEQD49BSuiY',
            "check_name"        =>  'NO_CHECK',
            "amount"            =>  $amount*100,
            "desc"              =>  '提现'
        );

        ksort($paramarr);
        reset($paramarr);

        $sign = sign($paramarr,$configpri['wx_key']);//生成签名
        $paramarr['sign'] = $sign;
        $this->writeLogData($paramarr);
        $paramXml = "<xml>";
        foreach($paramarr as $k => $v){
            $paramXml .= "<" . $k . ">" . $v . "</" . $k . ">";
        }
        $paramXml .= "</xml>";

        $this->writeLogData($configpri['wx_key'], true);

        $this->writeLogData($paramXml, true);

        $ch = curl_init ();
        @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
        @curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
        @curl_setopt($ch, CURLOPT_URL, "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers");
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_POST, 1);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $paramXml);
        @curl_setopt($ch, CURLOPT_SSLCERTTYPE, 'PEM');
        @curl_setopt($ch, CURLOPT_SSLCERT, CMF_ROOT.'/sdk/wxpay/cert/apiclient_cert.pem');
        @curl_setopt($ch, CURLOPT_SSLKEYTYPE, 'PEM');
        @curl_setopt($ch, CURLOPT_SSLKEY, CMF_ROOT.'/sdk/wxpay/cert/apiclient_key.pem');

        @$resultXmlStr = curl_exec($ch);
        if(curl_errno($ch)){
            $this->writeLogData("Post-Params-Data");
            $this->writeLogData(curl_error($ch));
        }
        curl_close($ch);

        $result2 = xmlToArray($resultXmlStr);
        $this->writeLogData("Return-Data");
        $this->writeLogData($result2);

        if($result2['return_code']=='SUCCESS' && $result2['result_code']=='SUCCESS'){

            // 付款成功 更新提现记录 
            $extend_data = array(
                'id'=>$id,
                'uid' => $uid,
                'user_nicename' => $now_user_info['user_nicename'],
                'openid' => $now_user_info['openid'],
                'orderno' => $orderno,
                'money' => $money,
            );
            if(isset($result2['payment_no']) && $result2['payment_no']){
                $this->cashProcessing($action,$result2['payment_no'],$extend_data);   
            }else{
                $this->error("未找到第三方交易单号");
            }

        }else{
            // throw new Exception();
            $this->error($result2['return_msg']);
        }

	}

    /**
     * 提现至零钱 业务处理程序
     *
     * @param string $action            操作标识代码 用于判断处理分支
     * @param string $payment_no        payment_no
     * @param array $extend_data        预留参数
     *
     *
     */
    public function cashProcessing($action,$payment_no,$extend_data=array()) {
        switch ($action) {
            case 'record':
                // 收益提现
                return $this->recordProcessing($payment_no,$extend_data);
                break;
            case 'd_record':
                // 佣金提现
                return $this->drecordProcessing($payment_no,$extend_data);
                break;
            case 'withdraw':
                // 店铺收益提现
                return $this->withdrawProcessing($payment_no,$extend_data);
                break;
            case 'pdcash':
                // 余额提现
                return $this->pdcashProcessing($payment_no,$extend_data);
                break;
        }
    }

    // 收益提现
    public function recordProcessing($payment_no,$extend_data) {
        $id = $extend_data['id'];
        $where = array(
            'id' => $id,
            'status' => 0
        );
        $data = array(
            'trade_no' => $payment_no,
            'status' => 1,
            'uptime' => time()
        );

        DB::name('cash_record')->where($where)->update($data);
    }
    // 佣金提现
    public function drecordProcessing($payment_no,$extend_data) {
        $id = $extend_data['id'];
        $where = array(
            'id' => $id,
            'status' => 0
        );
        $data = array(
            'trade_no' => $payment_no,
            'status' => 1,
            'uptime' => time()
        );

        DB::name('cash_distributerecord')->where($where)->update($data);
    }
    // 店铺收益提现
    public function withdrawProcessing($payment_no,$extend_data) {
        $id = $extend_data['id'];
        $info = Db::name('shop_withdraw')->where('state = 0 and id='.$id)->find();
        $shop_id = $info['shop_id'];
        $amount = $info['amount'];

        $where = array(
            'id' => $id,
            'state' => 0
        );
        $data = array(
            'trade_no' => $payment_no,
            'state' => 1,
            'checktime' => time()
        );
        Db::name('shop_withdraw')->where($where)->update($data);

        Db::name('shop')->where('id',$shop_id)->update([
            'balance_freeze'=>Db::raw('balance_freeze-'.$amount),
        ]);

        $log_data = array(
            'shop_id' => $shop_id,
            'order_id' => 0,
            'type' => 0,
            'amount' => $amount,
            'type2' => 0,
            'addtime' => time(),
            'text' => '平台确认商家提现'
        );
        Db::name('shop_amount_log')->insert($log_data);
    }
    // 余额提现
    public function pdcashProcessing($payment_no,$extend_data) {
        $id = $extend_data['id'];
        $where = array(
            'id' => $id,
            'payment_state' => '0'
        );
        $data = array(
            'trade_no' => $payment_no,
            'payment_state' => '1',
            'payment_time' => time()
        );

        $rs = DB::name('pd_cash')->where($where)->update($data);
        if($rs===false){
            throw new Exception("修改失败！");
        }

        // 更新余额
        $amount = $extend_data['money'];
        $data_log = array();
        $data_log['uid'] = $extend_data['uid'];
        $data_log['nickname'] = $extend_data['user_nicename'];
        $data_log['amount'] = $amount;
        $data_log['order_sn'] = $extend_data['orderno'];
        $data_log['admin_name'] = session('name');
        $pd = controller('pd');
        $pd->changePd('cash_pay',$data_log);

    }

}