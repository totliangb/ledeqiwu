<?php

namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;
/**
* 商家管理
*/
class MerchantController extends AdminbaseController
{

	protected $table = 'shop';
	//订单状态
	protected function state($num)
	{
		$arr = ['申请中', '正常', '取消'];
		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}

    public function check_shop()
    {
        if(!request()->isPost())
        {
            $id = request()->param('id', 'intval');
            $info = Db::name($this->table)->where('id',$id)->find();

            if(!$info) $this->error('数据不存在！');
            $info['shop_img'] = get_upload_path($info['shop_img'], 0);

            $assort_info = Db::name('shop_assort')->where(1)->select();
            $this->assign('assort_info', $assort_info);
            
            $this->assign('type', 1);
            $this->assign('id', $id);
            $this->assign('info', $info);
            return $this->fetch();
        }
    }

	//添加、修改
	public function checked(&$post)
	{
		if($post['id'] && (!isset($post['name']) || mb_strlen($post['name']) > 20 || mb_strlen($post['name']) <= 0))
        {
            $this->error('店铺名称字数应在0~20位之间');
        }

        if(!isset($post['description']) || mb_strlen($post['description']) > 100 || mb_strlen($post['description']) <= 0)
        {
            $this->error('店铺描述字数应在0~100位之间');
        }

        if(isset($post['shop_img']) && mb_strlen($post['shop_img']) > 100)
        {
            $this->error('头像/封面字数不能大于100个字符');
        }

        if(isset($post['certificate_img']) && mb_strlen($post['certificate_img']) > 100)
        {
            $this->error('营业执照字数不能大于100个字符');
        }

        $post['uid'] = intval($post['uid']);

        if($post['id'] && (!$post['uid'] || !Db::name('user_auth')->where(['uid' => $post['uid'], 'status' => 1])->value('uid'))) $this->error('该主播不存在');

        if(!$post['id'] && $data = Db::name('shop')->where('uid = '.$post['uid'].' or name = "'.$post['name'].'"')->value('uid'))
        {
        	if($data == $post['uid'])
        	{
        		$this->error('该主播已有申请的店铺');
        	}
        	$this->error('店铺名称重复');
        }


        $post['sort'] = intval($post['sort']);
        $post['state'] = intval($post['state']);
        $post['update_time'] = time();
	}
}