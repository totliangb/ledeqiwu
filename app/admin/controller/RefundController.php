<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
/**
 * 退款理由设置
 */
class RefundController extends AdminbaseController 
{
	protected $table = 'shop_refund_info';

	protected function checked(&$post)
	{
		if(mb_strlen($post['title']) > 100 || mb_strlen($post['title']) <= 0)
        {
            $this->error('说明字数应在1~100位之间');
        }
	}

	//订单状态
	protected function state($num)
	{
		$arr = ['取消', '正常'];
		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}
}