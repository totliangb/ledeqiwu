<?php

namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;
/**
* 商城分类管理
*/
class LogisticsController extends AdminBaseController
{
	protected $table = 'shop_logistics';

	//订单状态
	protected function state($num)
	{
		$arr = ['取消', '正常'];
		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}

	//添加、修改
	public function checked(&$post)
	{
		if(mb_strlen($post['name']) > 20 || mb_strlen($post['name']) <= 0)
        {
            $this->error('分类名称字数应在0~20位之间');
        }

        if(mb_strlen($post['nickname']) > 50 || mb_strlen($post['nickname']) <= 0)
        {
            $this->error('简称字数应在0~50位之间');
        }

        if(mb_strlen($post['url']) > 100)
        {
            $this->error('链接字数不能大于100位');
        }

        $post['sort'] = intval($post['sort']);
        $post['state'] = intval($post['state']);
	}


}