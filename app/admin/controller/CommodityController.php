<?php

namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;
/**
* 商品管理
*/
class CommodityController extends AdminbaseController
{
	protected $table = 'shop_commodity';

	public function lists()
    {
        
        if(request()->param('desc'))
        {
            $order = request()->param('desc').' desc';
        }
        else if(request()->param('desc'))
        {
            $order = request()->param('desc').' asc';
        }
        else
        {
            $order = 'a.id desc';
        }

        $list = Db::name($this->table)->alias('a')
                ->order($order)
                ->leftJoin('__SHOP__ b', 'a.shop_id = b.id')
                ->field('a.*,b.name')
                ->paginate(20);
        $lists = $list->items();

        foreach ($lists as $key => &$value)
        {
            if(isset($value['state']) ) $value['statename'] = $this->state($value['state']);
            if(isset($value['addtime'])) $value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
        }

        $this->assign("page", $list->render());
        $this->assign('list', $lists);
        return $this->fetch();
    }

    public function add_or_edit()
    {
        if(!request()->isPost())
        {
            $id = 0;
            if(request()->param('id'))
            {
                $id = request()->param('id', 'intval');
                $info = Db::name($this->table)->alias('a')
                        ->leftJoin('__SHOP__ b', 'a.shop_id = b.id')
                        ->leftJoin('__SHOP_COMMODITY_INFO__ c', 'a.id = c.commodity_id')
                        ->where('a.id', $id)
                        ->field('a.*,b.name,c.carousel,c.text')
                        ->find();
                $info['carousel'] = json_decode($info['carousel'], true);
                if($info['carousel']) {
                    foreach ($info['carousel'] as $key => &$value) {
                        $value = get_upload_path($value, 0);
                    }
                }
                if($info['text']) {
                    $info['text'] = json_decode($info['text'], true);
                    if (is_array($info['text'])){
                        foreach ($info['text'] as $key => &$va) {
                        $va = get_upload_path($va, 0);
                    }
                    }
                    
                }

                $this->assign('info', $info);
            }

            $this->assign('id', $id);
            return $this->fetch('add');
        }
    }

	protected function state($num)
	{
		$arr = ['申请中', '正常', '取消'];
        return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}
}