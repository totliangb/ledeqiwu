<?php

/**
 * 余额提现
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class PdController extends AdminbaseController {

    /**
     * 余额
     */
    public function changePd($change_type,$data = array()) {
        $data_log = array();
        $data_pd = array();
        $where = array();
        $data_log['uid'] = $data['uid'];
        $data_log['nickname'] = $data['nickname'];
        $data_log['add_time'] = time();
        $data_log['type'] = $change_type;

        switch ($change_type){
            case 'appraise_pay':
                // 鉴定支付完成 鉴定专家增加冻结金额
                $data_log['freeze_amount'] = $data['amount'];
                $data_log['desc'] = '鉴定订单支付成功: '.$data['order_sn'];
                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('freeze_predeposit')->find();
                $where['freeze_predeposit'] = $now_data['freeze_predeposit'];
                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] + $data['amount'];
                break;
            case 'appraise':
                // 鉴定专家完成鉴定 鉴定专家冻结金额转入余额
                $data_log['av_amount'] = $data['amount'];
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '鉴定订单完成，冻结转可用: '.$data['order_sn'];
                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('freeze_predeposit')->find();
                $where['available_predeposit'] = $now_data['available_predeposit'];
                $where['freeze_predeposit'] = $now_data['freeze_predeposit'];
                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'cash_pay':
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '提现成功，提现单号: '.$data['order_sn'];
                $data_log['admin_name'] = $data['admin_name'];
                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('freeze_predeposit')->find();
                $where['freeze_predeposit'] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'cash_del':
                $data_log['av_amount'] = $data['amount'];
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '取消提现申请，解冻余额，提现单号: '.$data['order_sn'];
                $data_log['admin_name'] = $data['admin_name'];

                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('available_predeposit,freeze_predeposit')->find();
                $where['available_predeposit'] = $now_data['available_predeposit'];
                $where['freeze_predeposit'] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'recharge':
                $data_log['av_amount'] = $data['amount'];
                $data_log['desc'] = '充值，充值单号: '.$data['order_sn'];

                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('available_predeposit,freeze_predeposit')->find();
                $where['available_predeposit'] = $now_data['available_predeposit'];

                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                break;
            default:
                throw new Exception('参数错误');
                break;
        }

        $where['id'] = $data['uid'];
        $update = Db::name('user')->where($where)->update($data_pd);
        if (!$update) {
            throw new Exception('操作失败');
        }
        $insert = Db::name('pd_log')->insert($data_log);
        if (!$insert) {
            throw new Exception('操作失败');
        }

        return $insert;
    }

    protected function getStatus($k=''){
        $status=array(
            '0'=>'未处理',
            '1'=>'提现成功',
            '2'=>'拒绝提现',
        );
        if($k===''){
            return $status;
        }
        
        return isset($status[$k]) ? $status[$k]: '';
    }
    
    protected function getTypes($k=''){
        $type=array(
            '1'=>'支付宝',
            '2'=>'微信',
            '3'=>'银行卡',
        );
        if($k===''){
            return $type;
        }
        
        return isset($type[$k]) ? $type[$k]: '';
    }
    
    /**
     * 列表
     */
    public function index(){
        $data = $this->request->param();
        $map=[];
		
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $map[]=['payment_state','=',$status];
            $cash['type']=1;
        }
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['cash_sn','like',"%".$keyword."%"];
        }
        
    	$lists = DB::name("pd_cash")
            ->where($map)
            ->order('id desc')
            ->paginate(20);
        
        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
            return $v;
        });
        
        $lists->appends($data);
        $page = $lists->render();

        $cashrecord_total = DB::name("pd_cash")->where($map)->sum("amount");
        if($status=='')
        {
            $success=$map;
            $success[]=['payment_state','=',1];
            $fail=$map;
            $fail[]=['payment_state','=',2];
            $cashrecord_success = DB::name("pd_cash")->where($success)->sum("amount");
            $cashrecord_fail = DB::name("pd_cash")->where($fail)->sum("amount");
            $cash['success']=$cashrecord_success;
            $cash['fail']=$cashrecord_fail;
            $cash['type']=0;
        }
        $cash['total']=$cashrecord_total;
            
    	$this->assign('cash', $cash);
        
    	$this->assign('lists', $lists);

    	$this->assign('type', $this->getTypes());
    	$this->assign('status', $this->getStatus());
    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }

    /**
     * 详情
     */
	public function info(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('pd_cash')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['userinfo']=getUserInfo($data['uid']);

        $payToThirdFlag = false;
        if($data['name'] == '提现至零钱' && $data['type'] == 2){
            $payToThirdFlag = true;
        }
        
        $this->assign('type', $this->getTypes());
        $this->assign('status', $this->getStatus());
        
        $this->assign('payToThirdFlag', $payToThirdFlag);
            
        $this->assign('data', $data);
        return $this->fetch();
	}
    
    /**
     * 修改
     */
    public function editPost(){
		if ($this->request->isPost()) {
            $updateWhere = array();
            $updateData = array();
            $data      = $this->request->param();
            
			$updateData['payment_state'] = $status=$data['payment_state'];
			$updateWhere['uid'] = $uid=$data['uid'];
			$updateWhere['id'] = $id=$data['id'];
            if (isset($data['trade_no']) && $data['trade_no']) {
                $updateData['trade_no']=$data['trade_no'];
            }

            $payToThirdFlag = $data['payToThirdFlag'];

			if($status=='0'){
				$this->success("修改成功！");
			}else{
                try {
                    DB::name('pd_cash')->startTrans();
                    $order=Db::name('pd_cash')->where("id={$id}")->find();

                    if(!$order){
                        $this->error("信息错误");
                    }

                    if($payToThirdFlag && $status=='1'){
                        // 提现至零钱
                        $payController = Controller('Pay');
                        $payController->payToWexin($id,$order['cash_sn'],$order['amount'],$order['uid'],'pdcash');

                        $action="修改提现记录：{$id} - 同意";
                    }else{

                        $updateWhere['payment_state'] = $order['payment_state'];
                        $updateData['payment_time']=time();

                        if($status=='2'){
                            $updateData['refuse_desc'] = $data['refuse_desc'];
                        }

                        $rs = DB::name('pd_cash')->where($updateWhere)->update($updateData);
                        if($rs===false){
                            throw new Exception("修改失败！");
                        }

                        // 更新余额
                        $amount = $order['amount'];
                        $now_user_info = Db::name('user')->where(array('id'=>$uid))->field('user_nicename')->find();
                        $data_log = array();
                        $data_log['uid'] = $uid;
                        $data_log['nickname'] = $now_user_info['user_nicename'];
                        $data_log['amount'] = $amount;
                        $data_log['order_sn'] = $order['cash_sn'];
                        $data_log['admin_name'] = session('name');

                        if($status=='2'){
                            $this->changePd('cash_del',$data_log);
                            $action="修改提现记录：{$id} - 拒绝";
                        }else if($status=='1'){
                            $this->changePd('cash_pay',$data_log);
                            $action="修改提现记录：{$id} - 同意";
                        }
                    }
                    
                    setAdminLog($action);

                    DB::name('pd_cash')->commit();
                    $this->success("修改成功！");
                } catch (Exception $e) {
                    DB::name('pd_cash')->rollback();
                    $this->error($e->getMessage());
                }
            }
		}
	}
    
    /**
     * 导出
     */
    public function export()
    {
        $data = $this->request->param();
        $map=[];
		
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $map[]=['payment_state','=',$status];
        }
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['cash_sn','like',"%".$keyword."%"];
        }
        
        $xlsName  = "提现";
        
        $xlsData=DB::name("pd_cash")
            ->where($map)
            ->order('id desc')
            ->select()
            ->toArray();

        foreach ($xlsData as $k => $v)
        {
            $userinfo=getUserInfo($v['uid']);
            $xlsData[$k]['user_nicename']= $userinfo['user_nicename']."(".$v['uid'].")";
            $xlsData[$k]['addtime']=date("Y-m-d H:i:s",$v['addtime']); 
            $xlsData[$k]['payment_time']=date("Y-m-d H:i:s",$v['payment_time']); 
            $xlsData[$k]['status']=$this->getStatus($v['payment_state']);
        }


        $action="导出余额提现记录：".DB::name("pd_cash")->getLastSql();
        setAdminLog($action);
        $cellName = array('A','B','C','D','E','F','G','H');
        $xlsCell  = array(
            array('id','序号'),
            array('user_nicename','会员'),
            array('amount','提现金额'),
            array('cash_sn','单号'),
            array('trade_no','第三方支付订单号'),
            array('status','状态'),
            array('addtime','提交时间'),
            array('payment_time','处理时间'),
        );
        exportExcel($xlsName,$xlsCell,$xlsData,$cellName);
    }
    
}
