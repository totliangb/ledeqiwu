<?php
/**
 * 鉴宝申请订单管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
    
class AppraiseController extends AdminbaseController {

    protected $statusData = array(
            '0'=>array(
                'name'=>'待鉴定',
                'where'=>array(
                    'pay_status'=>'1',
                    'status' => '0'
                )
            ),
            '1'=>array(
                'name'=>'鉴定完成',
                'where'=>array(
                    'pay_status'=>'1',
                    'status' => '1'
                )
            ),
            '2'=>array(
                'name'=>'鉴定超时',
                'where'=>array(
                    'pay_status'=>'1',
                    'status' => '2'
                )
            ),
            '3'=>array(
                'name'=>'待付款',
                'where'=>array(
                    'pay_status'=>'0',
                    'status' => '0'
                )
            ),
        );
    protected $payTypeData = array(
            'wx'=>'微信支付',
            'ali'=>'支付宝支付',
        );

    protected $answerRealData = array(
            '0'=>'未答复',
            '1'=>'已答复',//'真品',
            '2'=>'已答复',//'赝品',
        );

    /**
     * 申请状态 
     */
    protected function getStatus($k=''){
        $status=$this->statusData;
        if($k===''){
            return $status;
        }
        
        return isset($status[$k]) ? $status[$k]: '';
    }

    /**
     * 获取申请状态名称 
     */
    protected function getStatusName($status,$pay_status){
        $returnName = '-';
        foreach ($this->statusData as $key => $value) {
            $itemWhere = $value['where'];
            if($status == $itemWhere['status'] && $pay_status == $itemWhere['pay_status']){
                $returnName = $value['name'];
            }
        }
        return $returnName;
    }

    /**
     * 获取支付方式名称及支付方式集合 
     */
    protected function getPayType($k=''){
        $pay_type=$this->payTypeData;
        if($k===''){
            return $pay_type;
        }
        
        return isset($pay_type[$k]) ? $pay_type[$k]: '';
    }

    /**
     * 获取答复结果 
     */
    protected function getAnswerReal($k=''){
        $answer_real=$this->answerRealData;
        
        return isset($answer_real[$k]) ? $answer_real[$k]: '';
    }

    /**
     * 鉴定订单列表 
     */
    public function index()
    {
        /**搜索条件**/
        $data = $this->request->param();
        $map=[];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['add_time','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['add_time','<=',strtotime($end_time) + 60*60*24];
        }
        
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $statusItem = $this->getStatus($status);
            if(!empty($statusItem) && isset($statusItem['where']) && !empty($statusItem['where'])){
                $map = array_merge($map,$statusItem['where']);
            }
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['uid','=',$uid];
            }
        }

        $order_sn=isset($data['order_sn']) ? $data['order_sn']: '';
        if($order_sn!=''){
            $map[]=['apply_order_sn','like','%'.$order_sn.'%'];
        }

        $lists = Db::name("appraise_apply")
                ->where($map)
                ->order("add_time DESC")
                ->paginate(20);
        $lists->each(function($v,$k){
            $v['status_name'] = $this->getStatusName($v['status'],$v['pay_status']);
            $v['pay_type_name'] = $v['pay_type'] ? $this->getPayType($v['pay_type']) : '';

            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

        $this->assign('lists', $lists);

        $this->assign("page", $page);

        $this->assign('status', $this->getStatus());
        
        return $this->fetch();
    }

    /**
     * 鉴定订单详情
     */
    public function info()
    {
        $id = request()->param('id', 'intval');
        //获取订单信息
        $order = Db::name('appraise_apply')->where('id', $id)->find();
        
        if(!$order) exit('信息不存在');
        $order['status_name'] = $this->getStatusName($order['status'],$order['pay_status']);
        $order['pay_type_name'] = $order['pay_type'] ? $this->getPayType($order['pay_type']) : '';
        $order['answer_real_name'] = $this->getAnswerReal($order['answer_real']);
        if($order['answer_real']!='0' && !empty($order['answer'])){
            $order['answer'] = get_upload_path($order['answer'], 0);
        }
        $order['add_time'] = $order['add_time'] ? date("Y-m-d H:i:s",$order['add_time']) : '';
        $order['pay_time'] = $order['pay_time'] ? date("Y-m-d H:i:s",$order['pay_time']) : '';
        $order['over_time'] = $order['over_time'] ? date("Y-m-d H:i:s",$order['over_time']) : '';

        // 获取申请鉴定的用户信息
        $user = Db::name("user")->field("user_nicename")->where("id",$order['uid'])->find();
        if (!empty($user)) {
            $order['user_nickname'] = $user['user_nicename'];
        }

        // 获取指定的鉴定专家用户信息
        $touser = Db::name("user")
                    ->alias("a")
                    ->join('__USER_EXPERT__ b', 'a.id =b.uid')
                    ->field("a.user_nicename,b.realname")
                    ->where(array("a.id"=>$order['touid']))
                    ->find();
        if (!empty($touser)) {
            $order['touser_nickname'] = $touser['user_nicename'];
            $order['touser_realname'] = $touser['realname'];
        }

        // 获取鉴定结果查看订单记录
        $answerViewList = Db::name("appraise_answer_view")->where("apply_id",$order['id'])->select();
        foreach ($answerViewList as $key => $value) {
            // 获取结果查看订单申请用户信息
            $item_user = Db::name("user")->field("user_nicename")->where("id",$value['uid'])->find();
            $value['user_nickname'] = !empty($item_user) ? $item_user['user_nicename'] : '';
            $value['pay_type_name'] = $value['pay_type'] ? $this->getPayType($value['pay_type']) : '';
            $value['add_time'] = $value['add_time'] ? date("Y-m-d H:i:s",$value['add_time']) : '';
            $value['pay_time'] = $value['pay_time'] ? date("Y-m-d H:i:s",$value['pay_time']) : '';

            $answerViewList[$key] = $value;
        }
        $this->assign('id', $id);
        $this->assign('order', $order);
        $this->assign("answerViewList",$answerViewList);
        return $this->fetch();
    }

}