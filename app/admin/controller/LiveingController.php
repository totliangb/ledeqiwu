<?php

/**
 * 直播列表
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class LiveingController extends AdminbaseController {
    protected function getLiveClass(){
        $liveclass=Db::name("live_class")->order('list_order asc, id desc')->column('name','id');
        
        /*$list=[
            '0'=>'默认分类',
        ];*/
        $liveclass[0] = '默认分类';
        //$liveclass=array_merge($list,$liveclass);
        return $liveclass;
    }
    
    protected function getTypes($k=''){
        $type=[
            '0'=>'普通房间',
            '1'=>'密码房间',
            '2'=>'门票房间',
            '3'=>'计时房间',
        ];
        
        if($k==''){
            return $type;
        }
        return $type[$k];
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        $map[]=['islive','=',1];
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        $is_home=isset($data['is_home']) ? $data['is_home']: '';
        
        if($start_time!=""){
           $map[]=['starttime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['starttime','<=',strtotime($end_time) + 60*60*24];
        }

        if($is_home!=""){
           $map[]=['is_home','=',$is_home];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $this->configpri=getConfigPri();
			

    	$lists = Db::name("live")
                ->where($map)
                ->order("starttime DESC")
                ->paginate(20);
        
        $lists->each(function($v,$k){

             $v['userinfo']=getUserInfo($v['uid']);
             $where=[];
             $where['action']=1;
             $where['touid']=$v['uid'];
             $where['showid']=$v['showid'];
             /* 本场总收益 */
             $totalcoin=Db::name("user_coinrecord")->where($where)->sum('totalcoin');
             if(!$totalcoin){
                $totalcoin=0;
             }
             /* 送礼物总人数 */
             $total_nums=Db::name("user_coinrecord")->where($where)->group("uid")->count();
             if(!$total_nums){
                $total_nums=0;
             }
             /* 人均 */
             $total_average=0;
             if($totalcoin && $total_nums){
                $total_average=round($totalcoin/$total_nums,2);
             }
             
             /* 人数 */
            $nums=zSize('user_'.$v['stream']);
            
            $v['totalcoin']=$totalcoin;
            $v['total_nums']=$total_nums;
            $v['total_average']=$total_average;
            $v['nums']=$nums;
            
            if($v['isvideo']==0 && $this->configpri['cdn_switch']!=5){
                $v['pull']=PrivateKeyA('rtmp',$v['stream'],0);
            }
                
            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        
    	$this->assign("liveclass", $this->getLiveClass());
        
    	$this->assign("type", $this->getTypes());
    	
    	return $this->fetch();

    }

    public function recommend()
    {
        $id = $this->request->param('uid', 0, 'intval');
        $type = $this->request->param('type', 0, 'intval');

        if($id && in_array($type, [0,1]))
        {
            // 校验当前是否为设置推荐首页操作
            // 首页推荐操作只能有且只有一个
            if($type == 1){
                $hasHome = DB::name('live')->where("is_home", 1)->count();
                if($hasHome){
                    $this->error('已经存在首页推荐直播间了，请先取消后再次尝试。');
                }
            }
            if(DB::name('live')->where("uid", $id)->update(['is_home'=>$type])) $this->success('操作成功');
            $this->error('操作失败');
        }

        $this->error('数据传入失败！');
    }

	function del(){
        
        $uid = $this->request->param('uid', 0, 'intval');
        
        $rs = DB::name('live')->where("uid={$uid}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $this->success("删除成功！",url("liveing/index"));
            
	}
    
	function add(){
        
        $this->assign("liveclass", $this->getLiveClass());
        
        $this->assign("type", $this->getTypes());
        
        return $this->fetch();
	}
    
	function addPost(){
		if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
            $nowtime=time();
			$uid=$data['uid'];
            
            $userinfo=DB::name('user')->field("ishot,isrecommend")->where(["id"=>$uid])->find();
			if(!$userinfo){
				$this->error('用户不存在');
			}
            
            $liveinfo=DB::name('live')->field('uid,islive')->where(["uid"=>$uid])->find();
			if($liveinfo['islive']==1){
				$this->error('该用户正在直播');
			}
            
            $pull=urldecode($data['pull']);
            $type=$data['type'];
            $type_val=$data['type_val'];
            $anyway=$data['anyway'];
            $liveclassid=$data['liveclassid'];
            $isshop=$data['isshop'];
            if ($isshop == 1){
                $shop = Db::name("shop")->where("uid = ".$uid)->find();
                if (!$shop){
                    $isshop = 0;
                }
            }
            $stream=$uid.'_'.$nowtime;
            $title='';
            
            $data2=array(
				"uid"=>$uid,
				"ishot"=>$userinfo['ishot'],
				"isrecommend"=>$userinfo['isrecommend'],
                
				"showid"=>$nowtime,
				"starttime"=>$nowtime,
				"title"=>$title,
				"province"=>'',
				"city"=>'好像在火星',
				"stream"=>$stream,
				"thumb"=>'',
				"pull"=>$pull,
				"lng"=>'',
				"lat"=>'',
				"type"=>$type,
				"type_val"=>$type_val,
				"isvideo"=>1,
				"islive"=>1,
				"isshop"=>$isshop,
				"anyway"=>$anyway,
				"liveclassid"=>$liveclassid,
			);
            
            if($liveinfo){
                $rs = DB::name('live')->update($data2);
            }else{
                $rs = DB::name('live')->insert($data2);
            }

            if(!$rs){
                $this->error("添加失败！");
            }
            
            $this->success("添加成功！");
            
		}			
	}
	
	function edit(){
        $uid   = $this->request->param('uid', 0, 'intval');
        
        $data=Db::name('live')
            ->where("uid={$uid}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('data', $data);
        
        $this->assign("liveclass", $this->getLiveClass());
        
        $this->assign("type", $this->getTypes());
        
        return $this->fetch();


	}
	
	function editPost(){
		if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
			$data['pull']=urldecode($data['pull']);
            
			$rs = DB::name('live')->update($data);
            if($rs===false){
                $this->error("修改失败！");
            }
            
            $this->success("修改成功！");
		}
	}
		
}
