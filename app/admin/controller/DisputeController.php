<?php
namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;

//订单纠纷
class DisputeController extends AdminbaseController{
	protected $table = 'shop_dispute';

	protected function state($num)
	{
		$arr = ['申请中', '退款、退货', '取消退款退货'];

		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}

	public function views()
	{
		
		$id = request()->param('id', 'intval');
		if(empty($id)) exit('参数错误');

		$info = Db::name($this->table)->where('id',$id)->find();

		if(!$info) exit('参数错误');
		$info['img'] = json_decode($info['img'], true);
		foreach ($info['img'] as &$v){
		    $v = get_upload_path($v,0);
		}
		$info['addtime'] = date('Y-m-d H:i:s', $info['addtime']);
		$info['statename'] = $this->state($info['state']);
		$userinfo = getUserInfo($info['uid']);
		$shop = Db::name("shop")->where("id = ".$info['shop_id'])->find();
		if ($shop['uid'] == $info['uid']){
		    $info['from'] = $userinfo['user_nicename']."({$info['uid']})-店铺";
		}else{
		    $info['from'] = $userinfo['user_nicename']."({$info['uid']})-买家";
		}
        
		$order = Db::name('shop_order')->where('id',$info['order_id'])->find();

		$this->assign('info', $info);
		$this->assign('order', $order);
		return $this->fetch();
	}

	public function reply()
	{
		$post['id'] = request()->param('id', 'intval');
		$post['reply'] = request()->param('reply', 'htmlspecialchars');
		$post['state'] = request()->param('state', 'intval');
        

		if(!in_array($post['state'], [1,2]) || !$post['id'] || mb_strlen($post['reply']) > 500 || mb_strlen($post['reply']) <= 0) $this->error('参数错误');
		$info = Db::name($this->table)->where('id='.$post['id'].' and state=0')->find();

        $order = Db::name('shop_order')->where('id='.$info['order_id'])->find();
        if (in_array($order['state'], [41,42,43,50,60])){
            $this->error('订单已处理');
        }
        
		if(!$info) $this->error('参数错误');
        
		if(1 == $post['state']) //退款
		{
			require_once CMF_ROOT.'sdk/Alipaysdk/Lite.php';
			$ali = new \Alipay_Lite();
			try {
				Db::startTrans();

				if(!Db::name($this->table)->where('order_id='.$info['order_id'].' and state=0')->update(['state' => 1,'reply' => $post['reply'], 'checktime' => time()])) throw new \Exception("修改失败");
				

				if(!$order) throw new \Exception("修改失败");
				

				if(!Db::name('shop')->where('id='.$order['shop_id'])->update(['balance_freeze' => Db::raw('balance_freeze-'.$order['payment_amount'])])) throw new \Exception("修改失败");

				if(!Db::name('shop_amount_log')->insert([
					'shop_id' => $order['shop_id'],
					'order_id' => $order['id'],
					'type' => 0,
					'amount' => $order['payment_amount'],
					'type2' => 0,
					'addtime' => time(),
					'text' => '申请平台介入，平台判退款退货'
				])) throw new \Exception("修改失败");
                jpushNotice($order['uid'], '订单号:'.$order['numbering'].'订单平台介入，平台判定退款退货');

				if(!Db::name('shop_order')->where('id',$info['order_id'])->update(['state' => 41])) throw new \Exception("修改失败");
				
				if ($order['is_pay_type'] == 1) {
                    if(!wxTradeRefundRequest2($order))  throw new \Exception("修改失败");
                } else {
                    if(!$ali->setCofig()->TradeRefundRequest($order))  throw new \Exception("修改失败");
                }
	
				Db::commit(); 
			} catch (\Exception $e) {
				Db::rollback();
				$this->error('回复失败'.$e->getMessage());
			}
			$this->success('处理成功');
		}
		else if(2 == $post['state']) //取消退款
		{
			try {
				Db::startTrans();

				if(!Db::name($this->table)->where('id='.$post['id'].' and state=0')->update(['state' => 2,'reply' => $post['reply'], 'checktime' => time()])) throw new \Exception("修改失败");

				if(!Db::name('shop_order')->where('id',$info['order_id'])->update(['state' => 30])) throw new \Exception("修改失败");

                jpushNotice($order['uid'], '订单号:'.$order['numbering'].'订单平台介入，平台判定取消退款退货');
				Db::commit(); 
			} catch (\Exception $e) {
				Db::rollback();
				$this->error('回复失败');
			}
			$this->success('处理成功');
		}
	}
}