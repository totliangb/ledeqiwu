<?php

namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;
/**
* 圈子
*/
class CircleController extends AdminbaseController
{
	protected $table = 'circle';

	protected function state($num)
	{
		$arr = ['待审核', '正常', '不通过/取消'];

		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}

	public function views()
	{
		$id = request()->param('id', 'intval');
		if(!$id) $this->error('参数错误');

		$info = Db::name($this->table)->where('id',$id)->find();
		if(!$info) $this->error('参数错误');

		$info['flie'] = json_decode($info['flie']);
		foreach ($info['flie'] as $key => &$value) {
			$value = get_upload_path($value, 0);
		}


		$this->assign('info', $info);
		return $this->fetch();
	}
}