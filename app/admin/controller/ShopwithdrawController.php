<?php

namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;
/**
* 商家提现记录
*/
class ShopwithdrawController extends AdminbaseController
{
	protected $table = 'shop_withdraw';

	public function lists()
	{
        $list = Db::name($this->table)->alias('a')
        		->leftJoin('__CASH_ACCOUNT__ b ',' a.account_id = b.id')
        		->join('__SHOP__ c ',' a.shop_id = c.id')
        		->field('a.id,a.amount,a.addtime,a.state,b.type,b.account_bank,b.name,b.account,c.name shopname')
        		->order('a.state asc,a.addtime desc')
        		->paginate(20);

       	$lists = $list->items();
        foreach ($lists as $key => &$value) 
        {
        	$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
        	$value['statename'] = $this->state($value['state']);
        	$value['typename'] = $this->type($value['type']);

	        $payToThirdFlag = false;
	        if($value['name'] == '提现至零钱' && $value['type'] == 2){
	            $payToThirdFlag = true;
	        }
	        $value['payToThirdFlag'] = $payToThirdFlag;
        }

        $this->assign("page", $list->render());
        $this->assign('list', $lists);
        return $this->fetch();
	}

	//确认、取消打款
	public function make_money()
	{
		$id = request()->param('id', 'intval');
		$type = request()->param('type', 'intval');

		if(!$id || !$type || !in_array($type, [1,2,3])) $this->error('参数错误');

		$info = Db::name($this->table)->where('state = 0 and id='.$id)->find();

		if(!$info) $this->error('操作失败，改申请已取消或已打款');

		if($type == 1) //确认打款
		{
			try {
				Db::startTrans();

				if(!Db::name($this->table)->where('id',$info['id'])->update(['state' => 1, 'checktime' => time()])) throw new \Exception("确认打款失败");

				if(!Db::name('shop')->where('id',$info['shop_id'])->update([
					'balance_freeze'=>Db::raw('balance_freeze-'.$info['amount']),
				])) throw new \Exception("确认打款失败");

				if(!Db::name('shop_amount_log')->insert([
					'shop_id' => $info['shop_id'],
					'order_id' => 0,
					'type' => 0,
					'amount' => $info['amount'],
					'type2' => 0,
					'addtime' => time(),
					'text' => '平台确认商家提现'
				])) throw new \Exception("确认打款失败");

				Db::commit();
			} catch (\Exception $e) {
				Db::rollback();
				$this->error($e->getMessage());
				$this->error('确认打款失败');
			}
			$this->success('确认打款成功');
		}
		else if($type == 3) // 提现至零钱
		{
            $payController = Controller('Pay');
            $payController->payToWexin($id,$info['orderno'],$info['amount'],$info['shop_id'],'withdraw');

			$this->success('打款至零钱成功');
		}
		else//取消打款
		{
			try {
				Db::startTrans();

				if(!Db::name($this->table)->where('id',$info['id'])->update(['state' => 2, 'checktime' => time()])) throw new \Exception("取消打款失败");
				
				if(!Db::name('shop')->where('id',$info['shop_id'])->update([
					'balance_freeze'=>Db::raw('balance_freeze-'.$info['amount']),
					'balance'=>Db::raw('balance+'.$info['amount']),
				])) throw new Exception("取消打款失败");

				if(!Db::name('shop_amount_log')->insert([
					'shop_id' => $info['shop_id'],
					'order_id' => 0,
					'type' => 0,
					'amount' => $info['amount'],
					'type2' => 0,
					'addtime' => time(),
					'text' => '平台取消商家提现'
				])) throw new \Exception("取消打款失败");

				if(!Db::name('shop_amount_log')->insert([
					'shop_id' => $info['shop_id'],
					'order_id' => 0,
					'type' => 1,
					'amount' => $info['amount'],
					'type2' => 1,
					'addtime' => time(),
					'text' => '平台取消商家提现'
				])) throw new \Exception("取消打款失败");

				Db::commit();
			} catch (\Exception $e) {
				Db::rollback();
				$this->error('取消打款失败');
			}
			$this->success('取消打款成功');
		}
	}

	protected function type($num)
	{
		$arr = [1=>'支付宝', 2=>'微信', 3=>'银行卡'];

		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}

	protected function state($num)
	{
		$arr = ['申请中', '通过', '取消'];

		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}
}