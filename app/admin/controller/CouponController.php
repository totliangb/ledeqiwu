<?php
namespace app\admin\controller;
use think\Db;
use cmf\controller\AdminBaseController;
class CouponController extends AdminbaseController{
	protected $table = 'shop_coupon';
	public function lists()
	{
		if(request()->param('desc'))
        {
            $order = request()->param('desc').' desc';
        }
        else if(request()->param('desc'))
        {
            $order = request()->param('desc').' asc';
        }
        else
        {
            $order = 'a.id desc';
        }

        $list = Db::name($this->table)->alias('a')->leftjoin('__SHOP__ b ','a.shop_id = b.id')->field('a.*,b.name')->order($order)->paginate(20);
        $lists = $list->items();
        foreach ($list as $key => &$value) 
        {
            $value['start_time'] = date('Y-m-d H:i', $value['start_time']);
            $value['end_time'] = date('Y-m-d H:i', $value['end_time']);
            $value['receive_start_time'] = date('Y-m-d H:i', $value['receive_start_time']);
            $value['receive_end_time'] = date('Y-m-d H:i', $value['receive_end_time']);
        }

        $this->assign("page", $list->render());
        $this->assign('list', $lists);
        return $this->fetch();
	}
}