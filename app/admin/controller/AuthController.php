<?php

/**
 * 认证
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class AuthController extends AdminbaseController {
    
    protected function getStatus($k=''){
        $status=array(
            '0'=>'处理中',
            '1'=>'认证成功',
            '2'=>'认证失败',
        );
        if($k===''){
            return $status;
        }
        
        return isset($status[$k]) ? $status[$k]: '';
    }
    
    protected function getRealStatus($uid,$status){
        $time = time();
        $userinfo = getUserInfo($uid);
        switch($status){
            case 0:
                return "审核中";
                break;
            case 1:
                if ($userinfo['is_bond'] == 0){
                    return "未缴纳保证金";
                }
                if ($userinfo['cert_overdue'] <= $time){
                    return "认证已过期";
                }
                if ($userinfo['cert_overdue'] == 0){
                    return "未缴纳认证费";
                }
                return "认证成功";
                break;
            case 2:
                return "认证失败";
        }
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $map[]=['status','=',$status];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['uid','=',$uid];
            }
        }
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['real_name|mobile','like','%'.$keyword.'%'];
        }
			

    	$lists = Db::name("user_auth")
                ->where($map)
                ->order("addtime DESC")
                ->paginate(20);
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
			$v['mobile']=m_s($v['mobile']);
			$v['cer_no']=m_s($v['cer_no']);
			$v['realstatus']=$this->getRealStatus($v['uid'],$v['status']);
            return $v;           
        });
        
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        
        $this->assign('status', $this->getStatus());
    	
    	return $this->fetch();
    }
    
	function del(){
        
        $uid = $this->request->param('uid', 0, 'intval');
        
        $rs = DB::name('user_auth')->where("uid={$uid}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $action="删除会员认证信息：{$uid}";
        setAdminLog($action);
        
        $this->success("删除成功！");
            
	}
    
    function edit(){
        
        $uid   = $this->request->param('uid', 0, 'intval');
        
        $data=Db::name('user_auth')
            ->where("uid={$uid}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['userinfo']=getUserInfo($data['uid']);
        $data['mobile']=m_s($data['mobile']);
        $data['cer_no']=m_s($data['cer_no']);
        
        $this->assign('status', $this->getStatus());
        
        $payment = Db::name("user_auth_payment")->where("uid={$uid}")->order("addtime desc")->select()->toArray();
        if ($payment){
            $typename = [
                "bond"=>"保证金",
                "cert"=>"认证费"
            ];
            $statusname = [
                "SUCCESS" => "支付成功",
                "REFUND" => "转入退款",
                "NOTPAY" => "未支付",
                "CLOSED" => "已关闭",
                "PAYERROR" => "支付失败(其他原因)",
                "ACCEPT" => "已接收，等待扣款",
                "NULL"   => "未知"
            ];
            foreach ($payment as &$v){
                $v['typename'] = $typename[$v['type']];
                $v['amount'] = "￥".$v['num'];
                $v['statusname'] = $v['status']?$statusname[$v['status']]:"未支付";
            }
            
        }
        $this->assign("payment",$payment);
        
        $refund = Db::name("user_auth_refund")->where("uid={$uid}")->order("addtime desc")->select()->toArray();
        if ($refund){
            $typename = [
                "bond"=>"保证金",
                "cert"=>"认证费",
                "rebond"=>"保证金退款",
            ];
            $statusname = [
                "SUCCESS" => "退款成功",
                "CLOSED" => "退款关闭",
                "PROCESSING" => "退款处理中",
                "ABNORMAL" => "退款异常"
            ];
            foreach ($refund as &$v){
                $v['status'] = $v['status']=='1' ? 'SUCCESS' : '';
                $v['typename'] = $typename[$v['type']];
                $v['amount'] = "￥".$v['num'];
                $v['statusname'] = $v['status']?$statusname[$v['status']]:"未处理";
            }
            
        }
        $this->assign("refund",$refund);
        $this->assign('data', $data);
        return $this->fetch();
	}
	function editPost(){
		if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
			$status=$data['status'];
			$uid=$data['uid'];

			if($status=='0'){
				$this->success("修改成功！");
			}

            
            $data['uptime']=time();
            
			$rs = DB::name('user_auth')->update($data);
            if($rs===false){
                $this->error("修改失败！");
            }
            
            if($status=='2'){
                $action="修改会员认证信息：{$uid} - 拒绝";
            }else if($status=='1'){
                $action="修改会员认证信息：{$uid} - 同意";
            }
            
            setAdminLog($action);
            
            $this->success("修改成功！");
		}
	}
	
    function checkStatus($id,$type){
        $rs = ["code" => 0, "msg" => "无需更新", "info" => []];
        $configpri = getConfigPri();
        $mchid = $configpri['wx_mchid'];
        if ($type == 1){
            $statusname = [
                "SUCCESS" => "支付成功",
                "REFUND" => "转入退款",
                "NOTPAY" => "未支付",
                "CLOSED" => "已关闭",
                "PAYERROR" => "支付失败(其他原因)",
                "ACCEPT" => "已接收，等待扣款"
            ];
            $check = Db::name("user_auth_payment")->where("id = ".$id)->find();
            $url = "https://api.mch.weixin.qq.com/v3/pay/transactions/out_trade_no/".$check['payment']."?mchid=".$mchid;
            $result = curl_get($url);
            if ($result['trade_state'] == "SUCCESS" && $check['status'] == "SUCCESS"){
                return $rs;
            }else{
                $check = Db::name("user_auth_payment")->where("id = ".$id)->update(['status' => $result['trade_state'], 'paytime' => strtotime(str_replace("T"," ",substr($result['success_time'],0,-6)))]);
                $rs['info']['statusname'] = $statusname[$result['trade_state']];
                $rs['info']['paytime'] = str_replace("T"," ",substr($result['success_time'],0,-6));
                $rs['code'] = 1;
                $rs['msg'] = "更新成功";
                return $rs;
            }
        }else if ($type == 2){
            $statusname = [
                "SUCCESS" => "退款成功",
                "CLOSED" => "退款关闭",
                "PROCESSING" => "退款处理中",
                "ABNORMAL" => "退款异常"
            ];
            $check = Db::name("user_auth_refund")->where("id = ".$id)->find();
            $url = "https://api.mch.weixin.qq.com/v3/refund/domestic/refunds/".$check['refund_payment']."?mchid=".$mchid;
            $result = curl_get($url);
            if ($result['status'] == "SUCCESS" && $check['status'] == "SUCCESS"){
                return $rs;
            }else{
                $check = Db::name("user_auth_refund")->where("id = ".$id)->update(['status' => $result['status'],'refundtime' => strtotime(str_replace("T"," ",substr($result['success_time'],0,-6)))]);
                $rs['info']['statusname'] = $statusname[$result['trade_state']];
                $rs['info']['refundtime'] = str_replace("T"," ",substr($result['success_time'],0,-6));
                $rs['code'] = 1;
                $rs['msg'] = "更新成功";
                return $rs;
            }
        }
    }
    
    public function index2(){
        $data = $this->request->param();
        $map=[];
        // $map['a.status'] = '';
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['a.addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['a.addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['a.uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['a.uid','=',$uid];
            }
        }
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['b.real_name|b.mobile','like','%'.$keyword.'%'];
        }
			

    	$lists = Db::name("user_auth_refund")
    	        ->alias("a")
                ->where($map)
                ->join("user_auth b","a.uid = b.uid")
                ->field("a.*,b.mobile,b.real_name,b.cer_no")
                ->order("a.addtime DESC")
                ->paginate(20);
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
			$v['mobile']=m_s($v['mobile']);
			$v['cer_no']=m_s($v['cer_no']);
            return $v;           
        });
        
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        
        $this->assign('status', $this->getStatus());
    	
    	return $this->fetch();
    }
    
    public function edit2(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name("user_auth_refund")
    	        ->alias("a")
                ->where("a.id = ".$id)
                ->join("user_auth b","a.uid = b.uid")
                ->join("cash_account c","a.uid = c.uid")
                ->field("a.*,b.mobile,b.real_name,b.cer_no,c.type as account_type,c.account_bank,c.name,c.account,a.num as pay_num")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['userinfo']=getUserInfo($data['uid']);
        $data['mobile']=m_s($data['mobile']);
        $data['cer_no']=m_s($data['cer_no']);
        $account_type = ["1"=>"微信","2"=>"支付宝","3"=>"银行卡"];
        $data['account_type'] = $account_type[$data['account_type']];
        
        $this->assign('data', $data);
        return $this->fetch();
    }

    function editPost2(){
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();

            $status=$data['status'];
            $uid=$data['uid'];

            if($status=='0'){
                $this->success("修改成功！");
            }

            if($status == '1'){
                $data['status'] == 'SUCCESS';
                $data['refundtime']=time();   
            }
            
            try {
                
                Db::startTrans();

                $rs = DB::name('user_auth_refund')->update($data);
                if($rs===false){
                    throw new \Exception("修改失败");
                }

                // 更新状态
                $updateFlag = DB::name('user')->where("id = ".$uid)->update(array("is_bond" => 0));
                if($updateFlag === false){
                    throw new \Exception("更新状态失败");
                }
                // 删除保证金交款记录
                $deleteFlag = DB::name('user_auth')->where("uid = ".$uid)->update(array("status"=>0));
                $deleteFlag = DB::name('user_auth_payment')->where("type = 'bond' AND uid = ".$uid)->delete();
                if($updateFlag === false){
                    throw new \Exception("删除保证金交款记录失败");
                }

                Db::commit(); 
            } catch (\Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }

            if($status=='1'){
                $action="保证金退款信息：{$uid} - 退款完成";
            }
            
            setAdminLog($action);
            
            $this->success("修改成功！");
        }
    }

}
