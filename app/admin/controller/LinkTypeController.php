<?php

namespace app\admin\controller;

use think\Db;
use cmf\controller\AdminBaseController;

class LinkTypeController extends AdminBaseController
{

    /**
     * 链接类型列表
     */
    public function index()
    {
        $result  = Db::name('linkType')->select();

        $this->assign('result', $result);
        return $this->fetch();
    }

    /**
     * 链接类型添加
     */
    public function add()
    {
        return $this->fetch();
    }

    /**
     * 
     */
    public function addPost()
    {
        $data = $this->request->param();
        Db::name('linkType')->insert($data['post']);
        
        $this->resetcache();
        
        $this->success("添加成功！", url("linkType/index"));
    }

    /**
     * 链接类型编辑
     */
    public function edit()
    {
        $id     = $this->request->param('id', 0, 'intval');
        $result = Db::name('linkType')->where('id', $id)->find();

        $this->assign('result', $result);
        return $this->fetch();
    }

    /**
     * 链接类型编辑
     */
    public function editPost()
    {
        $data = $this->request->param();

        Db::name('linkType')->update($data['post']);
        $this->resetcache();

        $this->success("保存成功！", url("linkType/index"));

    }

    /**
     * 链接类型删除
     */
    public function delete()
    {
        $id = $this->request->param('id', 0, 'intval');

        $result = Db::name('linkType')->delete($id);
        if ($result) {
            $this->resetcache();
            $this->success("删除成功！", url("linkType/index"));
        } else {
            $this->error('删除失败！');
        }

    }

    function resetcache(){
        $key='getLinkTypes';
        $rs=Db::name("linkType")->select()->toArray();
        if($rs){
            setcaches($key,$rs);
        }
        return 1;
    }
}