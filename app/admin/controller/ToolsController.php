<?php

/**
 * 运营工具类
 */

namespace app\admin\controller;

use think\Db;
use cmf\controller\AdminBaseController;

class ToolsController extends AdminbaseController 
{
	// 最小允许的虚拟用户ID
	private $min_allow_uid = 39000;
	// 最大允许的虚拟用户ID
	private $max_allow_uid = 98224;

	// 虚拟用户池(理想规划的)
	private $unRealUserPool = array(
			array(10000,36000),
			array(39001,98224),
			array(99953,153457),
		);
	// 当前数据库中的虚拟用户池
	private $userPool;

	// 
	public function index()
	{
		
	}

	// 生成用户
	public function create_unreal_user()
	{
    	return $this->fetch();
	}

	// 获取当前最小的虚拟用户ID
	public function getMinUnRealUserId()
	{
		// 获取当前 范围 当前最小的用户ID 不含1 且 小于 36000

		$his_where = "id != 1 AND id > ".$this->min_allow_uid." AND id < ".$this->max_allow_uid;
		$min_data = DB::name('user')
						->field("min(id) as min_id")
						->where($his_where)
						->find();
		return $min_data['min_id'] ? $min_data['min_id'] : $this->max_allow_uid;
	}

	// 生成用户数据 
	public function getUnRealUserData($needToRunData)
	{
		$userData = array();
		foreach ($needToRunData as $key => $value) {
			$item_data = array(
				'user_type' => 2,
				'avatar' => '/default.jpg',
				'avatar_thumb' => '/default_thumb.jpg',
				'signature' => '这家伙很懒，什么都没留下',
				'last_login_ip' => '223.74.38.255',
				'login_type' => 'phone',
				'source' => 'android',
			);

			$item_data['id'] = $id = $value;
			$item_data['user_type'] = $user_type = 2;
			$item_data['mobile'] = $item_data['user_login'] = $mobile = '180026'.$id;
			$item_data['user_pass'] = $user_pass = '###1dec74e2a99cd4e2e253975538219738';
			$item_data['user_nicename'] = $user_nicename = '手机用户'.substr($id, 0,4);

			$userData[] = $item_data;
		}

		return $userData;
	}


	// 获取已生成 虚拟用户ID
	public function getAllUnRealUserId()
	{
		$his_where = " 1 ";

		if($this->unRealUserPool){
			$his_where_item = array();
			foreach ($this->unRealUserPool as $key => $value) {
				$his_where_item[] = "(id > ".$value[0]." AND id < ".$value[1].")";
			}
			if(!empty($his_where_item)){
				$his_where .= " AND (";
				$his_where .= implode(" OR ", $his_where_item);
				$his_where .= ")";
			}
		}

		$user_data = DB::name('user')
						->field("id")
						->where($his_where)
						->select()->toArray();

		if(!empty($user_data)){
			$user_data = array_column($user_data,'id');
		}
		
		return $user_data;
	}

	// 获取未入库的虚拟用户数据
	public function getAllowCreateUnRealUserData($old_data,$num)
	{
		$data = array();
		// 虚拟用户 数组化
		$all_pools = array();
		foreach ($this->unRealUserPool as $key => $value) {
			for ($i=$value[0]; $i < $value[1]; $i++) { 
				$all_pools[] = $i;
			}
		}

		// 取差集 得到未入库集合
		$diff_arr = array_diff($all_pools,$old_data);

		if(count($diff_arr) >= $num){
			// 满足数量
			// 获取制定数量数据
			$data = $this->getUnRealUserIds($diff_arr,$num);
		}

		return $data;
	}

	// 生成用户处理
	public function unRealUserPost()
	{
		$error_msg = "";
		$num = $this->request->param('num', 0, 'intval');
		if ($num > 0 && $num <= 3000) {
			// 已存在的虚拟用户信息
			$old_data = $this->getAllUnRealUserId();

			$needToRunData = $this->getAllowCreateUnRealUserData($old_data, $num);

			if(!empty($needToRunData)){
				// 生成数据
				$userData = $this->getUnRealUserData($needToRunData);

				if(!empty($userData)){
					$run_num = DB::name('user')->insertAll($userData);
					if($run_num){
						$this->success("执行成功！成功入库".$run_num);
					}else{
						$error_msg = "虚拟用户入库失败";
					}
				}else{
					$error_msg = "无虚拟用户可生成";
				}
			}else{
				$error_msg = "无虚拟用户可生成";
			}

		}else{
			if($num == 0){
				$error_msg = "执行条数不能为空";
			}else{
				$error_msg = "执行条数必须在3000以内";
			}
		}

		$this->error("执行失败！".$error_msg);
	}

	// 粉丝增加
	public function fans_plus()
	{
    	return $this->fetch();
	}

	// 校验主播用户ID 是否已存在
	public function checkLiveUid($liveuid)
	{
		$count = DB::name('user')->where('id='.$liveuid)->count();
		return $count;
	}

	// 获取当前主播粉丝最小的虚拟用户ID
	public function getMinFansUnRealUserId($liveuid)
	{
		$his_where = "uid < ".$this->max_allow_uid." AND touid = ".$liveuid;
		$min_data = DB::name('user_attention')
						->field("min(uid) as min_id")
						->where($his_where)
						->find();
		return $min_data['min_id'] ? $min_data['min_id'] : $this->max_allow_uid;
	}

	// 获取当前主播粉丝 虚拟用户ID
	public function getAllFansUnRealUserId($liveuid)
	{
		$his_where = "touid = ".$liveuid;

		if($this->unRealUserPool){
			$his_where_item = array();
			foreach ($this->unRealUserPool as $key => $value) {
				$his_where_item[] = "(uid > ".$value[0]." AND uid < ".$value[1].")";
			}
			if(!empty($his_where_item)){
				$his_where .= " AND (";
				$his_where .= implode(" OR ", $his_where_item);
				$his_where .= ")";
			}
		}

		$user_data = DB::name('user_attention')
						->field("uid")
						->where($his_where)
						->select()->toArray();

		if(!empty($user_data)){
			$user_data = array_column($user_data,'uid');
		}
		
		return $user_data;
	}

	// 获取当前可用数量的虚拟用户ID
	public function getUnRealUserPools()
	{
		$this->userPool = array();
		if($this->unRealUserPool){
			foreach ($this->unRealUserPool as $key => $value) {
				$his_where = "(id > ".$value[0]." AND id < ".$value[1].")";

				$item_data = DB::name('user')
						->field("MIN(id) as min_id,MAX(id) as max_id")
						->where($his_where)
						->find();
				$this->userPool[$key] = array($item_data['min_id'],$item_data['max_id']);
			}
		}

		return $this->userPool;
	}

	// 获取指定数量的可用虚拟用户ID
	public function getUnRealUserIds($diff_arr,$num)
	{
		$data = array();
		$index_data = array_rand($diff_arr,$num);
		// 获取指定键的值
		foreach ($index_data as $key => $value) {
			$data[] = $diff_arr[$value];
		}

		return $data;
	}

	// 获取当前可用数量的虚拟用户ID
	public function getAllowUseUnRealUserData($old_data,$num)
	{
		$data = array();
		// 虚拟用户 数组化
		$pools = array();
		$pools = $this->getAllUnRealUserId();

		// 取差集
		$diff_arr = array_diff($pools,$old_data);

		if(count($diff_arr) >= $num){
			// 满足数量
			// 获取制定数量数据
			$data = $this->getUnRealUserIds($diff_arr,$num);
		}

		return $data;
	}

	// 生成关注数据
	public function getUnRealUserFansData($liveuid,$needToRunData)
	{
		$focusData = array();
		foreach ($needToRunData as $key => $value) {
			$item_data['uid'] = $uid = $value;
			$item_data['touid'] = $liveuid;

			$focusData[] = $item_data;
		}

		return $focusData;
	}

	// 粉丝增加操作
	public function plusPost()
	{
		$error_msg = "";
		$liveuid = $this->request->param('liveuid', 0, 'intval');
		$num = $this->request->param('num', 0, 'intval');

		// 校验主播用户ID 是否存在
		$checkLiveUid = $this->checkLiveUid($liveuid);
		if(!$checkLiveUid){
			$error_msg = "主播用户未找到";
		}

		if ($num > 0 && $num <= 3000 && $error_msg == '') {
			// 当前虚拟用户池
			$this->getUnRealUserPools();
			// 当前主播粉丝的虚拟用户集合
			$fans_data = $this->getAllFansUnRealUserId($liveuid);

			// 校验是否满足可用数量生成
			$needToRunData = $this->getAllowUseUnRealUserData($fans_data,$num);

			if(!empty($needToRunData)){
				$focusData = $this->getUnRealUserFansData($liveuid,$needToRunData);

				if(!empty($focusData)){
					$run_num = DB::name('user_attention')->insertAll($focusData);
					if($run_num){
						$this->success("执行成功！成功入库".$run_num);
					}else{
						$error_msg = "粉丝添加入库失败";
					}
				}else{
					$error_msg = "无粉丝数据可生成";
				}
			}else{
				$error_msg = "无法生成当前数量的粉丝";
			}

		}else{
			if($num == 0){
				$error_msg = "执行条数不能为空";
			}else{
				$error_msg = "执行条数必须在3000以内";
			}
		}

		$this->error("执行失败！".$error_msg);
	}
	
}