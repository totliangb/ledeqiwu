<?php
/**
 * 提现记录
 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;
use think\Request;

class CashController extends HomebaseController {
    
    var $status=array(
        '0'=>'审核中',
        '1'=>'成功',
        '2'=>'失败',
    );

	function index(){       
		$data = $this->request->param();
        $uid=isset($data['uid']) ? $data['uid']: '';
        $token=isset($data['token']) ? $data['token']: '';
        $uid=(int)checkNull($uid);
        $token=checkNull($token);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$reason='您的登陆状态失效，请重新登陆！';
			$this->assign('reason', $reason);
			return $this->fetch(':error');
		}
        
        if (request()->isAjax()){
            $page = $data['page'];
            $start = ($page-1)*50;
            $type = $data['type'];
            dump($type);die;
        }
		$this->assign("uid",$uid);
		$this->assign("token",$token);
		

		$cashrecord=Db::name("cash_record")->where(["uid"=>$uid])->order("addtime desc")->limit(0,50)->select()->toArray();
		foreach($cashrecord as $k=>$v){
			$cashrecord[$k]['addtime']=date('Y-m-d H:i',$v['addtime']);
			$cashrecord[$k]['status_name']=$this->status[$v['status']];
		}
		
		$user = getUserInfo($uid);
		$cashdistribute = Db::name("cash_distributerecord")->where(["uid"=>$uid])->order("addtime desc")->limit(0,50)->select()->toArray();
		foreach($cashdistribute as $k=>$v){
		    $cashdistribute[$k]['username']=$user['user_nicename'];
			$cashdistribute[$k]['addtime']=date('Y-m-d H:i',$v['addtime']);
			$cashdistribute[$k]['status_name']=$this->status[$v['status']];
		}
		
		$shop = Db::name("shop")->where(["uid" => $uid])->find();
		$shopwithdrawal = Db::name("shop_withdraw")->where(["shop_id" => $shop["id"]])->order("addtime desc")->limit(0,50)->select()->toArray();
		foreach($shopwithdrawal as $k=>$v){
		    $shopwithdrawal[$k]['shopname']=$shop["name"];
			$shopwithdrawal[$k]['addtime']=date('Y-m-d H:i',$v['addtime']);
			$shopwithdrawal[$k]['status_name']=$this->status[$v['state']];
		}
		
		$this->assign("cashrecord",$cashrecord);
		$this->assign("cashdistribute",$cashdistribute);
		$this->assign("shopwithdrawal",$shopwithdrawal);
		
		return $this->fetch();
	    
	}
	
	public function getlistmore()
	{
		$data = $this->request->param();
        $uid=isset($data['uid']) ? $data['uid']: '';
        $token=isset($data['token']) ? $data['token']: '';
        $p=isset($data['page']) ? $data['page']: '1';
        $uid=(int)checkNull($uid);
        $token=checkNull($token);
        $p=checkNull($p);
		
		$result=array(
			'data'=>array(),
			'nums'=>0,
			'isscroll'=>0,
		);
	
		if(checkToken($uid,$token)==700){
			echo json_encode($result);
			exit;
		} 
		
		$pnums=50;
		$start=($p-1)*$pnums;

        $list=Db::name("cash_record")->where(["uid"=>$uid])->order("addtime desc")->limit($start,$pnums)->select()->toArray();
		foreach($list as $k=>$v){

			$list[$k]['addtime']=date('Y.m.d',$v['addtime']);
			$list[$k]['status_name']=$this->status[$v['status']];
		}
		
		$nums=count($list);
		if($nums<$pnums){
			$isscroll=0;
		}else{
			$isscroll=1;
		}
		
		$result=array(
			'data'=>$list,
			'nums'=>$nums,
			'isscroll'=>$isscroll,
		);

		echo json_encode($result);
		exit;
	}

}