<?php
/**
 * 个人主页
 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;

class HomeController extends HomebaseController {
	
	function index(){       
        $touid = $this->request->param('touid', 0, 'intval');
        
        if(!$touid){
            $this->assign("reason",'信息错误');
			return $this->fetch(':error');
        }

		$info=getUserInfo($touid);	

        if(!$info){
            $this->assign("reason",'信息错误');
			return $this->fetch(':error');
        }        

		$info['follows']=NumberFormat(getFollownums($touid));
		$info['fans']=NumberFormat(getFansnums($touid));
        
        $this->assign('info',$info);

		/* 贡献榜前三 */

		$contribute=Db::name("user_coinrecord")
				->field("uid,sum(totalcoin) as total")
				->where(["action"=>'1' , "touid"=>$touid])
				->group("uid")
				->order("total desc")
				->limit(0,3)
				->select()->toArray();
		foreach($contribute as $k=>$v){
			$userinfo=getUserInfo($v['uid']);
			$contribute[$k]['avatar']=$userinfo['avatar'];
		}		

        $this->assign('contribute',$contribute);
		
        /* 视频数 */
        $info['videonums']='0';
        /* 直播数 */
        $livenums=Db::name("live_record")
					->where(["uid"=>$touid])
					->count();

        $this->assign('livenums',$livenums);
		/* 直播记录 */
		$record=array();
		$record=Db::name("live_record")
					->field("id,uid,nums,starttime,endtime,title,city")
					->where(["uid"=>$touid])
					->order("id desc")
					->limit(0,20)
					->select()
                    ->toArray();
		foreach($record as $k=>$v){
            if($v['title']==''){
                $record[$k]['title']='无标题';
            }
			$record[$k]['datestarttime']=date("Y.m.d",$v['starttime']);
			$record[$k]['dateendtime']=date("Y.m.d",$v['endtime']);
            $cha=$v['endtime']-$v['starttime'];
            $record[$k]['length']=getSeconds($cha);
		}			

        $this->assign('liverecord',$record);
        
        
        /* 标签 */

        $label=getMyLabel($touid);
        
        $labels=array_slice($label,0,3);
        
		$this->assign('labels',$labels);

		
		return $this->fetch();
	    
	}

	
	public function jpush()
	{
	    $order_id = $this->request->param('order_id', 0, 'intval');
	    if (!$order_id) {
	        echo json_encode(['ret' => 200, 'data' => [
                'code' => 1000,
                'msg' => '参数错误1',
                'info' => []
            ]]);
            exit;
	    }
	    
	    $order_info = Db::name('shop_order_info')->alias('a')
	                ->join('__SHOP_ORDER__ b', 'a.order_id = b.id')
	                ->where('a.order_id', $order_id)->where('b.state', 20)->where('b.is_remind', 0)
	                ->column('a.uid', 'b.numbering');
	    if (!$order_info) {
	        echo json_encode(['ret' => 200, 'data' => [
                'code' => 1000,
                'msg' => '参数错误2',
                'info' => []
            ]]);
            exit;
	    }
	    $touid = implode(',', $order_info);
	    
		/* 极光推送 */
        $configpri=getConfigPri();
        $app_key = $configpri['jpush_key'];
        $master_secret = $configpri['jpush_secret'];
        
        if(!$app_key || !$master_secret) $this->error('请先设置推送配置'); 
        $issuccess=0;
        $error='推送失败';
        if($app_key && $master_secret ){
             
            require_once CMF_ROOT.'sdk/JPush/autoload.php';

            // 初始化
            $client = new \JPush\Client($app_key, $master_secret,null);
            $anthorinfo=array();
            
            $map=array();

            if($touid!=''){
                $uids=preg_split('/,|，|jiguang@/',$touid);
                $map[]  =['uid','in',array_values(array_filter($uids))];
            }
            
            $pushids=DB::name("user_pushid")->field("pushid")->where($map)->select();
			$pushids = json_decode($pushids,true);	
				

            $pushids=array_column($pushids,'pushid');
            $pushids=array_filter($pushids);
            
            $nums=count($pushids);
            
            $apns_production=false;
            if($configpri['jpush_sandbox']){
                $apns_production=true;
            }
            $title="订单号".implode(',', array_keys($order_info))."，提醒您尽快发货";
            for($i=0;$i<$nums;){
                $alias=array_slice($pushids,$i,900);
                $i+=900;
                try{
                    $result = $client->push()
                        ->setPlatform('all')
                        ->addRegistrationId($alias)
                        ->setNotificationAlert($title)
                        ->iosNotification($title, array(
                            'sound' => 'sound.caf',
                            'category' => 'jiguang',
                            'extras' => array(
                                'type' => '2',
                                'userinfo' => $anthorinfo
                            ),
                        ))
                        ->androidNotification('', array(
                            'extras' => array(
                                'type' => '2',
                                'title' => $title,
                                'userinfo' => $anthorinfo
                            ),
                        ))
                        ->options(array(
                            'sendno' => 100,
                            'time_to_live' => 0,
                            'apns_production' =>  $apns_production,
                        ))
                        ->send();
                    if($result['code']==0){
                        $issuccess=1;
                    }else{
                        $error=$result['msg'];
                    }
                } catch (Exception $e) {   
                    file_put_contents(CMF_ROOT.'data/jpush.txt',date('y-m-d h:i:s').'提交参数信息 设备名:'.json_encode($alias)."\r\n",FILE_APPEND);
                    file_put_contents(CMF_ROOT.'data/jpush.txt',date('y-m-d h:i:s').'提交参数信息:'.$e."\r\n",FILE_APPEND);
                    echo json_encode(['ret' => 200, 'data' => [
                        'code' => 1000,
                        'msg' => '提醒失败',
                        'info' => []
                    ]]);
                    exit;
                }					
            }			
        }
        /* 极光推送 */
        $data2=[
            'touid'=>$touid,
            'content'=>$title,
            'adminid'=>1,
            'admin'=>'admin',
            'ip'=>ip2long(get_client_ip(0,true)),
            'addtime'=>time(),
        ];
        
		$id = DB::name('pushrecord')->insertGetId($data2);
        if(!$id){
            echo json_encode(['ret' => 200, 'data' => [
                'code' => 1000,
                'msg' => '提醒失败',
                'info' => []
            ]]);
            exit;
        }
        Db::name('shop_order')->where('id', $order_id)->update(['is_remind' => 1]);
        $action="推送信息ID：{$id}";
        setAdminLog($action);
        echo json_encode(['ret' => 200, 'data' => [
            'code' => 0,
            'msg' => '提醒成功',
            'info' => []
        ]]);
        exit;
	}

}