<?php

namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;
use think\Exception;
/**
 * 余额
 */
class PdController extends HomebaseController {

    /**
     * 余额
     */
    public function changePd($change_type,$data = array()) {
        $data_log = array();
        $data_pd = array();
        $where = array();
        $data_log['uid'] = $data['uid'];
        $data_log['nickname'] = $data['nickname'];
        $data_log['add_time'] = time();
        $data_log['type'] = $change_type;

        switch ($change_type){
            case 'appraise_pay':
                // 鉴定支付完成 鉴定专家增加冻结金额
                $data_log['freeze_amount'] = $data['amount'];
                $data_log['desc'] = '鉴定订单支付成功: '.$data['order_sn'];
                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('freeze_predeposit')->find();
                $where['freeze_predeposit'] = $now_data['freeze_predeposit'];
                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] + $data['amount'];
                break;
            case 'appraise':
                // 鉴定专家完成鉴定 鉴定专家冻结金额转入余额
                $data_log['av_amount'] = $data['amount'];
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '鉴定订单完成，冻结转可用: '.$data['order_sn'];
                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('freeze_predeposit')->find();
                $where['available_predeposit'] = $now_data['available_predeposit'];
                $where['freeze_predeposit'] = $now_data['freeze_predeposit'];
                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'cash_pay':
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '提现成功，提现单号: '.$data['order_sn'];
                $data_log['admin_name'] = $data['admin_name'];
                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('freeze_predeposit')->find();
                $where['freeze_predeposit'] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'cash_del':
                $data_log['av_amount'] = $data['amount'];
                $data_log['freeze_amount'] = -$data['amount'];
                $data_log['desc'] = '取消提现申请，解冻余额，提现单号: '.$data['order_sn'];
                $data_log['admin_name'] = $data['admin_name'];

                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('available_predeposit,freeze_predeposit')->find();
                $where['available_predeposit'] = $now_data['available_predeposit'];
                $where['freeze_predeposit'] = $now_data['freeze_predeposit'];

                $data_pd['freeze_predeposit'] = $now_data['freeze_predeposit'] - $data['amount'];
                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                // 减少的余额小于原有余额 报错
                if($data_pd['freeze_predeposit']<0){
                    throw new Exception('冻结金额不足');
                }
                break;
            case 'recharge':
                $data_log['av_amount'] = $data['amount'];
                $data_log['desc'] = '充值，充值单号: '.$data['order_sn'];

                $now_data = Db::name('user')->where(array('id'=>$data['uid']))->field('available_predeposit,freeze_predeposit')->find();
                $where['available_predeposit'] = $now_data['available_predeposit'];

                $data_pd['available_predeposit'] = $now_data['available_predeposit'] + $data['amount'];
                break;
            default:
                throw new Exception('参数错误');
                break;
        }

        $where['id'] = $data['uid'];
        $update = Db::name('user')->where($where)->update($data_pd);
        if (!$update) {
            throw new Exception('操作失败');
        }
        $insert = Db::name('pd_log')->insert($data_log);
        if (!$insert) {
            throw new Exception('操作失败');
        }

        return $insert;
    }

}