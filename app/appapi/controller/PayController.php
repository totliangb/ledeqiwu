<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------

namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;
use app\appapi\controller\PdConTroller;
/**
 * 支付回调
 */
class PayController extends HomebaseController {
	
    private $wxDate = null;
    private $aliDate = null;	
	
	//支付宝 回调
	public function notify_ali() {
        $configpri=getConfigPri();
		require_once(CMF_ROOT."sdk/alipay_app/alipay.config.php");
        $alipay_config['partner']		= $configpri['aliapp_partner'];
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_core.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_rsa.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_notify.class.php");

		require_once(CMF_ROOT."sdk/Alipaysdk/AopClient.php");

		//计算得出通知验证结果
		// $alipayNotify = new \AlipayNotify($alipay_config);
		// $verify_result = $alipayNotify->verifyNotify();
		$aop = new \AopClient;
		$aop->alipayrsaPublicKey = $configpri['aliapp_alipayrsaPublicKey'];
		$verify_result = $aop->rsaCheckV1($_POST, NULL, "RSA2");

		$this->logali("ali_data:".json_encode($_POST));
		if($verify_result) {//验证成功
			//商户订单号
			$out_trade_no = $_POST['out_trade_no'];
			//支付宝交易号
			$trade_no = $_POST['trade_no'];
			//交易状态
			$trade_status = $_POST['trade_status'];
			
			//交易金额
			$total_fee = $_POST['total_amount'];
			
			if($_POST['trade_status'] == 'TRADE_FINISHED') {
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
					
				//注意：
				//退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
				//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的

				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
		
			}else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
					
				//注意：
				//付款完成后，支付宝系统发送该交易状态通知
				//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的

				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
                $where['orderno']=$out_trade_no;
                $where['money']=$total_fee;
                $where['type']=1;
                
                $data=[
                    'trade_no'=>$trade_no
                ];

				$this->logali("where:".json_encode($where));	
                $res=handelCharge($where,$data);
				if($res==0){
                    $this->logali("orderno:".$out_trade_no.' 订单信息不存在');	
                    echo "fail";
                    exit;
				}
                
                $this->logali("成功");
                echo "success";		//请不要修改或删除
                exit;    
			}
			//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

			echo "fail";		//请不要修改或删除
			exit;
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}else {
			$this->logali("验证失败");		
			//验证失败
			echo "fail";
            exit;
			//调试用，写文本函数记录程序运行情况是否正常
			//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
		}			
		
	}

	//支付宝支付商城回掉
	public function notify_ali2() {
        $configpri=getConfigPri();
		require_once(CMF_ROOT."sdk/alipay_app/alipay.config.php");
        $alipay_config['partner']		= $configpri['aliapp_partner'];
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_core.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_rsa.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_notify.class.php");

		require_once(CMF_ROOT."sdk/Alipaysdk/AopClient.php");

		//计算得出通知验证结果
		// $alipayNotify = new \AlipayNotify($alipay_config);
		// $verify_result = $alipayNotify->verifyNotify();
		$aop = new \AopClient;
		$aop->alipayrsaPublicKey = $configpri['aliapp_alipayrsaPublicKey'];
		$verify_result = $aop->rsaCheckV1($_POST, NULL, "RSA2");

		$this->logali("ali_shop_data:".json_encode($_POST));
		if($verify_result) {//验证成功
			//商户订单号
			$out_trade_no = $_POST['out_trade_no'];
			//支付宝交易号
			$trade_no = $_POST['trade_no'];
			//交易状态
			$trade_status = $_POST['trade_status'];
			
			//交易金额
			$total_fee = $_POST['total_amount'];
			
			if($_POST['trade_status'] == 'TRADE_FINISHED') {
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
					
				//注意：
				//退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
				//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的

				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
		
			}else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
					
				//注意：
				//付款完成后，支付宝系统发送该交易状态通知
				//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的

				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
				$this->wxDate = $_POST;
				$orderinfo=Db::name('shop_order')->where('state =0 and numbering='.$out_trade_no)->select()->toArray();
				$this->logali("orderinfo:".json_encode($orderinfo));	
				if($orderinfo){
					$this->shop_order($orderinfo, 'ali');
					$this->logali("成功");	
					echo "success";		//请不要修改或删除
					exit;
				}else{
					$this->logali("orderno:".$out_trade_no.' 订单信息不存在');		
				}	  
			}
			//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

			echo "fail";		//请不要修改或删除
			exit;
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}else {
			$this->logali("验证失败");		
			//验证失败
			echo "fail";
            exit;
			//调试用，写文本函数记录程序运行情况是否正常
			//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
		}		
	}

	private function jpush($touid)
	{
		/* 极光推送 */
        $configpri=getConfigPri();
        $app_key = $configpri['jpush_key'];
        $master_secret = $configpri['jpush_secret'];
        
        if(!$app_key || !$master_secret) $this->error('请先设置推送配置'); 
        $issuccess=0;
        $error='推送失败';
        if($app_key && $master_secret ){
             
            require_once CMF_ROOT.'sdk/JPush/autoload.php';

            // 初始化
            $client = new \JPush\Client($app_key, $master_secret,null);
            $anthorinfo=array();
            
            $map=array();

            if($touid!=''){
                $uids=preg_split('/,|，/',$touid);
                $map[]  =['uid','in',$uids];
            }

            $pushids=DB::name("user_pushid")->field("pushid")->where($map)->select();
			$pushids = json_decode($pushids,true);	
				

            $pushids=array_column($pushids,'pushid');
            $pushids=array_filter($pushids);

            $nums=count($pushids);
            
            $apns_production=false;
            if($configpri['jpush_sandbox']){
                $apns_production=true;
            }
            $title='有新的订单，请及时发货';
            for($i=0;$i<$nums;){
                $alias=array_slice($pushids,$i,900);
                $i+=900;
                try{
                    $result = $client->push()
                        ->setPlatform('all')
                        ->addRegistrationId($alias)
                        ->setNotificationAlert($title)
                        ->iosNotification($title, array(
                            'sound' => 'sound.caf',
                            'category' => 'jiguang',
                            'extras' => array(
                                'type' => '2',
                                'userinfo' => $anthorinfo
                            ),
                        ))
                        ->androidNotification('', array(
                            'extras' => array(
                                'type' => '2',
                                'title' => $title,
                                'userinfo' => $anthorinfo
                            ),
                        ))
                        ->options(array(
                            'sendno' => 100,
                            'time_to_live' => 0,
                            'apns_production' =>  $apns_production,
                        ))
                        ->send();
                    if($result['code']==0){
                        $issuccess=1;
                    }else{
                        $error=$result['msg'];
                    }
                } catch (Exception $e) {   
                    file_put_contents(CMF_ROOT.'data/jpush.txt',date('y-m-d h:i:s').'提交参数信息 设备名:'.json_encode($alias)."\r\n",FILE_APPEND);
                    file_put_contents(CMF_ROOT.'data/jpush.txt',date('y-m-d h:i:s').'提交参数信息:'.$e."\r\n",FILE_APPEND);
                }					
            }			
        }
        /* 极光推送 */
        $data2=[
            'touid'=>$touid,
            'content'=>$title,
            'adminid'=>1,
            'admin'=>'admin',
            'ip'=>ip2long(get_client_ip(0,true)),
            'addtime'=>time(),
        ];
        
		$id = DB::name('pushrecord')->insertGetId($data2);
        if(!$id){
            return 0;
        }
        
        $action="推送信息ID：{$id}";
        setAdminLog($action,1,"admin");
        return 1;
	}
	//商城订单修改
	private function shop_order($order, $type = 'wx')
	{
		//优惠券
		$coupon = $shop_id = $order_id = $amount = $coupon_id = [];
		$order_type = 0;
		$postage = 0;
        // dump($order);die;
		foreach ($order as $key => $value) {
			if (isset($value['coupon_id'])) $coupon_id[] = $value['coupon_id'];
			$order_id[] = $value['id'];
			if($value['order_type'] == '1'){
				$order_type = 1;
			}
		}

		if($coupon_id)
		{
			$coupon = Db::name('shop_coupon_receive')->where('state', 0)->whereIn('id', $coupon_id)->column('discounted_price', 'shop_id');
			//$coupon[$coupon_info['shop_id']] = $coupon_info['discounted_price'];
		}
 		
		if($order_type == 1){
			//订单详情
			$order_info = Db::name('shop_order_info')->alias('a')
					->whereIn('a.order_id', $order_id)
					->join('__SHOP_AUCTION__ b ',' a.commodity_id = b.id')
					->field('a.commodity_id,a.order_id,a.shop_id,b.win_price as price,a.num')
					->select()->toArray();//->column('a.order_id,a.shop_id,b.price,a.num', 'a.order_id')

			foreach ($order_info as $key => $value) 
			{
				if(!isset($shop_id[$value['shop_id']]))
				{
					$shop_id[$value['shop_id']] = $value['price']*$value['num'];
				}
				else
				{
					$shop_id[$value['shop_id']] += $value['price']*$value['num'];
				}
				
				$postage += 0;
			}
		}else{
			//订单详情
			$order_info = Db::name('shop_order_info')->alias('a')
					->whereIn('a.order_id', $order_id)
					->join('__SHOP_COMMODITY__ b ',' a.commodity_id = b.id')
					->field('a.commodity_id,a.order_id,a.shop_id,b.price,a.num,b.postage')
					->select()->toArray();//->column('a.order_id,a.shop_id,b.price,a.num', 'a.order_id')

			foreach ($order_info as $key => $value) 
			{
				if(!isset($amount[$value['commodity_id']])) $amount[$value['commodity_id']] = 0;
			    $amount[$value['commodity_id']] += $value['num'];
				if(!isset($shop_id[$value['shop_id']]))
				{
					$shop_id[$value['shop_id']] = $value['price']*$value['num'];
				}
				else
				{
					$shop_id[$value['shop_id']] += $value['price']*$value['num'];
				}
				
				$postage += $value['postage'];
			}
		}

		foreach ($shop_id as $key => &$value) 
		{
			if(isset($coupon[$key])) $value -= $coupon[$key];
		}

		if('wx' == $type && array_sum($shop_id)+$postage != $this->wxDate['total_fee']/100)
		{
			echo $this -> returnInfo("FAIL","签名失败");
			$this->logwx("shoporderno:".$this->wxDate['out_trade_no'].' 金额不对');//log打印保存
			exit;
		}
		else if('ali' == $type && array_sum($shop_id)+$postage != $this->wxDate['total_amount'])
		{
			echo "fail";
			$this->logali("shoporderno:".$this->wxDate['out_trade_no'].' 金额不对');
			exit;
		}
        
		try {
			Db::startTrans();
			$ret1 = Db::name('shop_order')->where('state = 0 and numbering="'.$this->wxDate['out_trade_no'].'"')->update([
				'state' => 20,
				'is_pay_type' => ('wx' == $type) ? 1 : 0,
				'pay_time' => time()
			]);
// 			echo $ret1;
			if(!$ret1){
			    echo 4;
				throw new \Exception('修改订单状态失败');
			}
            
			if ($amount) {
			    foreach ($amount as $k => $v) {
			        $ret2 = Db::name('shop_commodity')->where('id', $k)->update([
    					'amount' => Db::raw('amount-'.$v),
    					'sales' => Db::raw('sales+'.$v)
    				]);
    				// echo $ret2;
			        if(!$ret2){ 
			            echo 3;throw new \Exception('添加商家金额失败');
			            
			        }
			    }
			}

			foreach ($shop_id as $key => $value) 
			{
				//'balance' =>Db::raw('balance+'.$value),
				$ret3 = Db::name('shop')->where('id', $key)->update([
					'balance_freeze' => Db::raw('balance_freeze+'.$value)
				]);
				// echo $ret3;
				if(!$ret3){
				    echo 2;
				    throw new \Exception('添加商家金额失败');
				} 
				foreach ($order as $k => $v){
				    if ($key == $v['shop_id']){
				        $insertlog = 
        				    [
            					'shop_id' => $key,
            					'order_id' => $v['id'],
            					'type' => 1,
            					'amount' => $value,
            					'type2' => 0,
            					'addtime' => time(),
            					'text' => '支付回调成功'
            				];
        				$ret4 = Db::name('shop_amount_log')->insert($insertlog);
        				if(!$ret4) throw new \Exception("修改失败");
				    }
				}
			}
            
			if(count($coupon))
			{
				if(!Db::name('shop_coupon_receive')->where('state = 0 and id='.$order['coupon_id'])->update(['state' => 1]))
				{
				    echo 1;
					throw new \Exception('修改优惠券失败');
				}
			}

// 			Db::rollback();die;
			Db::commit(); 
		} catch (\Exception $e) {
		    echo $e->getMessage();
			Db::rollback();
			if('wx' == $type)
			{
				echo $this -> returnInfo("FAIL","签名失败");
				$this->logwx("shoporderno:".$this->wxDate['out_trade_no'].$e->getMessage());//log打印保存
			}
			else
			{
				echo "fail";
				$this->logali("shoporderno:".$this->wxDate['out_trade_no'].$e->getMessage());
			}
			exit;
		}
		$uid = Db::name('shop')->where('id', 'in', array_keys($shop_id))->column('uid');
		$this->jpush(implode(',', $uid));
	}
	/* 支付宝支付 */
	

	/* 微信支付 */	
	public function notify_wx(){
		$config=getConfigPri();

		//$xmlInfo = $GLOBALS['HTTP_RAW_POST_DATA'];

		$xmlInfo=file_get_contents("php://input"); 

		//解析xml
		$arrayInfo = $this -> xmlToArray($xmlInfo);
		$this -> wxDate = $arrayInfo;
		$this -> logwx("wx_data:".json_encode($arrayInfo));//log打印保存
		if($arrayInfo['return_code'] == "SUCCESS"){
			if(!empty($arrayInfo['return_msg'])){
				echo $this -> returnInfo("FAIL","签名失败");
				$this -> logwx("签名失败:".$sign);//log打印保存
				exit;
			}else{
				$wxSign = $arrayInfo['sign'];
				unset($arrayInfo['sign']);
				$arrayInfo['appid']  =  $config['wx_appid'];
				$arrayInfo['mch_id'] =  $config['wx_mchid'];
				$key =  $config['wx_key'];
				ksort($arrayInfo);//按照字典排序参数数组
				$sign = $this -> sign($arrayInfo,$key);//生成签名
				$this -> logwx("数据打印测试签名signmy:".$sign.":::微信sign:".$wxSign);//log打印保存
				if($this -> checkSign($wxSign,$sign)){
					echo $this -> returnInfo("SUCCESS","OK");
					$this -> logwx("签名验证结果成功:".$sign);//log打印保存
					$this -> orderServer();//订单处理业务逻辑
					exit;
				}else{
					echo $this -> returnInfo("FAIL","签名失败");
					$this -> logwx("签名验证结果失败:本地加密：".$sign.'：：：：：三方加密'.$wxSign);//log打印保存
					exit;
				}
			}
		}else{
			echo $this -> returnInfo("FAIL","签名失败");
			$this -> logwx($arrayInfo['return_code']);//log打印保存
			exit;
		}			
	}
	
	private function returnInfo($type,$msg){
		if($type == "SUCCESS"){
			return $returnXml = "<xml><return_code><![CDATA[{$type}]]></return_code></xml>";
		}else{
			return $returnXml = "<xml><return_code><![CDATA[{$type}]]></return_code><return_msg><![CDATA[{$msg}]]></return_msg></xml>";
		}
	}		
	
	//签名验证
	private function checkSign($sign1,$sign2){
		return trim($sign1) == trim($sign2);
	}
	/* 订单查询加值业务处理
	 * @param orderNum 订单号	   
	 */
	private function orderServer(){
		$info = $this -> wxDate;
		$this->logwx("info:".json_encode($info));
        $where['orderno']=$info['out_trade_no'];
        $where['type']=2;
        
        $trade_no=$info['transaction_id'];
        
        $data=[
            'trade_no'=>$trade_no
        ];
        
        $this->logwx("where:".json_encode($where));	
        $res=handelCharge($where,$data);
        if($res==0){
            $this->logwx("orderno:".$out_trade_no.' 订单信息不存在');	
            return false;
        }
        
        $this->logwx("成功");
        return true;
	}		
	/**
	* sign拼装获取
	*/
	private function sign($param,$key){
		
		$sign = "";
		foreach($param as $k => $v){
			$sign .= $k."=".$v."&";
		}
	
		$sign .= "key=".$key;
		$sign = strtoupper(md5($sign));
		return $sign;
	
	}
	/**
	* xml转为数组
	*/
	private function xmlToArray($xmlStr){
		$msg = array(); 
		$postStr = $xmlStr; 
		$msg = (array)simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA); 
		return $msg;
	}
	
	/* 微信支付 */

	/* 苹果支付 */
	
	public function notify_ios(){
		$content=file_get_contents("php://input");  
		$data = json_decode($content,true); 

        $this->logios("data:".json_encode($data));
        
		$receipt = $data["receipt-data"];     
		$isSandbox = $data["sandbox"];
		$out_trade_no = $data["out_trade_no"];
		$version_ios = $data["version_ios"];
        
		$info = $this->getReceiptData($receipt, $version_ios);   
		
		$this->logios("info:".json_encode($info));
		
		$iforderinfo=Db::name("charge_user")->where(["trade_no"=>$info['transaction_id'],"type"=>'3'])->find();

		if($iforderinfo){
			echo '{"status":"fail","info":"非法提交-001"}';
            exit;
		}
        
        $chargeinfo=Db::name("charge_rules")->where(["product_id"=>$info['product_id']])->find();
        if(!$chargeinfo){
            echo '{"status":"fail","info":"非法提交-002"}';
            exit;
        }

		//判断订单是否存在
        $where['orderno']=$out_trade_no;
        $where['coin']=$chargeinfo['coin'];
        $where['type']=3;
        
        $trade_no=$info['transaction_id'];
        $ambient=$info['ambient'];
        
        $data=[
            'trade_no'=>$trade_no,
            'ambient'=>$ambient,
        ];
        
        $this->logios("where:".json_encode($where));	
        
        $res=handelCharge($where,$data);
        if($res==0){
            $this->logios("orderno:".$out_trade_no.' 订单信息不存在');	
            echo '{"status":"fail","info":"订单信息不存在-003"}'; 		
			exit;
        }
        
        $this->logios("成功");
        echo '{"status":"success","info":"充值支付成功"}';
		exit;
	}		
	public function getReceiptData($receipt, $version_ios){ 
		$config=getConfigPri();
        
        $ios_shelves=$config['ios_shelves'];
        
        $this->logios("version_ios:".$version_ios);
        $this->logios("ios_shelves:".$ios_shelves);
        $ambient=0;
		if ($version_ios == $ios_shelves) {   
			//沙盒
			$endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
            $ambient=0;
		}else {  
			//生产
			$endpoint = 'https://buy.itunes.apple.com/verifyReceipt'; 
            $ambient=1;
		}   

		$postData = json_encode(   
				array('receipt-data' => $receipt)   
		);   

		$ch = curl_init($endpoint);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);	//关闭安全验证
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  	//关闭安全验证
		curl_setopt($ch, CURLOPT_POST, true);   
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);   

		$response = curl_exec($ch);   
		$errno    = curl_errno($ch);   
		$errmsg   = curl_error($ch);   
		curl_close($ch);   
        
        $this->logios("getReceiptData response:".json_encode($response));
        $this->logios("getReceiptData errno:".json_encode($errno));
        $this->logios("getReceiptData errmsg:".json_encode($errmsg));

		if($errno != 0) {   
			echo '{"status":"fail","info":"服务器出错，请联系管理员"}';
			exit;
		}   
		$data = json_decode($response,1);   

		if (!is_array($data)) {   
			echo '{"status":"fail","info":"验证失败,如有疑问请联系管理"}';
			exit;
		}   

		if (!isset($data['status']) || $data['status'] != 0) {   
			echo '{"status":"fail","info":"验证失败,如有疑问请联系管理"}';
			exit;
		}   

        $newdata=end($data['receipt']['in_app']);
		return array(     
			'product_id'     =>  $newdata['product_id'],   
			'transaction_id' =>  $newdata['transaction_id'],   
			'ambient' =>  $ambient,   
		);
	}  

	/* 微信支付 */	
	public function shop_notify_wx(){
		$config=getConfigPri();

		//$xmlInfo = $GLOBALS['HTTP_RAW_POST_DATA'];
		$xmlInfo=file_get_contents("php://input"); 
        $this -> logwx("wx_ys_data:".$xmlInfo);

		//解析xml
		$arrayInfo = $this -> xmlToArray($xmlInfo);

		$this -> wxDate = $arrayInfo;
		$this -> logwx("wx_data:".json_encode($arrayInfo));//log打印保存
		if($arrayInfo['result_code'] == "SUCCESS"){
			$wxSign = $arrayInfo['sign'];
			unset($arrayInfo['sign']);
			$arrayInfo['appid']  =  $config['wx_appid'];
			$arrayInfo['mch_id'] =  $config['wx_mchid'];
			$key =  $config['wx_key'];
			ksort($arrayInfo);//按照字典排序参数数组
			$sign = $this -> sign($arrayInfo,$key);//生成签名
			$this -> logwx("数据打印测试签名signmy:".$sign.":::微信sign:".$wxSign);//log打印保存
			
			$orderinfo=Db::name('shop_order')->where('state = 0 and numbering="'.$arrayInfo['out_trade_no'].'"')->select()->toArray();
            
			if($this -> checkSign($wxSign,$sign) && $orderinfo){
			    
			    $this->shop_order($orderinfo);
				echo $this -> returnInfo("SUCCESS","OK");
				$this -> logwx("签名验证结果成功:".$sign);//log打印保存
				//$this -> orderServer();//订单处理业务逻辑
				exit;
			}else{
				echo $this -> returnInfo("FAIL","签名失败");
				$this -> logwx("签名验证结果失败:本地加密：".$sign.'：：：：：三方加密'.$wxSign);//log打印保存
				exit;
			}
		}else{
			echo $this -> returnInfo("FAIL","签名失败");
			$this -> logwx($arrayInfo['return_code']);//log打印保存
			exit;
		}			
	} 
	
	public function cert_notify_wx(){
		$config=getConfigPri();

		//$xmlInfo = $GLOBALS['HTTP_RAW_POST_DATA'];
		$xmlInfo=file_get_contents("php://input");

// 		//解析xml
		$arrayInfo = $this -> xmlToArray($xmlInfo);
		$this -> wxDate = $arrayInfo;
		$this -> logwx("wx_cert_data:".json_encode($arrayInfo));//log打印保存
		
		if($arrayInfo['return_code'] == "SUCCESS"){
			$wxSign = $arrayInfo['sign'];
			unset($arrayInfo['sign']);
			$arrayInfo['appid']  =  $config['wx_appid'];
			$arrayInfo['mch_id'] =  $config['wx_mchid'];
			$key =  $config['wx_key'];
			ksort($arrayInfo);//按照字典排序参数数组
			$sign = $this -> sign($arrayInfo,$key);//生成签名
			$this -> logwx("数据打印测试签名signmy:".$sign.":::微信sign:".$wxSign);//log打印保存
			
			$orderinfo=Db::name('user_auth_payment')->where('status is null and payment="'.$arrayInfo['out_trade_no'].'"')->find();
			if($this -> checkSign($wxSign,$sign) && $orderinfo){
			    $this->checkAuth($arrayInfo['out_trade_no'],$arrayInfo['time_end']);
				echo $this -> returnInfo("SUCCESS","OK");
				$this -> logwx("签名验证结果成功:".$sign);//log打印保存
				//$this -> orderServer();//订单处理业务逻辑
				exit;
			}else{
				echo $this -> returnInfo("FAIL","签名失败");
				$this -> logwx("签名验证结果失败:本地加密：".$sign.'：：：：：三方加密'.$wxSign);//log打印保存
				exit;
			}
		}else{
			echo $this -> returnInfo("FAIL","签名失败");
			$this -> logwx($arrayInfo['return_code']);//log打印保存
			exit;
		}			
	}
	
	public function certrefund_notify_wx(){
		$config=getConfigPri();

		//$xmlInfo = $GLOBALS['HTTP_RAW_POST_DATA'];
		$xmlInfo=file_get_contents("php://input"); 
		//解析xml
		$arrayInfo = $this -> xmlToArray($xmlInfo);
		$this -> wxDate = $arrayInfo;
		$this -> logwx("wx_certrefund_data:".json_encode($arrayInfo));//log打印保存
		if($arrayInfo['return_code'] == "SUCCESS"){
			$wxSign = $arrayInfo['sign'];
			unset($arrayInfo['sign']);
			$arrayInfo['appid']  =  $config['wx_appid'];
			$arrayInfo['mch_id'] =  $config['wx_mchid'];
			$key =  $config['wx_key'];
			ksort($arrayInfo);//按照字典排序参数数组
			$sign = $this -> sign($arrayInfo,$key);//生成签名
			$this -> logwx("数据打印测试签名signmy:".$sign.":::微信sign:".$wxSign);//log打印保存
			
			$orderinfo=Db::name('user_auth_refund')->where('status is null and payment='.$arrayInfo['out_trade_no'])->find();
			if($this -> checkSign($wxSign,$sign) && $orderinfo){
			    $this->checkAuthRefund($arrayInfo['out_trade_no']);
				echo $this -> returnInfo("SUCCESS","OK");
				$this -> logwx("签名验证结果成功:".$sign);//log打印保存
				//$this -> orderServer();//订单处理业务逻辑
				exit;
			}else{
				echo $this -> returnInfo("FAIL","签名失败");
				$this -> logwx("签名验证结果失败:本地加密：".$sign.'：：：：：三方加密'.$wxSign);//log打印保存
				exit;
			}
		}else{
			echo $this -> returnInfo("FAIL","签名失败");
			$this -> logwx($arrayInfo['return_code']);//log打印保存
			exit;
		}			
	}
	
	//认证费OR保证金支付判断
	private function checkAuth($payment,$paytime){
	    Db::name("user_auth_payment")->where("payment='".$payment."'")->update(array("status"=>"SUCCESS","paytime"=>strtotime($paytime)));
	    $paymentinfo = Db::name("user_auth_payment")->where("payment='".$payment."'")->find();
	    if ($paymentinfo['type'] == "bond"){
	        $up = Db::name("user")->where("id = ".$paymentinfo['uid'])->update(array("is_bond"=>1));
	    }elseif ($paymentinfo['type'] == "cert"){
	        $overdue = strtotime("+1 year",strtotime(date("Y-m-d 24:00",strtotime($paytime))));
	        $up = Db::name("user")->where("id = ".$paymentinfo['uid'])->update(array("cert_overdue"=>$overdue));
	    }
	}
	
	//保证金退款申请判断
	private function checkAuthRefund($payment){
	    Db::name("user_auth_refund")->where("payment='".$payment."'")->update(array("status"=>"SUCCESS","refundtime"=>time()));
	    $paymentinfo = Db::name("user_auth_refund")->where("payment='".$payment."'")->find();
	    if ($paymentinfo['type'] == "rebond"){
	        $up = Db::name("user")->where("id = ".$paymentinfo['uid'])->update(array("is_bond"=>0));
	    }
	}
		
	/* 苹果支付 */	
			
	/* 打印log */
	public function logali($msg){
		file_put_contents(CMF_ROOT.'data/paylog/logali_'.date('Y-m-d').'.txt',date('Y-m-d H:i:s').'  msg:'.$msg."\r\n",FILE_APPEND);
	}		
	/* 打印log */
	public function logwx($msg){
		file_put_contents(CMF_ROOT.'data/paylog/logwx_'.date('Y-m-d').'.txt',date('Y-m-d H:i:s').'  msg:'.$msg."\r\n",FILE_APPEND);
	}			
	/* 打印log */
	public function logios($msg){
		file_put_contents(CMF_ROOT.'data/paylog/logios_'.date('Y-m-d').'.txt',date('Y-m-d H:i:s').'  msg:'.$msg."\r\n",FILE_APPEND);
	}	


	/* 专家鉴定消息推送 */
	private function expertjpush($touid)
	{
		/* 极光推送 */
        $configpri=getConfigPri();
        $app_key = $configpri['jpush_key'];
        $master_secret = $configpri['jpush_secret'];
        
        if(!$app_key || !$master_secret) $this->error('请先设置推送配置'); 
        $issuccess=0;
        $error='推送失败';
        if($app_key && $master_secret ){
             
            require_once CMF_ROOT.'sdk/JPush/autoload.php';

            // 初始化
            $client = new \JPush\Client($app_key, $master_secret,null);
            $anthorinfo=array();
            
            $map=array();

            if($touid!=''){
                $uids=preg_split('/,|，/',$touid);
                $map[]  =['uid','in',$uids];
            }

            $pushids=DB::name("user_pushid")->field("pushid")->where($map)->select();
			$pushids = json_decode($pushids,true);	
				

            $pushids=array_column($pushids,'pushid');
            $pushids=array_filter($pushids);

            $nums=count($pushids);
            
            $apns_production=false;
            if($configpri['jpush_sandbox']){
                $apns_production=true;
            }
            $title='有新的鉴定请求，请及时进行鉴定';
            for($i=0;$i<$nums;){
                $alias=array_slice($pushids,$i,900);
                $i+=900;
                try{
                    $result = $client->push()
                        ->setPlatform('all')
                        ->addRegistrationId($alias)
                        ->setNotificationAlert($title)
                        ->iosNotification($title, array(
                            'sound' => 'sound.caf',
                            'category' => 'jiguang',
                            'extras' => array(
                                'type' => '2',
                                'userinfo' => $anthorinfo
                            ),
                        ))
                        ->androidNotification('', array(
                            'extras' => array(
                                'type' => '2',
                                'title' => $title,
                                'userinfo' => $anthorinfo
                            ),
                        ))
                        ->options(array(
                            'sendno' => 100,
                            'time_to_live' => 0,
                            'apns_production' =>  $apns_production,
                        ))
                        ->send();
                    if($result['code']==0){
                        $issuccess=1;
                    }else{
                        $error=$result['msg'];
                    }
                } catch (Exception $e) {   
                    file_put_contents(CMF_ROOT.'data/jpush.txt',date('y-m-d h:i:s').'提交参数信息 设备名:'.json_encode($alias)."\r\n",FILE_APPEND);
                    file_put_contents(CMF_ROOT.'data/jpush.txt',date('y-m-d h:i:s').'提交参数信息:'.$e."\r\n",FILE_APPEND);
                }					
            }			
        }
        /* 极光推送 */
        $data2=[
            'touid'=>$touid,
            'content'=>$title,
            'adminid'=>1,
            'admin'=>'admin',
            'ip'=>ip2long(get_client_ip(0,true)),
            'addtime'=>time(),
        ];
		$id = DB::name('pushrecord')->insertGetId($data2);
        if(!$id){
            return 0;
        }
        
        $action="推送信息ID：{$id}";
        setAdminLog($action,1,"admin");
        return 1;
	}

	/* 微信支付（鉴定） */	
	public function appraise_notify_wx(){
		$config=getConfigPri();

		//$xmlInfo = $GLOBALS['HTTP_RAW_POST_DATA'];
		$xmlInfo=file_get_contents("php://input"); 
        $this -> logwx("wx_ys_data:".$xmlInfo);

		//解析xml
		$arrayInfo = $this -> xmlToArray($xmlInfo);

		$this -> wxDate = $arrayInfo;
		$this -> logwx("wx_data:".json_encode($arrayInfo));//log打印保存
		if($arrayInfo['result_code'] == "SUCCESS"){
			$wxSign = $arrayInfo['sign'];
			unset($arrayInfo['sign']);
			$arrayInfo['appid']  =  $config['wx_appid'];
			$arrayInfo['mch_id'] =  $config['wx_mchid'];
			$key =  $config['wx_key'];
			ksort($arrayInfo);//按照字典排序参数数组
			$sign = $this -> sign($arrayInfo,$key);//生成签名
			$this -> logwx("数据打印测试签名signmy:".$sign.":::微信sign:".$wxSign);//log打印保存
			
			// 获取未付款订单信息
			$orderinfo=Db::name('appraise_apply')->where("pay_status = '0' and pay_sn='".$arrayInfo['out_trade_no']."'")->find();

			if($this -> checkSign($wxSign,$sign) && $orderinfo){
			    $this->appraise_order($orderinfo);
				echo $this -> returnInfo("SUCCESS","OK");
				$this -> logwx("签名验证结果成功:".$sign);//log打印保存
				exit;
			}else{
				echo $this -> returnInfo("FAIL","签名失败");
				$this -> logwx("签名验证结果失败:本地加密：".$sign.'：：：：：三方加密'.$wxSign);//log打印保存
				exit;
			}
		}else{
			echo $this -> returnInfo("FAIL","签名失败");
			$this -> logwx($arrayInfo['return_code']);//log打印保存
			exit;
		}			
	}	

	/* 支付宝支付（鉴定） */	
	public function appraise_notify_ali() {
        $configpri=getConfigPri();
		require_once(CMF_ROOT."sdk/alipay_app/alipay.config.php");
        $alipay_config['partner']		= $configpri['aliapp_partner'];
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_core.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_rsa.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_notify.class.php");

		require_once(CMF_ROOT."sdk/Alipaysdk/AopClient.php");

		//计算得出通知验证结果
		// $alipayNotify = new \AlipayNotify($alipay_config);
		// $verify_result = $alipayNotify->verifyNotify();
		$aop = new \AopClient;
		$aop->alipayrsaPublicKey = $configpri['aliapp_alipayrsaPublicKey'];
		$verify_result = $aop->rsaCheckV1($_POST, NULL, "RSA2");

		$this->logali("ali_data:".json_encode($_POST));
		if($verify_result) {//验证成功
			//商户订单号
			$out_trade_no = $_POST['out_trade_no'];
			//支付宝交易号
			$trade_no = $_POST['trade_no'];
			//交易状态
			$trade_status = $_POST['trade_status'];
			
			//交易金额
			$total_fee = $_POST['total_amount'];

			$this->aliDate = $_POST;
			if($_POST['trade_status'] == 'TRADE_FINISHED') {

			}else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
				// 获取未付款订单信息
				$orderinfo=Db::name('appraise_apply')->where("pay_status = '0' and pay_sn='".$out_trade_no."'")->find();
				if(!empty($orderinfo)){
					$this->appraise_order($orderinfo,'ali');
                
	                $this->logali("成功");
	                echo "success";		//请不要修改或删除
	                exit;
				} 
			}

			echo "fail";		//请不要修改或删除
			exit;
		}else {
			$this->logali("验证失败");		
			//验证失败
			echo "fail";
            exit;
		}			
		
	}

	// 鉴宝订单修改
	private function appraise_order($order, $type = 'wx')
	{
		// 校验金额
		switch ($type) {
			case 'wx':
				$out_trade_no = $this->wxDate['out_trade_no'];
				$trade_no = $this->wxDate['transaction_id'];
				$total_fee = $this->wxDate['total_fee'];

				if($order['appraise_fee']*1 != $total_fee/100)
				{
					echo $this -> returnInfo("FAIL","签名失败");
					$this->logwx("appraiseorderno:".$out_trade_no.' 金额不对');//log打印保存
					exit;
				}
				break;
			case 'ali':
				//商户订单号
				$out_trade_no = $this->aliDate['out_trade_no'];
				//支付宝交易号
				$trade_no = $this->aliDate['trade_no'];
				$total_fee = $this->aliDate['total_amount'];

				if($order['appraise_fee']*1 != $total_fee)
				{
					echo "fail";
					$this->logali("appraiseorderno:".$out_trade_no.' 金额不对');
					exit;
				}
				break;
		}
        
		try {
			Db::startTrans();
			$ret1 = Db::name('appraise_apply')->where('pay_status = "0" and pay_sn="'.$out_trade_no.'"')->update([
				'pay_status' => '1',
				'pay_type' => $type,
				'trade_no' => $trade_no,
				'pay_time' => time()
			]);

			if(!$ret1){
			    echo 4;
				throw new \Exception('修改订单状态失败');
			}

            // 鉴定专家 冻结余额增加
			if ($order['appraise_fee']*1>0 && $order['expert_fee']*1>0) {
				$amount = $order['expert_fee'];
				$now_user_info = Db::name('user')->where(array('id'=>$order['touid']))->field('user_nicename')->find();
				$data_log = array();
				$data_log['uid'] = $order['touid'];
				$data_log['nickname'] = $now_user_info['user_nicename'];
				$data_log['amount'] = $amount;
				$data_log['order_sn'] = $order['apply_order_sn'];
				$pd = controller('Pd');
				$pd->changePd('appraise_pay',$data_log);
			}
            
			Db::commit(); 
		} catch (\Exception $e) {
		    echo $e->getMessage();
			Db::rollback();
			switch ($type) {
				case 'wx':
					echo $this -> returnInfo("FAIL","签名失败");
					$this->logwx("appraiseorderno:".$out_trade_no.$e->getMessage());//log打印保存
					break;
				case 'ali':
					echo "fail";
					$this->logali("appraiseorderno:".$out_trade_no.$e->getMessage());
					break;
			}
			exit;
		}
		// 推送给专家
		$this->expertjpush($order['touid']);
	}

	/* 微信支付（查看鉴定结果） */	
	public function appraise_answer_notify_wx(){
		$config=getConfigPri();

		//$xmlInfo = $GLOBALS['HTTP_RAW_POST_DATA'];
		$xmlInfo=file_get_contents("php://input"); 
        $this -> logwx("wx_ys_data:".$xmlInfo);

		//解析xml
		$arrayInfo = $this -> xmlToArray($xmlInfo);

		$this -> wxDate = $arrayInfo;
		$this -> logwx("wx_data:".json_encode($arrayInfo));//log打印保存
		if($arrayInfo['result_code'] == "SUCCESS"){
			$wxSign = $arrayInfo['sign'];
			unset($arrayInfo['sign']);
			$arrayInfo['appid']  =  $config['wx_appid'];
			$arrayInfo['mch_id'] =  $config['wx_mchid'];
			$key =  $config['wx_key'];
			ksort($arrayInfo);//按照字典排序参数数组
			$sign = $this -> sign($arrayInfo,$key);//生成签名
			$this -> logwx("数据打印测试签名signmy:".$sign.":::微信sign:".$wxSign);//log打印保存
			
			// 获取未付款订单信息
			$orderinfo=Db::name('appraise_answer_view')->where("pay_status = '0' and pay_sn='".$arrayInfo['out_trade_no']."'")->find();

			if($this -> checkSign($wxSign,$sign) && $orderinfo){
			    $this->appraise_answer_order($orderinfo);
				echo $this -> returnInfo("SUCCESS","OK");
				$this -> logwx("签名验证结果成功:".$sign);//log打印保存
				exit;
			}else{
				echo $this -> returnInfo("FAIL","签名失败");
				$this -> logwx("签名验证结果失败:本地加密：".$sign.'：：：：：三方加密'.$wxSign);//log打印保存
				exit;
			}
		}else{
			echo $this -> returnInfo("FAIL","签名失败");
			$this -> logwx($arrayInfo['return_code']);//log打印保存
			exit;
		}			
	}	

	/* 支付宝支付（查看鉴定结果） */	
	public function appraise_answer_notify_ali() {
        $configpri=getConfigPri();
		require_once(CMF_ROOT."sdk/alipay_app/alipay.config.php");
        $alipay_config['partner']		= $configpri['aliapp_partner'];
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_core.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_rsa.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_notify.class.php");

		require_once(CMF_ROOT."sdk/Alipaysdk/AopClient.php");

		//计算得出通知验证结果
		// $alipayNotify = new \AlipayNotify($alipay_config);
		// $verify_result = $alipayNotify->verifyNotify();
		$aop = new \AopClient;
		$aop->alipayrsaPublicKey = $configpri['aliapp_alipayrsaPublicKey'];
		$verify_result = $aop->rsaCheckV1($_POST, NULL, "RSA2");

		$this->logali("ali_data:".json_encode($_POST));
		if($verify_result) {//验证成功
			//商户订单号
			$out_trade_no = $_POST['out_trade_no'];
			//支付宝交易号
			$trade_no = $_POST['trade_no'];
			//交易状态
			$trade_status = $_POST['trade_status'];
			
			//交易金额
			$total_fee = $_POST['total_amount'];

			$this->aliDate = $_POST;
			if($_POST['trade_status'] == 'TRADE_FINISHED') {

			}else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
				// 获取未付款订单信息
				$orderinfo=Db::name('appraise_answer_view')->where("pay_status = '0' and pay_sn='".$out_trade_no."'")->find();
				if(!empty($orderinfo)){
					$this->appraise_answer_order($orderinfo,'ali');
                
	                $this->logali("成功");
	                echo "success";		//请不要修改或删除
	                exit;
				} 
			}

			echo "fail";		//请不要修改或删除
			exit;
		}else {
			$this->logali("验证失败");		
			//验证失败
			echo "fail";
            exit;
		}			
		
	}

	// 查看鉴宝结果订单修改
	private function appraise_answer_order($order, $type = 'wx')
	{
		switch ($type) {
			case 'wx':
				$out_trade_no = $this->wxDate['out_trade_no'];
				$trade_no = $this->wxDate['transaction_id'];
				$total_fee = $this->wxDate['total_fee'];

				if($order['view_fee']*1 != $total_fee/100)
				{
					echo $this -> returnInfo("FAIL","签名失败");
					$this->logwx("appraiseorderno:".$out_trade_no.' 金额不对');//log打印保存
					exit;
				}
				break;
			case 'ali':
				//商户订单号
				$out_trade_no = $this->aliDate['out_trade_no'];
				//支付宝交易号
				$trade_no = $this->aliDate['trade_no'];
				$total_fee = $this->aliDate['total_amount'];

				if($order['view_fee']*1 != $total_fee)
				{
					echo "fail";
					$this->logali("appraiseanswerorderno:".$out_trade_no.' 金额不对');
					exit;
				}
				break;
		}
        
		try {
			Db::startTrans();
			$ret1 = Db::name('appraise_answer_view')->where('pay_status = "0" and pay_sn="'.$out_trade_no.'"')->update([
				'pay_status' => '1',
				'pay_type' => $type,
				'trade_no' => $trade_no,
				'pay_time' => time()
			]);

			if(!$ret1){
			    echo 4;
				throw new \Exception('修改订单状态失败');
			}
            
			Db::commit(); 
		} catch (\Exception $e) {
		    echo $e->getMessage();
			Db::rollback();
			switch ($type) {
				case 'wx':
					echo $this -> returnInfo("FAIL","签名失败");
					$this->logwx("appraiseanswerorderno:".$out_trade_no.$e->getMessage());//log打印保存
					break;
				case 'ali':
					echo "fail";
					$this->logali("appraiseanswerorderno:".$out_trade_no.$e->getMessage());
					break;
			}
			exit;
		}
	}

	public function cert_notify_ali(){
        $configpri=getConfigPri();
		require_once(CMF_ROOT."sdk/alipay_app/alipay.config.php");
        $alipay_config['partner']		= $configpri['aliapp_partner'];
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_core.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_rsa.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_notify.class.php");

		require_once(CMF_ROOT."sdk/Alipaysdk/AopClient.php");

		//计算得出通知验证结果
		// $alipayNotify = new \AlipayNotify($alipay_config);
		// $verify_result = $alipayNotify->verifyNotify();
		$aop = new \AopClient;
		$aop->alipayrsaPublicKey = $configpri['aliapp_alipayrsaPublicKey'];
		$verify_result = $aop->rsaCheckV1($_POST, NULL, "RSA2");

		$this->logali("ali_data:".json_encode($_POST));
		if($verify_result) {//验证成功
			//商户订单号
			$out_trade_no = $_POST['out_trade_no'];
			//支付宝交易号
			$trade_no = $_POST['trade_no'];
			//交易状态
			$trade_status = $_POST['trade_status'];
			
			//交易金额
			$total_fee = $_POST['total_amount'];

			$this->aliDate = $_POST;
			if($_POST['trade_status'] == 'TRADE_FINISHED') {

			}else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
				// 获取未付款订单信息
				$orderinfo=Db::name('user_auth_payment')->where("status != 'SUCCESS' and payment='".$out_trade_no."'")->find();
				if(!empty($orderinfo)){
					$this->checkAuth($trade_no,date('Y-m-d H:i:s',time()));
                
	                $this->logali("成功");
	                echo "success";		//请不要修改或删除
	                exit;
				} 
			}

			echo "fail";		//请不要修改或删除
			exit;
		}else {
			$this->logali("验证失败");		
			//验证失败
			echo "fail";
            exit;
		}			
		
	}

	public function pd_notify_wx(){
		$config=getConfigPri();

		//$xmlInfo = $GLOBALS['HTTP_RAW_POST_DATA'];
		$xmlInfo=file_get_contents("php://input"); 
        $this -> logwx("wx_ys_data:".$xmlInfo);

		//解析xml
		$arrayInfo = $this -> xmlToArray($xmlInfo);

		$this -> wxDate = $arrayInfo;
		$this -> logwx("wx_data:".json_encode($arrayInfo));//log打印保存
		if($arrayInfo['result_code'] == "SUCCESS"){
			$wxSign = $arrayInfo['sign'];
			unset($arrayInfo['sign']);
			$arrayInfo['appid']  =  $config['wx_appid'];
			$arrayInfo['mch_id'] =  $config['wx_mchid'];
			$key =  $config['wx_key'];
			ksort($arrayInfo);//按照字典排序参数数组
			$sign = $this -> sign($arrayInfo,$key);//生成签名
			$this -> logwx("数据打印测试签名signmy:".$sign.":::微信sign:".$wxSign);//log打印保存
			
			// 获取未付款订单信息
			$orderinfo=Db::name('pd_recharge')->where("payment_state = '0' and recharge_sn='".$arrayInfo['out_trade_no']."'")->find();

			if($this -> checkSign($wxSign,$sign) && $orderinfo){
				$this->pd_recharge_order($orderinfo);
				echo $this -> returnInfo("SUCCESS","OK");
				$this -> logwx("签名验证结果成功:".$sign);//log打印保存
				exit;
			}else{
				echo $this -> returnInfo("FAIL","签名失败");
				$this -> logwx("签名验证结果失败:本地加密：".$sign.'：：：：：三方加密'.$wxSign);//log打印保存
				exit;
			}
		}else{
			echo $this -> returnInfo("FAIL","签名失败");
			$this -> logwx($arrayInfo['return_code']);//log打印保存
			exit;
		}

	}

	public function pd_notify_ali(){
		
        $configpri=getConfigPri();
		require_once(CMF_ROOT."sdk/alipay_app/alipay.config.php");
        $alipay_config['partner']		= $configpri['aliapp_partner'];
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_core.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_rsa.function.php");
		require_once(CMF_ROOT."sdk/alipay_app/lib/alipay_notify.class.php");

		require_once(CMF_ROOT."sdk/Alipaysdk/AopClient.php");

		//计算得出通知验证结果
		// $alipayNotify = new \AlipayNotify($alipay_config);
		// $verify_result = $alipayNotify->verifyNotify();
		$aop = new \AopClient;
		$aop->alipayrsaPublicKey = $configpri['aliapp_alipayrsaPublicKey'];
		$verify_result = $aop->rsaCheckV1($_POST, NULL, "RSA2");

		$this->logali("ali_data:".json_encode($_POST));
		if($verify_result) {//验证成功
			//商户订单号
			$out_trade_no = $_POST['out_trade_no'];
			//支付宝交易号
			$trade_no = $_POST['trade_no'];
			//交易状态
			$trade_status = $_POST['trade_status'];
			
			//交易金额
			$total_fee = $_POST['total_amount'];

			$this->aliDate = $_POST;
			if($_POST['trade_status'] == 'TRADE_FINISHED') {

			}else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
				// 获取未付款订单信息
				$orderinfo=Db::name('pd_recharge')->where("payment_state = '0' and recharge_sn='".$out_trade_no."'")->find();
				if(!empty($orderinfo)){
					$this->pd_recharge_order($orderinfo,'ali');
                
	                $this->logali("成功");
	                echo "success";		//请不要修改或删除
	                exit;
				} 
			}

			echo "fail";		//请不要修改或删除
			exit;
		}else {
			$this->logali("验证失败");		
			//验证失败
			echo "fail";
            exit;
		}
	}


	// 余额充值业务处理
	private function pd_recharge_order($order, $type = 'wx')
	{
		switch ($type) {
			case 'wx':
				$out_trade_no = $this->wxDate['out_trade_no'];
				$trade_no = $this->wxDate['transaction_id'];
				$total_fee = $this->wxDate['total_fee'];

				if($order['amount']*1 != $total_fee/100)
				{
					echo $this -> returnInfo("FAIL","签名失败");
					$this->logwx("appraiseorderno:".$out_trade_no.' 金额不对');//log打印保存
					exit;
				}
				break;
			case 'ali':
				//商户订单号
				$out_trade_no = $this->aliDate['out_trade_no'];
				//支付宝交易号
				$trade_no = $this->aliDate['trade_no'];
				$total_fee = $this->aliDate['total_amount'];

				if($order['amount']*1 != $total_fee)
				{
					echo "fail";
					$this->logali("appraiseanswerorderno:".$out_trade_no.' 金额不对');
					exit;
				}
				break;
		}
        
		try {
			Db::startTrans();

			// 更改订单状态
			$ret1 = Db::name('pd_recharge')->where('payment_state = "0" and recharge_sn="'.$out_trade_no.'"')->update([
				'payment_state' => '1',
				'payment_code' => $type,
				'trade_no' => $trade_no,
				'payment_time' => time()
			]);

			if(!$ret1){
			    echo 4;
				throw new \Exception('修改订单状态失败');
			}

			// 修改余额
			$amount = $order['amount'];
			$data_log = array();
			$data_log['uid'] = $order['uid'];
			$data_log['nickname'] = $order['nickname'];
			$data_log['amount'] = $amount;
			$data_log['order_sn'] = $order['recharge_sn'];

			$pd = controller('Pd');
			$pd->changePd('recharge',$data_log);

			// 邀请奖励
			// $data_ap = array();
			// $data_ap['orderno'] = $order['recharge_sn'];
			// $data_ap['money'] = $amount;

			// setAgentProfit($order['uid'],$data_ap);
            
			Db::commit(); 
		} catch (\Exception $e) {
		    echo $e->getMessage();
			Db::rollback();
			switch ($type) {
				case 'wx':
					echo $this -> returnInfo("FAIL","签名失败");
					$this->logwx("rechangeorderno:".$out_trade_no.$e->getMessage());//log打印保存
					break;
				case 'ali':
					echo "fail";
					$this->logali("rechangeorderno:".$out_trade_no.$e->getMessage());
					break;
			}
			exit;
		}
	}

}


