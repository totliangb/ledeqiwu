<?php
namespace app\Merchant\controller;
use think\Db;
use cmf\controller\MerchantController;

class ShopController extends MerchantController
{
	public function info()
	{
		if(!request()->isPost())
		{
			$shop_id = session('shop_id');
			$info = Db::name('shop')->where('id', $shop_id)->find();
			if ($info) {
			    $info['shop_img'] = get_upload_path($info['shop_img'], 0);
			}
			$this->assign('info', $info);
			return $this->fetch();
		}
		
		$id = request()->post('id', 'intval');
		$post['shop_img'] = request()->post('shop_img', 'htmlspecialchars');
		$post['description'] = request()->post('description', 'htmlspecialchars');

		if(!Db::name('shop')->where('id', $id)->update($post)) $this->error('修改失败');

		$this->success('修改成功');
	}

	//提现列表
	public function withdraw()
	{
		$list = Db::name('shop_withdraw')->alias('a')
				->leftJoin('__CASH_ACCOUNT__ b', 'a.account_id = b.id')
		        ->field('a.id,a.amount,a.state,a.addtime,a.checktime,b.type,b.name,b.account,b.account_bank')
		        ->where('a.shop_id', session('shop_id'))
		        ->paginate(20);
		$lists = $list->items();

		$state = ['申请中','通过','取消'];
		$type = [1=>'支付宝',2=>'微信',3=>'银行卡'];
		foreach ($lists as $key => &$value) 
		{
			$value['statename'] = isset($state[$value['state']]) ? $state[$value['state']] : '未定义状态';
			$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
			if(!empty($value['checktime'])) $value['checktime'] = date('Y-m-d H:i:s', $value['checktime']);
			$value['typename'] = isset($type[$value['type']]) ? $type[$value['type']] : '未定义状态';
		}

		$this->assign("page", $list->render());
        $this->assign('list', $lists);
        return $this->fetch();
	}

	//申请提现
	public function addwithdraw()
	{
		$id = request()->param('id', 'intval');
		$balance = Db::name('shop')->where('id', session('shop_id'))->value('balance');
		$getConfigPri = getConfigPri();

		if(!request()->isPost())
		{
			$account_list = Db::name('cash_account')->field('id,type,account_bank,name')->where('uid', $_SESSION['user_id'])->select()->toArray();

			$type = [1=>'支付宝',2=>'微信',3=>'银行卡'];
			foreach ($account_list as $key => &$value) 
			{
				$value['typename'] = isset($type[$value['type']]) ? $type[$value['type']] : '未定义状态';
			}

			if($id)
			{
				$info = Db::name('shop_withdraw')->field('id,account_id,amount')->where('id', $id)->where('shop_id', session('shop_id'))->find();
				$this->assign('info', $info);
			}

			$this->assign('balance', $balance);
			$this->assign('account_list', $account_list);
			$this->assign('cash_min', $getConfigPri['cash_min']);
			return $this->fetch();
		}

		$post['amount'] = request()->param('amount');
		if($getConfigPri['cash_min'] > $post['amount']) $this->error('不满足最低提现金额');

		if($post['amount'] > $balance) $this->error('提现余额不足');

		$post['account_id'] = request()->param('account_id', 'intval');
		$post['shop_id'] = session('shop_id');
		$post['addtime'] = time();
		try {
			Db::startTrans();

			if(!Db::name('shop_withdraw')->insert($post)) throw new \Exception("添加失败");

			if(!Db::name('shop')->where('id',$post['shop_id'])->update([
				'balance_freeze'=>Db::raw('balance_freeze+'.$post['amount']),
				'balance'=>Db::raw('balance-'.$post['amount']),
			])) throw new Exception("添加失败");

			$add = [
				[
					'shop_id' => $post['shop_id'],
					'order_id' => 0,
					'type' => 0,
					'amount' => $post['amount'],
					'type2' => 1,
					'addtime' => time(),
					'text' => '申请提现'
				],
				[
					'shop_id' => $post['shop_id'],
					'order_id' => 0,
					'type' => 1,
					'amount' => $post['amount'],
					'type2' => 0,
					'addtime' => time(),
					'text' => '申请提现'
				]
			];

			if(!Db::name('shop_amount_log')->insertAll($add)) throw new \Exception("添加失败");
			
			Db::commit(); 
		} catch (\Exception $e) {
			Db::rollback();
			$this->error('添加失败');
		}

		$this->success('添加成功');
	}

	//银行卡列表
	public function bankcard()
	{
		$list = Db::name('cash_account')->field('id,uid,type,account_bank,name,account')->where('uid', $_SESSION['user_id'])->paginate(20);

		$lists = $list->items();
		$type = [1=>'支付宝',2=>'微信',3=>'银行卡'];

		foreach ($lists as $key => &$value) 
		{
			$value['typename'] = isset($type[$value['type']]) ? $type[$value['type']] : '未定义状态';
		}

		$this->assign("page", $list->render());
        $this->assign('list', $lists);
        return $this->fetch();
	}

	//添加银行卡
	public function addbank()
	{
		$id = request()->param('id', 'intval');
		if(!request()->isPost())
		{
			$info = Db::name('cash_account')->field('id,uid,type,account_bank,name,account')->where('id', $id)->where('uid', $_SESSION['user_id'])->find();
			$this->assign('info', $info);
        	return $this->fetch();
		}

		$post['type'] = request()->param('type', 'intval');
		if(!in_array($post['type'], [1,2,3])) $this->error('请选择分类');

		$post['account_bank'] = request()->param('account_bank', 'htmlspecialchars');
		if(mb_strlen($post['account_bank']) > 100) $this->error('银行名称字数不能超过100个字');

		$post['name'] = request()->param('name', 'htmlspecialchars');
		if(mb_strlen($post['name']) > 100 || mb_strlen($post['name']) <= 0 ) $this->error('姓名字数应在1~100位之间');

		$post['account'] = request()->param('account', 'htmlspecialchars');
		if(mb_strlen($post['account']) > 100 || mb_strlen($post['account']) <= 0 ) $this->error('账号字数应在1~100位之间');

		if($id)
		{
			if(!Db::name('cash_account')->where('id', $id)->where('uid', $_SESSION['user_id'])->update($post))$this->error('修改失败');
			$this->success('修改成功');
		}
		$post['uid'] = $_SESSION['user_id'];
		$post['addtime'] = time();
		if(!Db::name('cash_account')->insert($post)) $this->error('添加失败');
		$this->success('添加成功');
	}
}