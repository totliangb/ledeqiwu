<?php
namespace app\Merchant\controller;
use think\Db;
use cmf\controller\MerchantController;

class OrderController extends MerchantController
{
	/**
     * 默认列表
     */
    public function index() 
    {
    	$type = request()->param('type');

    	$where = (empty($type)) ? '' : ' and a.state = '.$type;

        $shop_id = session('shop_id');

        $list = Db::name('shop_order')->alias('a')
        		->where('a.shop_id='.$shop_id.$where)
        		->field('a.id,a.numbering,a.state,a.addtime,a.payment_amount,b.user_nicename,a.is_reply,a.logistics_id')
        		->leftJoin('__USER__ b ',' a.uid=b.id')
        		->order('a.addtime desc')
        		->paginate(20);

        $lists = $list->items();
        if($lists)
        {
        	foreach ($lists as $key => &$value) 
        	{
        		$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
        		$value['statename'] = $this->state($value['state']);
        	}
        }

        $this->assign("page", $list->render());
        $this->assign('list', $lists);
        return $this->fetch();
    }

    //发货
    public function ship()
    {
    	if(!request()->isPost())
    	{
    		$id = request()->param('id', 'intval');

	    	if(!$id) $this->error('参数错误');

            $info = Db::name('shop_order')->alias('a')
                    ->field('a.id,a.numbering,a.logistics_id,a.single_number,c.province,c.city,c.area,c.info,c.name,c.phone')
                    ->where('a.state>=20 and a.id='.$id)
                    ->leftJoin('__SHOP_ADDRESS__ c', 'a.address = c.id')
                    ->find();
            $logistics = Db::name('shop_logistics')->field('id,name')->order('sort desc')->where('state=1')->select()->toArray();
            $areawhere = [];
            if (!empty($info['province'])) $areawhere[] = $info['province'];
            if (!empty($info['city'])) $areawhere[] = $info['city'];
            if (!empty($info['area'])) $areawhere[] = $info['area'];
            $area = Db::name('shop_area')->whereIn('coding', $areawhere)->column('name', 'coding');
            
            foreach ($logistics as $key => $value) 
            {
                if($value['id'] == $info['logistics_id'])
                {
                    $info['logistics_name'] = $value['name'];
                }
            }
            $info['areainfo'] = $area[$info['province']].$area[$info['city']].$area[$info['area']].$info['info'];

	    	if(!$info) $this->error('参数错误');

	    	$this->assign('info', $info);
	    	$this->assign('shop_logistics', json_encode($logistics));
	    	return $this->fetch();
    	}
    	
    	$id = request()->param('id', 'intval');
    	$data['logistics_id'] = request()->param('logistics_id', 'intval');
    	$data['single_number'] = request()->param('single_number');
    	if(!$id || !$data['logistics_id'] || !$data['single_number']) $this->error('参数错误');

    	if(mb_strlen($data['single_number']) > 100 || mb_strlen($data['single_number']) <= 0) $this->error('订单编号字数应在1~100位之间');

    	$data['state'] = 30;
    	if(!Db::name('shop_order')->where('state in (20,30) and id='.$id)->update($data)) $this->error('发货失败');

    	$this->success('发货成功');
    }

    //回复
    public function reply()
    {
    	if(!request()->isPost())
    	{
    		$id = request()->param('id', 'intval');

    		$info = Db::name('shop_evaluation')->where('order_id',$id)->find();

    		$info['addtime'] = date('Y-m-d H:i:s', $info['addtime']);

    		if(!empty($info['reply_time'])) $info['reply_time'] = date('Y-m-d H:i:s', $info['reply_time']);
    		if(!empty($info['img'])) $info['img'] = json_decode($info['img']);

    		if(!$info) $this->error('参数错误');

    		$order = Db::name('shop_order')->field('is_reply,numbering')->where('id',$id)->find();

    		$this->assign('info', $info);
    		$this->assign('order', $order);
    		$this->assign('username', Db::name('user')->where('id',$info['uid'])->value('user_nicename'));
    		return $this->fetch();
    	}

    	$id = request()->param('id', 'intval');
    	$ev_info = Db::name('shop_evaluation')->where('id',$id)->find();
    	$post['reply_content'] = request()->param('reply_content', 'htmlspecialchars');

    	if(!$id) $this->error('参数错误');
    	if(mb_strlen($post['reply_content']) > 100 || mb_strlen($post['reply_content']) <= 0) $this->error('回复内容的字数应在1~100位之间');
    	$post['reply_time'] = time();
    	try {
			Db::startTrans();

			if(!Db::name('shop_evaluation')->where('id',$id)->update($post) && !Db::name("shop_order")->where("id",$ev_info['order_id'])->update(array("is_reply"=>1))) throw new \Exception("修改失败");

			
			Db::commit(); 
		} catch (\Exception $e) {
			Db::rollback();
			$this->error('回复失败');
		}
		$this->success('回复成功');
    }

    //退款退货页面
    public function refundinfo()
    {
        $id = request()->param('id', 'intval');

        $info = Db::name('shop_order')->alias('a')
                ->leftJoin('__USER__ b ',' a.uid = b.id')
                ->field('a.id,a.numbering,a.state,a.is_pay_type,a.addtime,a.payment_amount,a.original_price,a.discount,b.user_nicename')
                ->where('a.id',$id)
                ->where('state','in', '40,41,42,43')
                ->find();
        if(!$info) $this->error('该订单未退货');

        $refund = Db::name('shop_refund')->where('order_id', $id)->find();
        $refund_info_id = [$refund['refund_info_id']];

        if(!empty($refund['img'])) {
            $refund['img'] = explode(',', $refund['img']);
            foreach ($refund['img'] as $k => &$v) {
                $v = get_upload_path($v, 0);
            }
        }

        if(empty($refund['refund_info_id2'])) $refund_info_id[] = $refund['refund_info_id2'];

        $shop_refund_info = Db::name('shop_refund_info')->where('state', 1)->column('id,type,title', 'id');

        $refund['addtime'] = date('Y-m-d H:i:s', $refund['addtime']);

        $this->assign('info', $info);
        $this->assign('refund', $refund);
        $this->assign('shop_refund_info', $shop_refund_info);
        return $this->fetch();
    }

    //退款、退货
    public function refund()
    {
		$id = request()->param('id', 'intval');
		$type = request()->param('state', 'intval');
		$data = request()->param();
        $order = Db::name('shop_order')->where('id',$id)->find();
        if ($order['state'] == 43){
            require_once CMF_ROOT.'sdk/Alipaysdk/Lite.php';
            $ali = new \Alipay_Lite();
            try{
                Db::startTrans();
                if(!Db::name('shop_order')->where('id',$id)->update(['state' => 41])){
                    throw new \Exception("修改失败");
                }
                if(!Db::name('shop')->where('id',$order['shop_id'])->setDec('balance_freeze',$order['payment_amount'])){
                    throw new \Exception('退款失败'); 
                }
                
                if ($order['is_pay_type'] == 1) {
                    if(!wxTradeRefundRequest2($order)){
                        throw new \Exception("修改失败");
                    }
                } else {
                    if(!$ali->setCofig()->TradeRefundRequest($order)){
                        throw new \Exception("修改失败");
                    }
                }
                Db::commit(); 
            }catch(\Exception $e){
                Db::rollback();
				$this->error('操作失败：'.$e->getMessage());
            }
            
            $this->success('操作成功');
            exit;
        }
    	if(!$id || !in_array($type, [1,2])) $this->error('参数错误');
        
    	if(1 == $type) //退款
		{
            require_once CMF_ROOT.'sdk/Alipaysdk/Lite.php';
            $ali = new \Alipay_Lite();

			try {
				Db::startTrans();

				if(!$order) throw new \Exception("修改失败");
				
                $refund = Db::name('shop_refund')->where('order_id', $id)->find();
                $update = ['state' => 1, 'check_time' => time()];
                if ($refund['order_state'] == 20){
                    $state = 41;
                }elseif($refund['order_state'] == 30){
                    $state = 42;
                    if ($data['receive_address'] != '' && $data['receive_name'] != '' && $data['receive_tel'] != ''){
                        $update['address'] = $data['receive_address'];
                        $update['name'] = $data['receive_name'];
                        $update['tel'] = $data['receive_tel'];
                    }else{
                        throw new \Exception("请完善地址信息");
                    }
                }

                if(!Db::name('shop_refund')->where('order_id', $id)->update($update)) throw new \Exception("修改失败");

				if(!Db::name('shop_amount_log')->insert([
					'shop_id' => $order['shop_id'],
					'order_id' => $order['id'],
					'type' => 0,
					'amount' => $order['payment_amount'],
					'type2' => 0,
					'addtime' => time(),
					'text' => '商家退款退货'
				])) throw new \Exception("修改失败");
                
				if(!Db::name('shop_order')->where('id',$id)->update(['state' => $state])) throw new \Exception("修改失败");
				if ($state == 41){
				    if(!Db::name('shop')->where('id', $order['shop_id'])->setDec('balance_freeze',$order['payment_amount'])) throw new \Exception('退款失败');
				    
				    jpushNotice($order['uid'], '订单号:'.$order['numbering'].'订单商家确认退款');
				    if ($order['is_pay_type'] == 1) {
                        if(!wxTradeRefundRequest2($order))  throw new \Exception("修改失败");
                    } else {
                        if(!$ali->setCofig()->TradeRefundRequest($order))  throw new \Exception("修改失败");
                    }
				}else{
				    jpushNotice($order['uid'], '订单号:'.$order['numbering'].'订单商家确认退货，请及时发货');
				}
				
				Db::commit(); 
			} catch (\Exception $e) {
				Db::rollback();
				$this->error('操作失败：'.$e->getMessage());
			}
			$this->success('操作成功');
		}
		else if(2 == $type) //取消退款
		{
            $data['refund_info_id2'] = request()->param('refund_info_id2', 'intval');
            $data['info2'] = request()->param('info2', 'htmlspecialchars');
            if(empty($data['refund_info_id2']) || empty($data['info2'])) $this->error('取消退款退款原因和退款说明必须填写');

            if(mb_strlen($data['info2']) > 100 || mb_strlen($data['info2']) <= 0) $this->error('退款说明字数应在1~100位之间');

            $shop_refund = Db::name('shop_refund')->where('order_id', $id)->value('order_state');

            $data['check_time'] = time();
            $data['state'] = 2;
            
			try {
				Db::startTrans();

				if(!Db::name('shop_order')->where('id',$id)->update(['state' => $shop_refund])) throw new \Exception("修改失败1");

                if(!Db::name('shop_refund')->where('order_id',$id)->update($data)) throw new \Exception("修改失败2");
                jpushNotice($order['uid'], '订单号:'.$order['numbering'].'订单商家取消退款');
				
				Db::commit(); 
			} catch (\Exception $e) {
				Db::rollback();
				$this->error($e->getMessage());
			}
			$this->success('操作成功');
		}
    }

    //查看详情
    public function views()
    {
    	$id = request()->param('id', 'intval');

    	if(!$id) $this->error('参数错误');

    	$info = Db::name('shop_order')->alias('a')
    	        ->leftJoin('__USER__ b ',' a.uid = b.id')
        		->field('a.id,a.numbering,a.state,a.is_pay_type,a.addtime,a.payment_amount,a.original_price,a.discount,b.user_nicename')
    	        ->where('a.id',$id)
    	        ->find();

    	if(!$info) $this->error('参数错误');

    	$info['addtime'] = date('Y-m-d H:i:s', $info['addtime']);
        $info['statename'] = $this->state($info['state']);

    	$order_list = Db::name('shop_order_info')->where('order_id', $id)->select()->toArray();

    	$this->assign('info', $info);
    	$this->assign('list', $order_list);
    	return $this->fetch();
    }

    //导出待发货订单
    function export() 
    {
        $shop_id = session('shop_id');
        
        $start_time = request()->param('start_time');
        $end_time = request()->param('end_time');
        
        $lists = Db::name('shop_order_info')->alias('a');
        
        if (!empty($start_time) && $start_time = strtotime($start_time)) $list->where('b.addtime', '>', $start_time);
        if (!empty($end_time) && $end_time = strtotime($end_time)) $list->where('b.addtime', '<', $end_time);
        
        
        $list = $lists->where('b.state in (20,30,50,60) and a.shop_id='.$shop_id)
                ->field('a.title,a.num,b.numbering,c.province,c.city,c.area,c.info,c.name,c.phone,e.name as classify_name,b.state,b.pay_time,b.addtime')
                ->leftJoin('__SHOP_ORDER__ b', 'a.order_id = b.id')
                ->leftJoin('__SHOP_ADDRESS__ c', 'b.address = c.id')
                ->leftJoin('__SHOP_COMMODITY__ d', 'd.id = a.commodity_id')
                ->leftJoin('__SHOP_ASSORT__ e', 'd.assort_id = e.id')
                ->order('b.numbering desc')
                ->select()->toArray();
                
        if ($list) {
            $areawhere = $list2 = [];
            foreach ($list as $k => $v) {
                if (!empty($v['province'])) $areawhere[] = $v['province'];
                if (!empty($v['city'])) $areawhere[] = $v['city'];
                if (!empty($v['area'])) $areawhere[] = $v['area'];
            }
            
            $area = Db::name('shop_area')->whereIn('coding', array_values(array_filter($areawhere)))->column('name', 'coding');
            
            $state = [20 => '待发货', 30 => '待收货', 50 => '待评价', 60 => '已评价'];
            
            foreach ($list as $k => $v) {
                $list2[] = [
                    'title' => $v['title'],
                    'num' => $v['num'],
                    'numbering' => $v['numbering'],
                    'name' => $v['name'],
                    'phone' => $v['phone'],
                    'classify_name' => $v['classify_name'],
                    'address' => $area[$v['province']].$area[$v['city']].$area[$v['area']].$v['info'],
                    'state' => $state[$v['state']],
                    'pay_time' => date('Y-m-d H:i:s', $v['pay_time']),
                    'addtime' => date('Y-m-d H:i:s', $v['addtime']),
                ];
            }
            
            $action="商品订单导出记录：".DB::name("shop_order_info")->getLastSql();
            setAdminLog($action);
            $cellName = array('A','B','C','D','E','F','G', 'H', 'I', 'J');
            $xlsCell  = array(
                array('title','商品名称'),
                array('classify_name','分类'),
                array('num','购买数量'),
                array('numbering','订单号'),
                array('name','收货人名称'),
                array('phone','手机号码'),
                array('address','收货地址'),
                array('state','订单状态'),
                array('addtime','下单时间'),
                array('pay_time','付款时间'),
            );
            $xlsName  = "待发货订单";
            exportExcel($xlsName,$xlsCell,$list2,$cellName);
        }
    }

    private function state($num)
    {
    	$arr = ['待支付', 10=>'取消', 20=>'已支付', 30=>'已发货', 40=>'退货/退款申请', 41=>'确认退货',42=>'卖家同意退货',43=>'正在退货', 50 =>'已收货', 60 =>'已评价'];
    	return isset($arr[$num]) ? $arr[$num] : '未定义状态';
    }
}