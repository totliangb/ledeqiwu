<?php
namespace app\Merchant\controller;
use think\Db;
use cmf\controller\MerchantController;

class CommodityController extends MerchantController
{
	protected $table = 'shop_commodity';
	//商品列表
	public function index()
	{
		$list = Db::name('shop_commodity')
				->field('id,title,img,original_price,price,amount,sales,state,addtime')
				->where('shop_id',session('shop_id'))
				->order('addtime desc')
				->paginate(20);

		$lists = $list->items();
		foreach ($lists as $key => &$value) 
		{
			$value['addtime'] = date('Y-m-d H:i:s', $value['addtime']);
			$value['statename'] = $this->state($value['state']);
			$value['img'] = get_upload_path($value['img'], 0);
		}

		$this->assign("page", $list->render());
		$this->assign('list', $lists);
		return $this->fetch();
	}

	private function state($num)
	{
		$arr = ['待审核','正常','取消'];

		return isset($arr[$num]) ? $arr[$num] : '未定义状态';
	}

	//删除
	public function del()
	{
		$id = request()->param('id', 'intval');

		try {
			Db::startTrans();

			if(!Db::name('shop_commodity')->delete($id)) throw new \Exception("删除失败");

			if(!Db::name('shop_commodity_info')->where('commodity_id', $id)->delete()) throw new \Exception("删除失败");

			
			Db::commit(); 
		} catch (\Exception $e) {
			Db::rollback();
			$this->error('删除失败');
		}

		$this->success('删除成功');

	}

	//修改
	public function edit()
	{
		if(!request()->isPost())
		{
			$id = request()->param('id', 'intval');

			if(!$id) $this->error('参数错误');

			$info = Db::name('shop_commodity')->alias('a')
					->join('__SHOP_COMMODITY_INFO__ b ',' a.id = b.commodity_id')
					->field('a.id,a.assort_id,a.title,a.description,a.img,a.original_price,a.price,a.amount,b.carousel,b.text')
					->where('a.id='.$id.' and a.shop_id='.session('shop_id'))
					->find();
			$info['carousel'] = json_decode($info['carousel'], true);
			if(!empty($info['text'])) {
			    $info['text'] = json_decode($info['text'], true);
			    if (is_array($info['text'])){
			        foreach ($info['text'] as $k => &$v) {
    			        $v = get_upload_path($v, 0);
    			    }
			    }
			    
			}
			if ($info['carousel']) {
			    foreach ($info['carousel'] as $k => &$vs) {
    			    $vs = get_upload_path($vs, 0);
    			}
			}
			
			if (!empty($info['img'])) $info['img'] = get_upload_path($info['img'], 0);
			
			$this->assign('info', $info);
			$this->assign('assort', Db::name('shop_assort')->field('id,name')->where('state=1')->order('sort desc')->select()->toArray());
			return $this->fetch();
		}

		$post = $this->check_post();

		if(empty($post['img'])) $this->error('商品图片不能为空');

		$post2 = [
			'carousel' => json_encode(array_filter($post['carousel_url'])),
			'text' => $post['text_url']
		];
		$id = $post['id'];
		unset($post['carousel_url'], $post['text_url'],$post['id']);

		try {
			Db::startTrans();

			if(!Db::name('shop_commodity')->where('id',$id)->update($post)) throw new \Exception("修改失败");

			if(!Db::name('shop_commodity_info')->where('commodity_id',$id)->update($post2)) throw new \Exception("修改失败");

			
			Db::commit(); 
		} catch (\Exception $e) {
			Db::rollback();
			$this->error('修改失败');
		}

		$this->success('修改成功');
	}

	/**
		 * 添加商品
		 */
		public function add() {
    			if(!request()->isPost())
    			{
    				$this->assign('assort', Db::name('shop_assort')->field('id,name')->where('state=1')->order('sort desc')->select()->toArray());
    				return $this->fetch();
    			}

				$post = $this->check_post();
				$shop_id = session('shop_id');
				$post2 = [
					'carousel' => json_encode(array_filter($post['carousel_url'])),
					'text' => $post['text_url'],
					'shop_id' => $shop_id
				];
				$post['img'] = $post['carousel_url'][0];
				$post['shop_id'] = $shop_id;
				$post['uid'] = $_SESSION["user_id"];
				$post['addtime'] = time();
				unset($post['carousel_url'], $post['text_url']);

				try {
        			Db::startTrans();
        			$commodity_id=Db::name('shop_commodity')->insertGetId($post);
        			if(!$commodity_id) throw new \Exception("添加失败");
        			$post2['commodity_id'] = $commodity_id;
        
        			if(!Db::name('shop_commodity_info')->insert($post2)) throw new \Exception("添加失败");
        
        			
        			Db::commit(); 
        		} catch (\Exception $e) {
        			Db::rollback();
        			$this->error('添加失败');
        		}
        
        		$this->success('添加成功');
		}


	private function check_post()
	{
		$post = input('post.');

				if(empty($post['title']) || mb_strlen($post['title']) > 50 || mb_strlen($post['title']) <= 0) $this->error('标题的字数应在1~50位之间');

				if(!empty($post['description']) && mb_strlen($post['description']) > 100) $this->error('描述的字数不能大于100位');

				if(empty($post['original_price']) || $post['original_price'] > 100000000 || $post['original_price'] <= 0) $this->error('商品原价应在0.1~100000000之间');

				if(empty($post['price']) || $post['price'] > 100000000 || $post['price'] <= 0) $this->error('商品价格应在0.1~100000000之间');

				if(empty($post['assort_id']) || $post['assort_id']  <= 0) $this->error('请选择商品分类');

				if(empty($post['amount']) || $post['amount']  < 0 || $post['amount']  > 100000000) $this->error('库存应在0~100000000之间');

				if(empty($post['carousel_url'])) $this->error('请上传轮播图');
				
				$post['carousel_url'] = array_values(array_filter($post['carousel_url']));
				unset($post['carousel_names']);

				if(isset($post['text_url']) && !empty($post['text_url']))
				{
					$post['text_url'] = json_encode(array_filter($post['text_url']));
				} else {
				    $post['text_url'] = [];
				}


				return $post;
	}
}