<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Tuolaji <479923197@qq.com>
// +----------------------------------------------------------------------
/**
 */
namespace app\Merchant\controller;
use think\Db;
use cmf\controller\MerchantController;
class PublicController extends MerchantController {

    function initialize() {
        $siteInfo = cmf_get_site_info();
        $this->assign("configpub", $siteInfo);
    }
    
    //后台登陆界面
    public function login() {

        $admin_id = session('shop_id');
        if (!empty($admin_id)) {//已经登录
            return redirect(url("Merchant/Index/index"));
        } else {
            session("__SP_ADMIN_LOGIN_PAGE_SHOWED_SUCCESS__", true);
            $result = hook_one('admin_login');
            if (!empty($result)) {
                return $result;
            }
            return $this->fetch(":login");
        }
    }
    
    public function logout(){
    	session('shop_id',null); 
        header('Location:http://'.$_SERVER['HTTP_HOST']);
    }
    
    public function dologin(){
        $login_page_showed_success=session("__SP_ADMIN_LOGIN_PAGE_SHOWED_SUCCESS__");
        if(!$login_page_showed_success){
            $this->error('login error!');
        }
    	$name = request()->param('username', 'trim');
    	if(empty($name)){
    		$this->error(lang('USERNAME_OR_EMAIL_EMPTY'));
    	}
    	$pass = request()->param('password', 'trim');
    	if(empty($pass)){
    		$this->error(lang('PASSWORD_REQUIRED'));
    	}
    	$captcha = request()->param('captcha');
    	if(empty($captcha)){
    		$this->error(lang('CAPTCHA_REQUIRED'));
    	}
    	//验证码
    	if(!cmf_captcha_check($captcha)){
    		$this->error(lang('CAPTCHA_NOT_RIGHT'));
    	}else{
    		if(strpos($name,"@")>0){//邮箱登陆
    			$where['user_email']=$name;
    		}else{
    			$where['user_login']=$name;
    		}
    		
    		$result = Db::name("user")->where($where)->find();
    		if(!empty($result) && $result['user_type']==2){
    			if(cmf_compare_password($pass,$result['user_pass'])){
    				
                    $shop_id = Db::name('shop')->where('uid='.$result['id'])->value('id');
                    if(!$shop_id)  $this->error('当前用户没有店铺');
    				
    				//登入成功页面跳转
                    
                    session('shop_id',$shop_id);
    				$_SESSION['shop_name']=$result["user_nicename"];
                    $_SESSION['user_id']=$result['id'];
                    
    				setcookie("merchant_username",$name,time()+30*24*3600,"/");
    				$this->success(lang('LOGIN_SUCCESS'),url("Merchant/Index/index"));
    			}else{
    				$this->error(lang('PASSWORD_NOT_RIGHT'));
    			}
    		}else{
    			$this->error(lang('USERNAME_NOT_EXIST'));
    		}
    	}
    }

}