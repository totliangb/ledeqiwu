<?php
namespace app\chatroom\controller;

use cmf\controller\HomeBaseController;
use think\Db;

/**
* 通用不用登录接口
*/
class UniversalController extends CommonController
{
	protected $loginType = 0;

	//分类接口
	public function classification()
	{

	}

	//聊天室列表
	public function chatroomlist()
	{
		$rs=array('code'=>0,'info'=>array(),'msg'=>'');
		$start=(($this->request->param('page') ?: 1) - 1) * $this->pageNum;
		
		$list = Db::name('chatroom')->alias('a')
				->field('a.id,a.name,a.roomid,a.showid,a.uid,a.img,a.rewards,a.typeid,a.starttime,a.city,a.province,a.type,a.charges,b.user_nicename,c.name typename')
				->join('cmf_user b', 'a.uid = b.id')
				->join('cmf_universal_type c', 'a.typeid = c.id')
				->where('a.status', 1)
				->where('b.user_status', 1)
				->order('a.is_home,a.rewards,a.hotvotes,a.id', 'desc')
				->limit($start, $this->pageNum);

		//验证是否要求分类
		if ($typeid = $this->request->param('typeid') ?: 0) {
			$list->where('b.typeid', $typeid);
		}

		//验证是否搜索房间名称
		if ($chatroomName  = $this->request->param('chatroomName') ?: '') {
			$list->whereLike('b.user_nicename', "%{$chatroomName}%");
		}

		$list->select();

		if ($list) {
			$getConfigPri = getConfigPri();

			foreach ($list as $key => &$value) 
			{
				$value->img = get_upload_path($value->img);

				//添加房间人数
				$this->redisKey->setRoom($value->uid);
				if ($this->redis->EXISTS($this->redisKey->getRoom())) {
					$num = $this->redis->SCARD($this->redisKey->getRoom());
					$value->num = $num * $getConfigPri['wheat_fans'] + $num;
				} else {
					$value->num = 0;
				}

				$value->starttime = date('Y-m-d H:i', $value->starttime);
			}
		}

		$rs['info'] = $list;
		exit(json_encode($rs));
	}
}