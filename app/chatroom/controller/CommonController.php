<?php

namespace app\chatroom\controller;

use cmf\controller\HomeBaseController;
use think\Db;
/**
* 聊天室接口父类
*/
class CommonController extends HomeBaseController
{
	public $redisKeyUrl = '../swoole/redisKey.php';
    protected $loginType = 1;//判断是否要登录 1要 0不需要
    protected $pageNum = 10; //每页数量
    public function __construct()
    {
    	parent::__construct();
        
        if ($this->loginType) {
            $data = $this->request->only(['uid', 'token']);
            $uid=isset($data['uid']) ? $data['uid']: '';
            $token=isset($data['token']) ? $data['token']: '';
            $uid=(int)checkNull($uid);
            $token=checkNull($token);
            $checkToken=checkToken($uid,$token);
            if ($checkToken==700) {
                $rs=array('code'=>700,'info'=>array(),'msg'=>'您的登陆状态失效，请重新登陆！');
                echo json_encode($rs);
                exit;
            }
        }
    }


    //调用方法时候验证是否有指定方法
    public function __get($name) {
        if (method_exists($this, $name) && !$this->$name) {
            $this->$name = $this->$name();
        }
    }

    protected function redisKey() 
    {
        require_once $this->redisKeyUrl;
        return new redisKey();
    }

    protected function redis() {
        connectionRedis();
        return $GLOBALS['redisdb'];
    }
}