<?php

namespace app\chatroom\controller;

use cmf\controller\HomeBaseController;
use think\Db;
/**
* 聊天室
*/
class HomeController extends CommonController
{
	//创建聊天室
    public function add()
    {
        $data = $this->request->only(['uid', 'name', 'img', 'typeid', 'province', 'city', 'lng', 'lat', 'content', 'type', 'charges']);

        $rs=array('code'=>0,'info'=>array(),'msg'=>'');
        if (empty($data['name']) || mb_strlen($data['name']) <= 0 || mb_strlen($data['name']) >= 100) {
            $rs['msg'] = '标题字数应在1~100位之间';
            $rs['code'] = 1000;
            echo json_encode($rs);
            exit;
        }

        if (empty($data['img'])) {
        	$rs['msg'] = '封面图片必须上传';
            $rs['code'] = 1000;
            echo json_encode($rs);
            exit;
        }
        $data['img'] = htmlspecialchars($data['img']);

        if (!empty($data['content'])) {
            unset($data['content']);
        } else {
            if (mb_strlen($data['content']) > 100) {
                $rs['msg'] = '房间公告字数应小于100位';
                $rs['code'] = 1000;
                echo json_encode($rs);
                exit;
            }
            $data['content'] = htmlspecialchars($data['content']);
        }
        

        if (empty($data['typeid'])) {
        	$rs['msg'] = '分类必须选择';
            $rs['code'] = 1000;
            echo json_encode($rs);
            exit;
        }

        //判断是否有传地区
        if (empty($data['province'])) {
            unset($data['province'], $data['city'], $data['lng'], $data['lat']);
        } else {
        	$data['is_area'] = 1;
        	$data['province'] = htmlspecialchars($data['province']);
        	$data['city'] = htmlspecialchars($data['city']);
        }

        //判断是否是收费房间
        if (empty($data['type'])) {
        	unset($data['type'], $data['charges']);
        }

        $data['starttime'] = time();
        $data['status'] = '1';
        $data['roomid'] = '10000'.$data['uid'].'a';
        $data['stream'] = $data['uid'].'_'.time();

        if (Db::name('chatroom')->insert($data)) {
        	$rs['msg'] = '创建成功';
        	exit(json_encode($rs));
        }

        $rs['msg'] = '创建失败';
        $rs['code'] = 1000;
        exit(json_encode($rs));
    }

    //排麦列表
    public function row_wheat_list()
    {
        $rs=array('code'=>0,'info'=>array(),'msg'=>'');

        $data = $this->request->only(['uid']);

        $this->redisKey->setRoom($data['uid']);
        if (!$this->redis->EXISTS($this->redisKey->getRoom())) {
            $rs['msg'] = '未开播';
            $rs['code'] = 1000;
            exit(json_encode($rs));
        }

        $this->redisKey->setApplyWheatList($data['uid']);
        $ApplyWheatList = $this->redis->get($this->redisKey->getApplyWheatList());
        if (!$ApplyWheatList) {
            $rs['msg'] = '没有申请上麦人';
            exit(json_encode($rs));
        }

        $ApplyWheatList = json_decode($ApplyWheatList);
        $uid = array_keys($ApplyWheatList);

        $userList = Db::name('user')->whereIn('id', implode(',', $uid))->column('user_nicename,avatar', 'id');
        foreach ($ApplyWheatList as $key => &$value) {
            if (isset($userList[$value->id])) {
                $value->user_nicename = $userList[$value->id]['user_nicename'];
                $value->avatar = get_upload_path($userList[$value->id]['avatar']);
            }
            unset($ApplyWheatList[$key]->fd);
        }

        $rs['info'] = array_values($ApplyWheatList);

        exit(json_encode($rs));
    }
    
    //管理员列表
    public function managerlist()
    {
        $rs=array('code'=>0,'info'=>array(),'msg'=>'');
        $data = $this->request->only(['uid']);

        $chatroomid = Db::name('chatroom')->where('uid', $data['uid'])->value('id');

        if (!$chatroomid) {
            $rs['msg'] = '未开播';
            $rs['code'] = 1000;
            exit(json_encode($rs));
        }

        $list = Db::name('chatroom_admin')->alias('a')
                ->where(['a.chatroomid' => $chatroomid, 'status' => 1])
                ->join('cmf_user b', 'a.uid = b.id')
                ->field('a.id,a.uid,b.user_nicename,b.avatar,a.addtime')
                ->select();

        foreach ($list as $key => &$value) {
            $value->addtime = date('Y-m-d H:i:s', $value->addtime);
            $value->avatar  = get_upload_path($value->avatar);
        }

        $rs['info'] = $list;
        exit(json_encode($rs));
    }
}