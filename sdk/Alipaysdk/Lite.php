<?php
require_once 'AopClient.php';
require_once 'AopCertification.php';
require_once 'request/AlipayTradeQueryRequest.php';
require_once 'request/AlipayTradeWapPayRequest.php';
require_once 'request/AlipayTradeAppPayRequest.php';
require_once 'request/AlipayTradeRefundRequest.php';

/**
* 阿里云支付
*/
class Alipay_Lite
{
    protected $config;

    //设置
    public function setCofig()
    {
        $configpri = getConfigPri();
        $this->config = [
            'aliscan_rsakey' => $configpri['aliscan_rsakey'],
            'aliscan_pubkey' => $configpri['aliscan_pubkey'],
            'appId' => $configpri['aliscan_appid']
        ];
        return $this;
    }

    public function TradeRefundRequest($data)
    {
        $aop = new AopClient();
        $aop->appId = $this->config['appId'];
        $aop->rsaPrivateKey = $this->config['aliscan_rsakey'];
        $aop->alipayrsaPublicKey=$this->config['aliscan_pubkey'];
        $aop->signType = 'RSA2';
        $request = new AlipayTradeRefundRequest();
        $request->setBizContent(json_encode([
            'out_trade_no' => $data['numbering'],
            'refund_amount' => $data['payment_amount'],
        ]));
        $result = $aop->execute ( $request); 

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if(!empty($resultCode)&&$resultCode == 10000){
            return 1;
        } else {
            return 0;
        }
    }
}